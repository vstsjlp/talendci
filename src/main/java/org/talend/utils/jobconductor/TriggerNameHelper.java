package org.talend.utils.jobconductor;

import java.util.Date;
import org.talend.utils.IdGenerator;

public final class TriggerNameHelper {
	private static final String P_TASK = "_task";
	public static final String PREFIX_TALEND_RECOVER_TRIGGER = "RecoverTrigger_task";
	public static final String PREFIX_QUARTZ_RECOVER_TRIGGER = "recover_";
	public static final String PREFIX_INSTANT_RUN_TRIGGER = "InstantRunTrigger";
	public static final String PREFIX_FILE_TRIGGER = "FileTrigger";
	private static final String PREFIX_TIME_TRIGGER = "";
	private static TriggerNameHelper instance;

	public static TriggerNameHelper getInstance() {
		if (instance == null) {
			instance = new TriggerNameHelper();
		}
		return instance;
	}

	public Integer parseQuartzTriggerId(String triggerName) {
		String[] split = triggerName.split("\\.");
		triggerName = split[(split.length - 1)];

		if (isTalendRecoverTriggerName(triggerName)) {

			String idStr = triggerName.substring("RecoverTrigger_task".length());
			return Integer.valueOf(Integer.parseInt(idStr));
		}
		if (isInstantRunTriggerName(triggerName)) {

			return null;
		}
		if (isFileTriggerName(triggerName)) {
			String idStr = triggerName.substring("FileTrigger".length());
			return Integer.valueOf(Integer.parseInt(idStr));
		}
		try {
			return Integer.valueOf(Integer.parseInt(triggerName));
		} catch (NumberFormatException localNumberFormatException) {
			throw new IllegalArgumentException(triggerName + " is not a valid trigger name");
		}
	}

	public int parseQuartzJobNameId(String quartzJobName) {
		try {
			String[] split = quartzJobName.split("\\.");
			quartzJobName = split[(split.length - 1)];

			if (quartzJobName.startsWith(TRIGGER_TYPE.getFileTriggerParentPrefix())) {
				String idStr = quartzJobName.substring(TRIGGER_TYPE.getFileTriggerParentPrefix().length());
				return Integer.parseInt(idStr);
			}
			return Integer.parseInt(quartzJobName);
		} catch (NumberFormatException localNumberFormatException) {
			throw new IllegalArgumentException(quartzJobName + " is not a valid job name");
		}
	}

	public boolean isInstantRunTriggerName(String triggerName) {
		return triggerName.matches(".*InstantRunTrigger.+");
	}

	public String buildRecoverTriggerName(int idTask) {
		return "RecoverTrigger_task" + idTask;
	}

	public String buildInstantRunTriggerName(int idTask) {
		return

		new StringBuilder("InstantRunTrigger").append(idTask).append("_").append(new Date().getTime()).append("_")
				+ IdGenerator.getAsciiRandomString(5);
	}

	public String buildQuartzTriggerName(TRIGGER_TYPE triggerType, int talendTriggerId) {
		String quartzTriggerName = null;
		if (triggerType == TRIGGER_TYPE.FILE_TRIGGER) {
			quartzTriggerName = "FileTrigger" + talendTriggerId;
		} else {
			quartzTriggerName = String.valueOf(talendTriggerId);
		}
		return quartzTriggerName;
	}

	public boolean isTalendRecoverTriggerName(String triggerName) {
		return triggerName.matches(".*RecoverTrigger_task.+");
	}

	public boolean isQuartzRecoverTriggerName(String triggerName) {
		return triggerName.matches("recover_.+");
	}

	public boolean isRecoverTriggerName(String triggerName) {
		return (isTalendRecoverTriggerName(triggerName)) || (isQuartzRecoverTriggerName(triggerName));
	}

	public boolean isFileTriggerName(String triggerName) {
		return triggerName.matches(".*FileTrigger.+");
	}

	public boolean isTimeTriggerName(String triggerName) {
		return triggerName.matches("\\d+");
	}

	public boolean isTalendTriggerDependent(String quartzTriggerName) {
		return (isTimeTriggerName(quartzTriggerName)) || (isFileTriggerName(quartzTriggerName));
	}
}
