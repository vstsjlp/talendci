package org.talend.utils.jobconductor;

public enum TRIGGER_TYPE
 {
   SIMPLE_TRIGGER("SimpleTrigger", true, getEmptyString()), 
   CRON_TRIGGER("CronTrigger", true, getEmptyString()), 
   CRON_UI_TRIGGER("CronUITrigger", true, getEmptyString()), 
   FILE_TRIGGER("FileTrigger", false, getFileTriggerPrefix());
   
 
   private static final String EMPTY_STRING = "";
   
   private static final String TIME_TRIGGER_PREFIX = "";
   
   private static final String FILE_TRIGGER_PREFIX = "FileTrigger";
   
   private static final String FILE_TRIGGER_PARENT_PREFIX = "FileTriggerJob";
   
   private String triggerType;
   private boolean timeTrigger;
   private String prefix;
   
   private TRIGGER_TYPE(String mappingForward, boolean timeTrigger)
   {
     this.triggerType = mappingForward;
     this.timeTrigger = timeTrigger;
   }
   
   private TRIGGER_TYPE(String mappingForward, boolean timeTrigger, String prefix) {
     this(mappingForward, timeTrigger);
     this.prefix = prefix;
   }
   
 
 
 
 
   public String getTriggerTypeStr()
   {
     return this.triggerType;
   }
   
   public static TRIGGER_TYPE get(String triggerTypeStr) {
     TRIGGER_TYPE[] values = values();
     for (int i = 0; i < values.length; i++) {
       TRIGGER_TYPE triggerTypeCurrent = values[i];
       if (triggerTypeCurrent.triggerType.equals(triggerTypeStr)) {
         return triggerTypeCurrent;
       }
     }
     return null;
   }
   
 
 
 
 
   public boolean isTimeTrigger()
   {
     return this.timeTrigger;
   }
   
 
 
 
 
   public boolean isFileTrigger()
   {
     return !this.timeTrigger;
   }
   
   public String getPrefix() {
     return this.prefix;
   }
   
   private static String getEmptyString() {
     return "";
   }
   
   public static String getFileTriggerPrefix() {
     return "FileTrigger";
   }
   
   public static String getFileTriggerParentPrefix() {
     return "FileTriggerJob";
   }
   
   public static String getTimeTriggerPrefix() {
     return "";
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\jobconductor\TRIGGER_TYPE.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */