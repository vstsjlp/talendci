package org.talend.utils.color;

import java.awt.Color;

public final class AWTColorUtils {
	public static final String[] COLOR_STRS = { "#236192", "#C4D600", "#DB662A", "#F7A800", "#787121", "#00A9CE",
			"#ECAB7C", "#B8B370", "#D4D3D3", "#83D3E6", "#FFD38B" };

	private static final Color[] COLORS = initializeColors();

	public static Color getColor(int idx) {
		assert (idx >= 0);
		idx %= COLORS.length;
		return COLORS[idx];
	}

	private static Color[] initializeColors() {
		Color[] colors = new Color[COLOR_STRS.length];
		int i = 0;
		String[] arrayOfString;
		int j = (arrayOfString = COLOR_STRS).length;
		for (int i = 0; i < j; i++) {
			String str = arrayOfString[i];
			colors[i] = Color.decode(str);
			i++;
		}
		return colors;
	}
}
