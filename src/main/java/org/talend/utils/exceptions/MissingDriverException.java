package org.talend.utils.exceptions;

public class MissingDriverException
   extends RuntimeException
 {
   private static final long serialVersionUID = 1L;
   
 
 
 
 
 
 
 
 
 
   private String errorMessage = "";
   
 
   public MissingDriverException() {}
   
   public MissingDriverException(String message)
   {
     super(message);
     this.errorMessage = message;
   }
   
   public MissingDriverException(Throwable cause, String message) {
     super(cause);
     this.errorMessage = message;
   }
   
   public MissingDriverException(Throwable cause) {
     super(cause);
     this.errorMessage = cause.getMessage();
   }
   
 
 
 
 
   public String getErrorMessage()
   {
     return this.errorMessage;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\exceptions\MissingDriverException.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */