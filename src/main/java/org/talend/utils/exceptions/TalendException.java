package org.talend.utils.exceptions;

public class TalendException extends Exception {
	private static final long serialVersionUID = -7462582102179127770L;

	public TalendException() {
	}

	public TalendException(String message, Throwable cause) {
		super(message, cause);
	}

	public TalendException(String message) {
		super(message);
	}

	public TalendException(Throwable cause) {
		super(cause);
	}
}
