package org.talend.utils.network;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.util.Random;
import org.apache.log4j.Logger;

public class ServerSocketFreePortOpener {
	private static Logger log = Logger.getLogger(ServerSocketFreePortOpener.class);

	private static Random random = new Random(System.currentTimeMillis());

	public ServerSocket openServerSocket(int port) throws IOException {
		ServerSocket serverSocket = new ServerSocket(port);
		return serverSocket;
	}

	public ServerSocket openServerSocketFromRangePort(int portRangeBound1, int portRangeBound2) {
		return openServerSocketFromRangePort(portRangeBound1, portRangeBound2, true);
	}

	public ServerSocket openServerSocketFromRangePort(int portRangeBound1, int portRangeBound2, boolean randomizeIndexStart)
   {
     int portBoundMin = portRangeBound1 < portRangeBound2 ? portRangeBound1 : portRangeBound2;
     int portBoundMax = portRangeBound1 < portRangeBound2 ? portRangeBound2 : portRangeBound1;
     int increment = 0;
     if (randomizeIndexStart) {
 4;
       if (maxRandomBound >= 0) {
         if (maxRandomBound == 0) {
           increment = 0;
         } else {
           increment = random.nextInt(maxRandomBound);
         }
         int portStart = portBoundMin + increment;
         boolean isFirstLoop = true;
         boolean isFirstPass = true;
         
         for (int port = portStart;; port++) {
           if (port > portBoundMax) {
             port = portBoundMin;
           }
           if ((!isFirstLoop) && (port == portStart)) {
             if (!isFirstPass) break;
             isFirstPass = false;
           }
           
 
 
           try
           {
             return openServerSocket(port);
 
           }
           catch (IOException localIOException)
           {
             isFirstLoop = false;
           }
         }
       } }
     return null;
   }

	public static void main(String[] args) {
		ServerSocketFreePortOpener serverSocketFreePortOpener = new ServerSocketFreePortOpener();
		System.out.println(serverSocketFreePortOpener.openServerSocketFromRangePort(10, 20));
	}
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\network\ServerSocketFreePortOpener.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/