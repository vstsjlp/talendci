package org.talend.utils.network;

import java.io.PrintStream;
import java.net.ServerSocket;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import org.apache.log4j.Logger;

public class FreePortFinder {
	private static Logger log = Logger.getLogger(FreePortFinder.class);

	private static Random random = new Random(System.currentTimeMillis());

	private static Object[] randomLock = new Object[0];

	public static Set<String> busyPorts = Collections.synchronizedSet(new HashSet());

	public boolean isPortFree(int port)
	{
		Byte code:
			0: iload_1
			String;
		4: astore_2
		5: iconst_1
		6: istore_3
		Set;
		10: dup
		11: astore 4
		13: monitorenter
		Set;
		17: aload_2
		18: invokeinterface 67 2 0
		23: istore_3
		24: aconst_null
		25: astore 5
		27: iload_3
		28: ifne +134 -> 162
		ServerSocket
		34: dup
		35: iload_1
		ServerSocket:<init>	(I)V
		39: astore 5
		41: iconst_0
		42: istore_3
		43: goto +88 -> 131
		46: astore 6
		48: iconst_1
		49: istore_3
		Logger;
		53: aload 6
		String;
		Object;)V
61: aload 5
ServerSocket:close	()V
Set;
		69: aload_2
		70: invokeinterface 91 2 0
		75: pop
		76: goto +86 -> 162
		79: astore 8
		Logger;
		84: aload 8
		String;
		Object;)V
92: goto +70 -> 162
95: astore 7
97: aload 5
ServerSocket:close	()V
Set;
		105: aload_2
		106: invokeinterface 91 2 0
		111: pop
		112: goto +16 -> 128
		115: astore 8
		Logger;
		120: aload 8
		String;
		Object;)V
128: aload 7
130: athrow
131: aload 5
ServerSocket:close	()V
Set;
		139: aload_2
		140: invokeinterface 91 2 0
		145: pop
		146: goto +16 -> 162
		149: astore 8
		Logger;
		154: aload 8
		String;
		Object;)V
162: aload 4
164: monitorexit
165: goto +7 -> 172
168: aload 4
170: monitorexit
171: athrow
172: iload_3
173: ifeq +7 -> 180
176: iconst_0
177: goto +4 -> 181
180: iconst_1
181: ireturn
Line number table:
	Java source line #58	-> byte code offset #0
	Java source line #59	-> byte code offset #5
	Java source line #60	-> byte code offset #7
	Java source line #61	-> byte code offset #14
	Java source line #62	-> byte code offset #24
	Java source line #63	-> byte code offset #27
	Java source line #65	-> byte code offset #31
	Java source line #66	-> byte code offset #41
	Java source line #67	-> byte code offset #43
	Java source line #68	-> byte code offset #48
	Java source line #69	-> byte code offset #50
	Java source line #72	-> byte code offset #61
	Java source line #73	-> byte code offset #66
	Java source line #74	-> byte code offset #76
	Java source line #75	-> byte code offset #81
	Java source line #70	-> byte code offset #95
	Java source line #72	-> byte code offset #97
	Java source line #73	-> byte code offset #102
	Java source line #74	-> byte code offset #112
	Java source line #75	-> byte code offset #117
	Java source line #77	-> byte code offset #128
	Java source line #72	-> byte code offset #131
	Java source line #73	-> byte code offset #136
	Java source line #74	-> byte code offset #146
	Java source line #75	-> byte code offset #151
	Java source line #60	-> byte code offset #162
	Java source line #81	-> byte code offset #172
	Local variable table:
		start	length	slot	name	signature
		0	182	0	this	FreePortFinder
		0	182	1	port	int
		4	136	2	freePort	String
		6	167	3	isBusyPort	boolean
		Object;	Object
		25	107	5	serverSocket	ServerSocket
		46	8	6	e	Throwable
		95	34	7	localObject1	Object
		79	6	8	e	Throwable
		115	6	8	e	Throwable
		149	6	8	e	Throwable
		Exception table:
			from	to	target	type
			Throwable
			Throwable
			31	61	95	finally
			Throwable
			Throwable
			14	165	168	finally
			168	171	168	finally
	}

	public void removePort(int port) {
		boolean containsPort = false;
		String usingPort = String.valueOf(port);
		synchronized (busyPorts) {
			containsPort = busyPorts.contains(usingPort);
			if (containsPort) {
				busyPorts.remove(usingPort);
			}
		}
	}

	protected ServerSocket openServerSocket(int port) {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
		} catch (Throwable e) {
			log.debug(e.getMessage());
		}
		return serverSocket;
	}

	public int searchFreePort(int portRangeBound1, int portRangeBound2) {
		return opensServerSocketFromRangePort(portRangeBound1, portRangeBound2, true);
	}

	public int opensServerSocketFromRangePort(int portRangeBound1, int portRangeBound2, boolean randomizeIndexStart)
	{
		int portBoundMin = portRangeBound1 < portRangeBound2 ? portRangeBound1 : portRangeBound2;
		int portBoundMax = portRangeBound1 < portRangeBound2 ? portRangeBound2 : portRangeBound1;
		int increment = 0;
		if (randomizeIndexStart) {
			4;
			if (maxRandomBound >= 0) {
				if (maxRandomBound == 0) {
					increment = 0;
				} else {
					synchronized (randomLock) {
						increment = random.nextInt(maxRandomBound);
					}
				}

				int portStart = portBoundMin + increment;
				boolean isFirstLoop = true;
				boolean isFirstPass = true;

				for (int port = portStart;; port++) {
					if (port > portBoundMax) {
						port = portBoundMin;
					}
					if ((!isFirstLoop) && (port == portStart)) {
						if (!isFirstPass) break;
						isFirstPass = false;
					}



					if (isPortFree(port)) {
						return port;
					}

					isFirstLoop = false;
				}
			}
		}

		return -1;
	}

	public static void main(String[] args) {
		FreePortFinder freePortFinder = new FreePortFinder();
		System.out.println(freePortFinder.searchFreePort(10, 20));
	}
}

