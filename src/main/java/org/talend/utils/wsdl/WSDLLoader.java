package org.talend.utils.wsdl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class WSDLLoader {
	public static final String DEFAULT_FILENAME = "";XMLSchema";";";
	private static final String NAME_ELEMENT_SCHEMA = "schema";
	private static final String NAME_ATTRIBUTE_TARGET_NAMESPACE = "targetNamespace";
	private static final String NAME_ATTRIBUTE_SCHEMA_LOCATION = "schemaLocation";
	private static DocumentBuilder documentBuilder = null;

	private static int filenameIndex;
	private final Map<String, Collection<URL>> importedSchemas = new HashMap();

	public Map<String, InputStream> load(String wsdlLocation, String filenameTemplate)
			throws InvocationTargetException {
		filenameIndex = 0;
		return load(null, wsdlLocation, filenameTemplate);
	}

	private Map<String, InputStream> load(URL baseURL, String wsdlLocation, String filenameTemplate) throws InvocationTargetException {
     Map<String, InputStream> wsdls = new HashMap();
     try {
       URL wsdlURL = getURL(baseURL, wsdlLocation);
       Document wsdlDocument = getDocumentBuilder().parse(wsdlURL.toExternalForm());
       
       NodeList schemas = wsdlDocument.getElementsByTagNameNS(
XMLSchema", "schema");
       
       Element[] schemaElements = new Element[schemas.getLength()];
       for (int index = 0; index < schemas.getLength(); index++)
         schemaElements[index] = ((Element)schemas.item(index));
       Element[] arrayOfElement1;
       int j = (arrayOfElement1 = schemaElements).length; for (int i = 0; i < j; i++) { Element schema = arrayOfElement1[i];
         this.importedSchemas.put(schema.getAttribute("targetNamespace"), new HashSet());
         loadSchemas(schema, schema, wsdlURL);
       }
       
 
       NodeList imports = wsdlDocument.getElementsByTagNameNS(
", "import");
       for (int index = 0; index < imports.getLength(); index++) {
         Element wsdlImport = (Element)imports.item(index);
         String filename = String.format(filenameTemplate, new Object[] { Integer.valueOf(filenameIndex++) });
         Map<String, InputStream> importedWsdls = new WSDLLoader().load(wsdlURL, wsdlImport.getAttribute("location"), filenameTemplate);
         wsdlImport.setAttribute("location", filename);
         wsdls.put(filename, (InputStream)importedWsdls.remove(""));
         wsdls.putAll(importedWsdls);
       }
       
       ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
       Object xmlSource = new DOMSource(wsdlDocument);
       Object outputTarget = new StreamResult(outputStream);
       TransformerFactory.newInstance().newTransformer().transform((Source)xmlSource, (Result)outputTarget);
       wsdls.put("", new ByteArrayInputStream(outputStream.toByteArray()));
       
       return wsdls;
     } catch (InvocationTargetException e) {
       throw e;
     } catch (Exception e) {
       throw new InvocationTargetException(e, "Error occured while processing schemas");
     } finally {
       this.importedSchemas.clear();
     }
   }

	private static URL getURL(URL contextURL, String spec) throws MalformedURLException {
		try {
			return new URL(contextURL, spec);
		} catch (MalformedURLException e) {
			File tempFile = new File(spec);
			if ((contextURL == null) || ((contextURL != null) && (tempFile.isAbsolute()))) {
				return tempFile.toURI().toURL();
			}

			throw e;
		}
	}

	private void loadSchemas(Element ownerSchemaNode, Element schemaNode, URL ownerFile)
     throws InvocationTargetException, IOException
   {
     Map<String, String> prefixMapping = new HashMap();
     
     Node childNode = schemaNode.getFirstChild();
     while (childNode != null) {
       Node nextNode = childNode.getNextSibling();
       if ((childNode.getNodeType() == 1) && 
XMLSchema".equals(childNode.getNamespaceURI()))) {
         if ("import".equals(childNode.getLocalName())) {
           Element importElement = (Element)childNode;
           
           String schemaLocation = importElement.getAttribute("schemaLocation");
           String schemaNS = importElement.getAttribute("namespace");
           
           if ((schemaLocation != null) && (schemaLocation.length() != 0) && 
             (schemaNS != null) && (schemaNS.length() != 0)) {
             try
             {
               URL schemaURL = getURL(ownerFile, schemaLocation);
               if (!this.importedSchemas.containsKey(schemaNS)) {
                 Element schemaImported = 
                   (Element)ownerSchemaNode.getOwnerDocument().importNode(
                   loadSchema(schemaURL, false), true);
                 ownerSchemaNode.getParentNode().insertBefore(schemaImported, ownerSchemaNode);
                 
 
                 Collection<URL> urls = new HashSet();
                 urls.add(schemaURL);
                 this.importedSchemas.put(schemaNS, urls);
                 
                 loadSchemas(schemaImported, schemaImported, schemaURL);
 
               }
               else if (((Collection)this.importedSchemas.get(schemaNS)).add(schemaURL)) {
XMLSchema", "schema");
                 for (int i = 0; i < nl.getLength(); i++) {
                   Element schema = (Element)nl.item(i);
                   if (schemaNS.equals(schema.getAttribute("targetNamespace"))) {
                     Element schemaElement = loadSchema(schemaURL, true);
                     
                     loadSchemas(schema, schemaElement, schemaURL);
                     
                     Node refChild = getInsertLocation(schema.getLastChild());
                     Node child = schemaElement.getFirstChild();
                     while (child != null) {
                       Node next = child.getNextSibling();
                       child = schema.getOwnerDocument().importNode(child, true);
                       
 
 
 
                       schema.insertBefore(child, refChild);
                       child = next;
                     }
                     break;
                   }
                 }
               }
             }
             catch (InvocationTargetException e) {
               throw e;
             } catch (IOException e) {
               throw e;
             } catch (Exception e) {
               throw new InvocationTargetException(e, 
                 "Unexpected error while loading external schema file: " + e.getMessage());
             }
           }
           
 
 
           if (importElement != null)
           {
             importElement.removeAttribute("schemaLocation");
             
             importElement.getParentNode().insertBefore(importElement, getInsertLocation(importElement));
           }
         } else if ("include".equals(childNode.getLocalName())) {
           Element includeElement = (Element)childNode;
           
           String schemaLocation = includeElement.getAttribute("schemaLocation");
           if ((schemaLocation == null) || (schemaLocation.length() == 0)) {
             String errMsg = "The schema include is incorrect: schemaLocation = [" + schemaLocation + "]";
             throw new InvocationTargetException(new Exception(errMsg));
           }
           try {
             URL schemaURL = getURL(ownerFile, schemaLocation);
             String schemaNamespace = ownerSchemaNode.getAttribute("targetNamespace");
             if (((Collection)this.importedSchemas.get(schemaNamespace)).add(schemaURL)) {
               Element schemaIncluded = loadSchema(schemaURL, true);
               String includeNamespace = schemaIncluded.getAttribute("targetNamespace");
               if ((includeNamespace != null) && (includeNamespace.length() != 0) && 
                 (!schemaNamespace.equals(includeNamespace))) {
                 String errMsg = "The schema include is incorrect: namespaces are not equals";
                 throw new InvocationTargetException(new Exception(errMsg));
               }
               loadSchemas(ownerSchemaNode, schemaIncluded, schemaURL);
               
 
               NamedNodeMap nnm = schemaIncluded.getAttributes();
               for (int i = 0; i < nnm.getLength(); i++) {
                 Node attr = nnm.item(i);
                 String attrNamespace = attr.getNamespaceURI();
                 String attrLocalName = attr.getLocalName();
                 String attrValueNew = attr.getNodeValue();
                 String attrValueOld = getAttributeValue(schemaNode, attrNamespace, attrLocalName);
                 if (attrValueOld != null) {
".equals(attrNamespace)) && (!attrValueNew.equals(attrValueOld)))
                   {
                     String prefixNew = getPrefix(schemaNode, attrValueNew);
                     if (prefixNew == null) {
                       prefixNew = generatePrefix(schemaIncluded, attrLocalName);
", "xmlns:" + prefixNew, attrValueNew);
                     }
                     prefixMapping.put(attrLocalName, prefixNew);
                   }
                 } else {
                   String attrName = attr.getNodeName();
                   
".equals(attrNamespace))
                   {
                     String prefixNew = getPrefix(schemaNode, attrValueNew);
                     if (prefixNew != null) {
                       prefixMapping.put(attrLocalName, prefixNew);
                     } else {
", attrName, attrValueNew);
                     }
                   } else {
                     schemaNode.setAttributeNS(attrNamespace, attrName, attrValueNew);
                   }
                 }
               }
               
 
               Node firstIncludedNode = null;
               Node child = schemaIncluded.getFirstChild();
               while (child != null) {
                 Node next = child.getNextSibling();
                 child = schemaNode.getOwnerDocument().importNode(child, true);
                 if (child.getNodeType() == 1) {
                   fixPrefixes((Element)child, prefixMapping);
                 }
                 child = schemaNode.insertBefore(child, includeElement);
                 if (firstIncludedNode == null) {
                   firstIncludedNode = child;
                 }
                 child = next;
               }
               if (firstIncludedNode != null) {
                 nextNode = firstIncludedNode;
               }
             }
           } catch (InvocationTargetException e) {
             throw e;
           } catch (IOException e) {
             throw e;
           } catch (Exception e) {
             throw new InvocationTargetException(e, 
               "Unexpected error while loading external schema file: " + e.getMessage());
           }
           
           includeElement.getParentNode().removeChild(includeElement);
         }
       }
       childNode = nextNode;
     }

	}

	private static final String generatePrefix(Element element, String initialPrefix) {
     String prefix = initialPrefix;
     
     for (int index = 0; 
", prefix) != null; 
         index++) {
       prefix = initialPrefix + index;
     }
     return prefix;
   }

	private static final String getPrefix(Element element, String namespace) {
     NamedNodeMap nnm = element.getAttributes();
     for (int i = 0; i < nnm.getLength(); i++) {
       Node attr = nnm.item(i);
".equals(attr.getNamespaceURI())) && (namespace.equals(attr.getNodeValue()))) {
         return attr.getLocalName();
       }
     }return null;

	}

	private static final String getAttributeValue(Element element, String namespace, String localName) {
		Attr attr = element.getAttributeNodeNS(namespace, localName);
		return attr != null ? attr.getNodeValue() : null;
	}

	private static final void fixPrefixes(Element element, Map<String, String> prefixMapping) {
		NamedNodeMap nnm = element.getAttributes();

		String prefix = element.getPrefix();
		String prefixNew = (String) prefixMapping.get(prefix);
		if (prefixNew != null) {
			if ("xmlns".equals(prefixNew)) {
				prefixNew = null;
			}
			element.setPrefix(prefixNew);
		}

		for (int i = 0; i < nnm.getLength(); i++) {
			Node attr = nnm.item(i);
			String value = attr.getNodeValue();
			if (value != null) {
				int index = value.indexOf(':');
				if (index != -1) {
					String prefixOld = value.substring(0, index);
					prefixNew = (String) prefixMapping.get(prefixOld);
					if (prefixNew != null) {
						if ("xmlns".equals(prefixNew)) {
							attr.setNodeValue(value.substring(index + 1));
						} else {
							attr.setNodeValue(prefixNew + ':' + value.substring(index + 1));
						}
					}
				} else {
					prefixNew = (String) prefixMapping.get("xmlns");
					if (prefixNew != null) {
						String name = attr.getLocalName();
						if (("type".equals(name)) || ("base".equals(name)) || ("ref".equals(name))) {
							attr.setNodeValue(prefixNew + ':' + value);
						}
					}
				}
			}
		}
		for (Node child = element.getFirstChild(); child != null; child = child.getNextSibling()) {
			if (child.getNodeType() == 1) {
				fixPrefixes((Element) child, prefixMapping);
			}
		}
	}

	private static final Element loadSchema(URL schemaFile, boolean cleanup)
			throws IOException, SAXException, ParserConfigurationException {
		InputStream is = null;
		try {
			is = schemaFile.openStream();
			Element schemaElement = getDocumentBuilder().parse(is).getDocumentElement();
			if (cleanup) {
				cleanupSchemaElement(schemaElement);
			}
			return schemaElement;
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}

	private static final void cleanupSchemaElement(Element element) {
     Node node = element.getFirstChild();
     while (node != null) {
       Node next = node.getNextSibling();
       if (8 == node.getNodeType()) {
         element.removeChild(node);
       } else if (1 == node.getNodeType()) {
         Element child = (Element)node;
XMLSchema".equals(child.getNamespaceURI())) && 
           ("annotation".equals(child.getLocalName()))) {
           element.removeChild(child);
         } else {
           cleanupSchemaElement(child);
         }
       }
       node = next;
     }
   }

	private static Node getInsertLocation(Node currentNode)
   {
     for (Node refChild = currentNode.getPreviousSibling(); refChild != null; refChild = refChild.getPreviousSibling()) {
       if ((refChild.getNodeType() == 1) && 
XMLSchema".equals(refChild.getNamespaceURI())) && 
         ("import".equals(refChild.getLocalName()))) {
         refChild = refChild.getNextSibling();
         break;
       }
     }
     if (refChild == null) {
       refChild = currentNode.getParentNode().getFirstChild();
       while (refChild.getNodeType() != 1) { refChild = refChild.getNextSibling();
       }
     }
     return refChild;
   }

	private static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
		if (documentBuilder == null) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setNamespaceAware(true);
			documentBuilder = factory.newDocumentBuilder();
		}
		return documentBuilder;
	}
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\wsdl\WSDLLoader.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/