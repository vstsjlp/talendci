 package org.talend.utils;
 
 import java.io.Serializable;
 import java.util.Comparator;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class LabelValueBean
   implements Comparable, Serializable
 {
   public static final Comparator CASE_INSENSITIVE_ORDER = new Comparator()
   {
     public int compare(Object o1, Object o2) {
       String label1 = ((LabelValueBean)o1).getLabel();
       String label2 = ((LabelValueBean)o2).getLabel();
       
       return label1.compareToIgnoreCase(label2);
     }
   };
   
 
 
 
 
 
   private String label = null;
   
 
 
 
   private String value = null;
   
 
 
 
 
 
 
   public LabelValueBean() {}
   
 
 
 
 
 
 
   public LabelValueBean(String label, String value)
   {
     this.label = label;
     this.value = value;
   }
   
   public String getLabel() {
     return this.label;
   }
   
   public void setLabel(String label) {
     this.label = label;
   }
   
   public String getValue() {
     return this.value;
   }
   
   public void setValue(String value) {
     this.value = value;
   }
   
 
 
 
 
 
 
 
 
   public int compareTo(Object o)
   {
     String otherLabel = ((LabelValueBean)o).getLabel();
     
     return getLabel().compareTo(otherLabel);
   }
   
 
 
   public String toString()
   {
     StringBuffer sb = new StringBuffer("LabelValueBean[");
     
     sb.append(this.label);
     sb.append(", ");
     sb.append(this.value);
     sb.append("]");
     
     return sb.toString();
   }
   
 
 
 
 
   public boolean equals(Object obj)
   {
     if (obj == this) {
       return true;
     }
     
     if (!(obj instanceof LabelValueBean)) {
       return false;
     }
     
     LabelValueBean bean = (LabelValueBean)obj;
     int nil = getValue() == null ? 1 : 0;
     
     nil += (bean.getValue() == null ? 1 : 0);
     
     if (nil == 2)
       return true;
     if (nil == 1) {
       return false;
     }
     return getValue().equals(bean.getValue());
   }
   
 
 
 
 
 
   public int hashCode()
   {
     return getValue() == null ? 17 : getValue().hashCode();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\LabelValueBean.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */