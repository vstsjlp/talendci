package org.talend.utils;

import java.util.Random;

public class IdGenerator
 {
   public static String getAsciiRandomString(int length)
   {
     Random random = new Random();
     int cnt = 0;
     StringBuffer buffer = new StringBuffer();
     
     int end = 123;
     int start = 32;
     while (cnt < length) {
       char ch = (char)(random.nextInt(end - start) + start);
       if (Character.isLetterOrDigit(ch)) {
         buffer.append(ch);
         cnt++;
       }
     }
     return buffer.toString();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\IdGenerator.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */