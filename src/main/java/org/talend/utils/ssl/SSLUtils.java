package org.talend.utils.ssl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class SSLUtils
 {
   private static SSLContext sslcontext;
   private static final String TAC_SSL_KEYSTORE = "clientKeystore.jks";
   private static final String TAC_SSL_TRUSTSTORE = "clientTruststore.jks";
   private static final String TAC_SSL_CLIENT_KEY = "tac.net.ssl.ClientKeyStore";
   private static final String TAC_SSL_CLIENT_TRUST_KEY = "tac.net.ssl.ClientTrustStore";
   private static final String TAC_SSL_KEYSTORE_PASS = "tac.net.ssl.KeyStorePass";
   private static final String TAC_SSL_TRUSTSTORE_PASS = "tac.net.ssl.TrustStorePass";
   
   public static String getContent(StringBuffer buffer, URL url, String userDir)
     throws Exception
   {
     BufferedReader in = null;
     if ("https".equals(url.getProtocol())) {
       SSLSocketFactory socketFactory = getSSLContext(userDir).getSocketFactory();
       HttpsURLConnection httpsCon = (HttpsURLConnection)url.openConnection();
       httpsCon.setSSLSocketFactory(socketFactory);
       httpsCon.setHostnameVerifier(new HostnameVerifier()
       {
         public boolean verify(String arg0, SSLSession arg1)
         {
           return true;
         }
       });
       httpsCon.connect();
       in = new BufferedReader(new InputStreamReader(httpsCon.getInputStream()));
     } else {
       in = new BufferedReader(new InputStreamReader(url.openStream()));
     }
     String inputLine;
     while ((inputLine = in.readLine()) != null) { String inputLine;
       buffer.append(inputLine);
     }
     in.close();
     return buffer.toString();
   }
   
   public static SSLContext getSSLContext(String userDir) throws Exception {
     if (sslcontext == null) {
       String keystorePath = System.getProperty("tac.net.ssl.ClientKeyStore");
       String trustStorePath = System.getProperty("tac.net.ssl.ClientTrustStore");
       String keystorePass = System.getProperty("tac.net.ssl.KeyStorePass");
       String truststorePass = System.getProperty("tac.net.ssl.TrustStorePass");
       if (keystorePath == null)
       {
 
         File keystorePathFile = new File(userDir + "clientKeystore.jks");
         if (keystorePathFile.exists()) {
           keystorePath = keystorePathFile.getAbsolutePath();
         }
       }
       if (trustStorePath == null) {
         File trustStorePathFile = new File(userDir + "clientTruststore.jks");
         if (trustStorePathFile.exists()) {
           trustStorePath = trustStorePathFile.getAbsolutePath();
         }
       }
       if (keystorePass == null)
       {
 
         keystorePass = "";
       }
       if (truststorePass == null)
       {
 
         truststorePass = "";
       }
       
       sslcontext = SSLContext.getInstance("SSL");
       KeyManager[] keystoreManagers = null;
       if (keystorePath != null) {
         KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
         KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
         ks.load(new FileInputStream(keystorePath), keystorePass.toCharArray());
         kmf.init(ks, keystorePass.toCharArray());
         keystoreManagers = kmf.getKeyManagers();
       }
       
       TrustManager[] truststoreManagers = null;
       if (trustStorePath != null) {
         TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
         KeyStore tks = KeyStore.getInstance(KeyStore.getDefaultType());
         tks.load(new FileInputStream(trustStorePath), truststorePass.toCharArray());
         tmf.init(tks);
         truststoreManagers = tmf.getTrustManagers();
       } else {
         truststoreManagers = new TrustManager[] { new TrustAnyTrustManager(null) };
       }
       sslcontext.init(keystoreManagers, truststoreManagers, null);
     }
     return sslcontext;
   }
   
   private static class TrustAnyTrustManager
     implements X509TrustManager
   {
     public void checkClientTrusted(X509Certificate[] chain, String authType)
       throws CertificateException
     {}
     
     public void checkServerTrusted(X509Certificate[] chain, String authType)
       throws CertificateException
     {}
     
     public X509Certificate[] getAcceptedIssuers()
     {
       return new X509Certificate[0];
     }
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\ssl\SSLUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */