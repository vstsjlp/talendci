package org.talend.utils.cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class SimpleCache<K, V> {
	class HashKeyValue<K, V> implements Comparable<SimpleCache<K, V>.HashKeyValue<K, V>> {
		private K key;
		private V value;
		private long addTime;

		public HashKeyValue() {
			this.key = key;
			this.addTime = System.currentTimeMillis();
		}

		public HashKeyValue(V key) {
			this(key);
			this.value = value;
		}

		public int hashCode() {
			int result = 1;
			result = 31 * result + (this.key == null ? 0 : this.key.hashCode());
			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			HashKeyValue other = (HashKeyValue) obj;
			if (this.key == null) {
				if (other.key != null) {
					return false;
				}
			} else if (!this.key.equals(other.key)) {
				return false;
			}
			return true;
		}

		public int compareTo(SimpleCache<K, V>.HashKeyValue<K, V> o) {
			if (equals(o)) {
				return 0;
			}
			return getAddTime() < o.getAddTime() ? -1 : 1;
		}

		public V getValue() {
			return (V) this.value;
		}

		public K getKey() {
			return (K) this.key;
		}

		public long getAddTime() {
			return this.addTime;
		}
	}

	private Set<SimpleCache<K, V>.HashKeyValue<K, V>> keysOrderedByPutTime = new TreeSet();

	private Map<SimpleCache<K, V>.HashKeyValue<K, V>, SimpleCache<K, V>.HashKeyValue<K, V>> cache = new HashMap();

	private Integer maxItems;

	private Long maxTime;

	public SimpleCache(long maxTime, int maxItems) {
		this.maxTime = Long.valueOf(maxTime);
		this.maxItems = Integer.valueOf(maxItems);
		if (((this.maxTime == null) || (this.maxTime.longValue() < 0L) || (this.maxTime.longValue() == Long.MAX_VALUE)
				|| (this.maxTime.longValue() == 2147483647L))
				&& ((this.maxItems == null) || (this.maxItems.intValue() < 0)
						|| (this.maxItems.intValue() == Integer.MAX_VALUE))) {
			throw new IllegalArgumentException(
					"At least one of maxTime or maxItems must be a value greater or equals 0, excepted the MAX_VALUE");
		}
	}

	public synchronized V get(K key) {
		SimpleCache<K, V>.HashKeyValue<K, V> internalKey = new HashKeyValue(key);
		SimpleCache<K, V>.HashKeyValue<K, V> keyValue = (HashKeyValue) this.cache.get(internalKey);
		if (keyValue != null) {
			return (V) keyValue.getValue();
		}
		return null;
	}

	public synchronized Long getAddTime(K key) {
		SimpleCache<K, V>.HashKeyValue<K, V> internalKey = new HashKeyValue(key);
		SimpleCache<K, V>.HashKeyValue<K, V> keyValue = (HashKeyValue) this.cache.get(internalKey);
		if (keyValue != null) {
			return Long.valueOf(keyValue.getAddTime());
		}
		return null;
	}

	public synchronized V put(K key, V value) {
		SimpleCache<K, V>.HashKeyValue<K, V> internalKeyValue = new HashKeyValue(key, value);
		this.keysOrderedByPutTime.add(internalKeyValue);
		SimpleCache<K, V>.HashKeyValue<K, V> previousKeyValue = (HashKeyValue) this.cache.put(internalKeyValue,
				internalKeyValue);
		int sizeItems = this.keysOrderedByPutTime.size();
		if ((this.maxTime != null) && (this.maxTime.longValue() >= 0L) && (this.maxTime.longValue() != Long.MAX_VALUE)
				&& (sizeItems > 0)) {
			Iterator<SimpleCache<K, V>.HashKeyValue<K, V>> keysOrderedByPutTimeIterator = this.keysOrderedByPutTime
					.iterator();
			while (keysOrderedByPutTimeIterator.hasNext()) {
				long currentTimeMillis = System.currentTimeMillis();
				SimpleCache<K, V>.HashKeyValue<K, V> hashKey = (HashKeyValue) keysOrderedByPutTimeIterator.next();

				if (currentTimeMillis - hashKey.addTime < this.maxTime.longValue())
					break;
				keysOrderedByPutTimeIterator.remove();
				this.cache.remove(hashKey);
			}
		}

		sizeItems = this.keysOrderedByPutTime.size();
		if ((this.maxItems != null) && (this.maxItems.intValue() >= 0)
				&& (this.maxItems.intValue() != Integer.MAX_VALUE) && (sizeItems > this.maxItems.intValue())
				&& (sizeItems > 0)) {
			Iterator<SimpleCache<K, V>.HashKeyValue<K, V>> setIterator = this.keysOrderedByPutTime.iterator();
			if (setIterator.hasNext()) {
				SimpleCache<K, V>.HashKeyValue<K, V> toRemoveFromMap = (HashKeyValue) setIterator.next();
				setIterator.remove();
				this.cache.remove(toRemoveFromMap);
			}
		}
		if (previousKeyValue != null) {
			return (V) previousKeyValue.getValue();
		}
		return null;
	}

	public synchronized int size() {
		return this.cache.size();
	}

	protected synchronized int internalTimeListSize() {
		return this.keysOrderedByPutTime.size();
	}
}
