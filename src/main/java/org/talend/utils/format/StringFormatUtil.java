package org.talend.utils.format;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

public final class StringFormatUtil {
	public static final int PERCENT = 0;
	public static final int INT_NUMBER = 1;
	public static final int DOUBLE_NUMBER = 2;
	public static final int OTHER = 99999;

	public static String padString(String stringToPad, int size) {
		return String.format("%" + size + "s", new Object[] { stringToPad });
	}

	public static Object format(Object input, int style) {
		try {
			if (checkInput(input)) {
				DecimalFormat format = null;

				BigDecimal zero = new BigDecimal(0);
				BigDecimal temp = new BigDecimal(input.toString());
				BigDecimal min = new BigDecimal(1.0E-4D);
				BigDecimal max = new BigDecimal(0.9999D);
				boolean isUseScientific = false;
				switch (style) {
				case 0:
					if ((temp.compareTo(min) == -1) && (temp.compareTo(zero) == 1)) {
						isUseScientific = true;
					} else if ((temp.compareTo(max) == 1) && (temp.compareTo(new BigDecimal(1)) == -1)) {
						input = max.toString();
					}
					format = (DecimalFormat) DecimalFormat.getPercentInstance(Locale.ENGLISH);
					format.applyPattern("0.00%");
					break;
				case 1:
					min = new BigDecimal(0.01D);
					if ((temp.compareTo(min) == -1) && (temp.compareTo(zero) == 1)) {
						isUseScientific = true;
					}
					format = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ENGLISH);
					format.applyPattern("0");
					break;
				case 2:
					min = new BigDecimal(0.01D);
					if ((temp.compareTo(min) == -1) && (temp.compareTo(zero) == 1)) {
						isUseScientific = true;
					}
					format = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ENGLISH);
					format.applyPattern("0.00");
					break;
				default:
					format = (DecimalFormat) DecimalFormat.getInstance(Locale.getDefault());
					return format.parse(input.toString());
				}
				if (isUseScientific) {
					format.applyPattern("0.###E0%");
				}

				return format.format(new Double(input.toString()));
			}
			return input;
		} catch (Exception localException) {
		}
		return input;
	}

	public static String formatPersent(Object input) {
		if (checkInput(input)) {
			Double db = new Double(input.toString());
			DecimalFormat format = (DecimalFormat) DecimalFormat.getPercentInstance(Locale.ENGLISH);
			format.applyPattern("0.00%");
			return format.format(db);
		}

		return null;
	}

	public static Double formatDouble(Object input) {
		if (checkInput(input)) {
			Double db = new Double(input.toString());
			DecimalFormat format = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ENGLISH);
			format.applyPattern("0.00");
			return Double.valueOf(format.format(db));
		}

		return null;
	}

	public static Double formatFourDecimalDouble(Object input) {
		if (checkInput(input)) {
			Double db = new Double(input.toString());
			DecimalFormat format = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ENGLISH);
			format.applyPattern("0.0000");
			return Double.valueOf(format.format(db));
		}

		return null;
	}

	public static Double parseDouble(Object input)
   {
     if (input != null)
     {
       DecimalFormat format = (DecimalFormat)DecimalFormat.getInstance(Locale.US);
       try {
         Number number = format.parse(input.toString());
         return Double.valueOf(number.doubleValue());
       } catch (ParseException localParseException) {
         return Double.valueOf(NaN.0D);
       }
     }
     
 
     return null;
   }

	private static boolean checkInput(Object input)
   {
     if ((input == null) || ("".equals(input))) {
       return false;
     }
     Double db = new Double(input.toString());
     if (db.equals(Double.valueOf(NaN.0D))) {
       return false;
     }
     
 
     return true;
   }

	public static Double formatPercentDecimalDouble(Object input) {
		if (checkInput(input)) {
			BigDecimal bd1 = new BigDecimal(input.toString());
			BigDecimal bd2 = new BigDecimal("100");
			return Double.valueOf(bd1.multiply(bd2).doubleValue());
		}
		return null;
	}
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\format\StringFormatUtil.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/