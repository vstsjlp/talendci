package org.talend.utils.format;

public final class PresentableBox {
	private static final String ENCAP = "!!";

	private static final String CR = "\n";

	private static final String DASH = "-";

	private static final String SPACE = " ";

	private static final int MAX_BOX_SIZE = 256;

	private static final String MANY_DASHES = makeRepeat("-", 256);

	private static final String MANY_SPACES = makeRepeat(" ", 256);
	private final String title;
	private final String subTitle;
	private final int boxSize;
	private final String topLine;

	private static String makeRepeat(String in, int nbFold) {
		String ret = "";
		for (int i = 0; i < nbFold; i++) {
			ret = ret + in;
		}
		return ret;
	}

	private final String titleLine;

	private final String subTitleLine;

	private final String bottomLine;

	private final String emptyLine;

	public String getTitleLine() {
		return this.titleLine;
	}

	public String getTopLine() {
		return this.topLine;
	}

	public String getBottomLine() {
		return this.bottomLine;
	}

	public String getEmptyLine() {
		return this.emptyLine;
	}

	public String getSubTitleLine() {
		return this.subTitleLine;
	}

	public String getFullBox(String startTokAtEachLine) {
		String tmp = startTokAtEachLine == null ? "" : startTokAtEachLine;
		String tokPar = !tmp.startsWith("\n") ? "\n" + tmp : tmp;
		String ret = "";
		ret = ret + tokPar + getTopLine();
		ret = ret + tokPar + getEmptyLine();
		ret = ret + tokPar + getTitleLine();
		ret = ret + tokPar + getSubTitleLine();
		ret = ret + tokPar + getEmptyLine();
		ret = ret + tokPar + getBottomLine();
		return ret;
	}

	public String getFullBox() {
		return getFullBox("");
	}

	public PresentableBox(String aTitle, String aSubTitle, int aBoxSize) {
		this.title = (aTitle == null ? "" : aTitle.trim());
		this.subTitle = (aSubTitle == null ? "" : aSubTitle.trim());
		int maxTextSize = Math.max(this.title.length(), this.subTitle.length());

		int lgAddedEncap = 2 * "!!".length();
		this.boxSize = Math.max(aBoxSize + aBoxSize % 2, maxTextSize + 2 * lgAddedEncap);

		this.topLine = surroundFill("-", "!!", "--", this.boxSize);
		this.bottomLine = this.topLine;
		this.emptyLine = surroundFill(" ", "!!", "  ", this.boxSize);
		this.titleLine = surroundFill(" ", "!!", this.title, this.boxSize);
		this.subTitleLine = surroundFill(" ", "!!", this.subTitle, this.boxSize);
	}

	private static String surroundFill(String filler, String startEnd, String center, int totalSize)
   {
     int totFillers = totalSize - center.length() - startEnd.length();
 2);
 2);
     return startEnd + filler1 + center + filler2 + startEnd;
   }

	private static String getFiller(String filler) {
		if (filler == null) {
			return MANY_SPACES;
		}
		if (filler.length() == 0) {
			return MANY_SPACES;
		}
		if (filler.charAt(0) == '-') {
			return MANY_DASHES;
		}
		return MANY_SPACES;
	}
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\format\PresentableBox.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/