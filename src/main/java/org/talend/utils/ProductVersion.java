package org.talend.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductVersion implements Comparable<ProductVersion> {
	private static final Pattern THREE_DIGIT_PATTERN = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+).*");

	private static final Pattern EXTENDED_PATTERN = Pattern.compile("(\\d+)\\.(\\d+)(?:\\.(\\d+))?.*");

	private int major;

	private int minor;

	private int micro = 0;

	private boolean setMicro = false;

	public ProductVersion(int major, int minor, int micro) {
		this.major = major;
		this.minor = minor;
		this.micro = micro;
		this.setMicro = true;
	}

	public ProductVersion(int major, int minor) {
		this.major = major;
		this.minor = minor;
		this.setMicro = false;
	}

	public static ProductVersion fromString(String version, boolean extendedVersion) {
		if (!extendedVersion) {
			return fromString(version);
		}
		Matcher matcher = EXTENDED_PATTERN.matcher(version);
		if (matcher.find()) {
			int major = Integer.parseInt(matcher.group(1));
			int minor = Integer.parseInt(matcher.group(2));
			String microStr = matcher.group(3);
			if (microStr != null) {
				int micro = Integer.parseInt(microStr);
				return new ProductVersion(major, minor, micro);
			}
			return new ProductVersion(major, minor);
		}

		return null;
	}

	public static ProductVersion fromString(String version) {
		Matcher matcher = THREE_DIGIT_PATTERN.matcher(version);
		if (matcher.matches()) {
			int major = Integer.parseInt(matcher.group(1));
			int minor = Integer.parseInt(matcher.group(2));
			String microStr = matcher.group(3);
			int micro = Integer.parseInt(microStr);
			return new ProductVersion(major, minor, micro);
		}
		return null;
	}

	public int compareTo(ProductVersion other) {
		int diff = this.major - other.major;
		if (diff != 0) {
			return diff;
		}
		diff = this.minor - other.minor;
		if (diff != 0) {
			return diff;
		}
		if ((this.setMicro) && (other.setMicro)) {
			diff = this.micro - other.micro;
			if (diff != 0) {
				return diff;
			}
		}
		return 0;
	}

	public int hashCode() {
		int result = 1;
		result = 31 * result + this.major;
		result = 31 * result + this.micro;
		result = 31 * result + this.minor;
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProductVersion other = (ProductVersion) obj;
		if (this.major != other.major) {
			return false;
		}
		if (this.minor != other.minor) {
			return false;
		}
		if (this.micro != other.micro) {
			return false;
		}

		return true;
	}

	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(this.major);
		stringBuilder.append(".");
		stringBuilder.append(this.minor);
		if (this.setMicro) {
			stringBuilder.append(".");
			stringBuilder.append(this.micro);
		}
		return stringBuilder.toString();
	}

	public int getMajor() {
		return this.major;
	}

	public int getMicro() {
		return this.micro;
	}

	public int getMinor() {
		return this.minor;
	}
}
