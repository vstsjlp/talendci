package org.talend.utils.string;

import java.security.MessageDigest;

public final class MD5
 {
   public static String getMD5(byte[] source)
   {
     String s = null;
     char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
     try {
       MessageDigest md = MessageDigest.getInstance("MD5");
       md.update(source);
       byte[] tmp = md.digest();
       char[] str = new char[32];
       int k = 0;
       for (int i = 0; i < 16; i++) {
         byte byte0 = tmp[i];
         str[(k++)] = hexDigits[(byte0 >>> 4 & 0xF)];
         str[(k++)] = hexDigits[(byte0 & 0xF)];
       }
       s = new String(str);
     }
     catch (Exception e) {
       e.printStackTrace();
     }
     return s;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\string\MD5.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */