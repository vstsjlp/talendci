package org.talend.utils.string;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import org.talend.utils.sugars.ReturnCode;

public final class StringUtilities {
	public static List<String> tokenize(String input, String delimiters) {
		List<String> stringArray = new ArrayList();

		if (input == null) {
			return stringArray;
		}
		if (delimiters == null) {
			return stringArray;
		}

		StringTokenizer t = new StringTokenizer(input, delimiters);

		while (t.hasMoreTokens()) {
			stringArray.add(t.nextToken());
		}
		return stringArray;
	}

	public static ReturnCode checkBalancedParenthesis(String input, char openingBlock, char closingBlock) {
		int level = 0;

		for (int i = 0; i < input.length(); i++) {
			char currentChar = input.charAt(i);
			if (currentChar == openingBlock) {
				level++;
			} else if (currentChar == closingBlock) {
				level--;
			}
			if (level < 0) {
				return new ReturnCode("too many " + closingBlock + " at position " + i, Boolean.valueOf(false));
			}
		}
		if (level < 0) {
			return new ReturnCode("too many " + closingBlock + " at position " + i, Boolean.valueOf(false));
		}
		if (level > 0) {
			return new ReturnCode("too many " + openingBlock + " at position " + i, Boolean.valueOf(false));
		}
		return new ReturnCode();
	}

	public static String getRandomString(int length) {
		String str = "abcdefghigklmnopkrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();
		StringBuffer sf = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(62);
			sf.append(str.charAt(number));
		}
		return sf.toString();
	}

	public static String removeEndingString(String fullStr, String endingStr) {
		String newStr = fullStr;

		while (newStr.length() > 0) {
			if (!newStr.endsWith(endingStr))
				break;
			newStr = newStr.substring(0, newStr.length() - endingStr.length());
		}

		return newStr;
	}

	public static String removeStartingString(String fullStr, String startingStr) {
		String newStr = fullStr;

		while (newStr.length() > 0) {
			if (!newStr.startsWith(startingStr))
				break;
			newStr = newStr.substring(startingStr.length());
		}

		return newStr;
	}
}
