package org.talend.utils.string;

import java.io.PrintStream;
import java.nio.charset.Charset;

public class EncodingExplainer
{
	private static final String[] TEST_CASES = { "éàùïô", "中国", "ºÜºÃºÜÇ¿´ó" };

	private static final String[] CHARSETS = { "iso8859-1", "gb2312", "utf-8" };

	private static final String TOP_LEFT_LABEL = "Encode\\Decode";

	private static final int MAX_LENGTH = 14;

	private static void printEncodingTableFor(String text)
	{
		System.out.println("----------Test for <" + text + ">-----------");
		for (int i = 0; i < CHARSETS.length + 1; i++) {
			for (int j = 0; j < CHARSETS.length + 1; j++) {
				if (i == 0) {
					if (j == 0) {
						System.out.print(appendWhitespaces("Encode\\Decode"));
					} else {
						System.out.print(appendWhitespaces(CHARSETS[(j - 1)]));
					}
				}
				else if (j == 0) {
					System.out.print(appendWhitespaces(CHARSETS[(i - 1)]));
				} else {
					System.out.print(appendWhitespaces(decodeWith(encodeWith(text, CHARSETS[(i - 1)]), CHARSETS[(j - 1)])));
				}

				if (j == CHARSETS.length) {
					System.out.println();
				}
			}
		}

		System.out.println();
	}

	private static byte[] encodeWith(String text, String charsetName) {
		return text.getBytes(Charset.forName(charsetName));
	}

	private static String decodeWith(byte[] bytes, String charsetName) {
		return new String(bytes, Charset.forName(charsetName));
	}

	private static String appendWhitespaces(String text) {
		int num = 14 - text.length();
		for (int i = 0; i < num; i++) {
			text = text + " ";
		}
		text = text + "\t";
		return text;
	}

	public static void main(String[] args) { String[] arrayOfString;
	int j = (arrayOfString = TEST_CASES).length; for (int i = 0; i < j; i++) { String text = arrayOfString[i];
	printEncodingTableFor(text);
	}
	}
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\string\EncodingExplainer.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */