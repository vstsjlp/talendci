 package org.talend.utils.json;
 
 
 
 
 
 
 public class JSONException
   extends Exception
 {
   private Throwable cause;
   
 
 
 
 
   public JSONException(String message)
   {
     super(message);
   }
   
   public JSONException(Throwable t) {
     super(t.getMessage());
     this.cause = t;
   }
   
   public Throwable getCause() {
     return this.cause;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\json\JSONException.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */