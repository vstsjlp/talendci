 package org.talend.utils.json;
 
 import java.util.Iterator;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class JSONUtil
 {
   public static JSONObject merge(JSONObject source, JSONObject target)
   {
     JSONObject mergedJson = new JSONObject();
     try {
       if ((source != null) && (target != null)) {
         mergedJson = new JSONObject(target.toString());
         
         Iterator<String> keys = source.keys();
         while (keys.hasNext()) {
           String key = (String)keys.next();
           Object o = source.get(key);
           if (mergedJson.has(key)) {
             if ((o instanceof JSONObject)) {
               JSONObject objectSource = (JSONObject)o;
               JSONObject objectTarget = mergedJson.getJSONObject(key);
               JSONObject subJson = merge(objectSource, objectTarget);
               mergedJson.put(key, subJson);
             } else if ((o instanceof JSONArray)) {
               JSONArray sourceArray = (JSONArray)o;
               JSONArray targetArray = mergedJson.getJSONArray(key);
               
               for (int i = 0; i < sourceArray.length(); i++) {
                 targetArray.put(sourceArray.get(i));
               }
             }
             else {
               mergedJson.put(key, o);
             }
           } else {
             mergedJson.put(key, o);
           }
         }
       }
       else if ((source == null) && (target != null)) {
         mergedJson = new JSONObject(target.toString());
       }
       else if ((target == null) && (source != null)) {
         mergedJson = new JSONObject(source.toString());
       }
     }
     catch (JSONException localJSONException) {}
     
 
     return mergedJson;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\json\JSONUtil.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */