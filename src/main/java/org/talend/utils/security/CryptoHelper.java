package org.talend.utils.security;

public class CryptoHelper extends org.talend.daikon.security.CryptoHelper {
	public CryptoHelper(String passPhrase) {
		super(passPhrase);
	}

	public static final CryptoHelper getDefault() {
		return new CryptoHelper("99ZwBDt1L9yMX2ApJx fnv94o99OeHbCGuIHTy22 V9O6cZ2i374fVjdV76VX9g49DG1r3n90hT5c1");
	}
}
