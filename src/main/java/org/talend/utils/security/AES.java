package org.talend.utils.security;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class AES {
	static {
		if (Security.getProvider("BC") == null) {
			Security.addProvider(new BouncyCastleProvider());
		}
	}

	private static Logger log = Logger.getLogger(AES.class);

	private static final String RANDOM_SHA1PRNG = "SHA1PRNG";

	private static final String ENCRYPTION_ALGORITHM = "AES";

	private static final String EMPTY_STRING = "";

	private static final String UTF8 = "UTF8";

	private static final byte[] KeyValues = { -87, -101, -56, 50, 86, 53, -29, 3 };

	private Cipher ecipher;
	private Cipher dcipher;

	public static AES getInstance() {
		return new AES();
	}

	public AES() {
		try {
			Provider p = Security.getProvider("BC");
			KeyGenerator keyGen = KeyGenerator.getInstance("AES", p);

			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			random.setSeed(KeyValues);
			keyGen.init(128, random);

			Key key = keyGen.generateKey();

			this.ecipher = Cipher.getInstance("AES", p);
			this.dcipher = Cipher.getInstance("AES", p);

			this.ecipher.init(1, key);
			this.dcipher.init(2, key);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public String encrypt(String data)
			throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		if ("".equals(data)) {
			return "";
		}
		byte[] enc = this.ecipher.doFinal(data.getBytes("UTF8"));
		String encryptedData = new String(Base64.encodeBase64(enc), "UTF8");
		return encryptedData;
	}

	public String decrypt(String encryptedData)
			throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
		if ("".equals(encryptedData)) {
			return "";
		}
		byte[] dec = Base64.decodeBase64(encryptedData.getBytes("UTF8"));
		dec = this.dcipher.doFinal(dec);
		String decryptedData = new String(dec, "UTF8");
		return decryptedData;
	}

	public static void main(String[] args) {
		AES aes = new AES();
		BNg==", "3IqdoqEElsy8Dzz9iP3HVQ==", "w4AXOA1a34afqqnlmVLB4A==", 
				VXudqA9QYULYn4xTb8=", 
				24iyhCFKyEuveDY=" };
				try { String[] arrayOfString1;
				int j = (arrayOfString1 = arr).length; for (int i = 0; i < j; i++) { String t = arrayOfString1[i];
				System.out.println(aes.decrypt(t));
				}
				} catch (Exception e) {
					e.printStackTrace();
				}
	}
}
