package org.talend.utils.properties;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.talend.utils.files.FileUtils;

public class PropertiesReloader {
	public static synchronized void changeProperties(String fileName, String key, String oldValue, String newValue)
			throws IOException, URISyntaxException {
		FileUtils.replaceInFile(fileName, key + "=" + oldValue, key + "=" + newValue);
	}

	public static synchronized void setProperties(Class<?> clazz, String propertiesFilename, String key,
			String oldValue, String newValue) throws IOException, URISyntaxException {
		URL resource = clazz.getClassLoader().getResource(propertiesFilename);
		changeProperties(resource.toURI().getPath(), key, oldValue, newValue);
	}

	public static void main(String[] args) {
     String key = "database.driver";
     String newValue = "tagada";
     String oldValue = "org.gjt.mm.mysql.Driver";
     try
     {
       new PropertiesReloader();
       changeProperties(
database.properties", 
         key, oldValue, newValue);
     }
     catch (IOException e) {
       e.printStackTrace();
     }
     catch (URISyntaxException e) {
       e.printStackTrace();
     }
   }
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\properties\PropertiesReloader.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/