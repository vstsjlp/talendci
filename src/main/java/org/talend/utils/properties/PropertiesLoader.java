package org.talend.utils.properties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Properties;
import org.apache.log4j.Logger;

public final class PropertiesLoader {
	private static Logger log = Logger.getLogger(PropertiesLoader.class);

	public static final String MY_PROP_KEY = "talend_props";

	private static final String PROPERTIES_FILENAME = System.getProperty("talend_props");

	private static final String USAGE = "Try with -Dtalend_props=file.properties with file.properties a relative or absolute file path.";

	private static final String QUOTE = "'";

	private static TypedProperties curProp;

	private static Properties convertToProperties(Dictionary config) {
		Enumeration keys = config.keys();
		Properties props = new Properties();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			props.setProperty(key, (String) config.get(key));
		}
		return props;
	}

	public static void setConfig(Dictionary config) {
		Properties prop = convertToProperties(config);
		curProp = new TypedProperties(prop);
	}

	private static String quotedAbsolutePath(File in) {
		return "'" + in.getAbsolutePath() + "'";
	}

	public static synchronized TypedProperties getProperties() {
		if (curProp == null) {
			curProp = initialize();
		}
		return curProp;
	}

	public static synchronized TypedProperties getProperties(Class<?> clazz, String propertiesFilename) {
		TypedProperties prop = new TypedProperties();

		InputStream inStream = clazz.getClassLoader().getResourceAsStream(propertiesFilename);
		if (inStream == null) {
			inStream = clazz.getResourceAsStream(propertiesFilename);
		}

		if (inStream == null) {
			log.error("Properties file not found: " + propertiesFilename);
		} else {
			try {
				prop.load(inStream);
			} catch (IOException e) {
				log.error("Properties file " + propertiesFilename + " not found: " + e.getMessage());
			}
		}
		return prop;
	}

	public static synchronized void setProperties(Class<?> clazz, String propertiesFilename, String key,
			String oldValue, String newValue) {
		if (oldValue.equals(newValue)) {
			return;
		}
		try {
			PropertiesReloader.setProperties(clazz, propertiesFilename, key, oldValue, newValue);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	private static synchronized TypedProperties initialize() {
		Properties sysProp = System.getProperties();
		TypedProperties prop = new TypedProperties(sysProp);
		if ((PROPERTIES_FILENAME == null) || (PROPERTIES_FILENAME.length() == 0)) {
			log.warn(
					"Warning: no properties file name given in JVM arguments.Try with -Dtalend_props=file.properties with file.properties a relative or absolute file path.");
		} else {
			initialize(prop, new File(PROPERTIES_FILENAME));
		}
		return prop;
	}

	public static synchronized boolean initialize(TypedProperties prop, File in) {
		boolean ok = true;
		try {
			if (in.exists()) {
				log.info("Loading Properties from file: " + quotedAbsolutePath(in));
			} else {
				ok = false;
				log.info("Given file for properties does not exist: " + quotedAbsolutePath(in));
			}
			String filename = in.getAbsolutePath();
			ok = (ok) && (loadPropertiesLow(filename, prop));
			if (!ok) {
				log.warn("Warning: Problem when loading properties from file " + filename);
			}
		} catch (Exception e) {
			ok = false;
			log.error("Try with -Dtalend_props=file.properties with file.properties a relative or absolute file path.");
			log.error("ERROR: could not load properties file=" + quoted(in.toString()), e);
		}

		return ok;
	}

	private static boolean loadPropertiesLow(String filename, TypedProperties prop)
     throws Exception
   {
 Byte code:
   0: iconst_1
   1: istore_2
   2: aconst_null
   3: astore_3
FileInputStream
   7: dup
   8: aload_0
String;)V
   12: astore_3
   13: aload_3
   14: ifnonnull +30 -> 44
Exception
   20: dup
StringBuilder
   24: dup
   25: ldc -14
String;)V
   30: aload_0
String;
StringBuilder;
String;
String;)V
   43: athrow
   44: aload_1
   45: aload_3
InputStream;)V
   49: aload_3
FileInputStream:close	()V
   53: aconst_null
   54: astore_3
   55: goto +35 -> 90
   58: astore 4
   60: iconst_0
   61: istore_2
   62: aload_3
   63: ifnull +11 -> 74
   66: aload_3
FileInputStream:close	()V
   70: goto +4 -> 74
   73: pop
   74: aload 4
   76: athrow
   77: astore 5
   79: aload_3
   80: ifnull +7 -> 87
   83: aload_3
FileInputStream:close	()V
   87: aload 5
   89: athrow
   90: aload_3
   91: ifnull +7 -> 98
   94: aload_3
FileInputStream:close	()V
   98: iload_2
   99: ireturn
 Line number table:
   Java source line #195	-> byte code offset #0
   Java source line #196	-> byte code offset #2
   Java source line #198	-> byte code offset #4
   Java source line #200	-> byte code offset #13
   Java source line #201	-> byte code offset #17
   Java source line #203	-> byte code offset #44
   Java source line #204	-> byte code offset #49
   Java source line #205	-> byte code offset #53
   Java source line #206	-> byte code offset #55
   Java source line #207	-> byte code offset #60
   Java source line #208	-> byte code offset #62
   Java source line #210	-> byte code offset #66
   Java source line #211	-> byte code offset #70
   Java source line #215	-> byte code offset #74
   Java source line #216	-> byte code offset #77
   Java source line #217	-> byte code offset #79
   Java source line #218	-> byte code offset #83
   Java source line #220	-> byte code offset #87
   Java source line #217	-> byte code offset #90
   Java source line #218	-> byte code offset #94
   Java source line #221	-> byte code offset #98
 Local variable table:
   start	length	slot	name	signature
   0	100	0	filename	String
   0	100	1	prop	TypedProperties
   1	98	2	ok	boolean
   3	92	3	is	java.io.FileInputStream
   58	17	4	e	Exception
   77	11	5	localObject	Object
   73	1	6	localException1	Exception
 Exception table:
   from	to	target	type
Exception
Exception
   4	77	77	finally
   }

	private static String quoted(String in) {
		return "'" + in + "'";
	}
}
