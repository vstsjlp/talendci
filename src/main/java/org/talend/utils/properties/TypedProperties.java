package org.talend.utils.properties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.apache.log4j.Logger;
import org.talend.utils.string.StringUtilities;

public class TypedProperties extends Properties {
	private static Logger log = Logger.getLogger(TypedProperties.class);

	private static final long serialVersionUID = 2581080352306726049L;

	public static final String DEFAULT_DELIMITERS = ",";

	Set<String> anomaliesAlreadyLogged = new HashSet();

	public TypedProperties() {
	}

	public TypedProperties(Properties defaults) {
		super(defaults);
	}

	public <T> boolean getBooleanValue(Class<T> clazz, String shortKey, boolean defaultValue) {
		return getBooleanValue(buildKey(clazz, shortKey), defaultValue);
	}

	public boolean getBooleanValue(String key, boolean defaultValue) {
		String value = getProperty(key);
		if (value == null) {
			logWarningPropertyNotFound(key, Boolean.valueOf(defaultValue));
			return defaultValue;
		}
		return Boolean.parseBoolean(value.trim());
	}

	private void logWarningPropertyNotFound(String notFoundKey, Object defaultValue) {
		if (!this.anomaliesAlreadyLogged.contains(notFoundKey)) {
			this.anomaliesAlreadyLogged.add(notFoundKey);
			log.warn("!!! PROPERTY NOT FOUND !!!: the key '" + notFoundKey
					+ "' can't be found in the JobServer properties file, the default value '" + defaultValue
					+ "' will be used, please be sure this is not an anomaly.");
		}
	}

	public Boolean getBooleanValue(String key) {
		String value = getProperty(key);
		if (value == null) {
			return null;
		}
		return Boolean.valueOf(Boolean.parseBoolean(value.trim()));
	}

	public <T> long getLongValue(Class<T> clazz, String shortKey, long defaultValue) {
		return getLongValue(buildKey(clazz, shortKey), defaultValue);
	}

	public <T> double getDoubleValue(Class<T> clazz, String shortKey, double defaultValue) {
		return getDoubleValue(buildKey(clazz, shortKey), defaultValue);
	}

	public double getDoubleValue(String key, double defaultValue) {
		String value = getProperty(key);
		if (value == null) {
			logWarningPropertyNotFound(key, Double.valueOf(defaultValue));
			return defaultValue;
		}
		return Double.parseDouble(value.trim());
	}

	public <T> int getIntValue(Class<T> clazz, String shortKey, int defaultValue) {
		return getIntValue(buildKey(clazz, shortKey), defaultValue);
	}

	public <T> String getStringValue(Class<T> clazz, String shortKey, String defaultValue) {
		return getStringValue(buildKey(clazz, shortKey), defaultValue);
	}

	private String getStringValue(String key, String defaultValue) {
		String value = getProperty(key);
		if (value == null) {
			logWarningPropertyNotFound(key, defaultValue);
			return defaultValue;
		}
		return value;
	}

	public long getLongValue(String key, long defaultValue) {
		String value = getProperty(key);
		if (value == null) {
			logWarningPropertyNotFound(key, Long.valueOf(defaultValue));
			return defaultValue;
		}
		return Long.parseLong(value.trim());
	}

	public int getIntValue(String key, int defaultValue) {
		String value = getProperty(key);
		if (value == null) {
			logWarningPropertyNotFound(key, Integer.valueOf(defaultValue));
			return defaultValue;
		}
		return Integer.parseInt(value.trim());
	}

	public List<String> getValues(String key, List<String> defaultValues, String delimiters) {
		String value = getProperty(key);
		if (value == null) {
			logWarningPropertyNotFound(key, String.valueOf(defaultValues));
			return defaultValues;
		}
		return StringUtilities.tokenize(value, delimiters);
	}

	public <T> List<String> getValuesWithoutWarning(Class<T> clazz, String shortKey, List<String> defaultValues) {
		String key = buildKey(clazz, shortKey);
		String value = getProperty(key);
		if (value == null) {
			return defaultValues;
		}
		return StringUtilities.tokenize(value, ",");
	}

	public List<String> getValues(String key, List<String> defaultValues) {
		return getValues(key, defaultValues, ",");
	}

	public <T> List<String> getValues(Class<T> clazz, String shortKey, List<String> defaultValues) {
		return getValues(buildKey(clazz, shortKey), defaultValues, ",");
	}

	public <T> List<String> getValues(Class<T> clazz, String shortKey, List<String> defaultValues, String delimiters) {
		return getValues(buildKey(clazz, shortKey), defaultValues, delimiters);
	}

	private <T> String buildKey(Class<T> clazz, String shortKey) {
		if (clazz == null) {
			return shortKey;
		}
		return clazz.getName() + "." + shortKey;
	}

	public String logProperties(boolean all, boolean main) {
		StringBuilder builder = new StringBuilder();
		if (all) {
			builder.append("PROPERTIES: List of input properties:\n");
			Enumeration<?> names = propertyNames();

			ArrayList<String> namesList = new ArrayList();

			while (names.hasMoreElements()) {
				String key = (String) names.nextElement();
				namesList.add(key);
			}
			Collections.sort(namesList);

			int namesListListSize = namesList.size();
			for (int i = 0; i < namesListListSize; i++) {
				String key = (String) namesList.get(i);
				String property = getProperty(key);
				builder.append(key + "=" + property + "\n");
			}
			builder.append("PROPERTIES: End of list.\n");
		}
		if ((!all) && (main)) {
			builder.append("TYPED PROPERTIES=" + toString());
		}
		return builder.toString();
	}
}
