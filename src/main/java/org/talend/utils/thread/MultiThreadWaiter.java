package org.talend.utils.thread;

import java.util.List;

public class MultiThreadWaiter
 {
   private List<? extends Thread> threads;
   
   public MultiThreadWaiter(List<? extends Thread> threadsToStart)
   {
     this.threads = threadsToStart;
   }
   
   public void start() {
     int iterableListSize = this.threads.size();
     for (int i = 0; i < iterableListSize; i++) {
       ((Thread)this.threads.get(i)).start();
     }
     for (int i = 0; i < iterableListSize; i++) {
       try {
         ((Thread)this.threads.get(i)).join();
       }
       catch (InterruptedException localInterruptedException) {}
     }
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\thread\MultiThreadWaiter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */