package org.talend.utils.thread;

public class ThreadUtils {
	public static boolean waitTimeBool(Object object, long time) {
		synchronized (object) {
			try {
				object.wait(time);
			} catch (InterruptedException localInterruptedException) {
				return true;
			}
		}
		return false;
	}

	public static boolean waitTimeBool(long time)
   {
     synchronized () {
       try {
         Thread.currentThread().wait(time);
       } catch (InterruptedException localInterruptedException) {
         return true;
       }
     }
     return false;
   }

	public static void waitTimeExcept(Object object, long time) throws InterruptedException {
		synchronized (object) {
			object.wait(time);
		}
	}

	public static void waitTimeExcept(long time)
     throws InterruptedException
   {
     synchronized () {
       Thread.currentThread().wait(time);
     }
   }
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\thread\ThreadUtils.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/