package org.talend.utils.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class BoundedExecutor {
	private final ExecutorService exec;
	private final Semaphore semaphore;

	public BoundedExecutor(ExecutorService exec, int bound) {
		this.exec = exec;
		this.semaphore = new Semaphore(bound);
	}

	public BoundedExecutor(String poolName, int bound) {
		this.exec = intializeBoundedPool(poolName, bound);
		this.semaphore = new Semaphore(bound);
	}

	protected ThreadPoolExecutor intializeBoundedPool(final String poolName, int poolSize) {
		LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue();
		ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(poolSize, poolSize, 0L, TimeUnit.SECONDS,
				workQueue, new ThreadFactory() {
			ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();

			public Thread newThread(Runnable r) {
				Thread newThread = this.defaultThreadFactory.newThread(r);
				newThread.setName(poolName + "_" + newThread.getName());
				return newThread;
			}

		});
		return threadPoolExecutor;
	}

	public void submitTask(final Runnable command) throws InterruptedException, RejectedExecutionException {
		this.semaphore.acquire();
		try {
			this.exec.execute(new Runnable()
			{

				public void run()
				{
					Byte code:
						0: aload_0
						Runnable;
					4: invokeinterface 26 1 0
					9: goto +16 -> 25
					12: astore_1
					13: aload_0
					BoundedExecutor;
					Semaphore;
					Semaphore:release	()V
					23: aload_1
					24: athrow
					25: aload_0
					BoundedExecutor;
					Semaphore;
					Semaphore:release	()V
					35: return
							Line number table:
								Java source line #75	-> byte code offset #0
								Java source line #76	-> byte code offset #9
								Java source line #77	-> byte code offset #13
								Java source line #78	-> byte code offset #23
								Java source line #77	-> byte code offset #25
								Java source line #79	-> byte code offset #35
								Local variable table:
									start	length	slot	name	signature
									0	36	0	this	2
									12	12	1	localObject	Object
									Exception table:
										from	to	target	type
										0	12	12	finally
				}
			});
		}
		catch (RejectedExecutionException e)
		{
			this.semaphore.release();
			throw e;
		}
	}

	public ExecutorService getExecutorService() {
		return this.exec;
	}
}
