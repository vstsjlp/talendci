package org.talend.utils.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

public class TimeoutTask<R> {
	private AtomicInteger counter = new AtomicInteger();

	private ExecutorService threadPool;

	public TimeoutTask() {
		this(null);
	}

	public TimeoutTask(final String taskName) {
		if (taskName != null) {
			ThreadFactory threadFactory = new ThreadFactory() {
				ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();

				public Thread newThread(Runnable r) {
					Thread newThread = this.defaultThreadFactory.newThread(r);
					String threadName = TimeoutTask.this.getClass().getSimpleName()
							+ (taskName != null ? "_" + taskName : "") + "_" + newThread.getName();
					newThread.setName(threadName);
					return newThread;
				}

			};
			this.threadPool = Executors.newCachedThreadPool(threadFactory);
		}
	}

	***

	@deprecated

	public FutureTask<R> run(Callable<R> task, long timeoutMs, boolean cancelTaskIfTimeout, boolean interruptIfTimeout)
			throws TimeoutException, InterruptedException, ExecutionException {
		return run(task, timeoutMs, cancelTaskIfTimeout);
	}

	public FutureTask<R> run(Callable<R> task, long timeoutMs, boolean cancelTaskIfTimeout)
			throws TimeoutException, InterruptedException, ExecutionException {
		FutureTask<R> futureTask = new FutureTask(task);
		run(futureTask, timeoutMs, cancelTaskIfTimeout);
		return futureTask;
	}

	private R run(FutureTask<R> futureTask, long timeoutMs, boolean cancelAndInterruptTaskIfTimeout)
     throws InterruptedException, ExecutionException, TimeoutException
   {
 Byte code:
   0: aload_0
ExecutorService;
   4: aload_1
   5: invokeinterface 84 2 0
   10: aload_1
   11: lload_2
TimeUnit;
Object;
   18: pop
   19: goto +19 -> 38
   22: astore 5
   24: iload 4
   26: ifeq +9 -> 35
   29: aload_1
   30: iconst_1
FutureTask:cancel	(Z)Z
   34: pop
   35: aload 5
   37: athrow
   38: iload 4
   40: ifeq +9 -> 49
   43: aload_1
   44: iconst_1
FutureTask:cancel	(Z)Z
   48: pop
   49: aload_1
Object;
   53: areturn
 Line number table:
   Java source line #115	-> byte code offset #0
   Java source line #117	-> byte code offset #10
   Java source line #118	-> byte code offset #19
   Java source line #119	-> byte code offset #24
   Java source line #120	-> byte code offset #29
   Java source line #122	-> byte code offset #35
   Java source line #119	-> byte code offset #38
   Java source line #120	-> byte code offset #43
   Java source line #123	-> byte code offset #49
 Local variable table:
   start	length	slot	name	signature
   0	54	0	this	TimeoutTask<R>
   0	54	1	futureTask	FutureTask<R>
   0	54	2	timeoutMs	long
   0	54	4	cancelAndInterruptTaskIfTimeout	boolean
   22	14	5	localObject	Object
 Exception table:
   from	to	target	type
   10	22	22	finally
   }

	public void release() {
		this.threadPool.shutdown();
		try {
			if (!this.threadPool.awaitTermination(10L, TimeUnit.SECONDS)) {
				this.threadPool.shutdownNow();

				this.threadPool.awaitTermination(10L, TimeUnit.SECONDS);
			}
		} catch (InterruptedException localInterruptedException) {
			this.threadPool.shutdownNow();

			Thread.currentThread().interrupt();
		}
	}
}
