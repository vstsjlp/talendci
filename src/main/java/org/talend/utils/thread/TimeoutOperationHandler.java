package org.talend.utils.thread;

import org.apache.log4j.Logger;

public abstract class TimeoutOperationHandler<R> {
	private static Logger log = Logger.getLogger(TimeoutOperationHandler.class);

	private R result;

	private boolean timeoutReached;

	private String labelOperation;

	private long timeout;

	private Throwable operationError;

	public TimeoutOperationHandler(long timeout) {
		this.timeout = timeout;
	}

	public TimeoutOperationHandler(long timeout, String labelOperation) {
		this.timeout = timeout;
		this.labelOperation = labelOperation;
	}

	public void start() {
		internalStart();
	}

	protected void internalStart()
   {
     Runnable runnable = new Runnable()
     {

       public void run()
       {
 Byte code:
   0: aload_0
TimeoutOperationHandler;
   4: aload_0
TimeoutOperationHandler;
Object;
Object;)V
   14: goto +43 -> 57
   17: astore_1
Logger;
   21: aload_1
String;
   25: aload_1
Throwable;)V
   29: aload_0
TimeoutOperationHandler;
   33: aload_1
Throwable;)V
   37: aload_0
TimeoutOperationHandler;
TimeoutOperationHandler:finalizeOperation	()V
   44: goto +20 -> 64
   47: astore_2
   48: aload_0
TimeoutOperationHandler;
TimeoutOperationHandler:finalizeOperation	()V
   55: aload_2
   56: athrow
   57: aload_0
TimeoutOperationHandler;
TimeoutOperationHandler:finalizeOperation	()V
   64: return
 Line number table:
   Java source line #59	-> byte code offset #0
   Java source line #60	-> byte code offset #14
   Java source line #61	-> byte code offset #18
   Java source line #62	-> byte code offset #29
   Java source line #64	-> byte code offset #37
   Java source line #63	-> byte code offset #47
   Java source line #64	-> byte code offset #48
   Java source line #65	-> byte code offset #55
   Java source line #64	-> byte code offset #57
   Java source line #66	-> byte code offset #64
 Local variable table:
   start	length	slot	name	signature
   0	65	0	this	1
   17	17	1	t	Throwable
   47	9	2	localObject	Object
 Exception table:
   from	to	target	type
Throwable
   0	37	47	finally
       }
     };
     Thread thread = null;
     if (this.labelOperation != null) {
       thread = new Thread(runnable, this.labelOperation);
     } else {
       thread = new Thread(runnable);
     }
     thread.start();
     
     long timeStart = System.currentTimeMillis();
     
     for (;;)
     {
       if (System.currentTimeMillis() - timeStart > this.timeout) {
         this.timeoutReached = true;
       }
       
       if ((this.timeoutReached) || (!thread.isAlive())) {
         break;
       }
       ThreadUtils.waitTimeBool(50L);
     }
   }

	protected R internalRun() {
		return (R) run();
	}

	public boolean hasValidResultOperation(R result) {
		return result != null;
	}

	public abstract R run();

	public R getResult() {
		return (R) this.result;
	}

	public Throwable getOperationError() {
		return this.operationError;
	}

	public void finalizeOperation() {
	}

	public void setLabelOperation(String labelOperation) {
		this.labelOperation = labelOperation;
	}
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\thread\TimeoutOperationHandler.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/