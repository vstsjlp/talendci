package org.talend.utils.time;

import java.io.PrintStream;
import org.apache.log4j.Logger;

public class TimeTracer {
	private static final int MAX = 20;
	private static final String EMPTY = "";
	private static final String SPACE = " ";
	private Logger log;
	private String name;
	private int idx = 0;

	private long[] starts = new long[20];

	private static String[] textIndentations = ;

	private static String[] makeTextIndentations() {
		String[] indentations = new String[20];
		String m = "";
		for (int i = 0; i < 20; i++) {
			indentations[i] = m;
			m = m + "\t";
		}
		return indentations;
	}

	public TimeTracer(String aName, Logger aLog) {
		this.log = aLog;
		this.name = (aName != null ? aName : "");
	}

	public void reset() {
		this.idx = 0;
	}

	public void start() {
		start("");
	}

	public void start(String text) {
		String msg = textIndentations[this.idx];
		this.starts[this.idx] = System.currentTimeMillis();
		if (this.idx < 19) {
			this.idx += 1;
		}
		if ((text == null) || (text.length() == 0)) {
			return;
		}

		msg = msg + "ZZ***B:" + this.name + " " + text;
		show(msg);
	}

	public long end(int nbTest) {
		return end("", nbTest);
	}

	public long end() {
		return end("", 1);
	}

	public long end(String text) {
		return end(text, 1);
	}

	public long end(String text, int nbTest) {
		if (this.idx > 0) {
			this.idx -= 1;
		}
		String msg = textIndentations[this.idx];
		long beginTime = this.starts[this.idx];
		long spent = System.currentTimeMillis() - beginTime;

		msg = msg + "ZZ***E:" + this.name + " ";
		if ((text != null) && (text.length() != 0)) {
			msg = msg + text;
		}

		if (nbTest >= 2) {
			msg = nbTest + " begin was" + beginTime;
		} else {
			msg = msg + " spent=" + spent + " ms";
		}
		show(msg);
		return spent;
	}

	private void show(String msg) {
		if (this.log != null) {
			this.log.info(msg);
		} else {
			System.out.println(msg);
		}
	}
}
