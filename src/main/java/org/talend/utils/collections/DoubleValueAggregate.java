package org.talend.utils.collections;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class DoubleValueAggregate<T>
extends ValueAggregate<T, Double>
{
	public void addValue(T key, Double[] values)
	{
		Double[] doubles = (Double[])this.keyToVal.get(key);
		if (doubles == null) {
			doubles = new Double[values.length];
			Arrays.fill(doubles, Double.valueOf(0.0D));
		}

		for (int i = 0; i < values.length; i++) {
			Double d = values[i];
			if (d == null) {
				this.nullResults.add(key); return;
			}


			int tmp64_62 = i; Double[] tmp64_61 = doubles;tmp64_61[tmp64_62] = Double.valueOf(tmp64_61[tmp64_62].doubleValue() + d.doubleValue());
		}
		this.keyToVal.put(key, doubles);
	}
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\collections\DoubleValueAggregate.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */