package org.talend.utils.collections;

import java.util.Arrays;

public class Tuple {
	private Object[] tuple;
	private int hashCode = 1;

	public Tuple(Object[] objects) {
		assert (objects != null);
		this.tuple = objects;

		this.hashCode = computeHashCode();
	}

	public Object[] getTuple() {
		return this.tuple;
	}

	private int computeHashCode() {
		return Arrays.hashCode(this.tuple);
	}

	public int hashCode() {
		return this.hashCode;
	}

	public boolean equals(Object obj) {
		if ((obj != null) && ((obj instanceof Tuple))) {
			Object[] otherTuple = ((Tuple) obj).tuple;
			return Arrays.equals(this.tuple, otherTuple);
		}
		return false;
	}
}
