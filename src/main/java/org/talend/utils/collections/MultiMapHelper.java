package org.talend.utils.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class MultiMapHelper
 {
   public static <KeyT, ValT> boolean addUniqueObjectToListMap(KeyT key, ValT value, Map<KeyT, List<ValT>> map)
   {
     List<ValT> obj = (List)map.get(key);
     if (obj == null) {
       List<ValT> list = new ArrayList();
       list.add(value);
       return map.put(key, list) == null;
     }
     
 
     List<ValT> coll = obj;
     
     if (coll.contains(value)) {
       return true;
     }
     
 
     coll.add(value);
     return true;
   }
   
 
 
 
 
 
 
 
 
 
 
 
   public static boolean addUniqueComparableToListMap(Object key, Comparable<Object> value, Map<Object, List<Comparable<Object>>> map)
   {
     List<Comparable<Object>> obj = (List)map.get(key);
     if (obj == null) {
       List<Comparable<Object>> list = new ArrayList();
       list.add(value);
       return map.put(key, list) == null;
     }
     
 
     List<Comparable<Object>> coll = obj;
     
     int idx = 0;
     for (Comparable<Object> element : coll) {
       if ((value == element) || (value.compareTo(element) == 0)) {
         return true;
       }
       if (value.compareTo(element) <= 0) {
         break;
       }
     }
     
 
 
     coll.add(idx, value);
     return true;
   }
   
 
 
 
 
 
 
 
 
   public static boolean addUniqueObjectToCollectionMap(Object key, Object value, Map<Object, Set<Object>> map)
   {
     Set<Object> obj = (Set)map.get(key);
     if (obj == null) {
       Set<Object> list = new HashSet();
       list.add(value);
       return map.put(key, list) == null;
     }
     
 
     Set<Object> coll = obj;
     coll.add(value);
     return true;
   }
   
 
 
 
 
   public static <ValT> boolean isContainedInListMap(ValT object, Map<Object, Collection<ValT>> map)
   {
     Collection<Collection<ValT>> values = map.values();
     
     for (Collection<ValT> list : values) {
       if (list != null)
       {
 
         if (list.contains(object)) {
           return true;
         }
       }
     }
     return false;
   }
   
 
 
 
 
 
 
 
 
 
   public static <KeyT, ValT> boolean removeObjectFromCollectionMap(KeyT key, ValT toBeRemoved, Map<KeyT, Collection<ValT>> map)
   {
     Collection<ValT> obj = (Collection)map.get(key);
     if (obj == null) {
       return false;
     }
     
 
     Collection<ValT> coll = obj;
     return coll.remove(toBeRemoved);
   }
   
 
 
 
 
 
 
 
 
 
   public static <KeyT, ValT> boolean removeAndCleanFromCollectionMap(KeyT key, ValT toBeRemoved, Map<KeyT, Collection<ValT>> map)
   {
     Collection<ValT> obj = (Collection)map.get(key);
     if (obj == null) {
       return false;
     }
     
     Collection<ValT> coll = obj;
     boolean ret = coll.remove(toBeRemoved);
     if (coll.isEmpty()) {
       map.remove(key);
     }
     return ret;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\collections\MultiMapHelper.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */