package org.talend.utils.collections;

public class MultipleKey
   implements Comparable<MultipleKey>
 {
   private Object[] internalKey;
   
 
 
 
 
 
 
 
 
   public MultipleKey(Object[] key, int nbElements)
   {
     this.internalKey = new Object[nbElements];
     for (int i = 0; i < nbElements; i++) {
       this.internalKey[i] = key[i];
     }
   }
   
 
 
 
 
 
   public boolean equals(Object obj)
   {
     if (obj == null) {
       return false;
     }
     if (!(obj instanceof MultipleKey)) {
       return false;
     }
     MultipleKey other = (MultipleKey)obj;
     return compareTo(other) == 0;
   }
   
 
 
 
 
 
   public int hashCode()
   {
     int hash = 0;
     Object[] arrayOfObject; int j = (arrayOfObject = this.internalKey).length; for (int i = 0; i < j; i++) { Object obj = arrayOfObject[i];
       if (obj == null) {
         obj = "null";
       }
       hash += 13 * obj.hashCode();
     }
     return hash;
   }
   
   public int compareTo(MultipleKey o) {
     if (o == null) {
       return -1;
     }
     int diff = this.internalKey.length - o.internalKey.length;
     if (diff != 0) {
       return diff;
     }
     for (int i = 0; i < this.internalKey.length; i++) {
       String internalObj = String.valueOf(this.internalKey[i]);
       String otherObj = String.valueOf(o.internalKey[i]);
       diff = internalObj.compareTo(otherObj);
       if (diff != 0) {
         return diff;
       }
     }
     
     return diff;
   }
   
 
 
 
 
 
   public String toString()
   {
     StringBuilder builder = new StringBuilder();
     for (int i = 0; i < this.internalKey.length; i++) {
       Object obj = this.internalKey[i];
       builder.append(obj);
       if (i < this.internalKey.length - 1) {
         builder.append(" | ");
       }
     }
     return builder.toString();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\collections\MultipleKey.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */