package org.talend.utils.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MapValueSorter
 {
   private static class AscByValueComparator
     implements Comparator<Object>
   {
     private Map<Object, Long> baseMap;
     
     public AscByValueComparator(Map<Object, Long> map)
     {
       this.baseMap = map;
     }
     
 
 
 
 
     public int compare(Object arg0, Object arg1)
     {
       Long value0 = (Long)this.baseMap.get(arg0);
       if (value0 == null) {
         return -1;
       }
       
       Long value1 = (Long)this.baseMap.get(arg1);
       return value0.compareTo(value1);
     }
   }
   
 
 
 
   private static class DescByValueComparator
     implements Comparator<Object>
   {
     private Map<Object, Long> baseMap;
     
 
 
 
     public DescByValueComparator(Map<Object, Long> map)
     {
       this.baseMap = map;
     }
     
 
 
 
 
     public int compare(Object arg0, Object arg1)
     {
       Long value1 = (Long)this.baseMap.get(arg1);
       if (value1 == null) {
         return -1;
       }
       Long value0 = (Long)this.baseMap.get(arg0);
       
       return value1.compareTo(value0);
     }
   }
   
 
 
 
 
 
 
 
   public List<Object> getMostFrequent(Map<Object, Long> map, int topN)
   {
     List<Object> sortedMap = sortMap(map, false);
     return getFirstElements(sortedMap, topN);
   }
   
 
 
 
 
 
 
   public List<Object> getLessFrequent(Map<Object, Long> map, int bottomN)
   {
     List<Object> sortedMap = sortMap(map, true);
     return getFirstElements(sortedMap, bottomN);
   }
   
 
 
 
 
 
   private List<Object> getFirstElements(List<Object> sortedMap, int bottomN)
   {
     List<Object> mostFrequent = new ArrayList();
     int i = 0;
     for (Iterator<Object> iterator = sortedMap.iterator(); iterator.hasNext();) {
       Object object = iterator.next();
       mostFrequent.add(object);
       i++;
       if (i == bottomN) {
         break;
       }
     }
     return mostFrequent;
   }
   
 
 
 
 
 
 
   public List<Object> sortMap(Map<Object, Long> map, boolean ascendingOrder)
   {
     Comparator<Object> cmp = ascendingOrder ? new AscByValueComparator(map) : new DescByValueComparator(map);
     List<Object> keySet = new ArrayList(map.keySet());
     Collections.sort(keySet, cmp);
     return keySet;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\collections\MapValueSorter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */