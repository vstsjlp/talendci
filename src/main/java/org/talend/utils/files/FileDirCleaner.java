package org.talend.utils.files;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class FileDirCleaner {
	private static Logger log = Logger.getLogger(FileDirCleaner.class);

	private long currentTime;

	private CleanResult cleanResult;

	private String filesRegExpPattern;

	private String directoriesRegExpPattern;

	private int maxEntriesByDirectoryAndByType;

	private long maxDurationBeforeCleaning;

	private boolean cleanDirectories;

	private boolean cleanFiles;

	private boolean recursively;

	private boolean doAction = false;

	private FileDirCleanerFilter filter;

	static class CleanResult {
		Throwable firstException;

		int countExceptions;

		int deletedEntries;

		boolean alreadyLogged;
	}

	public static enum SCAN_STRATEGY {
		FILES(false, true, false), DIRECTORIES(true, false, false), FILES_AND_DIRECTORIES(true, true,
				false), FILES_RECURSIVELY(false, true, true), DIRECTORIES_RECURSIVELY(true, false,
						true), FILES_AND_DIRECTORIES_RECURSIVELY(true, true, true);

		private boolean cleanDirectories;
		private boolean cleanFiles;
		private boolean recursively;

		private SCAN_STRATEGY(boolean cleanDirectories, boolean cleanFiles, boolean recursively) {
			this.cleanFiles = cleanFiles;
			this.cleanDirectories = cleanDirectories;
			this.recursively = recursively;
		}

		public boolean isCleanDirectories() {
			return this.cleanDirectories;
		}

		public boolean isCleanFiles() {
			return this.cleanFiles;
		}

		public boolean isRecursively() {
			return this.recursively;
		}
	}

	public FileDirCleaner(boolean doAction, int maxEntriesByDirectoryAndByType, long maxDurationBeforeCleaning) {
		this(doAction, SCAN_STRATEGY.FILES, maxEntriesByDirectoryAndByType, maxDurationBeforeCleaning);
	}

	public FileDirCleaner(boolean doAction, SCAN_STRATEGY strategy, int maxEntriesByDirectory,
			long cleanAfterThisDuration) {
		this.doAction = doAction;
		this.maxEntriesByDirectoryAndByType = maxEntriesByDirectory;
		this.maxDurationBeforeCleaning = cleanAfterThisDuration;
		this.cleanDirectories = strategy.isCleanDirectories();
		this.cleanFiles = strategy.isCleanFiles();
		this.recursively = strategy.isRecursively();
	}

	public FileDirCleaner(boolean doAction, SCAN_STRATEGY strategy, int maxEntriesByDirectory,
			long cleanAfterThisDuration, boolean isCleanLibs) {
		this.doAction = doAction;
		this.maxEntriesByDirectoryAndByType = maxEntriesByDirectory;
		this.maxDurationBeforeCleaning = cleanAfterThisDuration;
		this.cleanDirectories = strategy.isCleanDirectories();
		this.cleanFiles = strategy.isCleanFiles();
		this.recursively = strategy.isRecursively();
	}

	final Comparator<File> datComparatorFiles = new Comparator()
	{



		public int compare(File o1, File o2)
		{


			long compareResult = o1.lastModified() - o2.lastModified();
			if (compareResult == 0L) {
				return 0;
			}
			Math.abs(compareResult));
		}
	};

	public int clean(String pathDir) {
		return clean(pathDir, null, null);
	}

	public int clean(String pathDir, String filesRegExpPattern) {
		return clean(pathDir, filesRegExpPattern, null);
	}

	public int clean(String pathDir, String filesRegExpPattern, String directoriesRegExpPattern) {
		return clean(pathDir, filesRegExpPattern, directoriesRegExpPattern, null);
	}

	public int clean(String pathDir, String filesRegExpPattern, String directoriesRegExpPattern,
			FileDirCleanerFilter filter) {
		if (pathDir == null) {
			throw new IllegalArgumentException("pathFolder can't be null");
		}
		this.cleanResult = new CleanResult();
		this.directoriesRegExpPattern = directoriesRegExpPattern;
		this.filesRegExpPattern = filesRegExpPattern;
		this.currentTime = System.currentTimeMillis();
		this.filter = filter;
		File dir = new File(pathDir);
		if (dir.isDirectory()) {
			cleanFilesDirRecursively(dir, true);
		}
		return this.cleanResult.deletedEntries;
	}

	public void cleanFilesDirRecursively(File dir, boolean isRootDirectory) {
		try {
			File[] listFilesDirs = dir.listFiles();
			Arrays.sort(listFilesDirs, this.datComparatorFiles);
			int countMatchingDirs = 0;
			int countMatchingFiles = 0;
			int levelDeletedDir = 0;
			int levelDeletedFile = 0;
			File[] arrayOfFile1;
			int j = (arrayOfFile1 = listFilesDirs).length;
			boolean isDirectory;
			for (int i = 0; i < j; i++) {
				File fileDirJob = arrayOfFile1[i];
				isDirectory = fileDirJob.isDirectory();
				boolean fileMatches = false;
				boolean dirMatches = false;
				String fileDirName = fileDirJob.getName();
				if (isDirectory) {
					dirMatches = (this.directoriesRegExpPattern == null)
							|| (fileDirName.matches(this.directoriesRegExpPattern));
				} else {
					fileMatches = (this.filesRegExpPattern == null) || (fileDirName.matches(this.filesRegExpPattern));
				}
				if ((isDirectory) && (dirMatches)) {
					countMatchingDirs++;
				} else if ((!isDirectory) && (fileMatches)) {
					countMatchingFiles++;
				}
			}

			boolean parentDirMatches = (this.directoriesRegExpPattern == null)
					|| (dir.getName().matches(this.directoriesRegExpPattern));

			int k = (isDirectory = listFilesDirs).length;
			for (j = 0; j < k; j++) {
				File fileDirJob = isDirectory[j];

				String fileDirName = fileDirJob.getName();
				boolean fileMatches = false;
				boolean dirMatches = false;
				boolean isDirectory = fileDirJob.isDirectory();
				boolean tooManyDirs = ((isRootDirectory) || ((!isRootDirectory) && (this.recursively))) && (isDirectory)
						&& (this.maxEntriesByDirectoryAndByType > 0)
						&& (countMatchingDirs - levelDeletedDir > this.maxEntriesByDirectoryAndByType);
				boolean tooManyFiles = (!isDirectory) && (this.maxEntriesByDirectoryAndByType > 0)
						&& (countMatchingFiles - levelDeletedFile > this.maxEntriesByDirectoryAndByType);
				boolean timeExceeded = (this.maxDurationBeforeCleaning > 0L)
						&& (this.currentTime - fileDirJob.lastModified() > this.maxDurationBeforeCleaning * 1000L);
				try {
					if ((timeExceeded) || (tooManyDirs) || (tooManyFiles)) {
						if (isDirectory) {
							dirMatches = (this.directoriesRegExpPattern == null)
									|| (fileDirName.matches(this.directoriesRegExpPattern));
						} else {
							fileMatches = (this.filesRegExpPattern == null)
									|| (fileDirName.matches(this.filesRegExpPattern));
						}
						if (isDirectory) {
							if ((this.cleanDirectories) && (dirMatches)) {
								if (checkFilter(fileDirJob)) {
									if (this.doAction) {
										FileUtils.deleteDirectory(fileDirJob);
									} else {
										StringBuilder reason = new StringBuilder();
										String sep = "";
										if (timeExceeded) {
											reason.append("timeExceeded");
											sep = ", ";
										}
										if (tooManyDirs) {
											reason.append(sep + "tooManyDirs");
										}
										log.debug("'doAction' has to be true to remove recursively the directory ("
												+ reason.toString() + "): " + fileDirJob);
									}
									this.cleanResult.deletedEntries += 1;
									levelDeletedDir++;
								}
							} else if (this.recursively) {
								cleanFilesDirRecursively(fileDirJob, false);
							}
						} else if ((this.cleanFiles) && (fileMatches) && (parentDirMatches)
								&& (checkFilter(fileDirJob))) {
							if (this.doAction) {
								FileUtils.forceDelete(fileDirJob);
							} else {
								StringBuilder reason = new StringBuilder();
								String sep = "";
								if (timeExceeded) {
									reason.append("timeExceeded");
									sep = ", ";
								}
								if (tooManyFiles) {
									reason.append(sep + "tooManyFiles");
								}
								log.debug("'doAction' has to be true to remove the file (" + reason.toString() + "): "
										+ fileDirJob);
							}
							this.cleanResult.deletedEntries += 1;
							levelDeletedFile++;
						}

					} else if ((this.recursively) && (isDirectory)) {
						levelDeletedDir++;
						cleanFilesDirRecursively(fileDirJob, false);
					}
				} catch (Throwable t) {
					this.cleanResult.countExceptions += 1;
					if (this.cleanResult.firstException == null) {
						this.cleanResult.firstException = t;
					}
				}
			}
			if ((this.cleanResult.firstException != null) && (!this.cleanResult.alreadyLogged)) {
				log.warn(
						"TempDataCleaner: " + this.cleanResult.countExceptions
						+ " error(s) have occured when trying to clean the following file or directory '"
						+ dir.getAbsolutePath() + "', the first error is the following : ",
						this.cleanResult.firstException);
				this.cleanResult.alreadyLogged = true;
			}
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
		}
	}

	private boolean checkFilter(File fileDirJob) {
		fileDirJob.isDirectory();
		if ((this.filter != null) && (!this.filter.acceptClean(fileDirJob))) {
			return false;
		}

		return true;
	}
}
