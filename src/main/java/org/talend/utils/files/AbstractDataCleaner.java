package org.talend.utils.files;

import org.apache.log4j.Logger;
import org.talend.utils.thread.ThreadUtils;

public abstract class AbstractDataCleaner {
	private static Logger log = Logger.getLogger(AbstractDataCleaner.class);

	private String cleanerLabel;

	private Thread threadCleaner;

	private int frequencyCleaningAction;

	private boolean stop;

	public AbstractDataCleaner(String cleanerLabel, int frequencyCleaningAction) {
		this.cleanerLabel = cleanerLabel;
		this.frequencyCleaningAction = frequencyCleaningAction;
	}

	public boolean start() {
		if (this.frequencyCleaningAction > 0) {
			this.threadCleaner = new Thread(this.cleanerLabel) {

				public void run() {

					AbstractDataCleaner.this.cleanLoop();
				}

			};
			this.threadCleaner.start();
			return true;
		}
		return false;
	}

	public void stop() {
		this.stop = true;
		this.threadCleaner.interrupt();
	}

	private void cleanLoop() {
		log.info(this.cleanerLabel + " started.");
		while (!this.stop) {
			try {
				clean();
			} catch (Throwable e) {
				if (log.isDebugEnabled()) {
					log.debug(e.getMessage(), e);
				} else {
					log.warn(e.getMessage());
				}
			}
			ThreadUtils.waitTimeBool(this.frequencyCleaningAction * 1000);
		}
		log.info(this.cleanerLabel + " stopped.");
	}

	protected abstract void clean();

	public int getFrequencyCleaningAction() {
		return this.frequencyCleaningAction;
	}

	public void setFrequencyCleaningAction(int frequencyCleaningAction) {
		this.frequencyCleaningAction = frequencyCleaningAction;
	}
}
