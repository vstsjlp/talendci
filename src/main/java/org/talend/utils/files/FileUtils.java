package org.talend.utils.files;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import org.talend.utils.string.StringUtilities;
import org.talend.utils.sugars.ReturnCode;

public final class FileUtils {
	public static synchronized void replaceInFile(String path, String oldString, String newString)
			throws IOException, URISyntaxException {
		File file = new File(path);
		File tmpFile = new File(path + ".tmp");

		FileInputStream fis = null;
		BufferedInputStream bis = null;
		DataInputStream dis = null;

		fis = new FileInputStream(file);
		bis = new BufferedInputStream(fis);
		dis = new DataInputStream(bis);

		OutputStream tempOutputStream = new FileOutputStream(tmpFile);
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(tempOutputStream, "UTF8"));

		int len = 0;

		byte[] buf2 = new byte['Ѐ'];

		while ((len = dis.read(buf2)) != -1) {
			String line = new String(buf2, 0, len);
			String newLine = line.replace(oldString, newString);
			newLine = new String(newLine.getBytes(), "UTF8");
			bufferedWriter.write(newLine);
			bufferedWriter.flush();
		}

		bufferedWriter.close();
		dis.close();

		file.delete();
		tmpFile.renameTo(file);
	}

	public static synchronized List<ReturnCode> checkBracketsInFile(String path)
			throws IOException, URISyntaxException {
		List<ReturnCode> returncodes = new ArrayList();
		File file = new File(path);
		BufferedReader in = new BufferedReader(new FileReader(file));

		int lineNb = 0;
		String line;
		while ((line = in.readLine()) != null) {
			String line;
			ReturnCode checkBlocks = StringUtilities.checkBalancedParenthesis(line, '(', ')');
			lineNb++;
			if (!checkBlocks.isOk().booleanValue()) {
				String errorMsg = "Line " + lineNb + ": " + checkBlocks.getMessage();
				returncodes.add(new ReturnCode(errorMsg, Boolean.valueOf(false)));
			}
		}

		in.close();
		return returncodes;
	}

	public static void getAllFilesFromFolder(File aFolder, List<File> fileList, FilenameFilter filenameFilter) {
		if (aFolder != null) {
			File[] folderFiles = aFolder.listFiles(filenameFilter);
			if ((fileList != null) && (folderFiles != null)) {
				Collections.addAll(fileList, folderFiles);
			}
			File[] allFolders = aFolder.listFiles(new FileFilter() {
				public boolean accept(File arg0) {
					return arg0.isDirectory();
				}
			});
			if (allFolders != null) {
				File[] arrayOfFile1;
				int j = (arrayOfFile1 = allFolders).length;
				for (int i = 0; i < j; i++) {
					File folder = arrayOfFile1[i];
					getAllFilesFromFolder(folder, fileList, filenameFilter);
				}
			}
		}
	}

	public static List<File> getAllFilesFromFolder(File aFolder, FilenameFilter filenameFilter) {
		List<File> files = new ArrayList();
		getAllFilesFromFolder(aFolder, files, filenameFilter);
		return files;
	}

	public static List<File> getAllFilesFromFolder(File aFolder, Set<FilterInfo> filterInfo) {
		List<File> files = new ArrayList();
		if (filterInfo != null) {
			for (FilterInfo info : filterInfo) {
				FilterInfo thatInfo = info;
				files.addAll(getAllFilesFromFolder(aFolder, new FilenameFilter() {
					public boolean accept(File dir, String name) {
						if (name == null) {
							return false;
						}
						return (name.startsWith(FileUtils.this.getPrefix()))
								&& (name.endsWith(FileUtils.this.getSuffix()));
					}
				}));
			}
		}
		return files;
	}

	public static void deleteFiles(File folder, Function<String, Boolean> func) {
		if ((folder != null) && (func != null) && (folder.exists())) {
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File _dir, String name) {
					return ((Boolean) FileUtils.this.apply(name)).booleanValue();
				}

			};
			List<File> filesToRemove = getAllFilesFromFolder(folder, filter);
			for (File fileToRemove : filesToRemove) {
				fileToRemove.delete();
			}
		}
	}

	public static File createTmpFolder(String prefix, String suffix) {
		File tempFolder = null;
		try {
			tempFolder = File.createTempFile(prefix, suffix);
			tempFolder.delete();
		} catch (IOException localIOException) {
			String tempFolderName = prefix + System.currentTimeMillis() + suffix;
			tempFolder = createUserTmpFolder(tempFolderName);
		}
		tempFolder.mkdirs();
		return tempFolder;
	}

	public static File createUserTmpFolder(String folderName)
	{
		" + folderName);
		tmpFolder.mkdirs();
		return tmpFolder;
	}
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\files\FileUtils.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/