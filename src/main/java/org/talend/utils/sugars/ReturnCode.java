package org.talend.utils.sugars;

public class ReturnCode {
	private Boolean ok;

	private String message;

	public ReturnCode(String mess, Boolean retCode) {
		this.message = mess;
		this.ok = retCode;
	}

	public ReturnCode() {
		this.message = null;
		this.ok = Boolean.valueOf(true);
	}

	public ReturnCode(Boolean isOk) {
		this.message = null;
		this.ok = isOk;
	}

	public void setReturnCode(String mess, Boolean retCode) {
		setMessage(mess);
		setOk(retCode);
	}

	public Boolean isOk() {
		return this.ok;
	}

	public void setOk(Boolean ok) {
		this.ok = ok;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String toString() {
		return "KO: " + this.message;
	}
}
