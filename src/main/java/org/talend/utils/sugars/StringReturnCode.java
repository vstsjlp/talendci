package org.talend.utils.sugars;

public class StringReturnCode
   extends TypedReturnCode<String>
 {
   public String getValue()
   {
     return (String)getObject();
   }
   
   public void setValue(String value) {
     setObject(value);
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\sugars\StringReturnCode.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */