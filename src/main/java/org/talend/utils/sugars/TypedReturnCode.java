package org.talend.utils.sugars;

public class TypedReturnCode<T> extends ReturnCode {
	private T object;

	public TypedReturnCode(String mess, boolean retCode) {
		super(mess, Boolean.valueOf(retCode));
	}

	public TypedReturnCode() {
	}

	public TypedReturnCode(boolean isOk) {
		super(Boolean.valueOf(isOk));
	}

	public T getObject() {
		return (T) this.object;
	}

	public void setObject(T obj) {
		this.object = obj;
	}

	public void setReturnCode(String mess, boolean retCode, T obj) {
		super.setReturnCode(mess, Boolean.valueOf(retCode));
		setObject(obj);
	}
}
