package org.talend.utils.sql;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import org.talend.utils.format.StringFormatUtil;

public final class ResultSetUtils
 {
   private static final String NULLDATE = "0000-00-00 00:00:00";
   
   public static void printResultSet(ResultSet set, int width)
     throws SQLException
   {
     ResultSetMetaData metaData = set.getMetaData();
     int columnCount = metaData.getColumnCount();
     
     int minWidth = width;
     for (int i = 1; i <= columnCount; i++) {
       minWidth = Math.max(minWidth, metaData.getColumnName(i).length());
     }
     
     String header = new String();
     for (int i = 1; i <= columnCount; i++) {
       String columnName = StringFormatUtil.padString(metaData.getColumnName(i), minWidth);
       header = header + columnName;
     }
     System.out.println(header);
     
 
     String types = new String();
     for (int i = 1; i <= columnCount; i++) {
       String columnTypeName = StringFormatUtil.padString(metaData.getColumnTypeName(i), minWidth);
       types = types + columnTypeName;
     }
     System.out.println(types);
     
 
     while (set.next()) {
       System.out.println(formatRow(set, columnCount, minWidth));
     }
   }
   
 
 
 
 
 
 
 
   public static String formatRow(ResultSet set, int nbColumns, int width)
     throws SQLException
   {
     String row = new String();
     for (int i = 1; i <= nbColumns; i++) {
       Object col = set.getObject(i);
       row = row + StringFormatUtil.padString(col != null ? col.toString() : "", width);
     }
     return row;
   }
   
 
 
 
 
 
 
 
 
   public static Object getBigObject(ResultSet set, int columnIndex)
     throws SQLException
   {
     Object object = null;
     try {
       object = set.getObject(columnIndex);
       if ((object != null) && ((object instanceof Clob))) {
         Reader is = ((Clob)object).getCharacterStream();
         BufferedReader br = new BufferedReader(is);
         String str = br.readLine();
         StringBuffer sb = new StringBuffer();
         while (str != null) {
           sb.append(str);
           str = br.readLine();
         }
         return sb.toString();
       }
     } catch (SQLException e) {
       if ("0000-00-00 00:00:00".equals(set.getString(columnIndex))) {
         object = null;
       } else {
         throw e;
       }
     }
     catch (IOException localIOException) {
       object = null;
     }
     return object;
   }
   
 
 
 
 
 
 
 
 
   public static Object getBigObject(ResultSet set, String columnName)
     throws SQLException
   {
     Object object = null;
     try {
       object = set.getObject(columnName);
       if ((object != null) && ((object instanceof Clob))) {
         Reader is = ((Clob)object).getCharacterStream();
         BufferedReader br = new BufferedReader(is);
         String str = br.readLine();
         StringBuffer sb = new StringBuffer();
         while (str != null) {
           sb.append(str);
           str = br.readLine();
         }
         return sb.toString();
       }
     } catch (SQLException e) {
       if ("0000-00-00 00:00:00".equals(set.getString(columnName))) {
         object = null;
       } else {
         throw e;
       }
     }
     catch (IOException localIOException) {
       object = null;
     }
     return object;
   }
   
 
 
 
 
 
 
 
   public static Object getObject(ResultSet set, int columnIndex)
     throws SQLException
   {
     Object object = null;
     try {
       object = set.getObject(columnIndex);
     } catch (SQLException e) {
       if ("0000-00-00 00:00:00".equals(set.getString(columnIndex))) {
         object = null;
       } else {
         throw e;
       }
     }
     
     return object;
   }
   
 
 
 
 
 
 
 
   public static Object getObject(ResultSet set, String columnName)
     throws SQLException
   {
     Object object = null;
     try {
       object = set.getObject(columnName);
     } catch (SQLException e) {
       if ("0000-00-00 00:00:00".equals(set.getString(columnName))) {
         object = null;
       } else {
         throw e;
       }
     }
     
     return object;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\sql\ResultSetUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */