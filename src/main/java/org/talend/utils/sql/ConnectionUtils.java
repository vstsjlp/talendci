package org.talend.utils.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.talend.utils.sugars.ReturnCode;

public final class ConnectionUtils {
	private static Logger log = Logger.getLogger(ConnectionUtils.class);

	public static final String PASSPHRASE = "99ZwBDt1L9yMX2ApJx fnv94o99OeHbCGuIHTy22 V9O6cZ2i374fVjdV76VX9g49DG1r3n90hT5c1";

	private static List<String> sybaseDBProductsNames;

	public static final String IBM_DB2_ZOS_PRODUCT_NAME = "DB2";

	public static final String POSTGRESQL_PRODUCT_NAME = "POSTGRESQL";

	public static final String SYBASE_PRODUCT_NAME = "SYBASE";

	public static final String SYBASE_LANGUAGE = "Adaptive Server Enterprise | Sybase Adaptive Server IQ";

	private static final String ACCESS_DRIVER = "Microsoft Access Driver";

	public static final String SHUTDOWN_PARAM = ";shutdown=true";

	@Deprecated
	public static Connection createConnection(String url, String driverClassName, Properties props)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Driver driver = (Driver) Class.forName(driverClassName).newInstance();

		return createConnection(url, driver, props);
	}

	@Deprecated
	public static Connection createConnection(String url, Driver driver, Properties props)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Connection connection = null;
		if (driver != null) {
			try {
				DriverManager.registerDriver(driver);
				Class.forName(driver.getClass().getName());
				if ((isMsSqlServer(url)) || (isSybase(url)) || (isHiveServer(url))) {
					connection = driver.connect(url, props);
				} else {
					connection = DriverManager.getConnection(url, props);
				}
			} catch (ClassNotFoundException localClassNotFoundException) {
				try {
					connection = driver.connect(url, props);
				} catch (Exception exception) {
					log.info(exception);
				}

			}

		} else if (isODBCServer(url)) {
			connection = DriverManager.getConnection(url, props);
		}

		return connection;
	}

	private static boolean isODBCServer(String url) {
		return url.startsWith("jdbc:odbc:");
	}

	public static boolean isJDBCURL(String url) {
		return (url != null) && (url.startsWith("jdbc:"));
	}

	public static boolean isSybase(String url) {
		return url.indexOf("sybase") > -1;
	}

	public static boolean isMsSqlServer(String url) {
		return url.indexOf("sqlserver") > -1;
	}

	public static boolean isHiveServer(String url) {
		return url.indexOf("hive") > -1;
	}

	public static boolean isHiveConnection(String url) {
		return (url != null) && (url.startsWith("jdbc:hive"));
	}

	public static boolean isAccess(String url) {
		if ((url != null) && (url.contains("Microsoft Access Driver"))) {
			return true;
		}
		return false;
	}

	public static boolean isHsql(String url) {
		return (url != null) && (url.startsWith("jdbc:hsqldb"));
	}

	public static boolean isInProcessModeHsql(String url) {
		return (url != null) && (url.startsWith("jdbc:hsqldb:file"));
	}

	public static String addShutDownForHSQLUrl(String url, String AdditionalParams) {
		String dbUrl = url;
		if (AdditionalParams.indexOf(";shutdown=true") == -1) {
			dbUrl = dbUrl + ";shutdown=true";
		}
		return dbUrl;
	}

	public static void executeShutDownForHSQL(Connection connection) throws SQLException {
		Statement statement = connection.createStatement();
		statement.executeUpdate("SHUTDOWN;");
		statement.close();
	}

	public static boolean isServerModeHsql(String url) {
		return (url != null) && (url.startsWith("jdbc:hsqldb:hsql"));
	}

	public static boolean isTeradata(String url) {
		return (url != null) && (url.startsWith("jdbc:teradata"));
	}

	public static boolean isVertica(String url) {
		return (url != null) && (url.startsWith("jdbc:vertica"));
	}

	public static ReturnCode isValid(Connection connection) {
		ReturnCode retCode = new ReturnCode();
		if (connection == null) {
			retCode.setReturnCode("Connection is null!", Boolean.valueOf(false));
			return retCode;
		}

		ResultSet ping = null;
		try {
			if (connection.isClosed()) {
				retCode.setReturnCode("Connection is closed", Boolean.valueOf(false));
				return retCode;
			}

			connection.getAutoCommit();

			return retCode;
		} catch (SQLException sqle) {
			ReturnCode localReturnCode1;
			retCode.setReturnCode(
					"SQLException caught:" + sqle.getMessage() + " SQL error code: " + sqle.getErrorCode(),
					Boolean.valueOf(false));
			return retCode;
		} finally {
			if (ping != null) {
				try {
					ping.close();
				} catch (Exception localException4) {
				}
			}
		}
	}

	public static ReturnCode closeConnection(Connection connection)
	{
		Byte code:
			ConnectionUtils:$assertionsDisabled	Z
			3: ifne +15 -> 18
			6: aload_0
			7: ifnonnull +11 -> 18
			AssertionError
			13: dup
			AssertionError:<init>	()V
			17: athrow
			ReturnCode
			21: dup
			22: iconst_1
			Boolean;
			Boolean;)V
29: astore_1
30: aload_0
31: ifnull +150 -> 181
34: aload_0
35: invokeinterface 239 1 0
40: ifne +141 -> 181
43: aload_0
44: invokeinterface 279 1 0
49: ifnull +132 -> 181
52: aload_0
53: invokeinterface 279 1 0
58: invokeinterface 283 1 0
63: astore_2
64: aload_2
String;)Z
68: ifeq +113 -> 181
71: aload_0
Connection;)V
75: goto +106 -> 181
78: astore_2
79: aload_1
StringBuilder
83: dup
84: ldc_w 292
String;)V
90: aload_2
String;
StringBuilder;
String;
100: iconst_0
Boolean;
Boolean;)V
107: aload_0
108: ifnull +107 -> 215
111: aload_0
112: invokeinterface 239 1 0
117: ifne +98 -> 215
120: aload_0
121: invokeinterface 294 1 0
126: goto +89 -> 215
129: astore 4
Logger;
134: aload 4
136: aload 4
Throwable;)V
141: goto +74 -> 215
144: astore_3
145: aload_0
146: ifnull +33 -> 179
149: aload_0
150: invokeinterface 239 1 0
155: ifne +24 -> 179
158: aload_0
159: invokeinterface 294 1 0
164: goto +15 -> 179
167: astore 4
Logger;
172: aload 4
174: aload 4
Throwable;)V
179: aload_3
180: athrow
181: aload_0
182: ifnull +33 -> 215
185: aload_0
186: invokeinterface 239 1 0
191: ifne +24 -> 215
194: aload_0
195: invokeinterface 294 1 0
200: goto +15 -> 215
203: astore 4
Logger;
208: aload 4
210: aload 4
Throwable;)V
215: aload_1
216: areturn
Line number table:
	Java source line #295	-> byte code offset #0
	Java source line #296	-> byte code offset #18
	Java source line #298	-> byte code offset #30
	Java source line #299	-> byte code offset #43
	Java source line #300	-> byte code offset #52
	Java source line #303	-> byte code offset #64
	Java source line #304	-> byte code offset #71
	Java source line #308	-> byte code offset #75
	Java source line #309	-> byte code offset #79
	Java source line #312	-> byte code offset #107
	Java source line #313	-> byte code offset #120
	Java source line #315	-> byte code offset #126
	Java source line #316	-> byte code offset #131
	Java source line #310	-> byte code offset #144
	Java source line #312	-> byte code offset #145
	Java source line #313	-> byte code offset #158
	Java source line #315	-> byte code offset #164
	Java source line #316	-> byte code offset #169
	Java source line #318	-> byte code offset #179
	Java source line #312	-> byte code offset #181
	Java source line #313	-> byte code offset #194
	Java source line #315	-> byte code offset #200
	Java source line #316	-> byte code offset #205
	Java source line #319	-> byte code offset #215
	Local variable table:
		start	length	slot	name	signature
		0	217	0	connection	Connection
		29	187	1	rc	ReturnCode
		63	2	2	url	String
		78	13	2	e	SQLException
		144	36	3	localObject	Object
		129	8	4	e	SQLException
		167	8	4	e	SQLException
		203	8	4	e	SQLException
		Exception table:
			from	to	target	type
			SQLException
			SQLException
			30	107	144	finally
			SQLException
			SQLException
	}

	public static boolean isSybase(Connection connection) throws SQLException {
		DatabaseMetaData connectionMetadata = connection.getMetaData();
		if ((connectionMetadata.getDriverName() != null) && (connectionMetadata.getDatabaseProductName() != null)) {
			String[] arrayOfString;
			int j = (arrayOfString = getSybaseDBProductsName()).length;
			for (int i = 0; i < j; i++) {
				String keyString = arrayOfString[i];
				if (keyString.equals(connectionMetadata.getDatabaseProductName().trim())) {
					return true;
				}
			}
		}
		return false;
	}

	@Deprecated
	public static DatabaseMetaData getConnectionMetadata(Connection conn)
			throws SQLException
	{
		DatabaseMetaData dbMetaData = conn.getMetaData();

		if ((dbMetaData != null) && (dbMetaData.getDatabaseProductName() != null) && 
				(dbMetaData.getDatabaseProductName().equals("DB2"))) {
			dbMetaData = conn.getMetaData();
			OS");
		}

		return dbMetaData;
	}

	public static boolean isDB2(DatabaseMetaData metadata) throws SQLException {
		if ((metadata != null) && (metadata.getDatabaseProductName() != null)
				&& (metadata.getDatabaseProductName().indexOf("DB2") > -1)) {
			return true;
		}
		return false;
	}

	public static boolean isOracleForSid(DatabaseMetaData metadata, String oracleProduct) throws SQLException {
		if ((metadata != null) && (metadata.getDatabaseProductName() != null)
				&& (metadata.getDatabaseProductName().indexOf(oracleProduct) > -1)) {
			return true;
		}
		return false;
	}

	public static boolean isSqlite(DatabaseMetaData metadata, String sqliteProduct) throws SQLException {
		if ((metadata != null) && (metadata.getDatabaseProductName() != null)
				&& (metadata.getDatabaseProductName().toUpperCase().indexOf(sqliteProduct) > -1)) {
			return true;
		}
		return false;
	}

	public static boolean isPostgresql(DatabaseMetaData metadata) {
		boolean result = false;
		try {
			if ((metadata != null) && (metadata.getDatabaseProductName() != null)
					&& (metadata.getDatabaseProductName().toUpperCase().indexOf("POSTGRESQL") > -1)) {
				result = true;
			}
		} catch (SQLException localSQLException) {
			result = false;
		}
		return result;
	}

	public static boolean isSybase(DatabaseMetaData metadata) {
		boolean result = false;

		if ((metadata != null)
				&& ("org.talend.commons.utils.database.SybaseDatabaseMetaData".equals(metadata.getClass().getName()))) {
			result = true;
		}
		return result;
	}

	public static boolean isExasol(DatabaseMetaData metadata) throws SQLException {
		if ((metadata != null) && (metadata.getDriverName() != null)
				&& (metadata.getDriverName().toLowerCase().startsWith("exasol"))
				&& (metadata.getDatabaseProductName() != null)
				&& (metadata.getDatabaseProductName().toLowerCase().startsWith("exasol"))) {
			return true;
		}
		return false;
	}

	public static String[] getSybaseDBProductsName() {
		if (sybaseDBProductsNames == null) {
			sybaseDBProductsNames = new ArrayList();
			String[] arrayOfString;
			int j = (arrayOfString = "Adaptive Server Enterprise | Sybase Adaptive Server IQ".split("\\|")).length;
			for (int i = 0; i < j; i++) {
				String name = arrayOfString[i];
				sybaseDBProductsNames.add(name.trim());
			}
			sybaseDBProductsNames.add("Sybase");
			sybaseDBProductsNames.add("Sybase IQ");
			sybaseDBProductsNames.add("Adaptive Server Enterprise | Sybase Adaptive Server IQ");
		}
		return (String[]) sybaseDBProductsNames.toArray(new String[sybaseDBProductsNames.size()]);
	}

	public static boolean isOdbcTeradata(DatabaseMetaData metadata) throws SQLException {
		if ((metadata.getDriverName() != null)
				&& (metadata.getDriverName().toLowerCase().startsWith("jdbc-odbc bridge (tdata32.dll)"))
				&& (metadata.getDatabaseProductName() != null)
				&& (metadata.getDatabaseProductName().toLowerCase().indexOf("teradata") > -1)) {
			return true;
		}
		return false;
	}

	public static boolean isOdbcHyperFileSQL(DatabaseMetaData metadata) {
		try {
			if ((metadata.getDriverName() != null)
					&& (metadata.getDriverName().toLowerCase().startsWith("jdbc-odbc bridge"))
					&& (metadata.getDatabaseProductName() != null)
					&& (metadata.getDatabaseProductName().toLowerCase().indexOf("hyperfilesql") > -1)) {
				return true;
			}
		} catch (SQLException localSQLException) {
			return false;
		}
		return false;
	}

	public static boolean isNetezza(DatabaseMetaData databaseMetaData) {
		try {
			if ((databaseMetaData != null) && (databaseMetaData.getDriverName() != null)
					&& (databaseMetaData.getDriverName().toLowerCase().startsWith("netezza"))
					&& (databaseMetaData.getDatabaseProductName() != null)
					&& (databaseMetaData.getDatabaseProductName().toLowerCase().startsWith("netezza"))) {
				return true;
			}
		} catch (SQLException e) {
			log.warn(e);
		}
		return false;
	}
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\sql\ConnectionUtils.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/