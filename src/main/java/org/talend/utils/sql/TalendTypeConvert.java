package org.talend.utils.sql;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

public final class TalendTypeConvert
 {
   private static Map<String, Integer> map;
   private static String idStr = "id_";
   
 
 
 
   private static Map<String, Integer> getMap()
   {
     if (map == null) {
       map = new HashMap();
       map.put(talendTypeName(Boolean.class), Integer.valueOf(16));
       map.put(talendTypeName(Byte.class), Integer.valueOf(3));
       map.put(talendTypeName(Character.class), Integer.valueOf(1));
       map.put(talendTypeName(Date.class), Integer.valueOf(91));
       map.put(talendTypeName(String.class), Integer.valueOf(2005));
       map.put(talendTypeName(Double.class), Integer.valueOf(8));
       map.put(talendTypeName(Float.class), Integer.valueOf(6));
       map.put(talendTypeName(Integer.class), Integer.valueOf(4));
       map.put(talendTypeName(Long.class), Integer.valueOf(4));
       map.put(talendTypeName(Short.class), Integer.valueOf(5));
     }
     return map;
   }
   
   private static String talendTypeName(Class<?> nullableClass) {
     return idStr + nullableClass.getSimpleName();
   }
   
 
 
 
 
 
 
   public static int convertToJDBCType(String talendType)
   {
     Integer type = (Integer)getMap().get(talendType);
     return type == null ? 0 : type.intValue();
   }
   
 
 
 
 
 
 
 
 
   public static Object convertToObject(String talendType, String value, String datePattern)
   {
     Object object = null;
     
     value = value.trim();
     value = StringUtils.remove(value, "\r");
     value = StringUtils.remove(value, "\n");
     try
     {
       if (talendType.equals(talendTypeName(Boolean.class))) {
         object = Boolean.valueOf(Boolean.valueOf(value).booleanValue());
       } else if (talendType.equals(talendTypeName(Byte.class))) {
         object = Byte.valueOf(Byte.valueOf(value).byteValue());
       } else if (talendType.equals(talendTypeName(Date.class)))
       {
         if ((datePattern == null) || ("".equals(datePattern.trim()))) {
           datePattern = "yyyy-MM-dd";
         } else {
           datePattern = StringUtils.replace(datePattern, "\"", "");
         }
         
 
         SimpleDateFormat sdf = new SimpleDateFormat(datePattern, Locale.US);
         object = sdf.parse(value);
       } else if (talendType.equals(talendTypeName(Double.class))) {
         object = Double.valueOf(Double.parseDouble(value));
       } else if (talendType.equals(talendTypeName(Float.class))) {
         object = Float.valueOf(Float.parseFloat(value));
       } else if (talendType.equals(talendTypeName(Integer.class))) {
         object = Integer.valueOf(Integer.parseInt(value));
       } else if (talendType.equals(talendTypeName(Long.class))) {
         object = Long.valueOf(Long.parseLong(value));
       } else if (talendType.equals(talendTypeName(Short.class))) {
         object = Short.valueOf(Short.parseShort(value));
       } else if ((talendType.equals(talendTypeName(String.class))) || (talendType.equals(talendTypeName(Character.class)))) {
         object = value;
       }
     } catch (ClassCastException localClassCastException) {
       return null;
     } catch (Exception localException) {
       return null;
     }
     return object;
   }
   
 
 
 
 
 
 
   public static String convertToJavaType(String talendType)
   {
     if (talendType == null) {
       return "";
     }
     talendType = StringUtils.remove(talendType, idStr);
     return talendType;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\sql\TalendTypeConvert.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */