package org.talend.utils.sql.metadata.constants;

public enum GetForeignKey
{
	PKTABLE_CAT,  PKTABLE_SCHEM,  PKTABLE_NAME,  PKCOLUMN_NAME,  FKTABLE_CAT,  FKTABLE_SCHEM,  FKTABLE_NAME,  FKCOLUMN_NAME,  KEY_SEQ,  UPDATE_RULE,  DELETE_RULE,  FK_NAME,  PK_NAME,  DEFERRABILITY;
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\sql\metadata\constants\GetForeignKey.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */