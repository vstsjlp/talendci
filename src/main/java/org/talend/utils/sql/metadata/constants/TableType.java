package org.talend.utils.sql.metadata.constants;

import java.util.ArrayList;
import java.util.List;

public enum TableType {
	TABLE, VIEW, SYSTEM_TABLE, GLOBAL_TEMPORARY, LOCAL_TEMPORARY, ALIAS, SYNONYM;

	public String toString() {
		switch (this) {
		case LOCAL_TEMPORARY:
			return "SYSTEM TABLE";
		case SYNONYM:
			return "GLOBAL TEMPORARY";
		case SYSTEM_TABLE:
			return "LOCAL TEMPORARY";
		}
		return name();
	}

	public static String[] getTableTypes(TableType... type) {
		if (type == null) {
			return null;
		}
		List<String> tablesTypes = new ArrayList();
		TableType[] arrayOfTableType = type;
		int j = type.length;
		for (int i = 0; i < j; i++) {
			TableType tableType = arrayOfTableType[i];
			if (tableType == null) {
				tablesTypes.add(null);
			} else {
				tablesTypes.add(tableType.toString());
			}
		}
		return (String[]) tablesTypes.toArray(new String[tablesTypes.size()]);
	}
}
