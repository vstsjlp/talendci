package org.talend.utils.sql.metadata.constants;

public enum GetPrimaryKey
{
	TABLE_CAT,  TABLE_SCHEM,  TABLE_NAME,  COLUMN_NAME,  KEY_SEQ,  PK_NAME;
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\sql\metadata\constants\GetPrimaryKey.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */