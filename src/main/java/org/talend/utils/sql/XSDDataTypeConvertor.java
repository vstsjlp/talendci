package org.talend.utils.sql;

import java.util.HashMap;
import java.util.Map;

public final class XSDDataTypeConvertor
 {
   private static Map<String, Integer> map;
   
   public static Map<String, Integer> getMap()
   {
     if (map == null) {
       map = new HashMap();
       map.put("anySimpleType", Integer.valueOf(2009));
       map.put("anyURI", Integer.valueOf(70));
       map.put("duration", Integer.valueOf(12));
       map.put("base64Binary", Integer.valueOf(-2));
       map.put("boolean", Integer.valueOf(16));
       map.put("date", Integer.valueOf(91));
       map.put("dateTime", Integer.valueOf(93));
       map.put("decimal", Integer.valueOf(3));
       map.put("integer", Integer.valueOf(3));
       map.put("nonPositiveInteger", Integer.valueOf(3));
       map.put("long", Integer.valueOf(-5));
       map.put("nonNegativeInteger", Integer.valueOf(3));
       map.put("negativeInteger", Integer.valueOf(3));
       map.put("int", Integer.valueOf(4));
       map.put("unsignedLong", Integer.valueOf(3));
       map.put("positiveInteger", Integer.valueOf(3));
       map.put("short", Integer.valueOf(5));
       map.put("unsignedInt", Integer.valueOf(3));
       map.put("byte", Integer.valueOf(3));
       map.put("unsignedShort", Integer.valueOf(3));
       map.put("unsignedByte", Integer.valueOf(3));
       map.put("double", Integer.valueOf(8));
       map.put("float", Integer.valueOf(6));
       map.put("gMonth", Integer.valueOf(91));
       map.put("gMonthDay", Integer.valueOf(91));
       map.put("gDay", Integer.valueOf(91));
       map.put("gYearMonth", Integer.valueOf(91));
       map.put("gYear", Integer.valueOf(91));
       map.put("NOTATION", Integer.valueOf(2005));
       map.put("hexBinary", Integer.valueOf(-2));
       map.put("QName", Integer.valueOf(2005));
       map.put("time", Integer.valueOf(93));
       map.put("string", Integer.valueOf(2005));
     }
     return map;
   }
   
 
 
 
 
 
   public static int convertToJDBCType(String xsdDataType)
   {
     Integer type = (Integer)getMap().get(xsdDataType);
     return type == null ? 0 : type.intValue();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\sql\XSDDataTypeConvertor.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */