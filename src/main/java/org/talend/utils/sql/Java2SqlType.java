 package org.talend.utils.sql;
 
 
 
 
 
 public final class Java2SqlType
 {
   public static final int NCHAR = -15;
   
 
 
 
   public static final int NTEXT = -16;
   
 
 
 
   public static final int NVARCHAR = -9;
   
 
 
 
   public static final int NVARCHAR2 = 1111;
   
 
 
 
   public static final int BIT = -7;
   
 
 
   public static final int VARBINARY = 2004;
   
 
 
   public static final int TERADATA_INTERVAL = 1000;
   
 
 
   public static final int TERADATA_INTERVAL_TO = 1001;
   
 
 
 
   public static boolean isBinaryInSQL(int type)
   {
     switch (type) {
     case -7: 
     case 2004: 
       return true;
     }
     return false;
   }
   
   public static boolean isTextInSQL(int type)
   {
     switch (type)
     {
 
     case -16: 
     case -15: 
     case -9: 
     case -1: 
     case 1: 
     case 12: 
     case 1111: 
     case 2005: 
       return true; }
     
     return false;
   }
   
   public static boolean isNumbericInSQL(int type)
   {
     switch (type) {
     case -6: 
     case -5: 
     case 2: 
     case 3: 
     case 4: 
     case 5: 
     case 6: 
     case 7: 
     case 8: 
       return true;
     }
     return false;
   }
   
   public static boolean isDateInSQL(int type)
   {
     switch (type)
     {
     case 91: 
     case 92: 
     case 93: 
       return true;
     }
     return false;
   }
   
   public static boolean isDateTimeSQL(int type)
   {
     return type == 93;
   }
   
   public static boolean isTimeSQL(int type)
   {
     return type == 92;
   }
   
   public static boolean isOtherTypeInSQL(int type) {
     if ((isTextInSQL(type)) || (isNumbericInSQL(type)) || (isDateInSQL(type))) {
       return false;
     }
     
     return true;
   }
   
   public static boolean isGenericSameType(int type1, int type2) {
     if ((type1 == type2) || ((isTextInSQL(type1)) && (isTextInSQL(type2))) || ((isNumbericInSQL(type1)) && (isNumbericInSQL(type2))) || (
       (isDateInSQL(type1)) && (isDateInSQL(type2)))) {
       return true;
     }
     
     return false;
   }
   
   public static int getJavaTypeBySqlType(String sqlType)
   {
     if ("DATE".equalsIgnoreCase(sqlType.trim()))
       return 91;
     if (("BIGINT".equalsIgnoreCase(sqlType.trim())) || ("LONG".equalsIgnoreCase(sqlType.trim())))
       return -5;
     if ("INTEGER".equalsIgnoreCase(sqlType.trim()))
       return 4;
     if ("SMALLINT".equalsIgnoreCase(sqlType.trim()))
       return 5;
     if ("FLOAT".equalsIgnoreCase(sqlType.trim()))
       return 6;
     if ("CHAR".equalsIgnoreCase(sqlType.trim()))
       return 1;
     if ("VARCHAR".equalsIgnoreCase(sqlType.trim()))
       return 12;
     if ("DECIMAL".equalsIgnoreCase(sqlType.trim()))
       return 3;
     if ("TIME".equalsIgnoreCase(sqlType.trim()))
       return 92;
     if ("TIMESTMP".equalsIgnoreCase(sqlType.trim()))
       return 93;
     if ("TIMESTAMP".equalsIgnoreCase(sqlType.trim()))
       return 93;
     if ("BLOB".equalsIgnoreCase(sqlType.trim()))
       return 2004;
     if ("CLOB".equalsIgnoreCase(sqlType.trim()))
       return 2005;
     if ("DISTINCT".equalsIgnoreCase(sqlType.trim()))
       return 2001;
     if ("DOUBLE".equalsIgnoreCase(sqlType.trim()))
       return 8;
     if ("LONGVAR".equalsIgnoreCase(sqlType.trim()))
       return -1;
     if ("LONGVARCHAR".equalsIgnoreCase(sqlType.trim()))
       return -1;
     if ("REAL".equalsIgnoreCase(sqlType.trim())) {
       return 7;
     }
     return 0;
   }
   
 
 
 
 
 
 
 
 
 
   public static int getTeradataJavaTypeBySqlTypeAsInt(String sqlType)
   {
     if (sqlType.trim().equals("DA"))
       return 91;
     if (sqlType.trim().equals("I"))
       return 4;
     if ((sqlType.trim().equals("I2")) || (sqlType.trim().equals("I1")))
       return 5;
     if (sqlType.trim().equals("F"))
       return 6;
     if ((sqlType.trim().equals("CF")) || (sqlType.trim().equals("BF")))
       return 1;
     if ((sqlType.trim().equals("CV")) || (sqlType.trim().equals("BV")))
       return 12;
     if (sqlType.trim().equals("D"))
       return 3;
     if ((sqlType.trim().equals("TS")) || (sqlType.trim().equals("SZ")))
       return 93;
     if (sqlType.trim().equals("BO"))
       return 2004;
     if (sqlType.trim().equals("CO"))
       return 2005;
     if ((sqlType.trim().equals("YR")) || (sqlType.trim().equals("MO")) || (sqlType.trim().equals("DY")) || 
       (sqlType.trim().equals("HR")) || (sqlType.trim().equals("SC")) || (sqlType.trim().equals("MI")))
     {
       return 7; }
     if ((sqlType.trim().equals("YM")) || (sqlType.trim().equals("DM")) || (sqlType.trim().equals("DH")) || 
       (sqlType.trim().equals("DS")) || (sqlType.trim().equals("HM")) || (sqlType.trim().equals("HS")) || (sqlType.trim().equals("MS")))
     {
       return -9;
     }
     return 0;
   }
   
 
 
 
 
 
   public static String getTeradataJavaTypeBySqlTypeAsString(String sqlType)
   {
     if (sqlType.trim().equals("DA"))
       return "DATE";
     if (sqlType.trim().equals("I"))
       return "INTEGER";
     if ((sqlType.trim().equals("I2")) || (sqlType.trim().equals("I1")))
       return "SMALLINT";
     if (sqlType.trim().equals("F"))
       return "FLOAT";
     if ((sqlType.trim().equals("CF")) || (sqlType.trim().equals("BF")))
       return "CHAR";
     if ((sqlType.trim().equals("CV")) || (sqlType.trim().equals("BV")))
       return "VARCHAR";
     if (sqlType.trim().equals("D"))
       return "DECIMAL";
     if ((sqlType.trim().equals("TS")) || (sqlType.trim().equals("SZ")))
       return "TIMESTAMP";
     if (sqlType.trim().equals("BO"))
       return "BLOB";
     if (sqlType.trim().equals("CO"))
       return "CLOB";
     if (sqlType.trim().equals("YR"))
     {
       return "INTERVAL YEAR"; }
     if (sqlType.trim().equals("DH"))
       return "INTERVAL DAY TO HOUR";
     if (sqlType.trim().equals("DM"))
       return "INTERVAL DAY TO MINUTE";
     if (sqlType.trim().equals("DS"))
       return "INTERVAL DAY TO SECOND";
     if (sqlType.trim().equals("DY"))
       return "INTERVAL DAY";
     if (sqlType.trim().equals("HM"))
       return "INTERVAL HOUR TO MINUTE";
     if (sqlType.trim().equals("HR"))
       return "INTERVAL HOUR";
     if (sqlType.trim().equals("HS"))
       return "INTERVAL HOUR TO SECOND";
     if (sqlType.trim().equals("MI"))
       return "INTERVAL MINUTE";
     if (sqlType.trim().equals("MO"))
       return "INTERVAL MONTH";
     if (sqlType.trim().equals("MS"))
       return "INTERVAL MINUTE TO SECOND";
     if (sqlType.trim().equals("SC"))
       return "INTERVAL SECOND";
     if (sqlType.trim().equals("YM")) {
       return "INTERVAL YEAR TO MONTH";
     }
     return "";
   }
   
 
 
 
 
 
   public static int isTeradataIntervalType(String typeName)
   {
     if ((typeName != null) && (typeName.startsWith("INTERVAL"))) {
       if (typeName.contains("TO")) {
         return 1001;
       }
       return 1000;
     }
     return 0;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\sql\Java2SqlType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */