package org.talend.utils.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CheckConnectionHelper {
	public static void connect(String login, String passwd, String url)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName(Constants.getDriverFromUrl(url)).newInstance();
		Connection connection = DriverManager.getConnection(url, login, passwd);
		connection.close();
	}

	public static Connection getConnection(String login, String passwd, String url)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName(Constants.getDriverFromUrl(url)).newInstance();
		return DriverManager.getConnection(url, login, passwd);
	}
}
