package org.talend.utils.database;

public class Constants {
	public static final String JDBC_MYSQL_URL = "jdbc:mysql://";

	public static final String JDBC_MSSQL_URL = "jdbc:jtds:sqlserver://";

	public static final String JDBC_ORACLE_URL = "jdbc:oracle:thin:";

	public static final String JDBC_H2_URL = "jdbc:h2";

	public static final String JDBC_POSTGRESQL_URL = "jdbc:postgresql";

	public static final String JDBC_MYSQL_DRIVER = "org.gjt.mm.mysql.Driver";

	public static final String JDBC_MSSQL_DRIVER = "net.sourceforge.jtds.jdbc.Driver";

	public static final String JDBC_ORACLE_DRIVER = "oracle.jdbc.OracleDriver";

	public static final String JDBC_H2_DRIVER = "org.h2.Driver";

	public static final String JDBC_POSTGRESQL_DRIVER = "org.postgresql.Driver";

	public static final String JDBC_MARIADB_URL = "jdbc:mariadb://";

	public static final String JDBC_MARIADB_DRIVER = "org.mariadb.jdbc.Driver";

	public static String getDriverFromUrl(String url)
	{
		")) {
		return "org.gjt.mm.mysql.Driver";
	}")) {return"net.sourceforge.jtds.jdbc.Driver";}if(url.startsWith("jdbc:oracle:thin:")){return"oracle.jdbc.OracleDriver";}if(url.startsWith("jdbc:h2")){return"org.h2.Driver";}if(url.startsWith("jdbc:postgresql")){return"org.postgresql.Driver";}"))

	{return"org.mariadb.jdbc.Driver";}throw new IllegalStateException("unknown url type");
}}
