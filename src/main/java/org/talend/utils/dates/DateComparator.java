package org.talend.utils.dates;

import java.util.Comparator;
import java.util.Date;

public class DateComparator<T>
implements Comparator<T>
{
	public int compare(T date1, T date2)
	{
		Date firstDate = (Date)date1;
		Date secondDate = (Date)date2;
		return firstDate.compareTo(secondDate);
	}
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\dates\DateComparator.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */