package org.talend.utils.dates;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import org.apache.log4j.Logger;

public final class DateUtils
{
	protected static Logger log = Logger.getLogger(DateUtils.class);



	yyyy";



	public static final String PATTERN_2 = "yyyy-MM-dd hh:mm:ss";



	public static final String PATTERN_3 = "yyyy-MM-dd";



	yyyy HH:mm";


	public static final String PATTERN_5 = "yyyy-MM-dd HH:mm:ss";


	public static final String PATTERN_6 = "yyyyMMddHHmmss";


	public static final String PATTERN_7 = "hh:mm:ss";


	public static final String UTC = "UTC";



	public static Date parse(String pattern, String dateText)
			throws ParseException
	{
		Date date = null;
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		date = sdf.parse(dateText);
		return date;
	}






	public static String getCurrentDate(String pattern)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(new Date());
	}








	public static String formatTimeStamp(String pattern, long date)
	{
		if ((pattern == null) || (pattern.length() == 0)) {
			pattern = "yyyyMMddHHmmss";
		}
		Calendar nowDate = new GregorianCalendar();
		nowDate.setTimeInMillis(date);
		DateFormat df = new SimpleDateFormat(pattern);
		return df.format(nowDate.getTime());
	}






	public static Date convert2StandardTime(Date localDate)
	{
		Date result = localDate;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		String dateStr = dateFormat.format(localDate);
		try {
			dateFormat.setTimeZone(TimeZone.getDefault());
			result = dateFormat.parse(dateStr);
		} catch (ParseException e) {
			log.error(e);
		}
		return result;
	}






	public static Date getDateWithoutTime(Date utcExecutionDate)
	{
		Date calendarDate = utcExecutionDate;
		try {
			calendarDate = parse("yyyy-MM-dd", formatTimeStamp("yyyy-MM-dd", calendarDate.getTime()));
		} catch (ParseException e) {
			log.warn(e);
		}
		return calendarDate;
	}


	public static DateFormat createDateFormater(int dataType)
	{
		SimpleDateFormat ret;

		SimpleDateFormat ret;

		SimpleDateFormat ret;

		switch (dataType) {
		case 92: 
			ret = new SimpleDateFormat("HH:mm:ss");
			break;
		case 93: 
			ret = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			break;
		case 91: 
		default: 
			ret = new SimpleDateFormat("yyyy-MM-dd");
		}

		return ret;
	}
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\dates\DateUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */