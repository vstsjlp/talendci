package org.talend.utils.dates;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class ElapsedTime {
	public static final int GREGORIAN_CAL_YEAR = 1582;
	public static final int GREGORIAN_CAL_MONTH = 10;
	public static final int GREGORIAN_CAL_DAY = 15;
	public static final double TROPICAL_YEAR_LENGTH = 365.25D;
	private static final Calendar TMP_GREG_CAL_1 = new GregorianCalendar();

	private static final Calendar TMP_GREG_CAL_2 = new GregorianCalendar();

	private static final int NB_MILLIS_PER_MINUTE = 60000;

	private static final int NB_MILLIS_PER_SECOND = 1000;

	public static long getNbDays(Date first, Date last) {
		Calendar c1 = TMP_GREG_CAL_1;
		Calendar c2 = TMP_GREG_CAL_2;

		c1.setTime(last);
		c2.setTime(first);

		long j1 = getJulianDays(c1.get(1), c1.get(2) + 1, c1.get(5));
		long j2 = getJulianDays(c2.get(1), c2.get(2) + 1, c2.get(5));

		long daysElapsed = j1 - j2;
		return daysElapsed;
	}

	public static long getNbMinutes(Date first, Date last)
	{
		60000L;
		return min;
	}

	public static long getNbSeconds(Date first, Date last)
	{
		1000L;
		return min;
	}

	public static long getJulianDays(int year, int month, int day)
			throws IllegalArgumentException
	{
		if ((year < 1582) || ((year == 1582) && (month < 10)) || (
				(year == 1582) && (month == 10) && (day < 15))) {
			" + month + 
			" + day);
		}


		int y = year;
		int m = month;
		if ((m == 1) || (m == 2)) {
			y--;
			m += 12;
		}

		100;
		4;
		int c = 2 - a + b;

		double e = 365.25D * (y + 4716);
		double e1 = Math.floor(e);
		long e2 = Math.round(e1);

		double f = 30.6001D * (m + 1);
		double f1 = Math.floor(f);
		long f2 = Math.round(f1);



		long julianDay = c + day + e2 + f2 - 1524L;

		return julianDay;
	}

	public static int fieldDifference(Calendar start, Date endTime, int field) {
		Date startTime = start.getTime();
		if (!startTime.before(endTime)) {
			return 0;
		}
		for (int i = 1;; i++) {
			start.add(field, 1);
			if (start.getTime().after(endTime)) {
				start.add(field, -1);
				return i - 1;
			}
		}
	}
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\utils\dates\ElapsedTime.class*

Java compiler version:7(51.0)*JD-
Core Version:0.7.1*/