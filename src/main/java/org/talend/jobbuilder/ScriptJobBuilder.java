package org.talend.jobbuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.maven.model.Model;
import org.apache.maven.plugin.AbstractMojoExecutionException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.talend.commandline.client.command.CommandStatus.CommandStatusLevel;
import org.talend.commandline.client.command.CreateProjectCommand;
import org.talend.commandline.client.command.IJavaCommand;
import org.talend.commandline.client.command.InitLocalCommand;
import org.talend.commandline.client.command.LogoffProjectCommand;
import org.talend.commandline.client.command.LogonProjectCommand;
import org.talend.commandline.client.command.extension.BuildProjectSourcesCommand;
import org.talend.jobbuilder.model.CommandStatus;
import org.talend.jobbuilder.model.TalendProject;
import org.talend.utils.io.FilesUtils;

@Mojo(name = "local-generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class ScriptJobBuilder extends AbstractJobBuilder {
	@Parameter(required = true, property = "product.path")
	private String productPath;
	@Parameter(required = false, property = "launcher.path", defaultValue = "${product.path}/plugins/org.eclipse.equinox.launcher_1.3.0.v20140415-2008.jar")
	private String launcherPath;
	@Parameter(required = false, property = "jvm.arguments", defaultValue = "-ea -Xms512m -Dfile.encoding=UTF-8 -Xmx2000m -XX:+CMSClassUnloadingEnabled -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=20 -XX:+HeapDumpOnOutOfMemoryError")
	private String jvmArguments;
	private final String LOG_ERROR = "ERROR";
	private final String LOG_OUTPUT = "OUTPUT";
	private final String LINE;
	private File scriptFile;
	private File statusFile;
	private FileWriter logWriter;
	private String scriptExecCommand;

	public ScriptJobBuilder() {
		this.LOG_ERROR = "ERROR";

		this.LOG_OUTPUT = "OUTPUT";

		this.LINE = System.getProperty("line.separator");
	}

	protected void executeCommandline(Model aggregatorPom) throws MojoExecutionException, MojoFailureException {
		Map<File, File> backupFilesMap = new HashMap();
		for (Iterator localIterator = this.talendProjectList.iterator(); localIterator.hasNext();) {
			project = (TalendProject) localIterator.next();
			File projectFolder = new File(this.commandlineWorkspace, project.getName());
			File projectFile = new File(projectFolder, ".project");

			File talendProjectFile = new File(projectFolder, "talend.project");
			File backupFile = new File(projectFolder, "talend.project.bak");
			try {
				FilesUtils.copyFile(talendProjectFile, backupFile);
				backupFilesMap.put(talendProjectFile, backupFile);
			} catch (IOException e) {
				throw new MojoFailureException("Can't backup the talend.project for: " + project.getName(), e);
			}

			projectFile.delete();
		}

		getLog().info("");
		getLog().info("Creating the detected projects for Commandline workspace.");

		Object commands = new ArrayList();
		((List) commands).add(new InitLocalCommand());
		for (TalendProject project : this.talendProjectList) {
			((List) commands)
					.add(new CreateProjectCommand(project.getName(), "from CI Builder", "java", this.commandlineUser));
		}

		((List) commands).add(new LogoffProjectCommand());
		generateScriptAndRun((List) commands);

		for (TalendProject project = backupFilesMap.keySet().iterator(); project.hasNext();) {
			talendProjectFile = (File) project.next();
			File backupFile = (File) backupFilesMap.get(talendProjectFile);
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(backupFile);
				FilesUtils.copyFile(fis, talendProjectFile);
			} catch (IOException e) {
				throw new MojoFailureException("Can't revert the talend.project in: " + talendProjectFile, e);
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
						getLog().error(e);
					}
				}
				if (backupFile.exists()) {
					backupFile.delete();
				}
			}
		}

		File talendProjectFile;
		getLog().info("");
		getLog().info("*****************************************************");
		getLog().info("Starting to generate sources for the detected projects");

		File sourceJavaFiles = new File(this.commandlineWorkspace, ".Java");
		for (TalendProject project : this.talendProjectList) {
			getLog().info("");
			getLog().info("Preparing for the project: " + project.getName());
			cleanResources(sourceJavaFiles);

			getLog().info(" Generating sources...");
			((List) commands).add(new InitLocalCommand());
			((List) commands).add(new LogonProjectCommand(project.getName(), this.commandlineUser, null, true));
			if ((this.deployVersion != null) && (this.deployVersion.trim().equals(""))) {
				this.deployVersion = null;
			}
			((List) commands).add(new BuildProjectSourcesCommand(this.itemFilter, this.deployVersion));
			((List) commands).add(new LogoffProjectCommand());
			generateScriptAndRun((List) commands);
			getLog().info(" Logoff project");

			copyResources(project, sourceJavaFiles, aggregatorPom);
		}
		getLog().info("");
		getLog().info("Finished to generate sources for all detected projects");
		getLog().info("*****************************************************");
	}

	private void generateScriptAndRun(List<IJavaCommand> commands) throws MojoFailureException, MojoExecutionException {
		try {
			initLogAndStatusFile();
			generateScriptFile(commands);
			String execCommand = getScriptExecCommand();
			new RuntimeExecutor().exec(execCommand, System.getProperty("java.io.tmpdir"));
			waitForExecutionDone();
			commands.clear();
			return;
		} catch (MojoFailureException | MojoExecutionException e) {
			throw e;
		} finally {
			try {
				if (this.logWriter != null) {
					this.logWriter.close();
				}
			} catch (IOException e) {
				getLog().error(e);
			}
		}
	}

	private void waitForExecutionDone() throws MojoFailureException, MojoExecutionException {
		for (;;) {
			CommandStatus status = getLastCommandStatus();
			if (status != null) {
				if ((CommandStatus.CommandStatusLevel.COMPLETED.name().equals(status.getLevel()))
						&& (LogoffProjectCommand.class.getSimpleName().equals(status.getCommandName())))
					break;
				if (CommandStatus.CommandStatusLevel.FAILED.name().equals(status.getLevel())) {
					throw new MojoExecutionException("Failed to execute [" + status.getCommandName()
							+ "], check ci-builder.log for more details");
				}
			}
			try {
				Thread.sleep(300L);
			} catch (InterruptedException e) {
				getLog().error(e);
			}
		}
	}

	private String getScriptExecCommand() throws MojoFailureException {
		if (this.scriptExecCommand == null) {
			StringBuilder scriptBuilder = new StringBuilder();
			scriptBuilder.append("java ");
			scriptBuilder.append(this.jvmArguments);

			scriptBuilder.append(" -classpath ");
			scriptBuilder.append(this.launcherPath);
			scriptBuilder.append(" org.eclipse.core.launcher.Main -application org.talend.commandline.CommandLine");
			scriptBuilder.append(" -data ");
			scriptBuilder.append(this.commandlineWorkspace);
			scriptBuilder.append(" scriptFile ");
			scriptBuilder.append(this.scriptFile.getName());
			this.scriptExecCommand = scriptBuilder.toString();
		}
		return this.scriptExecCommand;
	}

	private void generateScriptFile(List<IJavaCommand> commands) throws MojoFailureException {
		FileWriter writer = null;
		try {
			if (this.scriptFile == null) {
				this.scriptFile = File.createTempFile("script_command_", ".txt");
			}
			StringBuilder commandsBuilder = new StringBuilder();
			for (IJavaCommand command : commands) {
				commandsBuilder.append(command.writeToString());
				commandsBuilder.append(this.LINE);
			}
			if (commandsBuilder.length() > 0) {
				writer = new FileWriter(this.scriptFile);
				writer.write(commandsBuilder.toString());
			}
			return;
		} catch (IOException e) {
			throw new MojoFailureException("Failed to generate script command file.");
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				getLog().error(e);
			}
		}
	}

	private void initLogAndStatusFile() throws MojoFailureException {
		try {
			ci-builder.log");
			if (!logFile.exists()) {
				logFile.createNewFile();
			}
			if (logFile.length() > 10485760L) {
				logFile.delete();
				logFile.createNewFile();
			}
			this.logWriter = new FileWriter(logFile, true);
		}
		catch (IOException e) {
			throw new MojoFailureException("Failed to init log file.", e);
		}

		status.log");
		resetStatusFile();
	}

	private CommandStatus getLastCommandStatus() throws MojoFailureException {
		List<CommandStatus> list = loadStatusFromFile();
		if (!list.isEmpty()) {
			return (CommandStatus) list.get(list.size() - 1);
		}
		return null;
	}

	private List<CommandStatus> loadStatusFromFile() throws MojoFailureException {
		statusList = new ArrayList();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(this.statusFile));
			String line = null;
			while ((line = reader.readLine()) != null) {
				statusList.add(parseStatus(line));
			}

			return statusList;
		} catch (IOException e) {
			throw new MojoFailureException("Failed to load status from file.", e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				getLog().error(e);
			}
		}
	}

	private CommandStatus parseStatus(String line) {
		CommandStatus status = new CommandStatus();

		String[] lines1 = line.split(":");
		if ((lines1 != null) && (lines1.length > 1)) {
			status.setId(Integer.valueOf(lines1[0]).intValue());
			String[] lines2 = lines1[1].split(" ");
			if ((lines1 != null) && (lines1.length > 1)) {
				status.setLevel(lines2[0]);
				status.setCommandName(lines2[1]);
			}
		}
		return status;
	}

	private void resetStatusFile() throws MojoFailureException {
		try {
			if (this.statusFile.exists()) {
				this.statusFile.delete();
			}
			this.statusFile.createNewFile();
		} catch (IOException e) {
			throw new MojoFailureException("Failed to reset status file.", e);
		}
	}

	class RuntimeExecutor {
		RuntimeExecutor() {
		}

		public void exec(String command, String execDir) { if ((command == null) || (command.trim().equals(""))) {
			return;
		}
		try {
			String osName = System.getProperty("os.name");
			String[] cmd = new String[3];
			if (osName.startsWith("Windows")) {
				cmd[0] = "cmd.exe";
				C";
				cmd[2] = command;
			} else { if (osName.startsWith("Mac"))
			{
				throw new UnsupportedOperationException("Runtime for Mac not implemented yet");
			}
			bash";
			cmd[1] = "-c";
			cmd[2] = command;
			}
			Runtime runtime = Runtime.getRuntime();

			Process process = runtime.exec(cmd, null, new File(execDir));

			ScriptJobBuilder.StreamGobbler errorGobbler = new ScriptJobBuilder.StreamGobbler(ScriptJobBuilder.this, process.getErrorStream(), "ERROR");

			ScriptJobBuilder.StreamGobbler outputGobbler = new ScriptJobBuilder.StreamGobbler(ScriptJobBuilder.this, process.getInputStream(), "OUTPUT");

			errorGobbler.start();
			outputGobbler.start();

			int i = process.waitFor();
		}
		catch (Exception e) {
			ScriptJobBuilder.this.getLog().error(e);
		}
		}
	}

	class StreamGobbler extends Thread {
		private InputStream in;
		private String type;
		private Long markTime;

		StreamGobbler(InputStream in, String type) {
			this.in = in;
			this.type = type;
		}

		public void run() {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(this.in));
				String line = null;
				while ((line = br.readLine()) != null) {
					handleRuntimeExceptions(line);
					switch (this.type) {
					case "OUTPUT":
						ScriptJobBuilder.this.getLog().info(line.replace("[INFO]", ""));
						break;
					case "ERROR":
						ScriptJobBuilder.this.getLog().error(line.replace("[ERROR]", ""));
					}

					ScriptJobBuilder.this.logWriter.write(this.type + ">" + line + ScriptJobBuilder.this.LINE);
					ScriptJobBuilder.this.logWriter.flush();
				}
			} catch (IOException e) {
				ScriptJobBuilder.this.getLog().error(e);
			}
		}

		public void handleRuntimeExceptions(String line) {
			String ERR_LAUNCHER = "Error: Could not find or load main class org.eclipse.core.launcher.Main";

			String ERR_LICENSE = "Fetch License From Administrator!";

			if (this.markTime == null) {
				switch (line) {
				case "Fetch License From Administrator!":
					ScriptJobBuilder.this.getLog().error("Please set up license for commandline first!");
					System.exit(1);
				case "Error: Could not find or load main class org.eclipse.core.launcher.Main":
					this.markTime = Long.valueOf(System.currentTimeMillis());
				}
				if (this.markTime != null) {

					new Thread() {
						public void run() {
							for (;;) {
								if (System.currentTimeMillis()
										- ScriptJobBuilder.StreamGobbler.this.markTime.longValue() > 2000L) {

									System.exit(1);
								}
							}
						}
					}.start();
				}
			}
		}
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\jobbuilder\ScriptJobBuilder.class Java compiler version: 7 (51.0)
 * JD-Core Version: 0.7.1
 */