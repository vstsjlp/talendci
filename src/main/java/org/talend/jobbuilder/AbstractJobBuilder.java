package org.talend.jobbuilder;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import org.apache.maven.model.Build;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Parameter;
import org.talend.jobbuilder.model.TalendProject;
import org.talend.utils.io.FilesUtils;

public abstract class AbstractJobBuilder extends AbstractMojo {
	@Parameter(required = true, property = "commandline.workspace")
	protected String commandlineWorkspace;
	@Parameter(required = false, property = "commandline.user", defaultValue = "jobbuilder@talend.com")
	protected String commandlineUser;
	@Parameter(required = false, property = "itemFilter")
	protected String itemFilter;
	@Parameter(required = false, property = "projectSources")
	protected String projectsTargetDirectory;
	@Parameter(required = false, property = "deploy.version")
	protected String deployVersion;
	protected List<TalendProject> talendProjectList = new ArrayList();

	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().info("");
		getLog().info("*****************************************************");

		initializeProjectList();

		FilesUtils.removeFolder(this.projectsTargetDirectory, true);
		FilesUtils.createFoldersIfNotExists(this.projectsTargetDirectory);

		Model aggregatorPom = new Model();

		executeCommandline(aggregatorPom);

		createAggregatePom(aggregatorPom);
	}

	private void initializeProjectList() throws MojoExecutionException {
		getLog().info("List projects from:" + this.commandlineWorkspace);
		File workspace = new File(this.commandlineWorkspace);
		if ((workspace == null) || (!workspace.exists())) {
			throw new MojoExecutionException("Invalid or not existing workspace directory");
		}
		File[] files = workspace.listFiles();
		Arrays.sort(files, new Comparator() {
			public int compare(File f1, File f2) {
				return f1.getName().compareTo(f2.getName());
			}
		});
		for (File file : files) {
			if (file.isDirectory()) {
				File talendProjects = findTalendProjects(file);

				if (talendProjects == null) {
					getLog().info("  Not valid folder (no talend project) in:" + file.getName());
				}
			}
		}
	}

	private File findTalendProjects(File parentFolder) {
		File talendProjectFile = new File(parentFolder, "talend.project");
		if ((talendProjectFile.isFile()) && (talendProjectFile.exists())) {
			this.talendProjectList.add(new TalendProject(parentFolder.getName(), null));
			getLog().info("  Found project:" + parentFolder.getName());
			return talendProjectFile;
		}
		return null;
	}

	protected abstract void executeCommandline(Model paramModel) throws MojoExecutionException, MojoFailureException;

	protected void cleanResources(File sourceJavaFiles) {
		List<String> cleanList = new ArrayList();
		cleanList.add("lib");
		cleanList.add("src");
		cleanList.add("target");
		cleanList.add("tests");
		for (String folder : cleanList) {
			FilesUtils.deleteFolder(new File(sourceJavaFiles, folder), false);
		}
	}

	protected void copyResources(TalendProject project, final File sourceJavaFiles, Model aggregatorPom)
			throws MojoExecutionException, MojoFailureException
	{
		if (!sourceJavaFiles.exists()) {
			throw new MojoFailureException(".Java Sources folder is not exist, please check commandline workspace configuration.");
		}
		"), "assemblies");
		if ((!assembliesFolder.exists()) || (assembliesFolder.listFiles() == null) || (assembliesFolder.listFiles().length == 0)) {
			getLog().warn("There's no job in current project!");
		}
		String projectFolderName;
		String projectFolderName;
		if (project.getBranch() != null) {
			", "_");
		} else {
			projectFolderName = project.getName();
		}
		File targetDirectory = new File(this.projectsTargetDirectory, projectFolderName);
		try
		{
			FilesUtils.copyFolder(sourceJavaFiles, targetDirectory, false, new FileFilter()















					new FileFilter
					{

				public boolean accept(File folder)
				{
					if (folder.isDirectory()) {
						Path relativizePath = sourceJavaFiles.toPath().relativize(folder.toPath());
						String relativePath = relativizePath.toString();

						if ((relativePath.equals("lib")) || 
								(relativePath.equals("target")) || 
								(relativePath.equals(".settings")) || 
								(relativePath.equals("tests")))
						{
							return false;
						}
					}
					return true; } }, new FileFilter()
					{

						public boolean accept(File file)
						{

							if (file.isFile()) {
								Path relativizePath = sourceJavaFiles.toPath().relativize(file.toPath());
								String relativePath = relativizePath.toString();

								if ((relativePath.equals(".classpath")) || (relativePath.equals(".project"))) {
									return false;
								}
							}
							return true; } }, true);
		}catch(

				IOException e)
		{
			throw new MojoExecutionException(
					"Can't copy the .Java sources to target folder: " + this.projectsTargetDirectory, e);
		}

		getLog().info(" Successfully generated the sources to:");
		getLog().info("  " + targetDirectory.getAbsolutePath());


		aggregatorPom.addModule(projectFolderName);
	}

	private void createAggregatePom(Model aggregatorPom) throws MojoExecutionException, MojoFailureException {
		getLog().info("");

		aggregatorPom.setGroupId("org.talend.master");
		aggregatorPom.setArtifactId("aggregator");
		aggregatorPom.setVersion(this.deployVersion == null ? "6.0.0" : this.deployVersion);
		aggregatorPom.setPackaging("pom");
		aggregatorPom.setModelVersion("4.0.0");
		aggregatorPom.setBuild(new Build());
		File pomFile = new File(this.projectsTargetDirectory, "pom.xml");

		getLog().info("Create aggregator pom for all projects in:");
		getLog().info("  " + pomFile.getAbsolutePath());

		FileOutputStream fos = null;
		try {
			pomFile.createNewFile();
			fos = new FileOutputStream(pomFile);
			MavenXpp3Writer writer = new MavenXpp3Writer();
			writer.write(fos, aggregatorPom);
			return;
		} catch (IOException e) {
			throw new MojoFailureException(
					"Failed to create root sources pom of projects in  " + this.projectsTargetDirectory, e);
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					getLog().error(e);
				}
			}
		}
	}
}
