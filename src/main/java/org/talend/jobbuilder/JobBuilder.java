package org.talend.jobbuilder;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.maven.model.Model;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.talend.commandline.client.CommandLineJavaClient;
import org.talend.commandline.client.command.CommandStatus;
import org.talend.commandline.client.command.CreateProjectCommand;
import org.talend.commandline.client.command.IJavaCommand;
import org.talend.commandline.client.command.InitLocalCommand;
import org.talend.commandline.client.command.LogoffProjectCommand;
import org.talend.jobbuilder.model.TalendProject;
import org.talend.utils.io.FilesUtils;

@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class JobBuilder extends AbstractJobBuilder {
	@Parameter(required = false, property = "commandline.host", defaultValue = "localhost")
	private String commandlineHost;
	@Parameter(required = false, property = "commandline.port", defaultValue = "8002")
	private int commandlinePort;

	protected void executeCommandline(Model aggregatorPom)
			throws MojoExecutionException, MojoFailureException
	{
		     CommandLineJavaClient client = new CommandLineJavaClient(this.commandlineHost, this.commandlinePort);
		try {
			       client.connect();
		}
		catch (Exception e) {
			       throw new MojoFailureException("Can't connect to Commandline server : " + client.getHostname() + " with port: " + client.getPort(), e);
		}


		     addCommandAndCheck(client, new InitLocalCommand());


		     getLog().info("");
		     getLog().info("Creating the detected projects for Commandline workspace.");

		     Map<File, File> backupFilesMap = new HashMap();
		     for (TalendProject project : this.talendProjectList) {
			       File projectFolder = new File(this.commandlineWorkspace, project.getName());
			       File projectFile = new File(projectFolder, ".project");


			       File talendProjectFile = new File(projectFolder, "talend.project");
			       File backupFile = new File(projectFolder, "talend.project.bak");
			try {
				         FilesUtils.copyFile(talendProjectFile, backupFile);
				         backupFilesMap.put(talendProjectFile, backupFile);
			} catch (IOException e) {
				         throw new MojoFailureException("Can't backup the talend.project for: " + project.getName(), e);
			}



			       projectFile.delete();
			       addCommandAndCheck(client, new CreateProjectCommand(project.getName(), "from CI Builder", "java", this.commandlineUser));
			       getLog().info("  Created project (base on existing folder): " + project.getName());
		}



		     addCommandAndCheck(client, new LogoffProjectCommand());

		     for (??? = backupFilesMap.keySet().iterator(); ???.hasNext();) { talendProjectFile = (File)???.next();
		       File backupFile = (File)backupFilesMap.get(talendProjectFile);
		       FileInputStream fis = null;
		try {
			         fis = new FileInputStream(backupFile);
			         FilesUtils.copyFile(fis, talendProjectFile);
		} catch (IOException e) {
			         throw new MojoFailureException("Can't revert the talend.project in: " + talendProjectFile, e);
		} finally {
			         if (fis != null) {
				try {
					             fis.close();
				} catch (IOException e) {
					             getLog().error(e);
				}
			}
			         if (backupFile.exists()) {
				           backupFile.delete();
			}
		}
		}

		File talendProjectFile;
		     getLog().info("");
		     getLog().info("*****************************************************");
		     getLog().info("Starting to generate sources for the detected projects");

		     File sourceJavaFiles = new File(this.commandlineWorkspace, ".Java");
		     for (TalendProject project : this.talendProjectList) {
			       getLog().info("");
			       getLog().info("Preparing for the project: " + project.getName());
			       cleanResources(sourceJavaFiles);

			       getLog().info(" Logon project");
			       addCommandAndCheck(client, new LogonProjectCommand(project.getName(), this.commandlineUser, null, true));

			       getLog().info(" Generating sources...");
			       if ((this.deployVersion != null) && (this.deployVersion.trim().equals(""))) {
				         this.deployVersion = null;
			}
			       addCommandAndCheck(client, new BuildProjectSourcesCommand(this.itemFilter, this.deployVersion));

			       getLog().info(" Logoff project");
			       addCommandAndCheck(client, new LogoffProjectCommand());

			       copyResources(project, sourceJavaFiles, aggregatorPom);
		}

		     getLog().info("");
		     getLog().info("Finished to generate sources for all detected projects");
		     getLog().info("*****************************************************");
		try
		{
			       client.disconnect();
		}
		catch (IOException e)
		{
			       getLog().error(e);
		}
	}

	private void addCommandAndCheck(CommandLineJavaClient client, IJavaCommand command)
			throws MojoExecutionException, MojoFailureException {
		try {
			CommandStatus status = client.addCommandAndWait(command);
			if (status.getException() != null) {
				throw new MojoExecutionException("Have exceptions when execute the command: " + command.toString(),
						status.getException());
			}
		} catch (Exception e) {
			throw new MojoFailureException("Fail to execute command: " + command.toString(), e);
		}
	}
}
