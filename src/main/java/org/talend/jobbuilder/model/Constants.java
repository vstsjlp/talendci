package org.talend.jobbuilder.model;

public abstract interface Constants {
	public static final String JAVA_PROJECT = ".Java";
	public static final String FILE_TALEND_PROJECT = "talend.project";
	public static final String FILE_PROJECT = ".project";
	public static final String FILE_CLASSPATH = ".classpath";
	public static final String PATH_LIB = "lib";
	public static final String PATH_SRC = "src";
	public static final String PATH_TARGET = "target";
	public static final String PATH_TESTS = "tests";
	public static final String PATH_SETTINGS = ".settings";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\jobbuilder\model\Constants.class Java compiler version: 7 (51.0)
 * JD-Core Version: 0.7.1
 */