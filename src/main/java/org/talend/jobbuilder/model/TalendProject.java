package org.talend.jobbuilder.model;

public class TalendProject {
	private String name;

	private String branch;

	public TalendProject(String name, String branch) {
		this.name = name;
		this.branch = branch;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranch() {
		return this.branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\jobbuilder\model\TalendProject.class Java compiler version: 7 (51.0)
 * JD-Core Version: 0.7.1
 */