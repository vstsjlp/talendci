package org.talend.commandline.client;

public class CommandLineJavaCLientException extends Exception {
	private static final long serialVersionUID = 5188949845234836542L;

	public CommandLineJavaCLientException(String message) {
		super(message);
	}

	public CommandLineJavaCLientException(Throwable cause) {
		super(cause);
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\CommandLineJavaCLientException.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */