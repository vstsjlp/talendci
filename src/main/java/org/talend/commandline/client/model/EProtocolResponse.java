package org.talend.commandline.client.model;

public enum EProtocolResponse {
	BEGIN, END, ADDED_COMMAND, BAD_REQUEST, COMMAND_STATUS, LIST_COMMAND, STRING, FILE_MD5, FILE_CONTENT, FILE_SIZE;

	public String getName() {
		return name();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\model\EProtocolResponse.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */