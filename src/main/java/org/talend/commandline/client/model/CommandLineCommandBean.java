package org.talend.commandline.client.model;

import java.io.Serializable;
import org.talend.commandline.client.command.CommandStatus;

public class CommandLineCommandBean implements Serializable {
	private static final long serialVersionUID = -4913759612198578501L;
	private int id;
	private int groupId = -1;

	private String groupOriginInfors;

	private String commandName;

	private String command;

	private CommandStatus status;

	public CommandLineCommandBean(int id, String commandName, String command) {
		this.id = id;
		this.commandName = commandName;
		this.command = command;
	}

	public CommandLineCommandBean(int id, int groupId, String commandName, String command) {
		this.id = id;
		this.groupId = groupId;
		this.commandName = commandName;
		this.command = command;
	}

	public String getGroupOriginInfors() {
		return this.groupOriginInfors;
	}

	public void setGroupOriginInfors(String groupOriginInfors) {
		this.groupOriginInfors = groupOriginInfors;
	}

	public CommandStatus getStatus() {
		return this.status;
	}

	public void setStatus(CommandStatus status) {
		this.status = status;
	}

	public int getId() {
		return this.id;
	}

	public int getGroupId() {
		return this.groupId;
	}

	public String getCommandName() {
		return this.commandName;
	}

	public String getCommand() {
		return this.command;
	}

	public String toString() {
		return "CommandLineBean [id=" + this.id + ", groupId=" + this.groupId + ", commandName=" + this.commandName
				+ ", command=" + this.command + ", status=" + this.status + "]";
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\model\CommandLineCommandBean.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */