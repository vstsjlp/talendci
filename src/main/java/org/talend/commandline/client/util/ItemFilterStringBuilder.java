 package org.talend.commandline.client.util;
 
 import java.util.List;
 import org.talend.commandline.client.filter.ItemFilter;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class ItemFilterStringBuilder
 {
   public String writeGroup(List<ItemFilter> childs, String separator)
   {
     StringBuilder stringBuilder = new StringBuilder();
     boolean first = true;
     for (ItemFilter child : childs) {
       if (first) {
         first = false;
       } else {
         stringBuilder.append(separator);
       }
       stringBuilder.append('(');
       stringBuilder.append(child.writeToString());
       stringBuilder.append(')');
     }
     return stringBuilder.toString();
   }
   
   public String writeSimple(String property, char comparator, String value) {
     StringBuilder stringBuilder = new StringBuilder();
     stringBuilder.append(property);
     stringBuilder.append(comparator);
     stringBuilder.append(value);
     return stringBuilder.toString();
   }
   
   public String writeNot(ItemFilter itemFilter) {
     StringBuilder stringBuilder = new StringBuilder();
     stringBuilder.append("!");
     stringBuilder.append(itemFilter.writeToString());
     return stringBuilder.toString();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\util\ItemFilterStringBuilder.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */