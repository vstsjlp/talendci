package org.talend.commandline.client.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;

public class ReaderWriterHandler
 {
   protected InputStream inputStream;
   protected Writer writer;
   
   public ReaderWriterHandler(InputStream inputStream, Writer writer)
   {
     this.inputStream = inputStream;
     this.writer = writer;
   }
   
   public void readUntil(String pattern) throws IOException {
     int found = 0;
     do
     {
       byte[] bytes = new byte[1];
       int readInt = this.inputStream.read(bytes, 0, 1);
       char readChar = new String(bytes, "UTF8").charAt(0);
       
       if (readInt == -1) {
         this.writer.flush();
         throwException();
       } else {
         appendChar(readChar);
       }
       
       if (pattern.charAt(found) == readChar) {
         found++;
       } else {
         found = 0;
       }
       
     } while (found != pattern.length());
     this.writer.flush();
   }
   
   public void readUntil(char character) throws IOException
   {
     char readChar;
     do
     {
       byte[] bytes = new byte[1];
       int readInt = this.inputStream.read(bytes, 0, 1);
       readChar = new String(bytes, "UTF8").charAt(0);
       
       if (readInt == -1) {
         this.writer.flush();
         throwException();
       } else {
         appendChar(readChar);
       }
       
     } while (character != readChar);
     this.writer.flush();
   }
   
 
   public void appendChar(char readChar)
     throws IOException
   {
     this.writer.append(readChar);
   }
   
   public void throwException() throws IOException {
     throw new IOException("InputStream closed.");
   }
   
   public String getResult() {
     return this.writer.toString();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\util\ReaderWriterHandler.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */