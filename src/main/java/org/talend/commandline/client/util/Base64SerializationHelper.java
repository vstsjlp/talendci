package org.talend.commandline.client.util;

public class Base64SerializationHelper {
	/* Error */
	public Object decode(String string) throws java.io.IOException, java.lang.ClassNotFoundException {
		// Byte code:
		// 0: aload_1
		// 1: ldc 21
		// 3: invokevirtual 23 java/lang/String:getBytes (Ljava/lang/String;)[B
		// 6: invokestatic 29 org/apache/commons/codec/binary/Base64:decodeBase64 ([B)[B
		// 9: astore_2
		// 10: aconst_null
		// 11: astore_3
		// 12: aconst_null
		// 13: astore 4
		// 15: new 35 java/io/ObjectInputStream
		// 18: dup
		// 19: new 37 java/io/ByteArrayInputStream
		// 22: dup
		// 23: aload_2
		// 24: invokespecial 39 java/io/ByteArrayInputStream:<init> ([B)V
		// 27: invokespecial 42 java/io/ObjectInputStream:<init>
		// (Ljava/io/InputStream;)V
		// 30: astore_3
		// 31: aload_3
		// 32: invokevirtual 45 java/io/ObjectInputStream:readObject
		// ()Ljava/lang/Object;
		// 35: astore 4
		// 37: goto +21 -> 58
		// 40: astore 5
		// 42: aload 5
		// 44: athrow
		// 45: astore 6
		// 47: aload_3
		// 48: invokevirtual 49 java/io/ObjectInputStream:close ()V
		// 51: goto +4 -> 55
		// 54: pop
		// 55: aload 6
		// 57: athrow
		// 58: aload_3
		// 59: invokevirtual 49 java/io/ObjectInputStream:close ()V
		// 62: goto +4 -> 66
		// 65: pop
		// 66: aload 4
		// 68: areturn
		// Line number table:
		// Java source line #28 -> byte code offset #0
		// Java source line #29 -> byte code offset #10
		// Java source line #30 -> byte code offset #12
		// Java source line #32 -> byte code offset #15
		// Java source line #33 -> byte code offset #31
		// Java source line #34 -> byte code offset #37
		// Java source line #35 -> byte code offset #42
		// Java source line #36 -> byte code offset #45
		// Java source line #38 -> byte code offset #47
		// Java source line #39 -> byte code offset #51
		// Java source line #42 -> byte code offset #55
		// Java source line #38 -> byte code offset #58
		// Java source line #39 -> byte code offset #62
		// Java source line #44 -> byte code offset #66
		// Local variable table:
		// start length slot name signature
		// 0 69 0 this Base64SerializationHelper
		// 0 69 1 string String
		// 9 15 2 base64 byte[]
		// 11 48 3 objectInputStream java.io.ObjectInputStream
		// 13 54 4 object Object
		// 40 3 5 e java.io.IOException
		// 45 11 6 localObject1 Object
		// 54 1 7 localException1 Exception
		// 65 1 8 localException2 Exception
		// Exception table:
		// from to target type
		// 15 37 40 java/io/IOException
		// 15 45 45 finally
		// 47 51 54 java/lang/Exception
		// 58 62 65 java/lang/Exception
	}

	/* Error */
	public String encode(Object object) throws java.io.IOException {
		// Byte code:
		// 0: aconst_null
		// 1: astore_2
		// 2: new 70 java/io/ByteArrayOutputStream
		// 5: dup
		// 6: invokespecial 72 java/io/ByteArrayOutputStream:<init> ()V
		// 9: astore_2
		// 10: new 73 java/io/ObjectOutputStream
		// 13: dup
		// 14: aload_2
		// 15: invokespecial 75 java/io/ObjectOutputStream:<init>
		// (Ljava/io/OutputStream;)V
		// 18: aload_1
		// 19: invokevirtual 78 java/io/ObjectOutputStream:writeObject
		// (Ljava/lang/Object;)V
		// 22: aload_2
		// 23: invokevirtual 82 java/io/ByteArrayOutputStream:toByteArray ()[B
		// 26: invokestatic 86 org/apache/commons/codec/binary/Base64:encodeBase64
		// ([B)[B
		// 29: astore_3
		// 30: goto +21 -> 51
		// 33: astore 4
		// 35: aload 4
		// 37: athrow
		// 38: astore 5
		// 40: aload_2
		// 41: invokevirtual 89 java/io/ByteArrayOutputStream:close ()V
		// 44: goto +4 -> 48
		// 47: pop
		// 48: aload 5
		// 50: athrow
		// 51: aload_2
		// 52: invokevirtual 89 java/io/ByteArrayOutputStream:close ()V
		// 55: goto +4 -> 59
		// 58: pop
		// 59: new 24 java/lang/String
		// 62: dup
		// 63: aload_3
		// 64: ldc 21
		// 66: invokespecial 90 java/lang/String:<init> ([BLjava/lang/String;)V
		// 69: areturn
		// Line number table:
		// Java source line #48 -> byte code offset #0
		// Java source line #51 -> byte code offset #2
		// Java source line #52 -> byte code offset #10
		// Java source line #53 -> byte code offset #22
		// Java source line #54 -> byte code offset #30
		// Java source line #55 -> byte code offset #35
		// Java source line #56 -> byte code offset #38
		// Java source line #58 -> byte code offset #40
		// Java source line #59 -> byte code offset #44
		// Java source line #62 -> byte code offset #48
		// Java source line #58 -> byte code offset #51
		// Java source line #59 -> byte code offset #55
		// Java source line #64 -> byte code offset #59
		// Local variable table:
		// start length slot name signature
		// 0 70 0 this Base64SerializationHelper
		// 0 70 1 object Object
		// 1 51 2 byteArrayOutputStream java.io.ByteArrayOutputStream
		// 29 2 3 base64 byte[]
		// 51 13 3 base64 byte[]
		// 33 3 4 e java.io.IOException
		// 38 11 5 localObject Object
		// 47 1 7 localException1 Exception
		// 58 1 8 localException2 Exception
		// Exception table:
		// from to target type
		// 2 30 33 java/io/IOException
		// 2 38 38 finally
		// 40 44 47 java/lang/Exception
		// 51 55 58 java/lang/Exception
	}
}
