 package org.talend.commandline.client.util;
 
 import org.talend.commandline.client.filter.AndGroupFilter;
 import org.talend.commandline.client.filter.AuthorFilter;
 import org.talend.commandline.client.filter.EqualsLabelFilter;
 import org.talend.commandline.client.filter.EqualsPathFilter;
 import org.talend.commandline.client.filter.EqualsTypeFilter;
 import org.talend.commandline.client.filter.EqualsVersionFilter;
 import org.talend.commandline.client.filter.GreaterVersionFilter;
 import org.talend.commandline.client.filter.ItemFilter;
 import org.talend.commandline.client.filter.LesserVersionFilter;
 import org.talend.commandline.client.filter.NotFilter;
 import org.talend.commandline.client.filter.OrGroupFilter;
 import org.talend.commandline.client.filter.StatusFilter;
 import org.talend.commandline.client.filter.UniqueElementGroupFilter;
 import org.talend.commandline.client.filter.WildcardLabelFilter;
 import org.talend.commandline.client.filter.WildcardPathFilter;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public abstract class ItemFilterAbstractSwitch<T, X extends Exception>
 {
   public T doSwitch(ItemFilter filter)
     throws Exception
   {
     if (filter == null)
       return (T)caseNull();
     if ((filter instanceof AndGroupFilter))
       return (T)caseAndGroupFilter((AndGroupFilter)filter);
     if ((filter instanceof AuthorFilter))
       return (T)caseAuthorFilter((AuthorFilter)filter);
     if ((filter instanceof EqualsVersionFilter))
       return (T)caseEqualsVersionFilter((EqualsVersionFilter)filter);
     if ((filter instanceof GreaterVersionFilter))
       return (T)caseGreaterVersionFilter((GreaterVersionFilter)filter);
     if ((filter instanceof EqualsLabelFilter))
       return (T)caseEqualsLabelFilter((EqualsLabelFilter)filter);
     if ((filter instanceof EqualsPathFilter))
       return (T)caseEqualsPathFilter((EqualsPathFilter)filter);
     if ((filter instanceof LesserVersionFilter))
       return (T)caseLesserVersionFilter((LesserVersionFilter)filter);
     if ((filter instanceof OrGroupFilter))
       return (T)caseOrGroupFilter((OrGroupFilter)filter);
     if ((filter instanceof StatusFilter))
       return (T)caseStatusFilter((StatusFilter)filter);
     if ((filter instanceof EqualsTypeFilter))
       return (T)caseEqualsTypeFilter((EqualsTypeFilter)filter);
     if ((filter instanceof WildcardLabelFilter))
       return (T)caseWildcardLabelFilter((WildcardLabelFilter)filter);
     if ((filter instanceof WildcardPathFilter))
       return (T)caseWildcardPathFilter((WildcardPathFilter)filter);
     if ((filter instanceof UniqueElementGroupFilter))
       return (T)caseUniqueElementGroupFilter((UniqueElementGroupFilter)filter);
     if ((filter instanceof NotFilter)) {
       return (T)caseNotFilter((NotFilter)filter);
     }
     return (T)defaultCase(filter);
   }
   
   public abstract T caseNull()
     throws Exception;
   
   public abstract T caseUniqueElementGroupFilter(UniqueElementGroupFilter paramUniqueElementGroupFilter)
     throws Exception;
   
   public abstract T caseNotFilter(NotFilter paramNotFilter)
     throws Exception;
   
   public abstract T caseEqualsTypeFilter(EqualsTypeFilter paramEqualsTypeFilter)
     throws Exception;
   
   public abstract T caseStatusFilter(StatusFilter paramStatusFilter)
     throws Exception;
   
   public abstract T caseOrGroupFilter(OrGroupFilter paramOrGroupFilter)
     throws Exception;
   
   public abstract T caseLesserVersionFilter(LesserVersionFilter paramLesserVersionFilter)
     throws Exception;
   
   public abstract T caseEqualsLabelFilter(EqualsLabelFilter paramEqualsLabelFilter)
     throws Exception;
   
   public abstract T caseEqualsPathFilter(EqualsPathFilter paramEqualsPathFilter)
     throws Exception;
   
   public abstract T caseGreaterVersionFilter(GreaterVersionFilter paramGreaterVersionFilter)
     throws Exception;
   
   public abstract T caseEqualsVersionFilter(EqualsVersionFilter paramEqualsVersionFilter)
     throws Exception;
   
   public abstract T caseAuthorFilter(AuthorFilter paramAuthorFilter)
     throws Exception;
   
   public abstract T caseAndGroupFilter(AndGroupFilter paramAndGroupFilter)
     throws Exception;
   
   public abstract T caseWildcardLabelFilter(WildcardLabelFilter paramWildcardLabelFilter)
     throws Exception;
   
   public abstract T caseWildcardPathFilter(WildcardPathFilter paramWildcardPathFilter)
     throws Exception;
   
   public abstract T defaultCase(ItemFilter paramItemFilter)
     throws Exception;
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\util\ItemFilterAbstractSwitch.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */