 package org.talend.commandline.client.util;
 
 import org.talend.commandline.client.filter.AndGroupFilter;
 import org.talend.commandline.client.filter.AuthorFilter;
 import org.talend.commandline.client.filter.EqualsLabelFilter;
 import org.talend.commandline.client.filter.EqualsPathFilter;
 import org.talend.commandline.client.filter.EqualsTypeFilter;
 import org.talend.commandline.client.filter.EqualsVersionFilter;
 import org.talend.commandline.client.filter.GreaterVersionFilter;
 import org.talend.commandline.client.filter.ItemFilter;
 import org.talend.commandline.client.filter.LesserVersionFilter;
 import org.talend.commandline.client.filter.NotFilter;
 import org.talend.commandline.client.filter.OrGroupFilter;
 import org.talend.commandline.client.filter.StatusFilter;
 import org.talend.commandline.client.filter.UniqueElementGroupFilter;
 import org.talend.commandline.client.filter.WildcardLabelFilter;
 import org.talend.commandline.client.filter.WildcardPathFilter;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class ItemFilterNullSwitch<T, X extends Exception>
   extends ItemFilterAbstractSwitch<T, Exception>
 {
   public T caseAndGroupFilter(AndGroupFilter filter)
     throws Exception
   {
     return null;
   }
   
   public T caseNotFilter(NotFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseAuthorFilter(AuthorFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseEqualsVersionFilter(EqualsVersionFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseGreaterVersionFilter(GreaterVersionFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseEqualsLabelFilter(EqualsLabelFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseEqualsPathFilter(EqualsPathFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseLesserVersionFilter(LesserVersionFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseOrGroupFilter(OrGroupFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseStatusFilter(StatusFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseEqualsTypeFilter(EqualsTypeFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseUniqueElementGroupFilter(UniqueElementGroupFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseWildcardLabelFilter(WildcardLabelFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseWildcardPathFilter(WildcardPathFilter filter) throws Exception
   {
     return null;
   }
   
   public T defaultCase(ItemFilter filter) throws Exception
   {
     return null;
   }
   
   public T caseNull() throws Exception
   {
     return null;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\util\ItemFilterNullSwitch.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */