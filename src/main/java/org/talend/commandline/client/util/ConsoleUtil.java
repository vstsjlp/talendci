package org.talend.commandline.client.util;

import java.io.File;
import jline.ConsoleReader;
import jline.ConsoleReaderInputStream;
import jline.History;

public final class ConsoleUtil
{
	public static void unset() {}

	public static void set()
	{
		ConsoleReader consoleReader = getConsoleReader();
		if (consoleReader != null) {
			ConsoleReaderInputStream.setIn(consoleReader);
		}
	}

	public static ConsoleReader getConsoleReader() {
		ConsoleReader consoleReader = null;
		try {
			consoleReader = new ConsoleReader();
			consoleReader.setHistory(new History(new File(System.getProperty("user.home"), ".commandline_history")));
		}
		catch (Throwable localThrowable) {
			unset();
		}
		return consoleReader;
	}

	public static ConsoleReaderInputStream getConsoleReaderInputStream() {
		ConsoleReaderInputStream consoleReaderInputStream = null;
		ConsoleReader consoleReader = getConsoleReader();
		if (consoleReader != null) {
			consoleReaderInputStream = new ConsoleReaderInputStream(consoleReader);
		}
		return consoleReaderInputStream;
	}
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\util\ConsoleUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */