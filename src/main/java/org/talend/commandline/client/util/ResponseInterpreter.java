package org.talend.commandline.client.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.talend.commandline.client.CommandLineJavaCLientException;
import org.talend.commandline.client.CommandLineJavaServerException;
import org.talend.commandline.client.command.CommandStatus;
import org.talend.commandline.client.model.CommandLineCommandBean;
import org.talend.commandline.client.model.EProtocolResponse;

public class ResponseInterpreter
 {
   private boolean begin = false;
   
   private boolean end = false;
   
   private int addedCommandId;
   
   private CommandLineJavaServerException exception;
   
   private CommandStatus commandStatus;
   
   private CommandLineCommandBean[] commandlineCommandBeans;
   
   private String resultString;
   
   private String localPath;
   private long size;
   
   public int getAddedCommandId()
   {
     return this.addedCommandId;
   }
   
   public void setFilePath(String localPath) {
     this.localPath = localPath;
   }
   
   public CommandLineJavaServerException getException() {
     return this.exception;
   }
   
   public CommandStatus getCommandStatus() {
     return this.commandStatus;
   }
   
   public CommandLineCommandBean[] getCommandlineCommandBeans() {
     return this.commandlineCommandBeans;
   }
   
   public String getResultString() {
     return this.resultString;
   }
   
   public void parse(InputStream inputStream) throws IOException, CommandLineJavaCLientException {
     while (!this.end) {
       WithoutCrLfReaderWriterHandler readerWriterHandler = new WithoutCrLfReaderWriterHandler(inputStream);
       readerWriterHandler.readUntil('\n');
       parse(readerWriterHandler.getResult(), inputStream);
     }
   }
   
   private void parse(String string, InputStream inputStream) throws CommandLineJavaCLientException {
     if (string == null) {
       throw new CommandLineJavaCLientException("No response from server");
     }
     
     if (string.equals("talend> "))
     {
       return;
     }
     
     if (string.equals(EProtocolResponse.BEGIN.getName())) {
       this.begin = true;
       return;
     }
     
     if (this.begin) {
       if (string.equals(EProtocolResponse.END.getName())) {
         this.end = true;
         return;
       }
       
       if (string.startsWith(EProtocolResponse.ADDED_COMMAND.getName())) {
         string = string.substring(EProtocolResponse.ADDED_COMMAND.getName().length() + 1);
         this.addedCommandId = Integer.parseInt(string);
         return;
       }
       
       if (string.startsWith(EProtocolResponse.BAD_REQUEST.getName())) {
         string = string.substring(EProtocolResponse.BAD_REQUEST.getName().length() + 1);
         this.exception = new CommandLineJavaServerException(string);
         return;
       }
       
       if (string.startsWith(EProtocolResponse.COMMAND_STATUS.getName())) {
         string = string.substring(EProtocolResponse.COMMAND_STATUS.getName().length() + 1);
         try {
           this.commandStatus = ((CommandStatus)new Base64SerializationHelper().decode(string));
         } catch (Exception e) {
           throw new CommandLineJavaCLientException(e);
         }
         return;
       }
       if (string.startsWith(EProtocolResponse.LIST_COMMAND.getName())) {
         string = string.substring(EProtocolResponse.LIST_COMMAND.getName().length() + 1);
         try {
           List<CommandLineCommandBean> beanList = (ArrayList)new Base64SerializationHelper().decode(string);
           this.commandlineCommandBeans = ((CommandLineCommandBean[])beanList.toArray(new CommandLineCommandBean[0]));
         } catch (Exception e) {
           throw new CommandLineJavaCLientException(e);
         }
         return;
       }
       if (string.startsWith(EProtocolResponse.STRING.getName())) {
         this.resultString = string.substring(EProtocolResponse.STRING.getName().length() + 1);
         return;
       }
       
       if (string.startsWith(EProtocolResponse.FILE_SIZE.getName())) {
         this.size = Long.parseLong(string.substring(EProtocolResponse.FILE_SIZE.getName().length() + 1));
         return;
       }
       
       if (string.equals(EProtocolResponse.FILE_CONTENT.getName())) {
         try {
           FileTransferHandler.receiveFile(this.localPath, inputStream, this.size);
           this.resultString = FileTransferHandler.getMD5(this.localPath);
         } catch (Exception e) {
           throw new CommandLineJavaCLientException(e);
         }
         return;
       }
       
       if (string.startsWith(EProtocolResponse.FILE_MD5.getName())) {
         if (!this.resultString.equals(string.substring(EProtocolResponse.FILE_MD5.getName().length() + 1)))
           throw new CommandLineJavaCLientException("Bad md5");
         return;
       }
     }
     
     throw new CommandLineJavaCLientException("unable to parse " + string);
   }
   
   static class WithoutCrLfReaderWriterHandler extends ReaderWriterHandler
   {
     public WithoutCrLfReaderWriterHandler(InputStream inputStream)
     {
       super(new StringWriter());
     }
     
     public void appendChar(char readChar) throws IOException
     {
       if (('\n' != readChar) && ('\r' != readChar)) {
         super.appendChar(readChar);
       }
     }
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\util\ResponseInterpreter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */