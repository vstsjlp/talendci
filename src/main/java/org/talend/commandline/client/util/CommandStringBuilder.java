package org.talend.commandline.client.util;

public class CommandStringBuilder {
	private StringBuffer buffer = new StringBuffer(200);

	public CommandStringBuilder(String commandName) {
		this.buffer.append(commandName);
	}

	public CommandStringBuilder addArgument(String value) {
		this.buffer.append(" ");
		this.buffer.append('\'');
		addEscaped(value);
		this.buffer.append('\'');
		return this;
	}

	public CommandStringBuilder addOption(String optionShortName) {
		this.buffer.append(" ");
		this.buffer.append("-");
		this.buffer.append(optionShortName);
		return this;
	}

	public CommandStringBuilder addOptionWithArgument(String optionShortName, String argumentValue) {
		this.buffer.append(" ");
		this.buffer.append("-");
		this.buffer.append(optionShortName);
		addArgument(argumentValue);
		return this;
	}

	private void addEscaped(String value) {
		for (int i = 0; i < value.length(); i++) {
			char currentChar = value.charAt(i);
			if ((currentChar == '\'') || (currentChar == '"')) {
				this.buffer.append('\\');
			}
			this.buffer.append(currentChar);
		}
	}

	public String toString() {
		return this.buffer.toString();
	}
}
