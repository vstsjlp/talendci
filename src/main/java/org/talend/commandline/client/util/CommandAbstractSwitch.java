package org.talend.commandline.client.util;

import org.talend.commandline.client.command.ChangeStatusCommand;
import org.talend.commandline.client.command.ChangeVersionCommand;
import org.talend.commandline.client.command.DeleteItemsCommand;
import org.talend.commandline.client.command.ExecuteAllJobCommand;
import org.talend.commandline.client.command.ExecuteJobCommand;
import org.talend.commandline.client.command.ExportAllJobCommand;
import org.talend.commandline.client.command.ExportJobCommand;
import org.talend.commandline.client.command.IExtensionCommand;
import org.talend.commandline.client.command.ImportDatabaseMetadataCommand;
import org.talend.commandline.client.command.ImportDelimitedMetadataCommand;
import org.talend.commandline.client.command.InitLocalCommand;
import org.talend.commandline.client.command.InitRemoteCommand;
import org.talend.commandline.client.command.JavaServerCommand;
import org.talend.commandline.client.command.LogoffProjectCommand;
import org.talend.commandline.client.command.LogonProjectCommand;
import org.talend.commandline.client.command.StopServerCommand;

public abstract class CommandAbstractSwitch<T, X extends Exception> {
	public T doSwitch(JavaServerCommand command) throws Exception {
		if (command == null)
			return (T) caseNull();
		if ((command instanceof ExportAllJobCommand))
			return (T) caseExportAllJobCommand((ExportAllJobCommand) command);
		if ((command instanceof DeleteItemsCommand))
			return (T) caseDeleteItemsCommand((DeleteItemsCommand) command);
		if ((command instanceof ExecuteJobCommand))
			return (T) caseExecuteJobCommand((ExecuteJobCommand) command);
		if ((command instanceof ChangeVersionCommand))
			return (T) caseChangeVersionCommand((ChangeVersionCommand) command);
		if ((command instanceof StopServerCommand))
			return (T) caseStopServerCommand((StopServerCommand) command);
		if ((command instanceof ExecuteAllJobCommand))
			return (T) caseExecuteAllJobCommand((ExecuteAllJobCommand) command);
		if ((command instanceof LogonProjectCommand))
			return (T) caseLogonProjectCommand((LogonProjectCommand) command);
		if ((command instanceof LogoffProjectCommand))
			return (T) caseLogoffProjectCommand((LogoffProjectCommand) command);
		if ((command instanceof InitLocalCommand))
			return (T) caseInitLocalCommand((InitLocalCommand) command);
		if ((command instanceof InitRemoteCommand))
			return (T) caseInitRemoteCommand((InitRemoteCommand) command);
		if ((command instanceof ImportDatabaseMetadataCommand))
			return (T) caseImportDatabaseMetadataCommand((ImportDatabaseMetadataCommand) command);
		if ((command instanceof ImportDelimitedMetadataCommand))
			return (T) caseImportDelimitedMetadataCommand((ImportDelimitedMetadataCommand) command);
		if ((command instanceof ExportJobCommand))
			return (T) caseExportJobCommand((ExportJobCommand) command);
		if ((command instanceof ChangeStatusCommand))
			return (T) caseChangeStatusCommand((ChangeStatusCommand) command);
		if ((command instanceof IExtensionCommand)) {
			return (T) caseExtensionCommand((IExtensionCommand) command);
		}
		return (T) defaultCase(command);
	}

	public abstract T caseExportAllJobCommand(ExportAllJobCommand paramExportAllJobCommand) throws Exception;

	public abstract T caseDeleteItemsCommand(DeleteItemsCommand paramDeleteItemsCommand) throws Exception;

	public abstract T caseExecuteJobCommand(ExecuteJobCommand paramExecuteJobCommand) throws Exception;

	public abstract T caseChangeVersionCommand(ChangeVersionCommand paramChangeVersionCommand) throws Exception;

	public abstract T caseStopServerCommand(StopServerCommand paramStopServerCommand) throws Exception;

	public abstract T caseExecuteAllJobCommand(ExecuteAllJobCommand paramExecuteAllJobCommand) throws Exception;

	public abstract T caseLogonProjectCommand(LogonProjectCommand paramLogonProjectCommand) throws Exception;

	public abstract T caseLogoffProjectCommand(LogoffProjectCommand paramLogoffProjectCommand) throws Exception;

	public abstract T caseInitLocalCommand(InitLocalCommand paramInitLocalCommand) throws Exception;

	public abstract T caseInitRemoteCommand(InitRemoteCommand paramInitRemoteCommand) throws Exception;

	public abstract T caseImportDatabaseMetadataCommand(
			ImportDatabaseMetadataCommand paramImportDatabaseMetadataCommand) throws Exception;

	public abstract T caseImportDelimitedMetadataCommand(
			ImportDelimitedMetadataCommand paramImportDelimitedMetadataCommand) throws Exception;

	public abstract T caseExportJobCommand(ExportJobCommand paramExportJobCommand) throws Exception;

	public abstract T caseChangeStatusCommand(ChangeStatusCommand paramChangeStatusCommand) throws Exception;

	public abstract T caseExtensionCommand(IExtensionCommand paramIExtensionCommand) throws Exception;

	public abstract T defaultCase(JavaServerCommand paramJavaServerCommand) throws Exception;

	public abstract T caseNull() throws Exception;
}
