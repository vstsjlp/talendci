package org.talend.commandline.client.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class FileTransferHandler {
	private static final int BUFFER_SIZE = 1024;

	public static long getFileSize(String filePath) {
		return new File(filePath).length();
	}

	public static String getMD5(String filePath) throws IOException {
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			MessageDigest digest;
			throw new RuntimeException(e);
		}
		MessageDigest digest;
		InputStream is = new BufferedInputStream(new FileInputStream(filePath));
		byte[] buffer = new byte['Ѐ'];
		int read = 0;
		try {
			while ((read = is.read(buffer)) > 0) {
				digest.update(buffer, 0, read);
			}
			byte[] md5sum = digest.digest();
			BigInteger bigInt = new BigInteger(1, md5sum);
			return bigInt.toString(16);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException localIOException2) {
			}
		}
	}

	/* Error */
	public static void sendFile(String filePath, java.io.OutputStream outputStream)
     throws IOException
   {
 Byte code:
BufferedInputStream
   3: dup
FileInputStream
   7: dup
   8: aload_0
String;)V
InputStream;)V
   15: astore_2
   16: sipush 1024
   19: newarray <illegal type>
   21: astore_3
   22: iconst_0
   23: istore 4
   25: aload_1
OutputStream:flush	()V
   29: goto +11 -> 40
   32: aload_1
   33: aload_3
   34: iconst_0
   35: iload 4
OutputStream:write	([BII)V
   40: aload_2
   41: aload_3
InputStream:read	([B)I
   45: dup
   46: istore 4
   48: ifgt -16 -> 32
   51: aload_1
OutputStream:flush	()V
   55: goto +20 -> 75
   58: astore 5
   60: aload_2
   61: ifnull +11 -> 72
   64: aload_2
InputStream:close	()V
   68: goto +4 -> 72
   71: pop
   72: aload 5
   74: athrow
   75: aload_2
   76: ifnull +11 -> 87
   79: aload_2
InputStream:close	()V
   83: goto +4 -> 87
   86: pop
   87: return
 Line number table:
   Java source line #67	-> byte code offset #0
   Java source line #68	-> byte code offset #16
   Java source line #69	-> byte code offset #22
   Java source line #71	-> byte code offset #25
   Java source line #72	-> byte code offset #29
   Java source line #73	-> byte code offset #32
   Java source line #72	-> byte code offset #40
   Java source line #75	-> byte code offset #51
   Java source line #76	-> byte code offset #55
   Java source line #78	-> byte code offset #60
   Java source line #79	-> byte code offset #64
   Java source line #80	-> byte code offset #68
   Java source line #83	-> byte code offset #72
   Java source line #78	-> byte code offset #75
   Java source line #79	-> byte code offset #79
   Java source line #80	-> byte code offset #83
   Java source line #84	-> byte code offset #87
 Local variable table:
   start	length	slot	name	signature
   0	88	0	filePath	String
   0	88	1	outputStream	java.io.OutputStream
   15	65	2	inputStream	InputStream
   21	21	3	buffer	byte[]
   23	24	4	read	int
   58	15	5	localObject	Object
   71	1	6	localIOException1	IOException
   86	1	7	localIOException2	IOException
 Exception table:
   from	to	target	type
   25	58	58	finally
IOException
IOException
   }

	/* Error */
	public static void receiveFile(String filePath, InputStream inputStream, long size)
     throws IOException
   {
 Byte code:
BufferedOutputStream
   3: dup
FileOutputStream
   7: dup
   8: aload_0
String;)V
OutputStream;)V
   15: astore 4
   17: sipush 1024
   20: newarray <illegal type>
   22: astore 5
   24: iconst_0
   25: istore 6
   27: sipush 1024
   30: istore 7
   32: lload_2
   33: ldc2_w 124
   36: lcmp
   37: ifge +38 -> 75
   40: lload_2
   41: l2i
   42: istore 7
   44: goto +31 -> 75
   47: lload_2
   48: iload 6
   50: i2l
   51: lsub
   52: lstore_2
   53: lload_2
   54: ldc2_w 124
   57: lcmp
   58: ifge +7 -> 65
   61: lload_2
   62: l2i
   63: istore 7
   65: aload 4
   67: aload 5
   69: iconst_0
   70: iload 6
OutputStream:write	([BII)V
   75: aload_1
   76: aload 5
   78: iconst_0
   79: iload 7
InputStream:read	([BII)I
   84: dup
   85: istore 6
   87: ifgt -40 -> 47
   90: goto +22 -> 112
   93: astore 8
   95: aload 4
   97: ifnull +12 -> 109
   100: aload 4
OutputStream:close	()V
   105: goto +4 -> 109
   108: pop
   109: aload 8
   111: athrow
   112: aload 4
   114: ifnull +12 -> 126
   117: aload 4
OutputStream:close	()V
   122: goto +4 -> 126
   125: pop
   126: return
 Line number table:
   Java source line #87	-> byte code offset #0
   Java source line #88	-> byte code offset #17
   Java source line #89	-> byte code offset #24
   Java source line #90	-> byte code offset #27
   Java source line #92	-> byte code offset #32
   Java source line #93	-> byte code offset #40
   Java source line #94	-> byte code offset #44
   Java source line #95	-> byte code offset #47
   Java source line #96	-> byte code offset #53
   Java source line #97	-> byte code offset #61
   Java source line #98	-> byte code offset #65
   Java source line #94	-> byte code offset #75
   Java source line #100	-> byte code offset #90
   Java source line #102	-> byte code offset #95
   Java source line #103	-> byte code offset #100
   Java source line #104	-> byte code offset #105
   Java source line #107	-> byte code offset #109
   Java source line #102	-> byte code offset #112
   Java source line #103	-> byte code offset #117
   Java source line #104	-> byte code offset #122
   Java source line #108	-> byte code offset #126
 Local variable table:
   start	length	slot	name	signature
   0	127	0	filePath	String
   0	127	1	inputStream	InputStream
   0	127	2	size	long
   15	103	4	outputStream	java.io.OutputStream
   22	55	5	buffer	byte[]
   25	61	6	read	int
   30	50	7	toRead	int
   93	17	8	localObject	Object
   108	1	8	localIOException1	IOException
   125	1	9	localIOException2	IOException
 Exception table:
   from	to	target	type
   32	93	93	finally
IOException
IOException
   }
}

/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\util\FileTransferHandler.class*

Java compiler version:8(52.0)*JD-
Core Version:0.7.1*/