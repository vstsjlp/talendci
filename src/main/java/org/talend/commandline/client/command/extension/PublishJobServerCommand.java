package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class PublishJobServerCommand extends PublishServerCommand {
	public String getJobName() {
		return (String) getValue("JOB_NAME");
	}

	public String getJobVersion() {
		return (String) getValue("VERSION");
	}

	public String getJobGroup() {
		return (String) getValue("GROUP");
	}

	public String getType() {
		return (String) getValue("TYPE");
	}

	public String getJobContext() {
		return (String) getValue("JOB_CONTEXT");
	}

	public boolean isApplyContextToChildren() {
		return getBooleanValue("JOB_APPLY_CONTEXT_TO_CHILDREN");
	}

	public boolean isIncludeContext() {
		return getBooleanValue("JOB_INCLUDE_CONTEXT");
	}

	public boolean isExecuteTests() {
		return getBooleanValue("JOB_EXECUTE_TESTS");
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("publishJob");

		builder.addArgument(getJobName());

		String routeVersion = getJobVersion();
		if (routeVersion != null) {
			builder.addOptionWithArgument("v", routeVersion);
		}

		String publishVersion = getPublishVersion();
		if (publishVersion != null) {
			builder.addOptionWithArgument("pv", publishVersion);
		}

		String jobGroup = getJobGroup();
		if (jobGroup != null) {
			builder.addOptionWithArgument("g", jobGroup);
		}

		String artifactId = getArtifactId();
		if (artifactId != null) {
			builder.addOptionWithArgument("a", artifactId);
		}

		if (isSnapshot()) {
			builder.addOption("s");
		}
		builder.addOptionWithArgument("r", getArtifactRepository());
		builder.addOptionWithArgument("u", getUsername());

		String type = getType();
		if (type != null) {
			builder.addOptionWithArgument("t", type);
		}
		String jobContext = getJobContext();
		if (jobContext != null) {
			builder.addOptionWithArgument("jc", jobContext);
		}
		if (isApplyContextToChildren()) {
			builder.addOption("jactc");
		}

		if (isIncludeContext()) {
			builder.addOption("ic");
		}

		if (isExecuteTests()) {
			builder.addOption("et");
		}

		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\PublishJobServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */