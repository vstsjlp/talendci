package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class DeployJobToServerCommand extends AbstractExtensionCommand {
	private String jobName;
	private String executionServerName;
	private String jobVersion;
	private String jobContextName;
	private String log4jLevel;
	private boolean applyContextToChildren;
	private boolean useSSL;
	private String userName;
	private int statisticsPort = -1;

	private int tracePort = -1;

	private String passwd;

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public DeployJobToServerCommand(String jobName, String executionServerName, String jobVersion,
			String jobContextName, boolean applyContextToChildren) {
		this.jobName = jobName;
		this.executionServerName = executionServerName;
		this.jobVersion = jobVersion;
		this.jobContextName = jobContextName;
		this.applyContextToChildren = applyContextToChildren;
	}

	public DeployJobToServerCommand(String jobName, String executionServerName, String jobVersion,
			String jobContextName, String userName, String passwd, String log4jLevel, boolean applyContextToChildren,
			boolean useSSL) {
		this.jobName = jobName;
		this.executionServerName = executionServerName;
		this.jobVersion = jobVersion;
		this.jobContextName = jobContextName;
		this.log4jLevel = log4jLevel;
		this.applyContextToChildren = applyContextToChildren;
		this.userName = userName;
		this.passwd = passwd;
		this.useSSL = useSSL;
	}

	public DeployJobToServerCommand(String jobName, String executionServerName, String jobVersion,
			String jobContextName, String userName, String passwd, String log4jLevel, boolean applyContextToChildren,
			boolean useSSL, int statisticsPort, int tracePort) {
		this.jobName = jobName;
		this.executionServerName = executionServerName;
		this.jobVersion = jobVersion;
		this.jobContextName = jobContextName;
		this.log4jLevel = log4jLevel;
		this.applyContextToChildren = applyContextToChildren;
		this.userName = userName;
		this.passwd = passwd;
		this.useSSL = useSSL;
		this.statisticsPort = statisticsPort;
		this.tracePort = tracePort;
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String writeToString(boolean forDisplay) {
		CommandStringBuilder builder = new CommandStringBuilder("deployJobToServer");
		builder.addArgument(this.jobName);
		builder.addOptionWithArgument("es", this.executionServerName);

		if (this.jobVersion != null) {
			builder.addOptionWithArgument("jv", this.jobVersion);
		}

		if (this.log4jLevel != null) {
			builder.addOptionWithArgument("jall", this.log4jLevel);
		}

		if (this.jobContextName != null) {
			builder.addOptionWithArgument("jc", this.jobContextName);
		}

		if (this.applyContextToChildren) {
			builder.addOption("jactc");
		}
		if (this.useSSL) {
			builder.addOption("useSSL");
		}
		if (this.userName != null) {
			builder.addOptionWithArgument("un", this.userName);
		}
		if (this.passwd != null) {
			builder.addOptionWithArgument("pd", getDisplayPassword(this.passwd, forDisplay));
		}
		if (this.statisticsPort > -1) {
			builder.addOptionWithArgument("sp", String.valueOf(this.statisticsPort));
		}
		if (this.tracePort > -1) {
			builder.addOptionWithArgument("tp", String.valueOf(this.tracePort));
		}

		return builder.toString();
	}

	public String toString() {
		return writeToString(true);
	}

	public String getJobName() {
		return this.jobName;
	}

	public String getExecutionServerName() {
		return this.executionServerName;
	}

	public String getJobContextName() {
		return this.jobContextName;
	}

	public String getJobVersion() {
		return this.jobVersion;
	}

	public String getLog4jLevel() {
		return this.log4jLevel;
	}

	public boolean isApplyContextToChildren() {
		return this.applyContextToChildren;
	}

	public boolean useSSL() {
		return this.useSSL;
	}

	public int getStatisticsPort() {
		return this.statisticsPort;
	}

	public int getTracePort() {
		return this.tracePort;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\DeployJobToServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */