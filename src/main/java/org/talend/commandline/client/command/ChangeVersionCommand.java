package org.talend.commandline.client.command;

import org.talend.commandline.client.filter.ItemFilter;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ChangeVersionCommand extends JavaServerCommand {
	private String newVersion;
	private boolean fixLatestVersion;
	private boolean dependencies;
	private ItemFilter itemFilter;
	private String itemFilterAsString;

	public ChangeVersionCommand(String newVersion, boolean fixLatestVersion, boolean dependencies,
			ItemFilter itemFilter) {
		this.newVersion = newVersion;
		this.fixLatestVersion = fixLatestVersion;
		this.dependencies = dependencies;
		this.itemFilter = itemFilter;
	}

	public ChangeVersionCommand(String newVersion, String itemFilterAsString) {
		this.newVersion = newVersion;
		this.itemFilterAsString = itemFilterAsString;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("changeVersion");
		builder.addArgument(this.newVersion);

		if (this.fixLatestVersion) {
			builder.addOption("flv");
		}

		if (this.dependencies) {
			builder.addOption("d");
		}

		if (this.itemFilter != null) {
			builder.addOptionWithArgument("if", this.itemFilter.writeToString());
		} else if (this.itemFilterAsString != null) {
			builder.addOptionWithArgument("if", this.itemFilterAsString);
		}
		return builder.toString();
	}

	public String getNewVersion() {
		return this.newVersion;
	}

	public ItemFilter getItemFilter() {
		return this.itemFilter;
	}

	public boolean isFixLatestVersion() {
		return this.fixLatestVersion;
	}

	public boolean isDependencies() {
		return this.dependencies;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ChangeVersionCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */