package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class CheckAndUpdateServerCommand extends AbstractServerCommand {
	public CheckAndUpdateServerCommand() {
	}

	public CheckAndUpdateServerCommand(String tacUser, String tacUserPassword) {
		this();
		setValue("TAC_USER", tacUser);
		setValue("TAC_USER_PASSWORD", tacUserPassword);
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String writeToString(boolean forDisplay) {
		CommandStringBuilder builder = new CommandStringBuilder("checkAndUpdate");
		String user = getTACUser();
		if (user != null) {
			builder.addOptionWithArgument("TAC_USER", user);
			String password = getUserPassword();
			if ((password != null) && (password.length() > 0)) {
				builder.addOptionWithArgument("password", getDisplayPassword(password, forDisplay));
			}
		}

		return builder.toString();
	}

	public String getTACUser() {
		return (String) getValue("TAC_USER");
	}

	public String getUserPassword() {
		return (String) getValue("TAC_USER_PASSWORD");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\CheckAndUpdateServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */