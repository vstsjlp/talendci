package org.talend.commandline.client.command;

import org.talend.commandline.client.command.extension.AbstractServerCommand;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ExportAsCWMCommand extends AbstractServerCommand {
	public ExportAsCWMCommand() {
	}

	public ExportAsCWMCommand(String destDir, String dbConnectonName) {
		this();
		setValue("dbConnectionName", dbConnectonName);
		setValue("DEST_DIR", destDir);
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		buffer.append(" ");

		buffer.append(" DBConnection Name ");
		buffer.append(getDBConnectionName());

		buffer.append(" Destination dir ");
		buffer.append(getDestDir());

		return buffer.toString();
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("exportAsCWM");
		builder.addArgument(getDBConnectionName());
		builder.addOptionWithArgument("dd", getDestDir());

		return builder.toString();
	}

	public String getDBConnectionName() {
		return (String) getValue("dbConnectionName");
	}

	public String getDestDir() {
		return (String) getValue("DEST_DIR");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ExportAsCWMCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */