package org.talend.commandline.client.command;

import org.talend.commandline.client.filter.ItemFilter;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ChangeStatusCommand extends JavaServerCommand {
	private String newStatusCode;
	private boolean dependencies;
	private ItemFilter itemFilter;
	private String itemFilterAsString;

	public ChangeStatusCommand(String newStatusCode, boolean dependencies, ItemFilter itemFilter) {
		this.newStatusCode = newStatusCode;
		this.dependencies = dependencies;
		this.itemFilter = itemFilter;
	}

	public ChangeStatusCommand(String newStatusCode, String itemFilterAsString) {
		this.newStatusCode = newStatusCode;
		this.itemFilterAsString = itemFilterAsString;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("changeStatus");
		builder.addArgument(this.newStatusCode);

		if (this.dependencies) {
			builder.addOption("d");
		}

		if (this.itemFilter != null) {
			builder.addOptionWithArgument("if", this.itemFilter.writeToString());
		} else if (this.itemFilterAsString != null) {
			builder.addOptionWithArgument("if", this.itemFilterAsString);
		}
		return builder.toString();
	}

	public String getNewStatusCode() {
		return this.newStatusCode;
	}

	public ItemFilter getItemFilter() {
		return this.itemFilter;
	}

	public boolean isDependencies() {
		return this.dependencies;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ChangeStatusCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */