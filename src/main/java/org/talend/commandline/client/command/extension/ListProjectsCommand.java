package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ListProjectsCommand extends AbstractServerCommand {
	public ListProjectsCommand() {
	}

	public ListProjectsCommand(boolean showBranch) {
		this();
		setValue("showBranch", Boolean.valueOf(showBranch));
	}

	public boolean isShowBranch() {
		return getBooleanValue("showBranch");
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("listProject");
		if (isShowBranch()) {
			builder.addOption("b");
		}

		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\ListProjectsCommand.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */