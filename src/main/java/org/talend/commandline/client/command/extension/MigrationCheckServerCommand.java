package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class MigrationCheckServerCommand extends AbstractServerCommand {
	public MigrationCheckServerCommand() {
	}

	public MigrationCheckServerCommand(String dataPath) {
		setValue("DATA_PATH", dataPath);
	}

	public String writeToString(boolean forDisplay) {
		CommandStringBuilder builder = new CommandStringBuilder("migrationCheck");
		if (getDataPath() != null) {
			builder.addOptionWithArgument("dp", getDataPath());
		}

		return builder.toString();
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String getDataPath() {
		return (String) getValue("DATA_PATH");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\MigrationCheckServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */