package org.talend.commandline.client.command;

import org.talend.commandline.client.util.CommandStringBuilder;

public class LogonProjectCommand extends JavaServerCommand {
	private String projectTechnicalLabel;
	private String userLogin;
	private String userPassword;
	private boolean generateTemplates;
	private String branchSelection = null;
	private boolean readOnly;

	public LogonProjectCommand(String projectTechnicalLabel, String userLogin, String userPassword,
			boolean generateTemplates) {
		this.projectTechnicalLabel = projectTechnicalLabel;
		this.userLogin = userLogin;
		this.userPassword = userPassword;
		this.generateTemplates = generateTemplates;
	}

	public LogonProjectCommand(String projectTechnicalLabel, String userLogin, String userPassword,
			boolean generateTemplates, String branchSelection) {
		this.projectTechnicalLabel = projectTechnicalLabel;
		this.userLogin = userLogin;
		this.userPassword = userPassword;
		this.generateTemplates = generateTemplates;
		this.branchSelection = branchSelection;
	}

	public LogonProjectCommand(String projectTechnicalLabel, String userLogin, String userPassword,
			boolean generateTemplates, String branchSelection, boolean readOnly) {
		this.projectTechnicalLabel = projectTechnicalLabel;
		this.userLogin = userLogin;
		this.userPassword = userPassword;
		this.generateTemplates = generateTemplates;
		this.branchSelection = branchSelection;
		this.readOnly = readOnly;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		buffer.append(" log on ");
		buffer.append(this.projectTechnicalLabel);
		if (this.branchSelection != null) {
			buffer.append('(');
			buffer.append(this.branchSelection);
			buffer.append(')');
		}
		return buffer.toString();
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String writeToString(boolean forDisplay) {
		CommandStringBuilder builder = new CommandStringBuilder("logonProject");

		builder.addOptionWithArgument("pn", this.projectTechnicalLabel);

		if (this.userLogin != null) {
			builder.addOptionWithArgument("ul", this.userLogin);
			if ((this.userPassword != null) && (this.userPassword.length() > 0)) {
				builder.addOptionWithArgument("up", getDisplayPassword(this.userPassword, forDisplay));
			}
		}

		if (this.generateTemplates) {
			builder.addOption("gt");
		}

		if ((this.branchSelection != null) && (!this.branchSelection.equals(""))) {
			builder.addOptionWithArgument("br", this.branchSelection);
		}

		if (this.readOnly) {
			builder.addOption("ro");
		}

		return builder.toString();
	}

	public String getProjectTechnicalLabel() {
		return this.projectTechnicalLabel;
	}

	public String getUserLogin() {
		return this.userLogin;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public boolean isGenerateTemplates() {
		return this.generateTemplates;
	}

	public String getBranchSelection() {
		return this.branchSelection;
	}

	public boolean isReadOnly() {
		return this.readOnly;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\LogonProjectCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */