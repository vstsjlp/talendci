package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class BuildProjectSourcesCommand extends AbstractServerCommand {
	private String itemFilter;
	private String deployVersion;

	public BuildProjectSourcesCommand() {
	}

	public BuildProjectSourcesCommand(String itemFilter) {
		this.itemFilter = itemFilter;
	}

	public BuildProjectSourcesCommand(String itemFilter, String deployVersion) {
		this.itemFilter = itemFilter;
		this.deployVersion = deployVersion;
		setValue("DEPLOY_VERSION", deployVersion);
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("buildProjectSources");

		if (getItemFilter() != null) {
			builder.addOptionWithArgument("if", getItemFilter());
		}
		if (getDeployVersion() != null) {
			builder.addOptionWithArgument("dv", getDeployVersion());
		}

		return builder.toString();
	}

	public String getItemFilter() {
		if (this.itemFilter != null) {
			return this.itemFilter;
		}
		return (String) getValue("ITEM_FILTER");
	}

	public String getDeployVersion() {
		if (this.deployVersion != null) {
			return this.deployVersion;
		}
		return (String) getValue("DEPLOY_VERSION");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\BuildProjectSourcesCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */