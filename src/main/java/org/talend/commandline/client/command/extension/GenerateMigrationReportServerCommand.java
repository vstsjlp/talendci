package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.constant.extension.GenerateMigrationReportDefine;
import org.talend.commandline.client.util.CommandStringBuilder;

public class GenerateMigrationReportServerCommand extends AbstractServerCommand {
	public GenerateMigrationReportServerCommand() {
	}

	public GenerateMigrationReportServerCommand(String dataPath, String reportPath, String fromVersion,
			String toVersion, String projects) {
		setValue(GenerateMigrationReportDefine.DATA_PATH, dataPath);
		setValue(GenerateMigrationReportDefine.REPORT_PATH, reportPath);
		setValue(GenerateMigrationReportDefine.FROM_VERSION, fromVersion);
		setValue(GenerateMigrationReportDefine.TO_VERSION, toVersion);
		setValue(GenerateMigrationReportDefine.PROJECTS, projects);
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder(GenerateMigrationReportDefine.COMMAND_NAME);
		if (getDataPath() != null) {
			builder.addOptionWithArgument(GenerateMigrationReportDefine.DATA_PATH_SHORT, getDataPath());
		}
		if (getReportPath() != null) {
			builder.addOptionWithArgument(GenerateMigrationReportDefine.REPORT_PATH_SHORT, getReportPath());
		}

		if (getFromVersion() != null) {
			builder.addOptionWithArgument(GenerateMigrationReportDefine.FROM_VERSION_SHORT, getFromVersion());
		}
		if (getToVersion() != null) {
			builder.addOptionWithArgument(GenerateMigrationReportDefine.TO_VERSION_SHORT, getToVersion());
		}

		if (getProjects() != null) {
			builder.addOptionWithArgument(GenerateMigrationReportDefine.PROJECTS_SHORT, getProjects());
		}
		return builder.toString();
	}

	public String getDataPath() {
		return (String) getValue(GenerateMigrationReportDefine.DATA_PATH);
	}

	public String getReportPath() {
		return (String) getValue(GenerateMigrationReportDefine.REPORT_PATH);
	}

	public String getFromVersion() {
		return (String) getValue(GenerateMigrationReportDefine.FROM_VERSION);
	}

	public String getToVersion() {
		return (String) getValue(GenerateMigrationReportDefine.TO_VERSION);
	}

	public String getProjects() {
		return (String) getValue(GenerateMigrationReportDefine.PROJECTS);
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\
 * GenerateMigrationReportServerCommand.class Java compiler version: 8 (52.0)
 * JD-Core Version: 0.7.1
 */