package org.talend.commandline.client.command;

import java.io.PrintWriter;
import java.io.StringWriter;

public class CommandStatusWrapException extends Exception {
	private static final long serialVersionUID = -8134701395854163054L;
	private String wrappedMessage;

	public CommandStatusWrapException(Exception e) {
		this(exceptionToString(e));
		this.wrappedMessage = e.getMessage();
	}

	private CommandStatusWrapException(String message) {
		super(message);
	}

	public synchronized Throwable fillInStackTrace() {
		return null;
	}

	private static String exceptionToString(Exception exception) {
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}

	public String getWrappedMessage() {
		return this.wrappedMessage;
	}

	public void setWrappedMessage(String wrappedMessage) {
		this.wrappedMessage = wrappedMessage;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\CommandStatusWrapException.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */