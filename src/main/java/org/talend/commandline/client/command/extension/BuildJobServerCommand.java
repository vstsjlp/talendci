package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class BuildJobServerCommand extends AbstractServerCommand {
	public BuildJobServerCommand() {
	}

	public BuildJobServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, false, false, false,
				false);
	}

	public BuildJobServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean addStatisticsCode, boolean exportToOSGI) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, addStatisticsCode,
				exportToOSGI, false, false);
	}

	public BuildJobServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean addStatisticsCode, boolean exportToOSGI,
			boolean addAntScript, boolean addMavenScript) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, null, null, applyContextToChildren,
				addStatisticsCode, exportToOSGI, addAntScript, addMavenScript, false, false, false, false, false,
				false);
	}

	public BuildJobServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean exportToOSGI) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, false, exportToOSGI,
				false, false);
	}

	public BuildJobServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, String log4jLevel, boolean applyContextToChildren, boolean addStatisticsCode,
			boolean exportToOSGI, boolean addAntScript, boolean addMavenScript) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, null, log4jLevel, applyContextToChildren,
				addStatisticsCode, exportToOSGI, addAntScript, addMavenScript, false, false, false, false, false,
				false);
	}

	public BuildJobServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, String log4jLevel, boolean applyContextToChildren, boolean addStatisticsCode,
			boolean exportToOSGI) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, null, log4jLevel, applyContextToChildren,
				addStatisticsCode, exportToOSGI, false, false, true, true, true, false, true, true);
	}

	public BuildJobServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, String jobType, String log4jLevel, boolean applyContextToChildren,
			boolean addStatisticsCode, boolean exportToOSGI, boolean addAntScript, boolean addMavenScript,
			boolean includeLibs, boolean includeContext, boolean includeSrc, boolean includeTestSrc,
			boolean executeTests, boolean isBin) {
		this();
		setValue("JOB_NAME", jobName);
		setValue("DESTINATION_DIRECTORY", destDir);
		setValue("AECHIVE_FILENAME", archiveFileName);
		setValue("JOB_CONTEXT", jobContext);
		setValue("JOB_VERSION", jobVersion);
		setValue("JOB_TYPE", jobType);
		setValue("JOB_ADD_LOG4J_LEVEL", log4jLevel);
		setValue("JOB_APPLY_CONTEXT_TO_CHILDREN", Boolean.valueOf(applyContextToChildren));
		setValue("JOB_ADD_STATISTICS_CODE", Boolean.valueOf(addStatisticsCode));
		setValue("JOB_EXPORT_TO_OSGI_CODE", Boolean.valueOf(exportToOSGI));
		setValue("JOB_ADD_ANT_SCRIPT", Boolean.valueOf(addAntScript));
		setValue("JOB_ADD_MAVEN_SCRIPT", Boolean.valueOf(addMavenScript));
		setValue("JOB_INCLUDE_LIBS", Boolean.valueOf(includeLibs));
		setValue("JOB_INCLUDE_CONTEXT", Boolean.valueOf(includeContext));
		setValue("JOB_INCLUDE_JAVA_SOURCE", Boolean.valueOf(includeSrc));
		setValue("JOB_INCLUDE_TEST_SOURCE", Boolean.valueOf(includeTestSrc));
		setValue("JOB_EXECUTE_TESTS", Boolean.valueOf(executeTests));
		setValue("JOB_BINARIES", Boolean.valueOf(isBin));
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("buildJob");
		builder.addArgument(getJobName());
		String destDir = getDestDir();
		if (destDir != null) {
			builder.addOptionWithArgument("dd", destDir);
		}

		String archiveFileName = getArchiveFileName();
		if (archiveFileName != null) {
			builder.addOptionWithArgument("af", archiveFileName);
		}

		String jobContext = getJobContext();
		if (jobContext != null) {
			builder.addOptionWithArgument("jc", jobContext);
		}

		String log4jLevel = getLog4jLevel();
		if (log4jLevel != null) {
			builder.addOptionWithArgument("jall", log4jLevel);
		}

		String jobVersion = getJobVersion();
		if (jobVersion != null) {
			builder.addOptionWithArgument("jv", jobVersion);
		}
		String type = getJobType();
		if (type != null) {
			builder.addOptionWithArgument("jt", type);
		}
		if (isApplyContextToChildren()) {
			builder.addOption("jactc");
		}

		if (isAddStatisticsCode()) {
			builder.addOption("jstats");
		}

		if (isExportToOSGI()) {
			builder.addOption("eo");
		}

		if (isAddAntScript()) {
			builder.addOption("ant");
		}

		if (isAddMavenScript()) {
			builder.addOption("maven");
		}

		if (isIncludeLibs()) {
			builder.addOption("il");
		}

		if (isIncludeContext()) {
			builder.addOption("ic");
		}

		if (isIncludeJavaSource()) {
			builder.addOption("ijs");
		}

		if (isIncludeTestSource()) {
			builder.addOption("its");
		}

		if (isExecuteTests()) {
			builder.addOption("et");
		}

		if (isBinaries()) {
			builder.addOption("bin");
		}

		return builder.toString();
	}

	public String getJobName() {
		return (String) getValue("JOB_NAME");
	}

	public String getDestDir() {
		return (String) getValue("DESTINATION_DIRECTORY");
	}

	public String getArchiveFileName() {
		return (String) getValue("AECHIVE_FILENAME");
	}

	public String getJobContext() {
		return (String) getValue("JOB_CONTEXT");
	}

	public String getJobVersion() {
		return (String) getValue("JOB_VERSION");
	}

	public boolean isApplyContextToChildren() {
		return getBooleanValue("JOB_APPLY_CONTEXT_TO_CHILDREN");
	}

	public boolean isAddStatisticsCode() {
		return getBooleanValue("JOB_ADD_STATISTICS_CODE");
	}

	public boolean isExportToOSGI() {
		return getBooleanValue("JOB_EXPORT_TO_OSGI_CODE");
	}

	public boolean isAddMavenScript() {
		return getBooleanValue("JOB_ADD_MAVEN_SCRIPT");
	}

	public boolean isAddAntScript() {
		return getBooleanValue("JOB_ADD_ANT_SCRIPT");
	}

	public String getLog4jLevel() {
		return (String) getValue("JOB_ADD_LOG4J_LEVEL");
	}

	public String getJobType() {
		return (String) getValue("JOB_TYPE");
	}

	public boolean isIncludeLibs() {
		return getBooleanValue("JOB_INCLUDE_LIBS");
	}

	public boolean isIncludeContext() {
		return getBooleanValue("JOB_INCLUDE_CONTEXT");
	}

	public boolean isIncludeJavaSource() {
		return getBooleanValue("JOB_INCLUDE_JAVA_SOURCE");
	}

	public boolean isIncludeTestSource() {
		return getBooleanValue("JOB_INCLUDE_TEST_SOURCE");
	}

	public boolean isExecuteTests() {
		return getBooleanValue("JOB_EXECUTE_TESTS");
	}

	public boolean isBinaries() {
		return getBooleanValue("JOB_BINARIES");
	}
}
