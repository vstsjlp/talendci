package org.talend.commandline.client.command;

import org.talend.commandline.client.filter.ItemFilter;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ExportItemsCommand extends JavaServerCommand implements IExtensionCommand {
	private String exportItemsDestination;
	private ItemFilter itemFilter;
	private String itemFilterAsString;
	private boolean withDependencies;

	public ExportItemsCommand(String exportItemsDestination, boolean withDependencies) {
		this.exportItemsDestination = exportItemsDestination;
		this.withDependencies = withDependencies;
	}

	public ExportItemsCommand(String exportItemsDestination, boolean withDependencies, ItemFilter itemFilter) {
		this(exportItemsDestination, withDependencies);
		this.itemFilter = itemFilter;
	}

	public ExportItemsCommand(String exportItemsDestination, boolean withDependencies, String itemFilter) {
		this(exportItemsDestination, withDependencies);
		this.itemFilterAsString = itemFilter;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("exportItems");
		builder.addArgument(this.exportItemsDestination);

		if (this.itemFilter != null) {
			builder.addOptionWithArgument("if", this.itemFilter.writeToString());
		} else if (this.itemFilterAsString != null) {
			builder.addOptionWithArgument("if", this.itemFilterAsString);
		}

		if (this.withDependencies) {
			builder.addOption("d");
		}
		return builder.toString();
	}

	public String getExportItemsDestination() {
		return this.exportItemsDestination;
	}

	public ItemFilter getItemFilter() {
		return this.itemFilter;
	}

	public boolean isWithDependencies() {
		return this.withDependencies;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ExportItemsCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */