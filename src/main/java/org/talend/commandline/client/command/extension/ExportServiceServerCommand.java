package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ExportServiceServerCommand extends AbstractServerCommand {
	public String getDestDir() {
		return (String) getValue("DESTINATION_DIRECTORY");
	}

	public String getArchiveFileName() {
		return (String) getValue("ARCHIVE_FILENAME");
	}

	public String getServiceName() {
		return (String) getValue("SERVICE_NAME");
	}

	public String getServiceVersion() {
		return (String) getValue("SERVICE_VERSION");
	}

	public boolean isAddMavenScript() {
		return getBooleanValue("JOB_ADD_MAVEN_SCRIPT");
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("exportService");

		builder.addArgument(getServiceName());

		String destDir = getDestDir();
		if (destDir != null) {
			builder.addOptionWithArgument("dd", destDir);
		}

		String archiveFileName = getArchiveFileName();
		if (archiveFileName != null) {
			builder.addOptionWithArgument("af", archiveFileName);
		}

		String serviceVersion = getServiceVersion();
		if (serviceVersion != null) {
			builder.addOptionWithArgument("sv", serviceVersion);
		}

		boolean isAddMavenScript = isAddMavenScript();
		if (isAddMavenScript) {
			builder.addOption("JOB_ADD_MAVEN_SCRIPT");
		}

		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\ExportServiceServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */