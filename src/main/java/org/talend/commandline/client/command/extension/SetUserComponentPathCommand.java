package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class SetUserComponentPathCommand extends AbstractExtensionCommand {
	private String pathArg;
	private boolean clearPathArg;

	public SetUserComponentPathCommand(String pathArg, boolean clearPathArg) {
		this.pathArg = pathArg;
		this.clearPathArg = clearPathArg;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("setUserComponentPath");
		if (this.pathArg != null) {
			builder.addOptionWithArgument("up", this.pathArg);
		}
		if (isClearPathArg()) {
			builder.addOption("c");
		}

		return builder.toString();
	}

	public void setPathArg(String pathArg) {
		this.pathArg = pathArg;
	}

	public void setClearPathArg(boolean clearPathArg) {
		this.clearPathArg = clearPathArg;
	}

	public String getPathArg() {
		return this.pathArg;
	}

	public boolean isClearPathArg() {
		return this.clearPathArg;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\SetUserComponentPathCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */