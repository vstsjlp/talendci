 package org.talend.commandline.client.command;
 
 
 
 
 
 
 
 public abstract class JavaServerCommand
   implements IJavaCommand
 {
   private CommandGroupCommand groupCommand;
   
 
 
 
 
 
   public CommandGroupCommand getGroupCommand()
   {
     return this.groupCommand;
   }
   
   public void setGroupCommand(CommandGroupCommand groupCommand) {
     this.groupCommand = groupCommand;
   }
   
   public String toString()
   {
     return getClass().getSimpleName();
   }
   
   public String writeToString(boolean forDisplay) {
     return writeToString();
   }
   
   protected String getDisplayPassword(String password, boolean forDisplay) {
     if (!forDisplay) {
       return password;
     }
     String displayPassword = password;
     if (password == null)
     {
       displayPassword = "1234";
     }
     displayPassword = displayPassword.replaceAll(".", "*");
     return displayPassword;
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\command\JavaServerCommand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */