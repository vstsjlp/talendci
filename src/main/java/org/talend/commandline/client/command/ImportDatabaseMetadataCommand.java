package org.talend.commandline.client.command;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ImportDatabaseMetadataCommand extends JavaServerCommand {
	private String databaseMetadataFilePath;

	public ImportDatabaseMetadataCommand(String databaseMetadataFilePath) {
		this.databaseMetadataFilePath = databaseMetadataFilePath;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("importDatabaseMetadata");
		builder.addArgument(this.databaseMetadataFilePath);
		return builder.toString();
	}

	public String getDatabaseMetadataFilePath() {
		return this.databaseMetadataFilePath;
	}
}
