package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class CreateTaskServerCommand extends AbstractServerCommand {
	public CreateTaskServerCommand() {
	}

	public CreateTaskServerCommand(String taskName, String projectName, boolean active, String description,
			String branchName, String jobName, String jobVersion, String jobContext, boolean applyContextToChildren,
			boolean regenerateJobOnChange, String executionServer, boolean executeStatistics, String unaviableJobServer,
			boolean addStatisticsCodeEnable, String onUnknownStateJob) {
		this();
		setValue("taskName", taskName);
		setValue("PROJECT_NAME", projectName);
		setValue("PROJECT_BRANCH", branchName);
		setValue("ACTIVE", Boolean.valueOf(active));
		setValue("DESCRIPTION", description);
		setValue("JOB_NAME", jobName);
		setValue("JOB_VERSION", jobVersion);
		setValue("JOB_CONTEXT", jobContext);
		setValue("JOB_APPLY_CONTEXT_TO_CHILDREN", Boolean.valueOf(applyContextToChildren));
		setValue("REGENERATE_JOB_ON_CHANGE", Boolean.valueOf(regenerateJobOnChange));
		setValue("EXECUTION_SERVER", executionServer);
		setValue("EXEC_STATISTICS_ENABLED", Boolean.valueOf(executeStatistics));
		setValue("ON_UNAVAILABLE_JOBSERVER", unaviableJobServer);
		setValue("ADD_STATISTICS_CODE_ENABLE", Boolean.valueOf(addStatisticsCodeEnable));
		setValue("ON_UNKNOWN_STATE_JOB", onUnknownStateJob);
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("createTask");
		String taskName = getTaskName();
		if (taskName != null) {
			builder.addArgument(taskName);
		}
		String projectName = getProjectName();
		if (projectName != null) {
			builder.addOptionWithArgument("pn", projectName);
		}
		String jobName = getJobName();
		if (jobName != null) {
			builder.addOptionWithArgument("jn", jobName);
		}
		boolean active = getActive();
		if (active) {
			builder.addOption("a");
		}
		String desc = getDesc();
		if (desc != null) {
			builder.addOptionWithArgument("d", desc);
		}
		String jobVersion = getJobVersion();
		if (jobVersion != null) {
			builder.addOptionWithArgument("jv", jobVersion);
		}
		String jobContext = getJobConntext();
		if (jobContext != null) {
			builder.addOptionWithArgument("jc", jobContext);
		}
		String branchName = getBranchName();
		if (branchName != null) {
			builder.addOptionWithArgument("b", branchName);
		}
		boolean applayContext = isApplyContextToChildren();
		if (applayContext) {
			builder.addOption("jactc");
		}
		boolean isRegenerateJobOnChange = isRegenerateJobOnChange();
		if (isRegenerateJobOnChange) {
			builder.addOption("rjc");
		}
		String executeServer = getExecutionServer();
		if (executeServer != null) {
			builder.addOptionWithArgument("esn", executeServer);
		}
		boolean executeStatistics = isExecuteStatistics();
		if (executeStatistics) {
			builder.addOption("ese");
		}
		String unavilableJobServer = getUnavilableJobserver();
		if (unavilableJobServer != null) {
			builder.addOptionWithArgument("ouj", unavilableJobServer);
		}
		boolean addExecuteStatistics = isAddStatisticsCodeEnable();
		if (addExecuteStatistics) {
			builder.addOption("asce");
		}
		String onUnknownStateJob = onUnknownStateJob();
		if (onUnknownStateJob != null) {
			builder.addOptionWithArgument("ousj", onUnknownStateJob);
		}

		return builder.toString();
	}

	public String getTaskName() {
		return (String) getValue("taskName");
	}

	public boolean getActive() {
		return getBooleanValue("ACTIVE");
	}

	public String getDesc() {
		return (String) getValue("DESCRIPTION");
	}

	public String getProjectName() {
		return (String) getValue("PROJECT_NAME");
	}

	public String getJobName() {
		return (String) getValue("JOB_NAME");
	}

	public String getJobVersion() {
		return (String) getValue("JOB_VERSION");
	}

	public String getJobConntext() {
		return (String) getValue("JOB_CONTEXT");
	}

	public String getBranchName() {
		return (String) getValue("PROJECT_BRANCH");
	}

	public boolean isRegenerateJobOnChange() {
		return getBooleanValue("REGENERATE_JOB_ON_CHANGE");
	}

	public boolean isApplyContextToChildren() {
		return getBooleanValue("JOB_APPLY_CONTEXT_TO_CHILDREN");
	}

	public String getExecutionServer() {
		return (String) getValue("EXECUTION_SERVER");
	}

	public boolean isExecuteStatistics() {
		return getBooleanValue("EXEC_STATISTICS_ENABLED");
	}

	public String getUnavilableJobserver() {
		return (String) getValue("ON_UNAVAILABLE_JOBSERVER");
	}

	public boolean isAddStatisticsCodeEnable() {
		return getBooleanValue("ADD_STATISTICS_CODE_ENABLE");
	}

	public String onUnknownStateJob() {
		return (String) getValue("ON_UNKNOWN_STATE_JOB");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\CreateTaskServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */