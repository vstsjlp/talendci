package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class PublishServiceServerCommand extends PublishServerCommand {
	public String getServiceName() {
		return (String) getValue("SERVICE_NAME");
	}

	public String getServiceVersion() {
		return (String) getValue("VERSION");
	}

	public String getServiceGroup() {
		return (String) getValue("GROUP");
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("publishService");

		builder.addArgument(getServiceName());

		String serviceVersion = getServiceVersion();
		if (serviceVersion != null) {
			builder.addOptionWithArgument("v", serviceVersion);
		}

		String publishVersion = getPublishVersion();
		if (publishVersion != null) {
			builder.addOptionWithArgument("pv", publishVersion);
		}

		String serviceGroup = getServiceGroup();
		if (serviceGroup != null) {
			builder.addOptionWithArgument("g", serviceGroup);
		}

		String artifactId = getArtifactId();
		if (artifactId != null) {
			builder.addOptionWithArgument("a", artifactId);
		}

		if (isSnapshot()) {
			builder.addOption("s");
		}
		builder.addOptionWithArgument("r", getArtifactRepository());
		builder.addOptionWithArgument("u", getUsername());

		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\PublishServiceServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */