package org.talend.commandline.client.command;

import java.util.List;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ExecuteJobCommand extends JavaServerCommand {
	private String interpreter;
	private String jobName;
	private String jobContext;
	private String jobVersion;
	private List<String> jobContextParam;
	protected String jobResultDestDir;
	private Integer timeout;
	private boolean applyContextToChildren;

	public ExecuteJobCommand(String interpreter, String jobName, String jobContext, String jobVersion,
			List<String> jobContextParam, String jobResultDestDir, Integer timeout, boolean applyContextToChildren) {
		this.interpreter = interpreter;
		this.jobName = jobName;
		this.jobContext = jobContext;
		this.jobVersion = jobVersion;
		this.jobContextParam = jobContextParam;
		this.jobResultDestDir = jobResultDestDir;
		this.timeout = timeout;
		this.applyContextToChildren = applyContextToChildren;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		buffer.append(" ");
		buffer.append(this.jobName);
		if (this.jobContext != null) {
			buffer.append(" context ");
			buffer.append(this.jobContext);
		}
		if (this.jobVersion != null) {
			buffer.append(" version ");
			buffer.append(this.jobVersion);
		}
		if (this.jobContextParam != null) {
			for (String contextParam : this.jobContextParam) {
				buffer.append(" contextParam ");
				buffer.append(contextParam);
			}
		}
		return buffer.toString();
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("executeJob");
		builder.addArgument(this.jobName);
		builder.addOptionWithArgument("i", this.interpreter);

		if (this.jobContext != null) {
			builder.addOptionWithArgument("jc", this.jobContext);
		}

		if (this.jobVersion != null) {
			builder.addOptionWithArgument("jv", this.jobVersion);
		}

		if ((this.jobContextParam != null) && (this.jobContextParam.size() > 0)) {
			builder.addOption("jcp");
			for (String contextParam : this.jobContextParam) {
				builder.addArgument(contextParam);
			}
		}

		if (this.jobResultDestDir != null) {
			builder.addOptionWithArgument("jrdd", this.jobResultDestDir);
		}

		if (this.timeout != null) {
			builder.addOptionWithArgument("jt", this.timeout.toString());
		}

		if (this.applyContextToChildren) {
			builder.addOption("jactc");
		}

		return builder.toString();
	}

	public String getInterpreter() {
		return this.interpreter;
	}

	public String getJobName() {
		return this.jobName;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public String getJobVersion() {
		return this.jobVersion;
	}

	public List<String> getJobContextParam() {
		return this.jobContextParam;
	}

	public String getJobResultDestDir() {
		return this.jobResultDestDir;
	}

	public Integer getTimeout() {
		return this.timeout;
	}

	public boolean isApplyContextToChildren() {
		return this.applyContextToChildren;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ExecuteJobCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */