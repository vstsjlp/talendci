package org.talend.commandline.client.command;

import java.util.List;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ExecuteJobOnServerCommand extends JavaServerCommand implements IExtensionCommand {
	private String jobName;
	private String executionServerName;
	private String jobContext;
	private String jobVersion;
	private String log4jLevel;
	private List<String> jobContextParam;
	private boolean applyContextToChildren;
	private String userName;
	private String jobResultDestDir;
	private Integer timeout;
	private String runAsUser;
	private String passwd;
	private boolean useSSL;
	private int statisticsPort;
	private int tracePort;

	public String getUserName() {
		return this.userName;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public ExecuteJobOnServerCommand(String jobName, String executionServerName, String jobContext, String jobVersion,
			List<String> jobContextParam, String jobResultDestDir, Integer timeout, boolean applyContextToChildren,
			int statisticsPort, int tracePort, boolean useSSL, String userName, String passwd, String runAsUser) {
		this.jobName = jobName;
		this.executionServerName = executionServerName;
		this.jobContext = jobContext;
		this.jobVersion = jobVersion;
		this.jobContextParam = jobContextParam;
		this.applyContextToChildren = applyContextToChildren;
		this.statisticsPort = statisticsPort;
		this.tracePort = tracePort;
		this.userName = userName;
		this.passwd = passwd;
		this.useSSL = useSSL;
		this.jobResultDestDir = jobResultDestDir;
		this.timeout = timeout;
		this.runAsUser = runAsUser;
	}

	public ExecuteJobOnServerCommand(String jobName, String executionServerName, String jobContext, String jobVersion,
			List<String> jobContextParam, String jobResultDestDir, Integer timeout, String log4jLevel,
			boolean applyContextToChildren, int statisticsPort, int tracePort, boolean useSSL, String userName,
			String passwd, String runAsUser) {
		this.jobName = jobName;
		this.executionServerName = executionServerName;
		this.jobContext = jobContext;
		this.jobVersion = jobVersion;
		this.jobContextParam = jobContextParam;
		this.log4jLevel = log4jLevel;
		this.applyContextToChildren = applyContextToChildren;
		this.statisticsPort = statisticsPort;
		this.tracePort = tracePort;
		this.userName = userName;
		this.passwd = passwd;
		this.useSSL = useSSL;
		this.jobResultDestDir = jobResultDestDir;
		this.timeout = timeout;
		this.runAsUser = runAsUser;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		buffer.append(" ");
		buffer.append(this.jobName);
		buffer.append(" on server ");
		buffer.append(this.executionServerName);
		if (this.jobContext != null) {
			buffer.append(" context ");
			buffer.append(this.jobContext);
		}
		if (this.jobVersion != null) {
			buffer.append(" version ");
			buffer.append(this.jobVersion);
		}
		if (this.jobContextParam != null) {
			for (String contextParam : this.jobContextParam) {
				buffer.append(" contextParam ");
				buffer.append(contextParam);
			}
		}
		if (this.useSSL) {
			buffer.append(" useSSL ");
			buffer.append(this.useSSL);
		}
		return buffer.toString();
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("executeJobOnServer");
		builder.addArgument(this.jobName);
		builder.addOptionWithArgument("es", this.executionServerName);

		if (this.jobContext != null) {
			builder.addOptionWithArgument("jc", this.jobContext);
		}

		if (this.jobVersion != null) {
			builder.addOptionWithArgument("jv", this.jobVersion);
		}

		if (this.log4jLevel != null) {
			builder.addOptionWithArgument("jall", this.log4jLevel);
		}

		if ((this.jobContextParam != null) && (this.jobContextParam.size() > 0)) {
			builder.addOption("jcp");
			for (String contextParam : this.jobContextParam) {
				builder.addArgument(contextParam);
			}
		}

		if (this.applyContextToChildren) {
			builder.addOption("jactc");
		}
		if (this.statisticsPort > -1) {
			builder.addOptionWithArgument("jsp", String.valueOf(this.statisticsPort));
		}
		if (this.tracePort > -1) {
			builder.addOptionWithArgument("jtp", String.valueOf(this.tracePort));
		}
		if (this.useSSL) {
			builder.addOption("useSSL");
		}

		if ((this.userName != null) && (this.userName.length() > 0)) {
			builder.addOptionWithArgument("un", this.userName);
			if (this.passwd != null) {
				builder.addOptionWithArgument("pd", this.passwd);
			}
		}
		if (this.jobResultDestDir != null) {
			builder.addOptionWithArgument("jrdd", this.jobResultDestDir);
		}

		if (this.timeout != null) {
			builder.addOptionWithArgument("jt", this.timeout.toString());
		}

		if (this.runAsUser != null) {
			builder.addOptionWithArgument("ra", this.runAsUser);
		}

		return builder.toString();
	}

	public String getJobName() {
		return this.jobName;
	}

	public String getExecutionServerName() {
		return this.executionServerName;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public String getJobVersion() {
		return this.jobVersion;
	}

	public String getLog4jLevel() {
		return this.log4jLevel;
	}

	public List<String> getJobContextParam() {
		return this.jobContextParam;
	}

	public boolean isApplyContextToChildren() {
		return this.applyContextToChildren;
	}

	public int getStatisticsPort() {
		return this.statisticsPort;
	}

	public int getTracePort() {
		return this.tracePort;
	}

	public boolean useSSL() {
		return this.useSSL;
	}

	public String getJobResultDestDir() {
		return this.jobResultDestDir;
	}

	public Integer getTimeout() {
		return this.timeout;
	}

	public String getRunAsUser() {
		return this.runAsUser;
	}

	public void setRunAsUser(String runAsUser) {
		this.runAsUser = runAsUser;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ExecuteJobOnServerCommand.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */