package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class CreateJobCommand extends AbstractExtensionCommand {
	private String jobName;
	private String scriptFile;
	private boolean overWrite;

	public CreateJobCommand(String jobName, String scriptFile) {
		this.jobName = jobName;
		this.scriptFile = scriptFile;
	}

	public CreateJobCommand(String jobName, String scriptFile, boolean overWrite) {
		this.jobName = jobName;
		this.scriptFile = scriptFile;
		this.overWrite = overWrite;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("createJob");
		if (this.jobName != null) {
			builder.addArgument(this.jobName);
		}
		if (this.scriptFile != null) {
			builder.addOptionWithArgument("sf", this.scriptFile);
		}
		if (this.overWrite) {
			builder.addOption("o");
		}
		return builder.toString();
	}

	public String getJobName() {
		return this.jobName;
	}

	public String getScriptFile() {
		return this.scriptFile;
	}

	public boolean getOverWrite() {
		return this.overWrite;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\CreateJobCommand.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */