 package org.talend.commandline.client.command;
 
 import org.talend.commandline.client.util.CommandStringBuilder;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class InitRemoteCommand
   extends JavaServerCommand
 {
   private String administratorUrl;
   private String userLogin;
   private String userPassword;
   
   public InitRemoteCommand(String administratorUrl)
   {
     this.administratorUrl = administratorUrl;
   }
   
   public InitRemoteCommand(String administratorUrl, String userLogin, String userPassword) {
     this.administratorUrl = administratorUrl;
     this.userLogin = userLogin;
     this.userPassword = userPassword;
   }
   
   public String getAdministratorUrl() {
     return this.administratorUrl;
   }
   
   public String getUser() {
     return this.userLogin;
   }
   
   public String getPassword() {
     return this.userPassword;
   }
   
   public String toString()
   {
     StringBuffer buffer = new StringBuffer(100);
     buffer.append(super.toString());
     buffer.append(" initRemote");
     return buffer.toString();
   }
   
   public String writeToString()
   {
     return writeToString(false);
   }
   
   public String writeToString(boolean forDisplay)
   {
     CommandStringBuilder builder = new CommandStringBuilder("initRemote");
     builder.addArgument(this.administratorUrl);
     if (this.userLogin != null) {
       builder.addOptionWithArgument("ul", this.userLogin);
       if ((this.userPassword != null) && (this.userPassword.length() > 0)) {
         builder.addOptionWithArgument("up", 
           getDisplayPassword(this.userPassword, forDisplay));
       }
     }
     return builder.toString();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\command\InitRemoteCommand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */