package org.talend.commandline.client.command;

public abstract class AbstractPublishCommand extends JavaServerCommand {
	protected String itemName;

	protected String repositoryUrl;

	protected String userName;

	protected String password;

	protected String itemVersion;

	protected String publishVersion;

	protected String artifactId;

	protected String groupName;

	protected boolean isSnapshot;

	protected AbstractPublishCommand(String itemName, String repositoryUrl, String userName, String password,
			String itemVersion, String artifactId, String publishVersion, String groupName, boolean isSnapshot) {
		this.itemName = itemName;
		this.repositoryUrl = repositoryUrl;
		this.userName = userName;
		this.password = password;
		this.itemVersion = itemVersion;
		this.artifactId = artifactId;
		this.publishVersion = publishVersion;
		this.groupName = groupName;
		this.isSnapshot = isSnapshot;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\AbstractPublishCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */