package org.talend.commandline.client.command;

import org.talend.commandline.client.util.CommandStringBuilder;

public class LogoffProjectCommand extends JavaServerCommand {
	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		buffer.append(" log off");
		return buffer.toString();
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("logoffProject");
		return builder.toString();
	}
}
