package org.talend.commandline.client.command;

import org.talend.commandline.client.filter.ItemFilter;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ExportAllJobCommand extends JavaServerCommand {
	private String destDir;
	private String jobContext;
	private String log4jLevel;
	private boolean applyContextToChildren;
	private ItemFilter itemFilter;

	public ExportAllJobCommand(String destDir, String jobContext, String log4jLevel, boolean applyContextToChildren,
			ItemFilter itemFilter) {
		this.destDir = destDir;
		this.jobContext = jobContext;
		this.log4jLevel = log4jLevel;
		this.applyContextToChildren = applyContextToChildren;
		this.itemFilter = itemFilter;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("exportAllJob");
		builder.addOptionWithArgument("dd", this.destDir);

		if (this.jobContext != null) {
			builder.addOptionWithArgument("jc", this.jobContext);
		}

		if (this.log4jLevel != null) {
			builder.addOptionWithArgument("jall", this.log4jLevel);
		}

		if (this.applyContextToChildren) {
			builder.addOption("jactc");
		}

		if (this.itemFilter != null) {
			builder.addOptionWithArgument("if", this.itemFilter.writeToString());
		}
		return builder.toString();
	}

	public String getDestDir() {
		return this.destDir;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public String getLog4jLevel() {
		return this.log4jLevel;
	}

	public boolean isApplyContextToChildren() {
		return this.applyContextToChildren;
	}

	public ItemFilter getItemFilter() {
		return this.itemFilter;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ExportAllJobCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */