package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class GenerateAuditReportServerCommand extends AbstractServerCommand {
	public GenerateAuditReportServerCommand() {
	}

	public GenerateAuditReportServerCommand(String auditId, String filePath, String template) {
		this();
		setValue("auditId", auditId);
		setValue("FILEPATH", filePath);

		setValue("TEMPLATE", template);
	}

	public GenerateAuditReportServerCommand(String auditId, String template) {
		this(auditId, null, template);
	}

	public String getAuditId() {
		return (String) getValue("auditId");
	}

	public String getFilePath() {
		return (String) getValue("FILEPATH");
	}

	public String getTemplate() {
		return (String) getValue("TEMPLATE");
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("generateAuditReport");
		String projectName = getAuditId();
		if (projectName != null) {
			builder.addArgument(projectName);
		}
		String branch = getFilePath();
		if (branch != null) {
			builder.addOptionWithArgument("fp", branch);
		}

		String template = getTemplate();
		if (template != null) {
			builder.addOptionWithArgument("t", template);
		}
		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\GenerateAuditReportServerCommand.
 * class Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */