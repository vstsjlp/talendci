package org.talend.commandline.client.command.extension;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractServerCommand extends AbstractExtensionCommand implements Cloneable {
	private int id;
	private boolean inner;
	private Map<String, Object> valuesMap;

	public AbstractServerCommand() {
		reset();
	}

	public final int getId() {
		return this.id;
	}

	protected final void setId(int id) {
		this.id = id;
	}

	public boolean isInner() {
		return this.inner;
	}

	protected final void setInner(boolean inner) {
		this.inner = inner;
	}

	protected Map<String, Object> getValuesMap() {
		if (this.valuesMap == null) {
			this.valuesMap = new HashMap();
		}
		return this.valuesMap;
	}

	public void setValuesMap(Map<String, Object> valuesMap) {
		this.valuesMap = valuesMap;
	}

	protected Object getValue(String key) {
		return getValuesMap().get(key);
	}

	protected boolean getBooleanValue(String key) {
		Object object = getValuesMap().get(key);
		if (object != null) {
			return Boolean.parseBoolean(object.toString());
		}
		return false;
	}

	public void setValue(String key, Object obj) {
		if (key != null) {
			getValuesMap().put(key, obj);
		}
	}

	public void reset() {
		if (!getValuesMap().isEmpty()) {
			getValuesMap().clear();
		}
	}

	public AbstractServerCommand clone() throws CloneNotSupportedException {
		AbstractServerCommand clone = (AbstractServerCommand) super.clone();
		clone.setId(getId());
		clone.setInner(isInner());
		clone.setValuesMap(null);
		for (String key : getValuesMap().keySet()) {
			clone.getValuesMap().put(key, getValuesMap().get(key));
		}
		return clone;
	}
}
