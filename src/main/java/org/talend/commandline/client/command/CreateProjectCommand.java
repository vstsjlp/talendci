package org.talend.commandline.client.command;

import org.talend.commandline.client.command.extension.AbstractServerCommand;
import org.talend.commandline.client.util.CommandStringBuilder;

public class CreateProjectCommand extends AbstractServerCommand {
	public CreateProjectCommand() {
	}

	public CreateProjectCommand(String projectName, String projectDescription, String projectLanguage,
			String projectAuthor, String projectAuthorPass) {
		this();
		setValue("PROJECT_NAME", projectName);
		setValue("PROJECT_DESCRIPTION", projectDescription);
		setValue("PROJECT_LANGUAGE", projectLanguage);
		setValue("PROJECT_AUTHOR", projectAuthor);
		setValue("PROJECT_AUTHOR_PASS", projectAuthorPass);
	}

	public CreateProjectCommand(String projectName, String projectDescription, String projectLanguage,
			String projectAuthor) {
		this(projectName, projectDescription, projectLanguage, projectAuthor, null);
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		buffer.append(" ");

		buffer.append(" name ");
		buffer.append(getProjectName());

		String projectDescription = getProjectDescription();
		if (projectDescription != null) {
			buffer.append(" description ");
			buffer.append(projectDescription);
		}

		buffer.append(" language ");
		buffer.append(getProjectLanguage());

		buffer.append(" author ");
		buffer.append(getProjectAuthor());

		String projectAuthorPass = getProjectAuthorPass();
		if (projectAuthorPass != null) {
			buffer.append(" password ");
			buffer.append(projectAuthorPass);
		}

		return buffer.toString();
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String writeToString(boolean forDisplay) {
		CommandStringBuilder builder = new CommandStringBuilder("createProject");
		builder.addOptionWithArgument("pn", getProjectName());
		String projectDescription = getProjectDescription();
		if (projectDescription != null) {
			builder.addOptionWithArgument("pd", projectDescription);
		}
		builder.addOptionWithArgument("pl", getProjectLanguage());
		builder.addOptionWithArgument("pa", getProjectAuthor());

		String projectAuthorPass = getProjectAuthorPass();
		if ((projectAuthorPass != null) && (projectAuthorPass.length() > 0)) {
			builder.addOptionWithArgument("pap", getDisplayPassword(projectAuthorPass, forDisplay));
		}
		return builder.toString();
	}

	public String getProjectName() {
		return (String) getValue("PROJECT_NAME");
	}

	public String getProjectDescription() {
		return (String) getValue("PROJECT_DESCRIPTION");
	}

	public String getProjectLanguage() {
		return (String) getValue("PROJECT_LANGUAGE");
	}

	public String getProjectAuthor() {
		return (String) getValue("PROJECT_AUTHOR");
	}

	public String getProjectAuthorPass() {
		return (String) getValue("PROJECT_AUTHOR_PASS");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\CreateProjectCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */