package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class PopulateAuditServerCommand extends AbstractServerCommand {
	public PopulateAuditServerCommand() {
	}

	public PopulateAuditServerCommand(String jdbcUrl, String dbDriver, String dbUser, String dbPassword) {
		this();
		setValue("JDBC_URL", jdbcUrl);
		setValue("DB_DRIVER", dbDriver);
		setValue("DB_USER", dbUser);
		setValue("USER_PASSWORD", dbPassword);
	}

	public String getJdbcUrl() {
		return (String) getValue("JDBC_URL");
	}

	public String getDbDriver() {
		return (String) getValue("DB_DRIVER");
	}

	public String getDbUser() {
		return (String) getValue("DB_USER");
	}

	public String getDbPassword() {
		return (String) getValue("USER_PASSWORD");
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String writeToString(boolean forDisplay) {
		CommandStringBuilder builder = new CommandStringBuilder("populateAudit");
		String jdbcUrl = getJdbcUrl();
		if (jdbcUrl != null) {
			builder.addOptionWithArgument("ju", jdbcUrl);
		}
		String dbDriver = getDbDriver();
		if (dbDriver != null) {
			builder.addOptionWithArgument("dd", dbDriver);
		}
		String dbUser = getDbUser();
		if (dbUser != null) {
			builder.addOptionWithArgument("du", dbUser);
			String dbPassword = getDbPassword();
			if ((dbPassword != null) && (dbPassword.length() > 0)) {
				builder.addOptionWithArgument("up", getDisplayPassword(dbPassword, forDisplay));
			}
		}
		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\PopulateAuditServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */