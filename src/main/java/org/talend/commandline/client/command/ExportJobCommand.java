package org.talend.commandline.client.command;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ExportJobCommand extends JavaServerCommand {
	private String destDir;
	private String jobName;
	private String jobContext;
	private String jobVersion;
	private String log4jLevel;
	private String archiveFileName;
	private boolean applyContextToChildren;
	private boolean addStatisticsCode;
	private boolean exportToOSGI;
	private boolean addAntScript;
	private boolean addMavenScript;

	public ExportJobCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, false, false, false,
				false);
	}

	public ExportJobCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean addStatisticsCode, boolean exportToOSGI) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, addStatisticsCode,
				exportToOSGI, false, false);
	}

	public ExportJobCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean addStatisticsCode, boolean exportToOSGI,
			boolean addAntScript, boolean addMavenScript) {
		this.destDir = destDir;
		this.archiveFileName = archiveFileName;
		this.jobName = jobName;
		this.jobContext = jobContext;
		this.jobVersion = jobVersion;
		this.applyContextToChildren = applyContextToChildren;
		this.addStatisticsCode = addStatisticsCode;
		this.exportToOSGI = exportToOSGI;
		this.addAntScript = addAntScript;
		this.addMavenScript = addMavenScript;
	}

	public ExportJobCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, String log4jLevel, boolean applyContextToChildren, boolean addStatisticsCode,
			boolean exportToOSGI, boolean addAntScript, boolean addMavenScript) {
		this.destDir = destDir;
		this.archiveFileName = archiveFileName;
		this.jobName = jobName;
		this.jobContext = jobContext;
		this.jobVersion = jobVersion;
		this.log4jLevel = log4jLevel;
		this.applyContextToChildren = applyContextToChildren;
		this.addStatisticsCode = addStatisticsCode;
		this.exportToOSGI = exportToOSGI;
		this.addAntScript = addAntScript;
		this.addMavenScript = addMavenScript;
	}

	public ExportJobCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean exportToOSGI) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, false, exportToOSGI,
				false, false);
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		buffer.append(" ");
		buffer.append(this.jobName);
		if (this.jobContext != null) {
			buffer.append(" context ");
			buffer.append(this.jobContext);
		}
		if (this.jobVersion != null) {
			buffer.append(" version ");
			buffer.append(this.jobVersion);
		}
		return buffer.toString();
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("exportJob");
		builder.addArgument(this.jobName);
		if (this.destDir != null) {
			builder.addOptionWithArgument("dd", this.destDir);
		}

		if (this.archiveFileName != null) {
			builder.addOptionWithArgument("af", this.archiveFileName);
		}

		if (this.jobContext != null) {
			builder.addOptionWithArgument("jc", this.jobContext);
		}

		if (this.log4jLevel != null) {
			builder.addOptionWithArgument("jall", this.log4jLevel);
		}

		if (this.jobVersion != null) {
			builder.addOptionWithArgument("jv", this.jobVersion);
		}

		if (this.applyContextToChildren) {
			builder.addOption("jactc");
		}

		if (this.addStatisticsCode) {
			builder.addOption("jstats");
		}

		if (this.exportToOSGI) {
			builder.addOption("eo");
		}

		if (this.addAntScript) {
			builder.addOption("ant");
		}

		if (this.addMavenScript) {
			builder.addOption("maven");
		}

		return builder.toString();
	}

	public String getDestDir() {
		return this.destDir;
	}

	public String getJobName() {
		return this.jobName;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public String getJobVersion() {
		return this.jobVersion;
	}

	public String getArchiveFileName() {
		return this.archiveFileName;
	}

	public boolean isApplyContextToChildren() {
		return this.applyContextToChildren;
	}

	public boolean isAddStatisticsCode() {
		return this.addStatisticsCode;
	}

	public boolean isExportToOSGI() {
		return this.exportToOSGI;
	}

	public boolean isAddAntScript() {
		return this.addAntScript;
	}

	public boolean isAddMavenScript() {
		return this.addMavenScript;
	}

	public String getLog4jLevel() {
		return this.log4jLevel;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ExportJobCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */