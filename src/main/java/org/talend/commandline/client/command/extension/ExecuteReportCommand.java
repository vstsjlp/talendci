package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ExecuteReportCommand extends AbstractExtensionCommand {
	private String reportNames;
	private String reportFiles;
	private String reportContextName;

	public ExecuteReportCommand(String reportNames, String reportFiles, String reportContextName) {
		this.reportNames = reportNames;
		this.reportFiles = reportFiles;
		this.reportContextName = reportContextName;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("executeReport");
		if (this.reportNames != null) {
			builder.addOptionWithArgument("n", this.reportNames);
		}
		if (this.reportFiles != null) {
			builder.addOptionWithArgument("p", this.reportFiles);
		}
		if (this.reportContextName != null) {
			builder.addOptionWithArgument("pc", this.reportContextName);
		}
		return builder.toString();
	}

	public String getReportFiles() {
		return this.reportFiles;
	}

	public String getReportNames() {
		return this.reportNames;
	}

	public String getReportContextName() {
		return this.reportContextName;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\ExecuteReportCommand.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */