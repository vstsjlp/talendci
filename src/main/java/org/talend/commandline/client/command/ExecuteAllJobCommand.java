package org.talend.commandline.client.command;

import java.util.List;
import org.talend.commandline.client.filter.ItemFilter;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ExecuteAllJobCommand extends JavaServerCommand {
	private String interpreter;
	private List<String> jobContextParam;
	protected String jobResultDestDir;
	private Integer timeout;
	private String jobContext;
	private boolean applyContextToChildren;
	private ItemFilter itemFilter;

	public ExecuteAllJobCommand(String interpreter, String jobContext, List<String> jobContextParam,
			String jobResultDestDir, Integer timeout, boolean applyContextToChildren, ItemFilter itemFilter) {
		this.interpreter = interpreter;
		this.jobContext = jobContext;
		this.jobContextParam = jobContextParam;
		this.jobResultDestDir = jobResultDestDir;
		this.timeout = timeout;
		this.applyContextToChildren = applyContextToChildren;
		this.itemFilter = itemFilter;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("executeAllJob");
		builder.addOptionWithArgument("i", this.interpreter);

		if (this.jobContext != null) {
			builder.addOptionWithArgument("jc", this.jobContext);
		}

		if ((this.jobContextParam != null) && (this.jobContextParam.size() > 0)) {
			builder.addOption("jcp");
			for (String contextParam : this.jobContextParam) {
				builder.addArgument(contextParam);
			}
		}

		if (this.jobResultDestDir != null) {
			builder.addOptionWithArgument("jrdd", this.jobResultDestDir);
		}

		if (this.timeout != null) {
			builder.addOptionWithArgument("jt", this.timeout.toString());
		}

		if (this.applyContextToChildren) {
			builder.addOption("jactc");
		}
		if (this.itemFilter != null) {
			builder.addOptionWithArgument("if", this.itemFilter.writeToString());
		}
		return builder.toString();
	}

	public String getInterpreter() {
		return this.interpreter;
	}

	public List<String> getJobContextParam() {
		return this.jobContextParam;
	}

	public String getJobResultDestDir() {
		return this.jobResultDestDir;
	}

	public Integer getTimeout() {
		return this.timeout;
	}

	public String getJobContext() {
		return this.jobContext;
	}

	public boolean isApplyContextToChildren() {
		return this.applyContextToChildren;
	}

	public ItemFilter getItemFilter() {
		return this.itemFilter;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\ExecuteAllJobCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */