package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ExportRouteServerCommand extends AbstractServerCommand {
	public ExportRouteServerCommand() {
	}

	public ExportRouteServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean addStatisticsCode, boolean exportTOOsgi,
			boolean addAntScript, boolean addMavenScript, String buildType) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, false, exportTOOsgi,
				addAntScript, addMavenScript, "Runtime", false, false);
	}

	public ExportRouteServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean addStatisticsCode, boolean exportTOOsgi,
			boolean addAntScript, boolean addMavenScript, String buildType, boolean exportAsZip,
			boolean onlyExportDefaultContext) {
		this();
		setValue("DESTINATION_DIRECTORY", destDir);
		setValue("AECHIVE_FILENAME", archiveFileName);
		setValue("ROUTE_NAME", jobName);
		setValue("JOB_CONTEXT", jobContext);
		setValue("JOB_VERSION", jobVersion);
		setValue("JOB_APPLY_CONTEXT_TO_CHILDREN", Boolean.valueOf(applyContextToChildren));
		setValue("JOB_ADD_STATISTICS_CODE", Boolean.valueOf(addStatisticsCode));
		setValue("JOB_ADD_MAVEN_SCRIPT", Boolean.valueOf(addMavenScript));
		setValue("JOB_BUILD_TYPE", buildType);
		setValue("JOB_EXPORT_AS_ZIP", Boolean.valueOf(exportAsZip));
		setValue("JOB_EXPORT_ONLY_DEFAULT_CONTEXT", Boolean.valueOf(onlyExportDefaultContext));
	}

	public ExportRouteServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean exportTOOsgi, boolean addAntScript,
			boolean addMavenScript) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, false, exportTOOsgi,
				addAntScript, addMavenScript, "Runtime");
	}

	public ExportRouteServerCommand(String destDir, String archiveFileName, String jobName, String jobContext,
			String jobVersion, boolean applyContextToChildren, boolean exportTOOsgi, boolean addAntScript,
			boolean addMavenScript, String buildType) {
		this(destDir, archiveFileName, jobName, jobContext, jobVersion, applyContextToChildren, false, exportTOOsgi,
				addAntScript, addMavenScript, buildType);
	}

	public String getDestDir() {
		return (String) getValue("DESTINATION_DIRECTORY");
	}

	public String getJobName() {
		return (String) getValue("ROUTE_NAME");
	}

	public String getJobContext() {
		return (String) getValue("JOB_CONTEXT");
	}

	public String getJobVersion() {
		return (String) getValue("JOB_VERSION");
	}

	public String getBuildType() {
		return (String) getValue("JOB_BUILD_TYPE");
	}

	public String getArchiveFileName() {
		return (String) getValue("AECHIVE_FILENAME");
	}

	public boolean isApplyContextToChildren() {
		return getBooleanValue("JOB_APPLY_CONTEXT_TO_CHILDREN");
	}

	public boolean isAddStatisticsCode() {
		return getBooleanValue("JOB_ADD_STATISTICS_CODE");
	}

	public boolean isAddMavenScript() {
		return getBooleanValue("JOB_ADD_MAVEN_SCRIPT");
	}

	public boolean isExportAsZip() {
		return getBooleanValue("JOB_EXPORT_AS_ZIP");
	}

	public boolean isOnlyExportDefaultContext() {
		return getBooleanValue("JOB_EXPORT_ONLY_DEFAULT_CONTEXT");
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("exportRoute");

		builder.addArgument(getJobName());

		String destDir = getDestDir();
		if (destDir != null) {
			builder.addOptionWithArgument("dd", destDir);
		}

		String archiveFileName = getArchiveFileName();
		if (archiveFileName != null) {
			builder.addOptionWithArgument("af", archiveFileName);
		}

		String jobVersion = getJobVersion();
		if (jobVersion != null) {
			builder.addOptionWithArgument("jv", jobVersion);
		}
		String jobContext = getJobContext();
		if (jobContext != null) {
			builder.addOptionWithArgument("jc", jobContext);
		}
		boolean isApplyContextToChildren = isApplyContextToChildren();
		if (isApplyContextToChildren) {
			builder.addOption("jactc");
		}
		boolean isAddStatisticsCode = isAddStatisticsCode();
		if (isAddStatisticsCode) {
			builder.addOption("jstats");
		}

		boolean isAddMavenScript = isAddMavenScript();
		if (isAddMavenScript) {
			builder.addOption("JOB_ADD_MAVEN_SCRIPT");
		}

		String buildType = getBuildType();
		if (buildType != null) {
			builder.addOptionWithArgument("JOB_BUILD_TYPE", buildType);
		}

		boolean isExportAsZip = isExportAsZip();
		if (isExportAsZip) {
			builder.addOption("JOB_EXPORT_AS_ZIP");
		}

		boolean isOnlyExportDefaultContext = isOnlyExportDefaultContext();
		if (isOnlyExportDefaultContext) {
			builder.addOption("JOB_EXPORT_ONLY_DEFAULT_CONTEXT");
		}

		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\ExportRouteServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */