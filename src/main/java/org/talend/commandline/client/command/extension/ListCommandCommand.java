 package org.talend.commandline.client.command.extension;
 
 import org.talend.commandline.client.util.CommandStringBuilder;
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 public class ListCommandCommand
   extends AbstractServerCommand
 {
   public ListCommandCommand() {}
   
   public ListCommandCommand(boolean listAllCommand, boolean listQueueCommand, boolean listRunningCommand)
   {
     this();
     setValue("ALL", Boolean.valueOf(listAllCommand));
     setValue("QUEUE", Boolean.valueOf(listQueueCommand));
     setValue("RUN", Boolean.valueOf(listRunningCommand));
   }
   
   public boolean isListAllCommand() {
     return getBooleanValue("ALL");
   }
   
   public boolean isListQueueCommand() {
     return getBooleanValue("QUEUE");
   }
   
   public boolean isListRunningCommand() {
     return getBooleanValue("RUN");
   }
   
   public String writeToString() {
     CommandStringBuilder builder = new CommandStringBuilder("listCommand");
     if (isListAllCommand()) {
       builder.addOption("a");
     } else if ((isListQueueCommand()) && (isListRunningCommand())) {
       builder.addOption("q");
       builder.addOption("r");
     } else if (isListQueueCommand()) {
       builder.addOption("q");
     } else if (isListRunningCommand()) {
       builder.addOption("r");
     }
     
     return builder.toString();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\command\extension\ListCommandCommand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */