package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ListRoutesServerCommand extends AbstractServerCommand {
	public String writeToString() {
		return new CommandStringBuilder("listRoute").toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\ListRoutesServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */