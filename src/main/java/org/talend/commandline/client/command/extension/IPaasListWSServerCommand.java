 package org.talend.commandline.client.command.extension;
 
 import org.talend.commandline.client.util.CommandStringBuilder;
 
 public class IPaasListWSServerCommand extends AbstractServerCommand implements org.talend.commandline.client.constant.extension.IPaasCommandDefine
 {
   public String getUserName()
   {
     return (String)getValue("USER_NAME");
   }
   
   public String getPassword() {
     return (String)getValue("PASSWORD");
   }
   
   public String getURL() {
     return (String)getValue("INVENTORY_URL");
   }
   
   public String getItemName() {
     return (String)getValue("ITEM_NAME");
   }
   
   public String writeToString() {
     CommandStringBuilder builder = new CommandStringBuilder(
       "listCloudWorkspaces");
     
     String userName = getUserName();
     if (userName != null) {
       builder.addOptionWithArgument("u", userName);
     }
     
     String url = getURL();
     if (url != null) {
       builder.addOptionWithArgument("r", url);
     }
     
     String itemName = getItemName();
     if (itemName != null) {
       builder.addOptionWithArgument("ITEM_NAME", itemName);
     }
     
     return builder.toString();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\command\extension\IPaasListWSServerCommand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */