package org.talend.commandline.client.command.extension;

import java.util.List;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ExecuteRouteServerCommand extends AbstractServerCommand {
	public ExecuteRouteServerCommand() {
	}

	public ExecuteRouteServerCommand(String interpreter, String name, String context, String version,
			List<String> jobContextParam, String jobResultDestDir, Integer timeout, boolean applyContextToChildren) {
		this();
		setValue("INTERPRETER", interpreter);
		setValue("routeName", name);
		setValue("JOB_VERSION", version);
		setValue("JOB_CONTEXT", context);
		setValue("JOB_CONTEXT_PARAM", jobContextParam);
		setValue("JOB_RESULT_DESTINATION_DIR", jobResultDestDir);
		setValue("JOB_TIMEOUT", timeout);
		setValue("JOB_APPLY_CONTEXT_TO_CHILDREN", Boolean.valueOf(applyContextToChildren));
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("executeRoute");
		String jobName = getJobName();
		if (jobName != null) {
			builder.addArgument(jobName);
		}
		String interPreter = getInterPreter();
		if (interPreter != null) {
			builder.addOptionWithArgument("i", interPreter);
		}
		String version = getVersion();
		if (version != null) {
			builder.addOptionWithArgument("jv", version);
		}
		String jobContext = getJobConntext();
		if (jobContext != null) {
			builder.addOptionWithArgument("jc", jobContext);
		}
		List<String> jobContextParam = getJobContextParam();
		if ((jobContextParam != null) && (jobContextParam.size() > 0)) {
			builder.addOption("jcp");
			for (String contextParam : jobContextParam) {
				builder.addArgument(contextParam);
			}
		}
		String jobResultDestDir = getjobResultDestDir();
		if (jobResultDestDir != null) {
			builder.addOptionWithArgument("jrdd", jobResultDestDir);
		}
		Integer timeout = getTimeOut();
		if (timeout != null) {
			builder.addOptionWithArgument("jt", timeout.toString());
		}
		boolean applayContext = isApplyContextToChildren();
		if (applayContext) {
			builder.addOption("jactc");
		}
		return builder.toString();
	}

	public String getInterPreter() {
		return (String) getValue("INTERPRETER");
	}

	public String getJobName() {
		return (String) getValue("routeName");
	}

	public String getVersion() {
		return (String) getValue("JOB_VERSION");
	}

	public String getJobConntext() {
		return (String) getValue("JOB_CONTEXT");
	}

	public List<String> getJobContextParam() {
		return (List) getValue("JOB_CONTEXT_PARAM");
	}

	public String getjobResultDestDir() {
		return (String) getValue("JOB_RESULT_DESTINATION_DIR");
	}

	public Integer getTimeOut() {
		return (Integer) getValue("JOB_TIMEOUT");
	}

	public boolean isApplyContextToChildren() {
		return getBooleanValue("JOB_APPLY_CONTEXT_TO_CHILDREN");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\ExecuteRouteServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */