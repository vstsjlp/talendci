package org.talend.commandline.client.command.extension;

import java.util.List;
import org.talend.commandline.client.util.CommandStringBuilder;

public class HelpServerCommand extends AbstractServerCommand {
	public HelpServerCommand() {
	}

	public HelpServerCommand(List<String> commandNames) {
		this();
		setValue("commandName", commandNames);
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("help");
		List<String> commandNames = getCommandNames();
		for (String singlieCommand : commandNames) {
			builder.addArgument(singlieCommand);
		}
		return builder.toString();
	}

	public List<String> getCommandNames() {
		return (List) getValue("commandName");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\HelpServerCommand.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */