package org.talend.commandline.client.command.extension;

import org.apache.commons.cli2.CommandLine;
import org.apache.commons.cli2.Option;

public abstract interface IExecuteCommand {
	public abstract AbstractServerCommand getServerCommand();

	public abstract void setValues(CommandLine paramCommandLine, Option paramOption) throws Exception;

	public abstract Object execute() throws Exception;
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\IExecuteCommand.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */