package org.talend.commandline.client.command;

import org.talend.commandline.client.util.CommandStringBuilder;

public class InitLocalCommand extends JavaServerCommand {
	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		buffer.append(" initLocal");
		return buffer.toString();
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("initLocal");
		return builder.toString();
	}
}
