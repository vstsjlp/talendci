package org.talend.commandline.client.command;

public abstract interface IJavaCommand {
	public abstract String writeToString();
}
