package org.talend.commandline.client.command;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ImportDelimitedMetadataCommand extends JavaServerCommand {
	private String delimitedMetadataFilePath;

	public ImportDelimitedMetadataCommand(String delimitedMetadataFilePath) {
		this.delimitedMetadataFilePath = delimitedMetadataFilePath;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("importDelimitedMetadata");
		builder.addArgument(this.delimitedMetadataFilePath);
		return builder.toString();
	}

	public String getDelimitedMetadataFilePath() {
		return this.delimitedMetadataFilePath;
	}
}
