package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.command.CommandGroupCommand;
import org.talend.commandline.client.command.ExecuteJobOnServerCommand;
import org.talend.commandline.client.command.ExportItemsCommand;
import org.talend.commandline.client.command.IExtensionCommand;
import org.talend.commandline.client.command.ImportItemsCommand;

public abstract class AbstractExtensionCommandSwitch<T, X extends Exception> {
	public T doSwitch(IExtensionCommand command, Object cmdProcessor) throws Exception {
		if ((command instanceof AbstractServerCommand))
			return (T) caseExtensionServerCommand((AbstractServerCommand) command, cmdProcessor);
		if ((command instanceof SetUserComponentPathCommand))
			return (T) caseSetUserComponentPathCommand((SetUserComponentPathCommand) command, cmdProcessor);
		if ((command instanceof ExecuteReportCommand))
			return (T) caseExecuteReportCommand((ExecuteReportCommand) command, cmdProcessor);
		if ((command instanceof ImportItemsCommand))
			return (T) caseImportItemsCommand((ImportItemsCommand) command, cmdProcessor);
		if ((command instanceof ExportItemsCommand))
			return (T) caseExportItemsCommand((ExportItemsCommand) command, cmdProcessor);
		if ((command instanceof CommandGroupCommand))
			return (T) caseCommandGroupCommand((CommandGroupCommand) command, cmdProcessor);
		if ((command instanceof ExecuteJobOnServerCommand))
			return (T) caseExecuteJobOnServerCommand((ExecuteJobOnServerCommand) command, cmdProcessor);
		if ((command instanceof CreateJobCommand)) {
			return (T) caseCreateJobCommand((CreateJobCommand) command, cmdProcessor);
		}

		if ((command instanceof DeployJobToServerCommand)) {
			return (T) caseDeployJobToServerCommand((DeployJobToServerCommand) command, cmdProcessor);
		}
		return (T) defaultCase(command);
	}

	public T defaultCase(IExtensionCommand command) throws Exception {
		return null;
	}

	public abstract T caseExtensionServerCommand(AbstractServerCommand paramAbstractServerCommand, Object paramObject);

	public abstract T caseSetUserComponentPathCommand(SetUserComponentPathCommand paramSetUserComponentPathCommand,
			Object paramObject) throws Exception;

	public abstract T caseExecuteReportCommand(ExecuteReportCommand paramExecuteReportCommand, Object paramObject)
			throws Exception;

	public abstract T caseImportItemsCommand(ImportItemsCommand paramImportItemsCommand, Object paramObject)
			throws Exception;

	public abstract T caseExportItemsCommand(ExportItemsCommand paramExportItemsCommand, Object paramObject)
			throws Exception;

	public abstract T caseCommandGroupCommand(CommandGroupCommand paramCommandGroupCommand, Object paramObject)
			throws Exception;

	public abstract T caseExecuteJobOnServerCommand(ExecuteJobOnServerCommand paramExecuteJobOnServerCommand,
			Object paramObject) throws Exception;

	public abstract T caseCreateJobCommand(CreateJobCommand paramCreateJobCommand, Object paramObject) throws Exception;

	public abstract T caseDeployJobToServerCommand(DeployJobToServerCommand paramDeployJobToServerCommand,
			Object paramObject) throws Exception;
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\AbstractExtensionCommandSwitch.
 * class Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */