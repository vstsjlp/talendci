package org.talend.commandline.client.command;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CommandStatus implements Serializable {
	private static final String INDENT = "  ";
	private static final String DBL_INDENT = "    ";
	private List<String> messages;
	private static final long serialVersionUID = 6384695812137761711L;
	private CommandStatusLevel level;
	private Exception exception;
	private Date levelDate;
	private Map<ProcessInfo, ExecutionCommandStatus> executionCommandStatus;
	private String result;

	public static enum CommandStatusLevel implements Serializable {
		NEW, WAITING, RUNNING, COMPLETED, FAILED;
	}

	public static class ExecutionCommandStatus implements Serializable {
		private static final long serialVersionUID = 2983675194174861330L;

		private String standardOutput;

		private String errorOutput;

		private int exitValue;

		private long executionTime;

		public ExecutionCommandStatus(String standardOutput, String errorOutput, int exitValue, long executionTime) {
			this.standardOutput = standardOutput;
			this.errorOutput = errorOutput;
			this.exitValue = exitValue;
			this.executionTime = executionTime;
		}

		public String getStandardOutput() {
			return this.standardOutput;
		}

		public String getErrorOutput() {
			return this.errorOutput;
		}

		public int getExitValue() {
			return this.exitValue;
		}

		public long getExecutionTime() {
			return this.executionTime;
		}

		public String toString(String indent) {
			StringBuffer buffer = new StringBuffer(200);

			buffer.append(indent);
			buffer.append("Exit Value : ");
			buffer.append(this.exitValue);
			buffer.append('\n');

			if (this.standardOutput != null) {
				buffer.append(indent);
				buffer.append("StdOut : ");
				buffer.append(this.standardOutput);
				buffer.append('\n');
			}

			if (this.errorOutput != null) {
				buffer.append(indent);
				buffer.append("ErrOut : ");
				buffer.append(this.errorOutput);
				buffer.append('\n');
			}

			return buffer.toString();
		}

		public String toString() {
			return toString("");
		}
	}

	public static class ProcessInfo implements Serializable {
		private static final long serialVersionUID = 3390129067874059538L;

		private String label;

		public ProcessInfo(String label) {
			this.label = label;
		}

		public String getLabel() {
			return this.label;
		}

		public String toString() {
			return this.label;
		}
	}

	public CommandStatus() {
		this.level = CommandStatusLevel.NEW;
		this.levelDate = new Date();
	}

	public CommandStatusLevel getLevel() {
		return this.level;
	}

	public void setLevel(CommandStatusLevel level) {
		this.level = level;
		this.levelDate = new Date();
	}

	public Date getLevelDate() {
		return this.levelDate;
	}

	public void setLevelDate(Date levelDate) {
		this.levelDate = levelDate;
	}

	public void setExecutionCommandStatus(Map<ProcessInfo, ExecutionCommandStatus> executionCommandStatus) {
		this.executionCommandStatus = executionCommandStatus;
	}

	public Map<ProcessInfo, ExecutionCommandStatus> getExecutionCommandStatus() {
		return this.executionCommandStatus;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Exception getException() {
		return this.exception;
	}

	public void setException(Exception exception) {
		this.exception = new CommandStatusWrapException(exception);
	}

	public List<String> getMessages() {
		return this.messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(500);
		buffer.append(getLevel());
		buffer.append(" at ");
		buffer.append(getLevelDate());
		buffer.append('\n');
		if (getResult() != null) {
			buffer.append("Result : ");
			buffer.append(getResult());
			buffer.append('\n');
		}
		if (getException() != null) {
			buffer.append("exception : ");
			buffer.append(exceptionToString(getException()));
			buffer.append('\n');
		}
		if (getExecutionCommandStatus() != null) {
			for (ProcessInfo processInfo : getExecutionCommandStatus().keySet()) {
				buffer.append("  ");
				buffer.append("execution result for ");
				buffer.append(processInfo);
				buffer.append('\n');
				buffer.append(((ExecutionCommandStatus) getExecutionCommandStatus().get(processInfo)).toString("    "));
			}
		}
		if (getMessages() != null) {
			for (String message : getMessages()) {
				buffer.append(message);
				buffer.append('\n');
			}
		}

		return buffer.toString();
	}

	private String exceptionToString(Exception exception) {
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\CommandStatus.class Java compiler version:
 * 8 (52.0) JD-Core Version: 0.7.1
 */