package org.talend.commandline.client.command;

import org.talend.commandline.client.filter.ItemFilter;
import org.talend.commandline.client.util.CommandStringBuilder;

public class ImportItemsCommand extends JavaServerCommand implements IExtensionCommand {
	private String importItemsSource;
	private ItemFilter itemFilter;
	private String itemFilterAsString;
	private boolean overwrite;
	private boolean importStatus;
	private boolean importStatslogs;
	private boolean importImplicit;
	private boolean importRefprojects;

	public ImportItemsCommand(String importItemsSource, boolean overwrite) {
		this(importItemsSource, null, overwrite, false, false, false, false);
	}

	public ImportItemsCommand(String importItemsSource, ItemFilter itemFilter, boolean overwrite) {
		this(importItemsSource, itemFilter, overwrite, false, false, false, false);
	}

	public ImportItemsCommand(String importItemsSource, ItemFilter itemFilter, boolean overwrite, boolean importStatus,
			boolean importStatslogs) {
		this(importItemsSource, itemFilter, overwrite, importStatus, importStatslogs, false, false);
	}

	public ImportItemsCommand(String importItemsSource, ItemFilter itemFilter, boolean overwrite, boolean importStatus,
			boolean importStatslogs, boolean importImplicit) {
		this(importItemsSource, itemFilter, overwrite, importStatus, importStatslogs, false, importImplicit);
	}

	protected ImportItemsCommand(String importItemsSource, ItemFilter itemFilter, boolean overwrite,
			boolean importStatus, boolean importStatslogs, boolean importRefprojects, boolean importImplicit) {
		this.importItemsSource = importItemsSource;
		this.overwrite = overwrite;
		this.itemFilter = itemFilter;
		this.importStatus = importStatus;
		this.importStatslogs = importStatslogs;
		this.importRefprojects = importRefprojects;
		this.importImplicit = importImplicit;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("importItems");
		builder.addArgument(this.importItemsSource);

		if (this.itemFilter != null) {
			builder.addOptionWithArgument("if", this.itemFilter.writeToString());
		} else if (this.itemFilterAsString != null) {
			builder.addOptionWithArgument("if", this.itemFilterAsString);
		}

		if (this.overwrite) {
			builder.addOption("o");
		}
		if (this.importStatus) {
			builder.addOption("s");
		}
		if (this.importImplicit) {
			builder.addOption("im");
		}
		if (this.importStatslogs) {
			builder.addOption("sl");
		}

		return builder.toString();
	}

	public String getImportItemsSource() {
		return this.importItemsSource;
	}

	public ItemFilter getItemFilter() {
		return this.itemFilter;
	}

	public boolean isOverwrite() {
		return this.overwrite;
	}

	public boolean isImportStatus() {
		return this.importStatus;
	}

	public boolean isImportStatslogs() {
		return this.importStatslogs;
	}

	public boolean isImportImplicit() {
		return this.importImplicit;
	}

	public boolean isImportRefprojects() {
		return this.importRefprojects;
	}
}
