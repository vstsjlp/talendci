package org.talend.commandline.client.command;

import org.talend.commandline.client.filter.ItemFilter;
import org.talend.commandline.client.util.CommandStringBuilder;

public class DeleteItemsCommand extends JavaServerCommand {
	private ItemFilter itemFilter;

	public DeleteItemsCommand(ItemFilter itemFilter) {
		this.itemFilter = itemFilter;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("deleteItems");

		if (this.itemFilter != null) {
			builder.addOptionWithArgument("if", this.itemFilter.writeToString());
		}

		return builder.toString();
	}

	public ItemFilter getItemFilter() {
		return this.itemFilter;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\DeleteItemsCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */