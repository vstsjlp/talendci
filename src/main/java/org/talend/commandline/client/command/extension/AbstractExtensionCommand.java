package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.command.IExtensionCommand;
import org.talend.commandline.client.command.JavaServerCommand;

public abstract class AbstractExtensionCommand extends JavaServerCommand implements IExtensionCommand {
	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(getClass().getSimpleName());
		buffer.append(" ");
		buffer.append(writeToString());
		return buffer.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\AbstractExtensionCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */