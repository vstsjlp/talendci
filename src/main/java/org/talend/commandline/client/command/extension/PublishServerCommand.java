package org.talend.commandline.client.command.extension;

public abstract class PublishServerCommand extends AbstractServerCommand {
	public String getArtifactRepository() {
		return (String) getValue("ARTIFACT_REPOSITORY");
	}

	public String getUsername() {
		return (String) getValue("USERNAME");
	}

	public String getPassword() {
		return (String) getValue("PASSWORD");
	}

	public boolean isSnapshot() {
		return getBooleanValue("SNAPSHOT");
	}

	public String getPublishVersion() {
		return (String) getValue("PUBLISH_VERSION");
	}

	public String getArtifactId() {
		return (String) getValue("ARTIFACT");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\PublishServerCommand.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */