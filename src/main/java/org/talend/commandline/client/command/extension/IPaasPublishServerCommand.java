 package org.talend.commandline.client.command.extension;
 
 import org.talend.commandline.client.util.CommandStringBuilder;
 
 public class IPaasPublishServerCommand extends AbstractServerCommand implements org.talend.commandline.client.constant.extension.IPaasCommandDefine
 {
   public String getUserName()
   {
     return (String)getValue("USER_NAME");
   }
   
   public String getPassword() {
     return (String)getValue("PASSWORD");
   }
   
   public String getURL() {
     return (String)getValue("INVENTORY_URL");
   }
   
   public String getItemName() {
     return (String)getValue("ITEM_NAME");
   }
   
   public String getPublishVersion() {
     return (String)getValue("PUBLISH_VERSION");
   }
   
 
 
 
 
 
   public String getWorkspace()
   {
     return (String)getValue("WORKSPACE");
   }
   
   public boolean isExportSources() {
     return getBooleanValue("EXPORT_SOURCES");
   }
   
   public boolean isExportImage() {
     return getBooleanValue("EXPORT_IMAGE");
   }
   
   public boolean isNotAccelerate() {
     return getBooleanValue("NOT_ACCELERATE");
   }
   
   public String writeToString() {
     CommandStringBuilder builder = new CommandStringBuilder("publishAction");
     
     String itemName = getItemName();
     if (itemName != null) {
       builder.addArgument(itemName);
     }
     
     String userName = getUserName();
     if (userName != null) {
       builder.addOptionWithArgument("u", userName);
     }
     
     String password = getPassword();
     if (password != null) {
       builder.addOptionWithArgument("p", password);
     }
     
     String url = getURL();
     if (url != null) {
       builder.addOptionWithArgument("r", url);
     }
     
     String publishVersion = getPublishVersion();
     if (publishVersion != null) {
       builder.addOptionWithArgument("v", publishVersion);
     }
     
 
 
     String workspaceId = getWorkspace();
     if (workspaceId != null) {
       builder.addOptionWithArgument("w", workspaceId);
     }
     
     if (isExportSources()) {
       builder.addOption("s");
     }
     
     if (isExportImage()) {
       builder.addOption("i");
     }
     
     if (isNotAccelerate()) {
       builder.addOption("na");
     }
     
     return builder.toString();
   }
 }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\command\extension\IPaasPublishServerCommand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */