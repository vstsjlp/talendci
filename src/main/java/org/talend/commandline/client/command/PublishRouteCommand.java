package org.talend.commandline.client.command;

import org.talend.commandline.client.util.CommandStringBuilder;

public class PublishRouteCommand extends AbstractPublishCommand {
	public PublishRouteCommand(String itemName, String repositoryUrl, String userName, String password,
			String itemVersion, String artifactId, String publishVersion, String groupName, boolean isSnapshot) {
		super(itemName, repositoryUrl, userName, password, itemVersion, artifactId, publishVersion, groupName,
				isSnapshot);
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String writeToString(boolean forDisplay) {
		CommandStringBuilder builder = new CommandStringBuilder("publishRoute");
		builder.addArgument(this.itemName);
		builder.addOptionWithArgument("r", this.repositoryUrl);
		if (this.userName != null) {
			builder.addOptionWithArgument("u", this.userName);
			if ((this.password != null) && (this.password.length() > 0)) {
				builder.addOptionWithArgument("p", getDisplayPassword(this.password, forDisplay));
			}
		}

		builder.addOptionWithArgument("v", this.itemVersion);
		builder.addOptionWithArgument("a", this.artifactId);
		builder.addOptionWithArgument("pv", this.publishVersion);
		builder.addOptionWithArgument("g", this.groupName);
		if (this.isSnapshot) {
			builder.addOption("s");
		}

		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\PublishRouteCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */