package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class PopulateAndGenerateReportServerCommand extends AbstractServerCommand {
	public PopulateAndGenerateReportServerCommand() {
	}

	public PopulateAndGenerateReportServerCommand(String jdbcUrl, String dbDriver, String dbUser, String dbPassword,
			String filePath, String template) {
		this();
		setValue("JDBC_URL", jdbcUrl);
		setValue("DB_DRIVER", dbDriver);
		setValue("DB_USER", dbUser);
		setValue("USER_PASSWORD", dbPassword);
		setValue("FILEPATH", filePath);
		setValue("TEMPLATE", template);
	}

	public PopulateAndGenerateReportServerCommand(String jdbcUrl, String dbDriver, String dbUser, String dbPassword,
			String template) {
		this(jdbcUrl, dbDriver, dbUser, dbPassword, null, template);
	}

	public String writeToString(boolean forDisplay) {
		CommandStringBuilder builder = new CommandStringBuilder("populateAudit");
		String jdbcUrl = getJdbcUrl();
		if (jdbcUrl != null) {
			builder.addOptionWithArgument("ju", jdbcUrl);
		}
		String dbDriver = getDbDriver();
		if (dbDriver != null) {
			builder.addOptionWithArgument("dd", dbDriver);
		}
		String dbUser = getDbUser();
		if (dbUser != null) {
			builder.addOptionWithArgument("du", dbUser);
			String dbPassword = getDbPassword();
			if ((dbPassword != null) && (dbPassword.length() > 0)) {
				builder.addOptionWithArgument("up", getDisplayPassword(dbPassword, forDisplay));
			}
		}

		String branch = getFilePath();
		if (branch != null) {
			builder.addOptionWithArgument("fp", branch);
		}

		String template = getTemplate();
		if (template != null) {
			builder.addOptionWithArgument("t", template);
		}
		return builder.toString();
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String toString() {
		return writeToString(true);
	}

	public String getDbDriver() {
		return (String) getValue("DB_DRIVER");
	}

	public String getJdbcUrl() {
		return (String) getValue("JDBC_URL");
	}

	public String getDbUser() {
		return (String) getValue("DB_USER");
	}

	public String getDbPassword() {
		return (String) getValue("USER_PASSWORD");
	}

	public String getFilePath() {
		return (String) getValue("FILEPATH");
	}

	private String getTemplate() {
		return (String) getValue("TEMPLATE");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\
 * PopulateAndGenerateReportServerCommand.class Java compiler version: 8 (52.0)
 * JD-Core Version: 0.7.1
 */