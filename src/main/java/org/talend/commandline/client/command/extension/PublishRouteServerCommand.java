package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class PublishRouteServerCommand extends PublishServerCommand {
	public String getRouteName() {
		return (String) getValue("ROUTE_NAME");
	}

	public String getRouteVersion() {
		return (String) getValue("VERSION");
	}

	public String getRouteGroup() {
		return (String) getValue("GROUP");
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("publishRoute");

		builder.addArgument(getRouteName());

		String routeVersion = getRouteVersion();
		if (routeVersion != null) {
			builder.addOptionWithArgument("v", routeVersion);
		}

		String publishVersion = getPublishVersion();
		if (publishVersion != null) {
			builder.addOptionWithArgument("pv", publishVersion);
		}

		String routeGroup = getRouteGroup();
		if (routeGroup != null) {
			builder.addOptionWithArgument("g", routeGroup);
		}

		String artifactId = getArtifactId();
		if (artifactId != null) {
			builder.addOptionWithArgument("a", artifactId);
		}

		if (isSnapshot()) {
			builder.addOption("s");
		}
		builder.addOptionWithArgument("r", getArtifactRepository());
		builder.addOptionWithArgument("u", getUsername());

		return builder.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\PublishRouteServerCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */