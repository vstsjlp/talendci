package org.talend.commandline.client.command.extension;

import org.talend.commandline.client.util.CommandStringBuilder;

public class ChangeMavenVersionCommand extends AbstractServerCommand {
	private String newMavenVersion;
	private boolean subjobs;
	private String itemFilter;

	public ChangeMavenVersionCommand() {
	}

	public ChangeMavenVersionCommand(String newMavenVersion, boolean subjobs, String itemFilter) {
		this.newMavenVersion = newMavenVersion;
		this.subjobs = subjobs;
		this.itemFilter = itemFilter;
	}

	public String writeToString() {
		CommandStringBuilder builder = new CommandStringBuilder("changeMavenVersion");
		builder.addArgument(this.newMavenVersion);

		if (this.subjobs) {
			builder.addOption("s");
		}

		if (this.itemFilter != null) {
			builder.addOptionWithArgument("if", this.itemFilter);
		}

		return builder.toString();
	}

	public String getNewMavenVersion() {
		return (String) getValue("MAVEN_VERSION");
	}

	public boolean isSubjobs() {
		return getBooleanValue("SUBJOBS");
	}

	public String getItemFilter() {
		return (String) getValue("ITEM_FILTER");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\extension\ChangeMavenVersionCommand.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */