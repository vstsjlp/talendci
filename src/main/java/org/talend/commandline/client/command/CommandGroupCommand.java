package org.talend.commandline.client.command;

import java.util.ArrayList;
import java.util.List;
import org.talend.commandline.client.util.CommandStringBuilder;

public class CommandGroupCommand extends JavaServerCommand implements IExtensionCommand {
	private List<JavaServerCommand> commands = new ArrayList();
	private String originInfors;

	public CommandGroupCommand() {
		this(null);
	}

	public CommandGroupCommand(String originInfors) {
		this.originInfors = originInfors;
	}

	public CommandGroupCommand(JavaServerCommand command) {
		this(command, null);
	}

	public CommandGroupCommand(JavaServerCommand command, String originInfors) {
		this(originInfors);
		addCommand(command);
	}

	public CommandGroupCommand(List<JavaServerCommand> commands) {
		this(commands, null);
	}

	public CommandGroupCommand(List<JavaServerCommand> commands, String originInfors) {
		this(originInfors);
		for (JavaServerCommand command : commands) {
			addCommand(command);
		}
	}

	public synchronized void addCommand(JavaServerCommand command) {
		command.setGroupCommand(this);
		this.commands.add(command);
	}

	public List<JavaServerCommand> getCommands() {
		return this.commands;
	}

	public String writeToString() {
		return writeToString(false);
	}

	public String writeToString(boolean forDisplay) {
		StringBuffer buffer = new StringBuffer();

		CommandStringBuilder builder = new CommandStringBuilder("startGroup");
		if (this.originInfors != null) {
			builder.addOptionWithArgument("o", this.originInfors);
		}
		buffer.append(builder.toString());

		for (JavaServerCommand command : this.commands) {
			buffer.append(';');
			buffer.append(command.writeToString(forDisplay));
		}
		buffer.append(';');
		buffer.append("stopGroup");

		return buffer.toString();
	}

	public String getOriginInfors() {
		return this.originInfors;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(super.toString());
		if (this.originInfors != null) {
			buffer.append(" ");
			buffer.append(this.originInfors);
		}
		return buffer.toString();
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\command\CommandGroupCommand.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */