package org.talend.commandline.client.constant.extension;

public abstract interface IPaasCommandDefine
{
  public static final String INVENTORY_URL_SHORT = "r";
  public static final String PASSWORD_SHORT = "p";
  public static final String USER_NAME_SHORT = "u";
  public static final String WORKSPACE_SHORT = "w";
  public static final String EXPORT_SOURCES_SHORT = "s";
  public static final String EXPORT_IMAGE_SHORT = "i";
  public static final String PUBLISH_VERSION_SHORT = "v";
  public static final String NOT_ACCELERATE_SHORT = "na";
  public static final String INVENTORY_URL = "INVENTORY_URL";
  public static final String PASSWORD = "PASSWORD";
  public static final String USER_NAME = "USER_NAME";
  public static final String ITEM_NAME = "ITEM_NAME";
  public static final String PUBLISH_VERSION = "PUBLISH_VERSION";
  public static final String WORKSPACE = "WORKSPACE";
  public static final String EXPORT_SOURCES = "EXPORT_SOURCES";
  public static final String EXPORT_IMAGE = "EXPORT_IMAGE";
  public static final String NOT_ACCELERATE = "NOT_ACCELERATE";
  public static final String LIST_CLOUD_WORKSPACES_COMMAND = "listCloudWorkspaces";
  public static final String PUBLISH_ACTION = "publishAction";
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\constant\extension\IPaasCommandDefine.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */