package org.talend.commandline.client.constant.extension;

public abstract interface PopulateAuditDefine {
	public static final String COMMAND_NAME = "populateAudit";
	public static final String OPTION_JDBC_URL = "JDBC_URL";
	public static final String OPTION_JDBC_URL_SHORT = "ju";
	public static final String OPTION_DB_DRIVER = "DB_DRIVER";
	public static final String OPTION_DB_DRIVER_SHORT = "dd";
	public static final String OPTION_DB_USER = "DB_USER";
	public static final String OPTION_DB_USER_SHORT = "du";
	public static final String OPTION_USER_PASSWORD = "USER_PASSWORD";
	public static final String OPTION_USER_PASSWORD_SHORT = "up";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\PopulateAuditDefine.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */