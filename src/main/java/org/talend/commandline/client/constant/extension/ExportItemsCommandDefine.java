package org.talend.commandline.client.constant.extension;

public abstract interface ExportItemsCommandDefine {
	public static final int ID = 52;
	public static final String COMMAND_NAME = "exportItems";
	public static final String OPTION_DEPENDENCIES = "DEPENDENCIES";
	public static final String OPTION_DEPENDENCIES_SHORT = "d";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\ExportItemsCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */