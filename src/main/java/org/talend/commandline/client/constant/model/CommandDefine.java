package org.talend.commandline.client.constant.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @deprecated
 */
public class CommandDefine {
	private int commandID;
	private String command;
	private String commandDesc;
	private List<ArgumentDefine> args = new ArrayList();

	public CommandDefine(int commandID, String command) {
		this.commandID = commandID;
		this.command = command;
		initArguments();
	}

	public CommandDefine(int commandID, String command, String commandDesc) {
		this(commandID, command);
		this.commandDesc = commandDesc;
	}

	protected void initArguments() {
	}

	public void addArguments(ArgumentDefine argDef) {
		if (argDef != null) {
			this.args.add(argDef);
		}
	}

	public int getCommandID() {
		return this.commandID;
	}

	public String getCommand() {
		return this.command;
	}

	public String getCommandDesc() {
		return this.commandDesc;
	}

	public ArgumentDefine[] getArgs() {
		if (this.args != null) {
			return (ArgumentDefine[]) this.args.toArray(new ArgumentDefine[0]);
		}
		return new ArgumentDefine[0];
	}

	public ArgumentDefine findArg(String argName) {
		if (argName != null) {
			for (ArgumentDefine argDef : this.args) {
				if (argName.equals(argDef.getArgName())) {
					return argDef;
				}
			}
		}
		return null;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\model\CommandDefine.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */