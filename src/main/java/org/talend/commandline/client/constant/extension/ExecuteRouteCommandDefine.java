package org.talend.commandline.client.constant.extension;

public abstract interface ExecuteRouteCommandDefine {
	public static final String COMMAND_NAME = "executeRoute";
	public static final String ORG_JOB_NAME = "routeName";
	public static final String OPTION_INTERPRETER = "INTERPRETER";
	public static final String OPTION_INTERPRETER_SHORT = "i";
	public static final String OPTION_JOB_VERSION = "JOB_VERSION";
	public static final String OPTION_JOB_VERSION_SHORT = "jv";
	public static final String OPTION_JOB_CONTEXT = "JOB_CONTEXT";
	public static final String OPTION_JOB_CONTEXT_SHORT = "jc";
	public static final String OPTION_JOB_CONTEXT_PARAM = "JOB_CONTEXT_PARAM";
	public static final String OPTION_JOB_CONTEXT_PARAM_SHORT = "jcp";
	public static final String OPTION_JOB_RESULT_DESTINATION_DIR = "JOB_RESULT_DESTINATION_DIR";
	public static final String OPTION_JOB_RESULT_DESTINATION_DIR_SHORT = "jrdd";
	public static final String OPTION_JOB_TIMEOUT = "JOB_TIMEOUT";
	public static final String OPTION_JOB_TIMEOUT_SHORT = "jt";
	public static final String OPTION_JOB_APPLY_CONTEXT_TO_CHILDREN = "JOB_APPLY_CONTEXT_TO_CHILDREN";
	public static final String OPTION_JOB_APPLY_CONTEXT_TO_CHILDREN_SHORT = "jactc";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\ExecuteRouteCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */