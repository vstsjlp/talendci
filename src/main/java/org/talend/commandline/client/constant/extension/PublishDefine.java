package org.talend.commandline.client.constant.extension;

public abstract interface PublishDefine {
	public static final String OPTION_ARTIFACT_REPOSITORY = "ARTIFACT_REPOSITORY";
	public static final String OPTION_ARTIFACT_REPOSITORY_SHORT = "r";
	public static final String OPTION_USERNAME = "USERNAME";
	public static final String OPTION_USERNAME_SHORT = "u";
	public static final String OPTION_PASSWORD = "PASSWORD";
	public static final String OPTION_PASSWORD_SHORT = "p";
	public static final String OPTION_GROUP = "GROUP";
	public static final String OPTION_GROUP_SHORT = "g";
	public static final String OPTION_ARTIFACT = "ARTIFACT";
	public static final String OPTION_ARTIFACT_SHORT = "a";
	public static final String OPTION_VERSION = "VERSION";
	public static final String OPTION_VERSION_SHORT = "v";
	public static final String OPTION_PUBLISH_VERSION = "PUBLISH_VERSION";
	public static final String OPTION_PUBLISH_VERSION_SHORT = "pv";
	public static final String OPTION_SNAPSHOT = "SNAPSHOT";
	public static final String OPTION_SNAPSHOT_SHORT = "s";
	public static final String OPTION_TYPE = "TYPE";
	public static final String OPTION_TYPE_SHORT = "t";
	public static final String OPTION_JOB_CONTEXT = "JOB_CONTEXT";
	public static final String OPTION_JOB_CONTEXT_SHORT = "jc";
	public static final String OPTION_JOB_APPLY_CONTEXT_TO_CHILDREN = "JOB_APPLY_CONTEXT_TO_CHILDREN";
	public static final String OPTION_JOB_APPLY_CONTEXT_TO_CHILDREN_SHORT = "jactc";
	public static final String OPTION_JOB_INCLUDE_CONTEXT = "JOB_INCLUDE_CONTEXT";
	public static final String OPTION_JOB_INCLUDE_CONTEXT_SHORT = "ic";
	public static final String OPTION_JOB_EXECUTE_TESTS = "JOB_EXECUTE_TESTS";
	public static final String OPTION_JOB_EXECUTE_TESTS_SHORT = "et";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\PublishDefine.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */