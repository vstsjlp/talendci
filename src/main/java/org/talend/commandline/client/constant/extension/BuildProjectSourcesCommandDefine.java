package org.talend.commandline.client.constant.extension;

public class BuildProjectSourcesCommandDefine {
	public static final String COMMAND_NAME = "buildProjectSources";
	public static final String OPTION_ITEM_FILTER = "ITEM_FILTER";
	public static final String OPTION_ITEM_FILTER_SHORT = "if";
	public static final String OPTION_DEPLOY_VERSION = "DEPLOY_VERSION";
	public static final String OPTION_DEPLOY_VERSION_SHORT = "dv";
}
