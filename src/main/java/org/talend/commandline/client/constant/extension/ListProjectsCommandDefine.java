package org.talend.commandline.client.constant.extension;

public abstract interface ListProjectsCommandDefine {
	public static final String COMMAND_NAME = "listProject";
	public static final String OPTION_SHOW_BRANCH = "showBranch";
	public static final String OPTION_SHOW_BRANCH_SHORT = "b";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\ListProjectsCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */