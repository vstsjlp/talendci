package org.talend.commandline.client.constant.extension;

public abstract interface StartGroupCommandDefine {
	public static final int ID = 90;
	public static final String COMMAND_NAME = "startGroup";
	public static final String OPTION_ORIGIN = "ORIGIN";
	public static final String OPTION_ORIGIN_SHORT = "o";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\StartGroupCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */