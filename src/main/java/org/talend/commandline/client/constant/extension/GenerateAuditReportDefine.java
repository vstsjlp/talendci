package org.talend.commandline.client.constant.extension;

public abstract interface GenerateAuditReportDefine {
	public static final String COMMAND_NAME = "generateAuditReport";
	public static final String ARG_AUDIT_ID = "auditId";
	public static final String OPTION_FILEPATH = "FILEPATH";
	public static final String OPTION_FILEPATH_SHORT = "fp";
	public static final String OPTION_TEMPLATE = "TEMPLATE";
	public static final String OPTION_TEMPLATE_SHORT = "t";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\GenerateAuditReportDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */