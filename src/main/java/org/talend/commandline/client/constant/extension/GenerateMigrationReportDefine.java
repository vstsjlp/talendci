package org.talend.commandline.client.constant.extension;

public class GenerateMigrationReportDefine {
	public static String COMMAND_NAME = "generateMigrationReport";

	public static String DATA_PATH = "DATA_PATH";

	public static String DATA_PATH_SHORT = "dp";

	public static String REPORT_PATH = "REPORT_PATH";

	public static String REPORT_PATH_SHORT = "rp";

	public static String FROM_VERSION = "FROM_VERSION";

	public static String FROM_VERSION_SHORT = "from";

	public static String TO_VERSION = "TO_VERSION";

	public static String TO_VERSION_SHORT = "to";

	public static String PROJECTS = "PROJECTS";

	public static String PROJECTS_SHORT = "ps";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\GenerateMigrationReportDefine.
 * class Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */