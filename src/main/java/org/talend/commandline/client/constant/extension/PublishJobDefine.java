package org.talend.commandline.client.constant.extension;

public abstract interface PublishJobDefine extends PublishDefine {
	public static final String COMMAND_NAME = "publishJob";
	public static final String ARG_JOB_NAME = "JOB_NAME";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\PublishJobDefine.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */