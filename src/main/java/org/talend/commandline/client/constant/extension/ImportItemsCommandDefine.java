package org.talend.commandline.client.constant.extension;

public abstract interface ImportItemsCommandDefine {
	public static final int ID = 51;
	public static final String COMMAND_NAME = "importItems";
	public static final String ARG_SOURCE = "source (dir|.zip)";
	public static final String OPTION_ITEM_FILTER = "ITEM_FILTER";
	public static final String OPTION_OVERWRITE = "OVERWRITE";
	public static final String OPTION_OVERWRITE_SHORT = "o";
	public static final String OPTION_STATUS = "STATUS";
	public static final String OPTION_STATUS_SHORT = "s";
	public static final String OPTION_STATSLOGS = "STATSLOGS";
	public static final String OPTION_STATSLOGS_SHORT = "sl";
	public static final String OPTION_IMPLICIT = "IMPLICIT";
	public static final String OPTION_IMPLICIT_SHORT = "im";
	public static final String OPTION_REFPROJECTS = "REF_PROJECTS";
	public static final String OPTION_REFPROJECTS_SHORT = "r";
}
