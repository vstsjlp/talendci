package org.talend.commandline.client.constant.extension;

public abstract interface CreateTaskCommandDefine {
	public static final String COMMAND_NAME = "createTask";
	public static final String ORG_TASK_NAME = "taskName";
	public static final String OPTION_PROJECT_NAME = "PROJECT_NAME";
	public static final String OPTION_PROJECT_NAME_SHORT = "pn";
	public static final String OPTION_PROJECT_BRANCH = "PROJECT_BRANCH";
	public static final String OPTION_PROJECT_BRANCH_SHORT = "b";
	public static final String OPTION_JOB_NAME = "JOB_NAME";
	public static final String OPTION_JOB_NAME_SHORT = "jn";
	public static final String OPTION_JOB_VERSION = "JOB_VERSION";
	public static final String OPTION_JOB_VERSION_SHORT = "jv";
	public static final String OPTION_JOB_CONTEXT = "JOB_CONTEXT";
	public static final String OPTION_JOB_CONTEXT_SHORT = "jc";
	public static final String OPTION_JOB_APPLY_CONTEXT_TO_CHILDREN = "JOB_APPLY_CONTEXT_TO_CHILDREN";
	public static final String OPTION_JOB_APPLY_CONTEXT_TO_CHILDREN_SHORT = "jactc";
	public static final String OPTION_REGENERATE_JOB_ON_CHANGE = "REGENERATE_JOB_ON_CHANGE";
	public static final String OPTION_REGENERATE_JOB_ON_CHANGE_SHORT = "rjc";
	public static final String OPTION_EXECUTION_SERVER = "EXECUTION_SERVER";
	public static final String OPTION_EXECUTION_SERVER_SHORT = "esn";
	public static final String OPTION_EXEC_STATISTICS_ENABLED = "EXEC_STATISTICS_ENABLED";
	public static final String OPTION_EXEC_STATISTICS_ENABLED_SHORT = "ese";
	public static final String OPTION_ON_UNAVAILABLE_JOBSERVER = "ON_UNAVAILABLE_JOBSERVER";
	public static final String OPTION_ON_UNAVAILABLE_JOBSERVER_SHORT = "ouj";
	public static final String OPTION_ACTIVE = "ACTIVE";
	public static final String OPTION_ACTIVE_SHORT = "a";
	public static final String OPTION_DESCRIPTION = "DESCRIPTION";
	public static final String OPTION_DESCRIPTION_SHORT = "d";
	public static final String OPTION_ON_UNKNOWN_STATE_JOB = "ON_UNKNOWN_STATE_JOB";
	public static final String OPTION_ON_UNKNOWN_STATE_JOB_SHORT = "ousj";
	public static final String OPTION_ADD_STATISTICS_CODE_ENABLE = "ADD_STATISTICS_CODE_ENABLE";
	public static final String OPTION_ADD_STATISTICS_CODE_ENABLE_SHORT = "asce";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\CreateTaskCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */