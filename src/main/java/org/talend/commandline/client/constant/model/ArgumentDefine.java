package org.talend.commandline.client.constant.model;

/**
 * @deprecated
 */
public class ArgumentDefine {
	private String argName;

	private String longArg;

	private String shortArg;

	private String argDesc = "";

	public ArgumentDefine(String argName, String longArg, String shortArg) {
		this.argName = argName;
		this.longArg = longArg;
		this.shortArg = shortArg;
	}

	public ArgumentDefine(String argName, String longArg, String shortArg, String argDesc) {
		this.argName = argName;
		this.longArg = longArg;
		this.shortArg = shortArg;
		this.argDesc = argDesc;
	}

	public String getArgName() {
		return this.argName;
	}

	public String getLongArg() {
		return this.longArg;
	}

	public String getShortArg() {
		return this.shortArg;
	}

	public String getArgDesc() {
		return this.argDesc;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\model\ArgumentDefine.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */