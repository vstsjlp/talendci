package org.talend.commandline.client.constant.extension;

public abstract interface SetUserComponentPathCommandDefine {
	public static final int ID = 100;
	public static final String COMMAND_NAME = "setUserComponentPath";
	public static final String OPTION_PATH = "PATH";
	public static final String OPTION_PATH_SHORT = "up";
	public static final String OPTION_CLEAR = "CLEAR";
	public static final String OPTION_CLEAR_SHORT = "c";
	public static final String OPTION_CLEAR_LONG = "clear";
	public static final String USER_COMPONENTS_FOLDER = "USER_COMPONENTS_FOLDER";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\
 * SetUserComponentPathCommandDefine.class Java compiler version: 8 (52.0)
 * JD-Core Version: 0.7.1
 */