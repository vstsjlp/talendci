package org.talend.commandline.client.constant.extension;

public abstract interface FilterExprDefine {
	public static final String OPTION_ITEM_FILTER = "ITEM_FILTER";
	public static final String ARG_NAME = "filterExpr";
	public static final String ARG_SHORT = "if";
	public static final String ARG_LONG = "item-filter";
	public static final String ARG_DESC = "item filter expression";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\FilterExprDefine.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */