package org.talend.commandline.client.constant.extension;

public class ChangeMavenVersionCommandDefine {
	public static final String COMMAND_NAME = "changeMavenVersion";
	public static final String ARG_MAVEN_VERSION = "MAVEN_VERSION";
	public static final String OPTION_ITEM_FILTER = "ITEM_FILTER";
	public static final String OPTION_ITEM_FILTER_SHORT = "if";
	public static final String OPTION_SUBJOBS = "SUBJOBS";
	public static final String OPTION_SUBJOBS_SHORT = "s";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\ChangeMavenVersionCommandDefine.
 * class Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */