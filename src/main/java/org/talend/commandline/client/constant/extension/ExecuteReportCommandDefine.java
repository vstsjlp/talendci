package org.talend.commandline.client.constant.extension;

public abstract interface ExecuteReportCommandDefine {
	public static final int ID = 101;
	public static final String COMMAND_NAME = "executeReport";
	public static final String OPTION_NAMES = "NAME";
	public static final String OPTION_NAMES_SHORT = "n";
	public static final String OPTION_FILES = "PATH";
	public static final String OPTION_FILES_SHORT = "p";
	public static final String OPTION_CONTEXT_NAME = "CONTEXT_NAME";
	public static final String OPTION_CONTEXT_NAME_SHORT = "pc";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\ExecuteReportCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */