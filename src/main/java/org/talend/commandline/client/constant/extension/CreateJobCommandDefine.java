package org.talend.commandline.client.constant.extension;

public abstract interface CreateJobCommandDefine {
	public static final int ID = 120;
	public static final String COMMAND_NAME = "createJob";
	public static final String OPTION_SCRIPT_FILE = "SCRIPT_FILE";
	public static final String OPTION_SCRIPT_FILE_SHORT = "sf";
	public static final String OPTION_OVER_WRITE = "OVER_WRITE";
	public static final String OPTION_OVER_WRITE_SHORT = "o";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\CreateJobCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */