package org.talend.commandline.client.constant.extension;

public abstract interface DeployJobToServerCommandDefine {
	public static final int ID = 130;
	public static final String COMMAND_NAME = "deployJobToServer";
	public static final String OPTION_EXECUTION_SERVER_GROUP = "EXECUTION_SERVER";
	public static final String OPTION_EXECUTION_SERVER_SHORT = "es";
	public static final String OPTION_VERSION_GROUP = "VERSION";
	public static final String OPTION_VERSION_SHORT = "jv";
	public static final String OPTION_LOG4J_LEVEL_GROUP = "LOG4JLEVEL";
	public static final String OPTION_LOG4J_LEVEL_SHORT = "jall";
	public static final String OPTION_CONTEXT_NAME_GROUP = "CONTEXT_NAME";
	public static final String OPTION_CONTEXT_NAME_SHORT = "jc";
	public static final String OPTION_APPLY_CONTEXT_TO_CHILDREN_GROUP = "APPLY_CONTEXT_TO_CHILDREN";
	public static final String OPTION_APPLY_CONTEXT_TO_CHILDREN_SHORT = "jactc";
	public static final String OPTION_USE_SSL_GROUP = "USE_SSL";
	public static final String OPTION_USE_SSL_GROUP_SHORT = "useSSL";
	public static final String OPTION_USER_NAME_GROUP = "USERNAME";
	public static final String OPTION_USER_NAME_SHORT = "un";
	public static final String OPTION_PASS_WORD_GROUP = "PASSWORD";
	public static final String OPTION_PASS_WORD_SHORT = "pd";
	public static final String OPTION_STAT_GROUP = "STAT_PORT";
	public static final String OPTION_STAT_SHORT = "sp";
	public static final String OPTION_TRACE_GROUP = "TRACE_PORT";
	public static final String OPTION_TRACE_SHORT = "tp";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\DeployJobToServerCommandDefine.
 * class Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */