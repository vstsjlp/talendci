package org.talend.commandline.client.constant.extension;

public abstract interface ExportRouteDifine {
	public static final String COMMAND_NAME = "exportRoute";
	public static final String ARG_ROUTE_NAME = "ROUTE_NAME";
	public static final String OPTION_DESTINATION_DIRECTORY = "DESTINATION_DIRECTORY";
	public static final String OPTION_DESTINATION_DIRECTORY_SHORT = "dd";
	public static final String OPTION_JOB_VERSION = "JOB_VERSION";
	public static final String OPTION_JOB_VERSION_SHORT = "jv";
	public static final String OPTION_JOB_CONTEXT = "JOB_CONTEXT";
	public static final String OPTION_JOB_CONTEXT_SHORT = "jc";
	public static final String OPTION_AECHIVE_FILENAME = "AECHIVE_FILENAME";
	public static final String OPTION_AECHIVE_FILENAME_SHORT = "af";
	public static final String OPTION_JOB_APPLY_CONTEXT_TO_CHILDREN = "JOB_APPLY_CONTEXT_TO_CHILDREN";
	public static final String OPTION_JOB_APPLY_CONTEXT_TO_CHILDREN_SHORT = "jactc";
	public static final String OPTION_JOB_ADD_STATISTICS_CODE = "JOB_ADD_STATISTICS_CODE";
	public static final String OPTION_JOB_ADD_STATISTICS_CODE_SHORT = "jstats";
	public static final String OPTION_JOB_ADD_MAVEN_SCRIPT = "JOB_ADD_MAVEN_SCRIPT";
	public static final String OPTION_JOB_ADD_MAVEN_SCRIPT_SHORT = "maven";
	public static final String OPTION_JOB_BUILD_TYPE = "JOB_BUILD_TYPE";
	public static final String OPTION_JOB_BUILD_TYPE_SHORT = "bt";
	public static final String OPTION_JOB_EXPORT_AS_ZIP = "JOB_EXPORT_AS_ZIP";
	public static final String OPTION_JOB_EXPORT_AS_ZIP_SHORT = "az";
	public static final String OPTION_JOB_EXPORT_ONLY_DEFAULT_CONTEXT = "JOB_EXPORT_ONLY_DEFAULT_CONTEXT";
	public static final String OPTION_JOB_EXPORT_ONLY_DEFAULT_CONTEXT_SHORT = "od";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\ExportRouteDifine.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */