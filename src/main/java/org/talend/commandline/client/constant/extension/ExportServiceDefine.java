package org.talend.commandline.client.constant.extension;

public abstract interface ExportServiceDefine {
	public static final String COMMAND_NAME = "exportService";
	public static final String ARG_SERVICE_NAME = "SERVICE_NAME";
	public static final String OPTION_DESTINATION_DIRECTORY = "DESTINATION_DIRECTORY";
	public static final String OPTION_DESTINATION_DIRECTORY_SHORT = "dd";
	public static final String OPTION_SERVICE_VERSION = "SERVICE_VERSION";
	public static final String OPTION_SERVICE_VERSION_SHORT = "sv";
	public static final String OPTION_ARCHIVE_FILENAME = "ARCHIVE_FILENAME";
	public static final String OPTION_ARCHIVE_FILENAME_SHORT = "af";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\ExportServiceDefine.class Java
 * compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */