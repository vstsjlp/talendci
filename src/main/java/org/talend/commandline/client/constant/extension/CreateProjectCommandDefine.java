package org.talend.commandline.client.constant.extension;

public abstract interface CreateProjectCommandDefine {
	public static final String COMMAND_NAME = "createProject";
	public static final String OPTION_PROJECT_NAME = "PROJECT_NAME";
	public static final String OPTION_PROJECT_NAME_SHORT = "pn";
	public static final String OPTION_PROJECT_DESCRIPTION = "PROJECT_DESCRIPTION";
	public static final String OPTION_PROJECT_DESCRIPTION_SHORT = "pd";
	public static final String OPTION_PROJECT_LANGUAGE = "PROJECT_LANGUAGE";
	public static final String OPTION_PROJECT_LANGUAGE_SHORT = "pl";
	public static final String OPTION_PROJECT_AUTHOR = "PROJECT_AUTHOR";
	public static final String OPTION_PROJECT_AUTHOR_SHORT = "pa";
	public static final String OPTION_PROJECT_AUTHOR_PASS = "PROJECT_AUTHOR_PASS";
	public static final String OPTION_PROJECT_AUTHOR_PASS_SHORT = "pap";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\CreateProjectCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */