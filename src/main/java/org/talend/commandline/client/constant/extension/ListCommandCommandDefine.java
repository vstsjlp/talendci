package org.talend.commandline.client.constant.extension;

public abstract interface ListCommandCommandDefine
{
  public static final String COMMAND_NAME = "listCommand";
  public static final String OPTION_ALL = "ALL";
  public static final String OPTION_ALL_SHORT = "a";
  public static final String OPTION_QUEUE = "QUEUE";
  public static final String OPTION_QUEUE_SHORT = "q";
  public static final String OPTION_RUN = "RUN";
  public static final String OPTION_RUN_SHORT = "r";
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\talend\commandline\client\constant\extension\ListCommandCommandDefine.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */