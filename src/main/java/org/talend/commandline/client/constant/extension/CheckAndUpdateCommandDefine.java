package org.talend.commandline.client.constant.extension;

public abstract interface CheckAndUpdateCommandDefine {
	public static final String COMMAND_NAME = "checkAndUpdate";
	public static final String USER_LOGIN = "USER_LOGIN";
	public static final String USER_PASSWORD = "USER_PASSWORD";
	public static final String ARCHIVA_SERVICES_SEGMENT = "/restServices/archivaServices/";
	public static final String OPTION_TAC_USER = "TAC_USER";
	public static final String OPTION_TAC_USER_PASSWORD = "TAC_USER_PASSWORD";
	public static final String ARGUMENT_TAC_USER = "user";
	public static final String ARGUMENT_TAC_USER_PASSWORD = "password";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\CheckAndUpdateCommandDefine.
 * class Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */