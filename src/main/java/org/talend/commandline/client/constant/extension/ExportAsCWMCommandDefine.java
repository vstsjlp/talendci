package org.talend.commandline.client.constant.extension;

public abstract interface ExportAsCWMCommandDefine {
	public static final String COMMAND_NAME = "exportAsCWM";
	public static final String DBCONNECTION_NAME = "dbConnectionName";
	public static final String OPTION_DEST_DIR = "DEST_DIR";
	public static final String OPTION_DEST_DIR_SHORT = "dd";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\constant\extension\ExportAsCWMCommandDefine.class
 * Java compiler version: 8 (52.0) JD-Core Version: 0.7.1
 */