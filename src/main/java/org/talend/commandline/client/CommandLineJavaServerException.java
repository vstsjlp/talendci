package org.talend.commandline.client;

public class CommandLineJavaServerException extends Exception {
	private static final long serialVersionUID = -4982618316007498865L;

	public CommandLineJavaServerException(String message) {
		super(message);
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\CommandLineJavaServerException.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */