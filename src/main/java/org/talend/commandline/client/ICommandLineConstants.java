package org.talend.commandline.client;

public abstract interface ICommandLineConstants
{
	public static final int SLEEP_TIME = 100;


	public static final char LF = '\n';


	public static final char CR = '\r';


	public static final String HELP_LEFT_GUTTER = "|";


	public static final String HELP_RIGHT_GUTTER = "|";


	public static final String HELP_CENTER_GUTTER = "   ";


	public static final int HELP_FULL_WIDTH = 100;


	public static final String SPACE_SEPARATOR = " ";


	public static final char SPACE_SEPARATOR_CHAR = ' ';


	public static final char COMMAND_SEPARATOR = ';';


	public static final char PARSER_QUOTE = '\'';


	public static final char PARSER_DBL_QUOTE = '"';


	public static final char PARSER_ESCAPE_CHAR = '\\';


	public static final String OPTION_SHORT_PREFIX = "-";


	public static final String SHELL_COMMAND = "shell";


	public static final int SHELL_COMMAND_ID = 2;

	public static final String SHELL_COMMAND_DESC = "launches shell";

	public static final String QUIT_COMMAND = "quit";

	public static final int QUIT_COMMAND_ID = 3;

	public static final String QUIT_COMMAND_DESC = "exit";

	public static final String START_SERVER_COMMAND = "startServer";

	public static final int START_SERVER_COMMAND_ID = 4;

	public static final String START_SERVER_COMMAND_DESC = "starts server";

	public static final String STOP_SERVER_COMMAND = "stopServer";

	public static final int STOP_SERVER_COMMAND_ID = 5;

	public static final String STOP_SERVER_COMMAND_DESC = "stops server";

	public static final String JAVA_PROTOCOL_COMMAND = "javaProtocol";

	public static final int JAVA_PROTOCOL_COMMAND_ID = 7;

	public static final String JAVA_PROTOCOL_COMMAND_DESC = "switches to java protocol mode";

	public static final String INIT_LOCAL_COMMAND = "initLocal";

	public static final int INIT_LOCAL_COMMAND_ID = 11;

	public static final String INIT_LOCAL_COMMAND_DESC = "init local repository";

	public static final String INIT_REMOTE_COMMAND = "initRemote";

	public static final int INIT_REMOTE_COMMAND_ID = 12;

	public static final String INIT_REMOTE_COMMAND_DESC = "init remote repository";

	public static final String INIT_REMOTE_COMMAND_ARG = "adminUrl";

	public static final String LOGON_PROJECT_COMMAND = "logonProject";

	public static final int LOGON_PROJECT_COMMAND_ID = 15;

	public static final String LOGON_PROJECT_COMMAND_DESC = "logs on a project";

	public static final String LOGOFF_PROJECT_COMMAND = "logoffProject";

	public static final int LOGOFF_PROJECT_COMMAND_ID = 16;

	public static final String LOGOFF_PROJECT_COMMAND_DESC = "logs off";

	public static final String HELP_FILTER_COMMAND = "helpFilter";

	public static final int HELP_FILTER_COMMAND_ID = 17;

	public static final String HELP_FILTER_COMMAND_DESC = "shows filter help";

	public static final String SHOW_VERSION_COMMAND = "showVersion";

	public static final int SHOW_VERSION_COMMAND_ID = 18;

	public static final String SHOW_VERSION_COMMAND_DESC = "shows product version";

	public static final String EXECUTE_ALL_JOB_COMMAND = "executeAllJob";

	public static final int EXECUTE_ALL_JOB_COMMAND_ID = 21;

	public static final String EXECUTE_ALL_JOB_COMMAND_DESC = "executes all";

	public static final String EXECUTE_JOB_COMMAND = "executeJob";

	public static final int EXECUTE_JOB_COMMAND_ID = 22;

	public static final String EXECUTE_JOB_COMMAND_DESC = "executes";

	public static final String EXPORT_ALL_JOB_COMMAND = "exportAllJob";

	public static final int EXPORT_ALL_JOB_COMMAND_ID = 31;

	public static final String EXPORT_ALL_JOB_COMMAND_DESC = "exports all";

	public static final String EXPORT_JOB_COMMAND = "exportJob";

	public static final int EXPORT_JOB_COMMAND_ID = 32;

	public static final String EXPORT_JOB_COMMAND_DESC = "exports";

	public static final String LIST_EXECUTION_SERVER_COMMAND = "listExecutionServer";

	public static final int LIST_EXECUTION_SERVER_COMMAND_ID = 42;

	public static final String LIST_EXECUTION_SERVER_COMMAND_DESC = "lists [virtual] execution servers";

	public static final String LIST_ITEM_COMMAND = "listItem";

	public static final int LIST_ITEM_COMMAND_ID = 43;

	public static final String LIST_ITEM_COMMAND_DESC = "lists items";

	public static final String DELETE_ITEMS_COMMAND = "deleteItems";

	public static final int DELETE_ITEMS_COMMAND_ID = 53;

	public static final String DELETE_ITEMS_COMMAND_DESC = "delete items";

	public static final String SCRIPT_FILE_COMMAND_ARG = "path";

	public static final String SCRIPT_FILE_COMMAND = "scriptFile";

	public static final int SCRIPT_FILE_COMMAND_ID = 60;

	public static final String SCRIPT_FILE_COMMAND_DESC = "reads commands from file";

	public static final String CHANGE_VERSION_COMMAND_ARG = "newVersion (x.x | nextMajor | nextMinor)";

	public static final String CHANGE_VERSION_COMMAND_ARG_NEXT_MINOR = "nextMinor";

	public static final String CHANGE_VERSION_COMMAND_ARG_NEXT_MAJOR = "nextMajor";

	public static final String CHANGE_VERSION_COMMAND = "changeVersion";

	public static final int CHANGE_VERSION_COMMAND_ID = 61;

	public static final String CHANGE_VERSION_COMMAND_DESC = "changes items version \nNote: If you use the \"-d\" arg with the item filter together, we will do the filter first and then get the dependences of the filter items, finally change all of them.";

	public static final String CHANGE_STATUS_COMMAND_ARG = "newStatusCode";

	public static final String CHANGE_STATUS_COMMAND = "changeStatus";

	public static final int CHANGE_STATUS_COMMAND_ID = 62;

	public static final String CHANGE_STATUS_COMMAND_DESC = "changes items status \nNote: If you use the \"-d\" arg with the item filter together, we will do the filter first and then get the dependences of the filter items, finally change all of them.";

	public static final String IMPORT_DATABASE_METADATA_COMMAND_ARG = "filePath";

	public static final String IMPORT_DATABASE_METADATA_COMMAND = "importDatabaseMetadata";

	public static final int IMPORT_DATABASE_METADATA_COMMAND_ID = 70;

	public static final String IMPORT_DATABASE_METADATA_COMMAND_DESC = "imports database metadata";

	public static final String IMPORT_DELIMITED_METADATA_COMMAND_ARG = "filePath";

	public static final String IMPORT_DELIMITED_METADATA_COMMAND = "importDelimitedMetadata";

	public static final int IMPORT_DELIMITED_METADATA_COMMAND_ID = 71;

	public static final String IMPORT_DELIMITED_METADATA_COMMAND_DESC = "imports delimited metadata";

	public static final String PUT_FILE_COMMAND = "putFile";

	public static final int PUT_FILE_COMMAND_ID = 80;

	public static final String PUT_FILE_COMMAND_DESC = "put file";

	public static final String GET_FILE_COMMAND = "getFile";

	public static final int GET_FILE_COMMAND_ID = 81;

	public static final String GET_FILE_COMMAND_DESC = "get file";

	public static final String LOCAL_PATH_FILE_ARG = "localPath";

	public static final int PUBLISH_SERVICE_COMMAND_ID = 90;

	public static final String PUBLISH_SERVICE_COMMAND = "publishService";

	public static final String PUBLISH_SERVICE_COMMAND_DESC = "publish a service";

	public static final int PUBLISH_ROUTE_COMMAND_ID = 91;

	public static final String PUBLISH_ROUTE_COMMAND = "publishRoute";

	public static final String PUBLISH_ROUTE_COMMAND_DESC = "publish a route";

	public static final int PUBLISH_JOB_COMMAND_ID = 92;

	public static final String PUBLISH_JOB_COMMAND = "publishJob";

	public static final String PUBLISH_JOB_COMMAND_DESC = "publish a job";

	public static final String REMOTE_PATH_FILE_ARG = "remotePath";

	public static final String INTERPRETER_ARG = "path";

	java interpreter path";

	public static final String INTERPRETER_LONG = "interpreter";

	public static final String INTERPRETER_SHORT = "i";

	public static final String JOB_TIMEOUT_ARG = "time (in sec)";

	public static final String JOB_TIMEOUT_SHORT = "jt";

	public static final String JOB_TIMEOUT_LONG = "job-timeout";

	public static final String JOB_TIMEOUT_DESC = "timeout of execution";

	public static final String REPOSITORY_PROJECT_NAME_ARG = "technical name";

	public static final String REPOSITORY_PROJECT_NAME_DESC = "project name";

	public static final String REPOSITORY_PROJECT_NAME_LONG = "project-name";

	public static final String REPOSITORY_PROJECT_NAME_SHORT = "pn";

	public static final String REPOSITORY_USER_LOGIN_ARG = "login";

	public static final String REPOSITORY_USER_LOGIN_DESC = "user login";

	public static final String REPOSITORY_USER_LOGIN_LONG = "user-login";

	public static final String REPOSITORY_USER_LOGIN_SHORT = "ul";

	public static final String REPOSITORY_READONLY_LOGIN_SHORT = "ro";

	public static final String REPOSITORY_READONLY_LOGIN_LONG = "readOnly";

	public static final String REPOSITORY_READONLY_LOGIN_DESC = "readOnly on current project";

	public static final String REPOSITORY_USER_PASSWORD_ARG = "password";

	public static final String REPOSITORY_USER_PASSWORD_DESC = "user password";

	public static final String REPOSITORY_USER_PASSWORD_LONG = "user-password";

	public static final String REPOSITORY_USER_PASSWORD_SHORT = "up";

	public static final String SHADOW_CONSOLE_LOG_DESC = "adds logs to stdout";

	public static final String SHADOW_CONSOLE_LOG_SHORT = "consoleLog";

	public static final String SHADOW_WAIT_ON_EXIT_DESC = "wait on commandline exit";

	public static final String SHADOW_WAIT_ON_EXIT_SHORT = "woe";

	public static final String SHADOW_WAIT_ON_EXIT_LONG = "waitOnExit";

	public static final String SHADOW_WORKSPACE_LOCATION_ARG = "location";

	public static final String SHADOW_WORKSPACE_LOCATION_DESC = "workspace location";

	public static final String SHADOW_WORKSPACE_LOCATION_SHORT = "data";

	public static final String WITH_METADATA_SHORT = "m";

	public static final String WITH_METADATA_LONG = "metadata";

	public static final String WITH_METADATA_DESC = "show item's metadata";

	public static final String JOB_VERSION_ARG = "version";

	public static final String JOB_VERSION_SHORT = "jv";

	public static final String JOB_VERSION_LONG = "job-version";

	public static final String JOB_VERSION_DESC = "chooses a job version";

	public static final String JOB_CONTEXT_ARG = "context name";

	public static final String JOB_CONTEXT_SHORT = "jc";

	public static final String JOB_CONTEXT_LONG = "job-context";

	public static final String JOB_CONTEXT_DESC = "chooses a job context";

	public static final String JOB_CONTEXT_PARAM_ARG = "key=value";

	public static final String JOB_CONTEXT_PARAM_SHORT = "jcp";

	public static final String JOB_CONTEXT_PARAM_LONG = "job-context-param";

	public static final String JOB_CONTEXT_PARAM_DESC = "provides a job context param";

	public static final String JOB_APPLY_CONTEXT_TO_CHILDREN_SHORT = "jactc";

	public static final String JOB_APPLY_CONTEXT_TO_CHILDREN_LONG = "job-apply-context-to-children";

	public static final String JOB_APPLY_CONTEXT_TO_CHILDREN_DESC = "apply context to children";

	public static final String JOB_ADD_LOG4J_LEVEL_ARG = "log4j level name";

	public static final String JOB_ADD_LOG4J_LEVEL_SHORT = "jall";

	public static final String JOB_ADD_LOG4J_LEVEL_LONG = "job-add-log4j-level";

	public static final String JOB_ADD_LOG4J_LEVEL_DESC = "generate with the log4j levels";

	public static final String JOB_ADD_STATISTICS_CODE_SHORT = "jstats";

	public static final String JOB_ADD_STATISTICS_CODE_LONG = "job-add-statistics-code";

	public static final String JOB_ADD_STATISTICS_CODE_DESC = "generate with the statistics instructions";

	public static final String JOB_EXPORT_TO_OSGI_CODE_SHORT = "eo";

	public static final String JOB_EXPORT_TO_OSGI_CODE_LONG = "export-osgi";

	public static final String JOB_EXPORT_TO_OSGI_CODE_DESC = "export job to osgi system";

	public static final String JOB_ADD_ANT_SCRIPT_SHORT = "ant";

	public static final String JOB_ADD_ANT_SCRIPT_LONG = "add-ant-script";

	public static final String JOB_ADD_ANT_SCRIPT_DESC = "export job with ant build script";

	public static final String JOB_ADD_MAVEN_SCRIPT_SHORT = "maven";

	public static final String JOB_ADD_MAVEN_SCRIPT_LONG = "add-maven-script";

	public static final String JOB_ADD_MAVEN_SCRIPT_DESC = "export job with maven build script";

	public static final String FIX_LATEST_VERSION_SHORT = "flv";

	public static final String FIX_LATEST_VERSION_LONG = "fix-latest-version";

	public static final String FIX_LATEST_VERSION_DESC = "fixing latest version";

	public static final String DEPENDENCIES_SHORT = "d";

	public static final String DEPENDENCIES_LONG = "dependencies";

	public static final String DEPENDENCIES_DESC = "update the dependencies";

	public static final String DEST_DIR_ARG = "path";

	public static final String DEST_DIR_SHORT = "dd";

	public static final String DEST_DIR_LONG = "destination-directory";

	public static final String DEST_DIR_DESC = "destination directory";

	tmp";

	public static final String ARCHIVE_FILENAME_ARG = "filename";

	public static final String ARCHIVE_FILENAME_SHORT = "af";

	public static final String ARCHIVE_FILENAME_LONG = "archive-filename";

	public static final String ARCHIVE_FILENAME_DESC = "archive filename without extension";

	public static final String JOB_RESULT_DEST_DIR_ARG = "path";

	public static final String JOB_RESULT_DEST_DIR_SHORT = "jrdd";

	public static final String JOB_RESULT_DEST_DIR_LONG = "job-result-destination-dir";

	public static final String JOB_RESULT_DEST_DIR_DESC = "job execution result destination dir";

	public static final String SERVER_PORT_ARG = "port";

	public static final Integer SERVER_PORT_ARG_DEFAULT_INTEGER = Integer.valueOf(8002);

	public static final String SERVER_PORT_ARG_DEFAULT = SERVER_PORT_ARG_DEFAULT_INTEGER.toString();
	public static final String SERVER_PORT_SHORT = "p";
	public static final String SERVER_PORT_LONG = "port";
	public static final String SERVER_PORT_DESC = "server port";
	public static final String EXECUTE_JOB_COMMAND_ARG = "jobName";
	public static final String GENERATE_TEMPLATES_SHORT = "gt";
	public static final String GENERATE_TEMPLATES_LONG = "generate-templates";
	public static final String GENERATE_TEMPLATES_DESC = "generate templates";
	public static final String SELECT_BRANCH_ARG = "branch";
	public static final String SELECT_BRANCH_SHORT = "br";
	public static final String SELECT_BRANCH_LONG = "branch";
	<tagName>";
	public static final String FORCE_STOP_SERVER_SHORT = "f";
	public static final String FORCE_STOP_SERVER_LONG = "force";
	public static final String FORCE_STOP_SERVER_DESC = "don't wait for queued commands";
	public static final String ITEM_FILTER_ARG = "filterExpr";
	public static final String ITEM_FILTER_SHORT = "if";
	public static final String ITEM_FILTER_LONG = "item-filter";
	public static final String ITEM_FILTER_DESC = "item filter expression";
	public static final String REPOSITORY_LOCAL_ID = "local";
	public static final String REPOSITORY_REMOTE_ID = "remote";
	public static final String PROMPT = "talend> ";
	public static final String UTF8 = "UTF8";
	public static final String COPYRIGHT = " Copyright (C) 2006-2016 Talend - www.talend.com";
	public static final String LOCALHOST = "localhost";
	public static final String EXECUTION_SERVER_TEMP_FILE_NAME = "talend_commandline_execution_server_temp_file";
	public static final String EXECUTION_SERVER_TEMP_FILE_EXT = ".zip";
	public static final String LOG_FILE = "logFile";
	public static final String MEMORY_STATUS = "memoStatus";
	public static final String DESTINATION_DIRECTORY = "DESTINATION_DIRECTORY";
	public static final String AECHIVE_FILENAME = "AECHIVE_FILENAME";
	public static final String JOB_CONTEXT = "JOB_CONTEXT";
	public static final String JOB_ADD_LOG4J_LEVEL = "JOB_ADD_LOG4J_LEVEL";
	public static final String JOB_VERSION = "JOB_VERSION";
	public static final String JOB_APPLY_CONTEXT_TO_CHILDREN = "JOB_APPLY_CONTEXT_TO_CHILDREN";
	public static final String JOB_ADD_STATISTICS_CODE = "JOB_ADD_STATISTICS_CODE";
	public static final String JOB_EXPORT_TO_OSGI_CODE = "JOB_EXPORT_TO_OSGI_CODE";
	public static final String JOB_ADD_ANT_SCRIPT = "JOB_ADD_ANT_SCRIPT";
	public static final String JOB_ADD_MAVEN_SCRIPT = "JOB_ADD_MAVEN_SCRIPT";
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\ICommandLineConstants.class Java compiler version:
 * 8 (52.0) JD-Core Version: 0.7.1
 */