package org.talend.commandline.client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class CommandLineAbstractClient {
	private Socket socket;
	protected InputStream inputStream;
	protected OutputStream outputStream;
	protected String hostname;
	protected int port;
	protected PrintWriter printWriter;

	public CommandLineAbstractClient(String hostname) {
		this(hostname, ICommandLineConstants.SERVER_PORT_ARG_DEFAULT_INTEGER);
	}

	public CommandLineAbstractClient(int port) {
		this("localhost", Integer.valueOf(port));
	}

	public CommandLineAbstractClient() {
		this("localhost", ICommandLineConstants.SERVER_PORT_ARG_DEFAULT_INTEGER);
	}

	public CommandLineAbstractClient(String hostname, Integer port) {
		this.hostname = hostname;
		this.port = port.intValue();
	}

	public void connect() throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		this.socket = new Socket(this.hostname, this.port);
		this.inputStream = new BufferedInputStream(this.socket.getInputStream());
		this.outputStream = new BufferedOutputStream(this.socket.getOutputStream());
		this.printWriter = new PrintWriter(new OutputStreamWriter(this.outputStream));
	}

	public void disconnect() throws IOException {
		if (this.socket != null) {
			this.socket.close();
		}
	}

	protected void sendCommand(String commandString) {
		this.printWriter.println(commandString);
		this.printWriter.flush();
	}

	public String getHostname() {
		return this.hostname;
	}

	public int getPort() {
		return this.port;
	}

	public String toString() {
		return getClass().getSimpleName() + " on " + this.hostname + ":" + this.port;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\CommandLineAbstractClient.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */