package org.talend.commandline.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import org.talend.commandline.client.util.ConsoleUtil;
import org.talend.commandline.client.util.ReaderWriterHandler;

public class CommandLineTextClient extends CommandLineAbstractClient {
	public CommandLineTextClient(int port) {
		super(port);
	}

	public CommandLineTextClient(String hostname, Integer port) {
		super(hostname, port);
	}

	public CommandLineTextClient(String hostname) {
		super(hostname);
	}

	public static void main(String[] args)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException, InterruptedException {
		CommandLineTextClient client;
		if (args.length == 1) {
			client = new CommandLineTextClient(args[0]);
		} else {
			CommandLineTextClient client;
			if (args.length == 2) {
				client = new CommandLineTextClient(args[0], Integer.valueOf(Integer.parseInt(args[1])));
			} else {
				System.out.println("Arguments : host [port]");
				return;
			}
		}
		CommandLineTextClient client;
		client.connect();
		client.init();
		client.disconnect();
	}

	private void init() throws IOException {
		ConsoleUtil.set();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

		String readLine = null;
		readUntilPrompt();
		boolean connected = true;
		while (connected) {
			try {
				readLine = bufferedReader.readLine();
				sendCommand(readLine);
				readUntilPrompt();
			} catch (IOException localIOException) {
				connected = false;
				System.out.println("Connection closed by foreign host.");
			}
		}

		ConsoleUtil.unset();
	}

	private void readUntilPrompt() throws IOException {
		ReaderWriterHandler readerWriterHandler = new ReaderWriterHandler(this.inputStream,
				new OutputStreamWriter(System.out));
		readerWriterHandler.readUntil("talend> ");
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\CommandLineTextClient.class Java compiler version:
 * 8 (52.0) JD-Core Version: 0.7.1
 */