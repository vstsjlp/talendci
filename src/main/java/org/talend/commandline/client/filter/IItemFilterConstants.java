package org.talend.commandline.client.filter;

public abstract interface IItemFilterConstants {
	public static final char BEGIN_BLOCK = '(';
	public static final char END_BLOCK = ')';
	public static final char EQUALS = '=';
	public static final char DBL_QUOTE = '"';
	public static final String OR = "or";
	public static final String AND = "and";
	public static final String LABEL = "label";
	public static final String TYPE = "type";
	public static final String VERSION = "version";
	public static final String AUTHOR = "author";
	public static final String STATUS = "status";
	public static final String PATH = "path";
}
