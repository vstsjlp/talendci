package org.talend.commandline.client.filter;

import org.talend.commandline.client.util.ItemFilterStringBuilder;

public abstract class SimpleFilter extends ItemFilter {
	protected String value;

	public SimpleFilter() {
	}

	public SimpleFilter(String value) {
		setValue(value);
	}

	public abstract char getComparatorChar();

	public abstract String getProperty();

	public String getPattern() {
		return getProperty() + getComparatorChar();
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String writeToString() {
		return new ItemFilterStringBuilder().writeSimple(getProperty(), getComparatorChar(), getValue());
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\filter\SimpleFilter.class Java compiler version: 8
 * (52.0) JD-Core Version: 0.7.1
 */