package org.talend.commandline.client.filter;

public class EqualsVersionFilter extends VersionFilter {
	public EqualsVersionFilter(String string) {
		super(string);
	}

	public EqualsVersionFilter() {
	}

	public char getComparatorChar() {
		return '=';
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\filter\EqualsVersionFilter.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */