package org.talend.commandline.client.filter;

public class WildcardPathFilter extends SimpleFilter {
	public WildcardPathFilter(String string) {
		super(string);
	}

	public WildcardPathFilter() {
	}

	public String getProperty() {
		return "path";
	}

	public char getComparatorChar() {
		return '%';
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\filter\WildcardPathFilter.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */