package org.talend.commandline.client.filter;

import java.util.ArrayList;
import java.util.List;
import org.talend.commandline.client.util.ItemFilterStringBuilder;

public abstract class GroupFilter extends ItemFilter {
	protected List<ItemFilter> childs = new ArrayList();

	public abstract GroupFilter addChild(ItemFilter paramItemFilter);

	public List<ItemFilter> getChilds() {
		return this.childs;
	}

	public String writeToString() {
		return new ItemFilterStringBuilder().writeGroup(this.childs, getGroupSeparator());
	}

	public abstract String getGroupSeparator();
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\filter\GroupFilter.class Java compiler version: 8
 * (52.0) JD-Core Version: 0.7.1
 */