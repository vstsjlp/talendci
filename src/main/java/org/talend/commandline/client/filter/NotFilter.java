package org.talend.commandline.client.filter;

import org.talend.commandline.client.util.ItemFilterStringBuilder;

public class NotFilter extends ItemFilter {
	private ItemFilter negatedFilter;

	public NotFilter(ItemFilter filter) {
		this.negatedFilter = filter;
	}

	public ItemFilter getNegatedFilter() {
		return this.negatedFilter;
	}

	public String writeToString() {
		return new ItemFilterStringBuilder().writeNot(this.negatedFilter);
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\filter\NotFilter.class Java compiler version: 8
 * (52.0) JD-Core Version: 0.7.1
 */