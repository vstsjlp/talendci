package org.talend.commandline.client.filter;

import java.util.List;

public class AndGroupFilter extends GroupFilter {
	public AndGroupFilter addChild(ItemFilter filter) {
		this.childs.add(filter);
		return this;
	}

	public String getGroupSeparator() {
		return "and";
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\filter\AndGroupFilter.class Java compiler version:
 * 8 (52.0) JD-Core Version: 0.7.1
 */