package org.talend.commandline.client.filter;

import java.util.List;

public class UniqueElementGroupFilter extends GroupFilter {
	public UniqueElementGroupFilter addChild(ItemFilter filter) {
		if (this.childs.size() > 0) {
			throw new IllegalStateException(getClass().getName() + " could not have more than one child");
		}
		this.childs.add(filter);
		return this;
	}

	public String getGroupSeparator() {
		return null;
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\filter\UniqueElementGroupFilter.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */