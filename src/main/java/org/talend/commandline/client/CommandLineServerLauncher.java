package org.talend.commandline.client;

import java.io.IOException;

public class CommandLineServerLauncher {
	private Process process;

	public String createStartServerCommandLine(String tisPath, boolean consoleLog) {
		return createStartServerCommandLine(tisPath, ICommandLineConstants.SERVER_PORT_ARG_DEFAULT_INTEGER.intValue(),
				consoleLog);
	}

	public String createStartServerCommandLine(String tisPath, int port, boolean consoleLog) {
		StringBuffer buffer = new StringBuffer(100);
		buffer.append(tisPath);
		buffer.append(" ");
		buffer.append("startServer");
		buffer.append(" ");
		buffer.append("-");
		buffer.append("p");
		buffer.append(" ");
		buffer.append(port);
		buffer.append(" ");
		buffer.append("-application");
		buffer.append(" ");
		buffer.append("org.talend.commandline.CommandLine");
		if (consoleLog) {
			buffer.append(" -consoleLog");
		}
		return buffer.toString();
	}

	public void start(String tisPath, int port, boolean consoleLog) throws IOException {
		String commandLine = createStartServerCommandLine(tisPath, port, consoleLog);
		doStart(commandLine);
	}

	public void start(String tisPath, boolean consoleLog) throws IOException {
		String commandLine = createStartServerCommandLine(tisPath, consoleLog);
		doStart(commandLine);
	}

	private void doStart(String commandLine) throws IOException {
		if (this.process != null) {
			throw new IllegalStateException();
		}
		this.process = Runtime.getRuntime().exec(commandLine);
	}

	public void waitFor() throws InterruptedException {
		if (this.process != null) {
			this.process.waitFor();
		} else {
			throw new IllegalStateException();
		}
	}

	public void kill() {
		if (this.process != null) {
			this.process.destroy();
		} else {
			throw new IllegalStateException();
		}
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\CommandLineServerLauncher.class Java compiler
 * version: 8 (52.0) JD-Core Version: 0.7.1
 */