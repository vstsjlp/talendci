package org.talend.commandline.client;

import java.io.IOException;
import java.io.PrintWriter;
import org.talend.commandline.client.command.CommandStatus;
import org.talend.commandline.client.command.CommandStatus.CommandStatusLevel;
import org.talend.commandline.client.command.IJavaCommand;
import org.talend.commandline.client.command.JavaServerCommand;
import org.talend.commandline.client.command.extension.ListCommandCommand;
import org.talend.commandline.client.command.extension.ListProjectsCommand;
import org.talend.commandline.client.filter.ItemFilter;
import org.talend.commandline.client.model.CommandLineCommandBean;
import org.talend.commandline.client.model.EProtocolResponse;
import org.talend.commandline.client.util.Base64SerializationHelper;
import org.talend.commandline.client.util.CommandStringBuilder;
import org.talend.commandline.client.util.FileTransferHandler;
import org.talend.commandline.client.util.ResponseInterpreter;

public class CommandLineJavaClient extends CommandLineAbstractClient {
	private boolean passed;
	private IOException ioException;
	private CommandLineJavaCLientException cmdClientException;
	private CommandLineJavaServerException cmdServerException;
	private static int timeout = 30;

	public CommandLineJavaClient(String hostname, int port) {
		super(hostname, Integer.valueOf(port));
	}

	public CommandLineJavaClient(String hostname) {
		super(hostname);
	}

	public CommandLineJavaClient(int port) {
		super(port);
	}

	public CommandLineJavaClient() {
	}

	public void connect() throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		connect(timeout);
	}

	public void connect(int timeout)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		this.passed = false;
		this.ioException = null;
		this.cmdClientException = null;
		this.cmdServerException = null;

		if (timeout <= 0) {
			super.connect();
			addCommand("javaProtocol");
			return;
		}

		new Thread() {
			public void run() {
				CommandLineJavaClient.this.command();
			}
		}.start();
		try {
			synchronized (this) {
				wait(timeout * 1000);
			}
		} catch (InterruptedException e) {
			throw new CommandLineJavaCLientException(e);
		}
		if (this.passed) {
			return;
		}
		handleException();
	}

	private void command() {
		try {
			super.connect();
			addCommand("javaProtocol");
			synchronized (this) {
				this.passed = true;
				notify();
			}
		} catch (IOException e) {
			this.ioException = e;
		} catch (CommandLineJavaCLientException e) {
			this.cmdClientException = e;
		} catch (CommandLineJavaServerException e) {
			this.cmdServerException = e;
		}
	}

	public static void main(String[] args)
	{
		Byte code:
			System:currentTimeMillis	()J
			3: lstore_1
			CommandLineJavaClient
			7: dup
			8: ldc 111
			10: sipush 8002
			String;I)V
CommandLineJavaClient:connect	()V
19: goto +164 -> 183
22: astore_3
23: aload_3
IOException:printStackTrace	()V
PrintStream;
StringBuilder
33: dup
System:currentTimeMillis	()J
37: lload_1
38: lsub
39: ldc2_w 125
42: ldiv
String;
String;)V
49: ldc -123
StringBuilder;
String;
String;)V
60: goto +156 -> 216
63: astore_3
64: aload_3
CommandLineJavaCLientException:printStackTrace	()V
PrintStream;
StringBuilder
74: dup
System:currentTimeMillis	()J
78: lload_1
79: lsub
80: ldc2_w 125
83: ldiv
String;
String;)V
90: ldc -123
StringBuilder;
String;
String;)V
101: goto +115 -> 216
104: astore_3
105: aload_3
CommandLineJavaServerException:printStackTrace	()V
PrintStream;
StringBuilder
115: dup
System:currentTimeMillis	()J
119: lload_1
120: lsub
121: ldc2_w 125
124: ldiv
String;
String;)V
131: ldc -123
StringBuilder;
String;
String;)V
142: goto +74 -> 216
145: astore 4
PrintStream;
StringBuilder
153: dup
System:currentTimeMillis	()J
157: lload_1
158: lsub
159: ldc2_w 125
162: ldiv
String;
String;)V
169: ldc -123
StringBuilder;
String;
String;)V
180: aload 4
182: athrow
PrintStream;
StringBuilder
189: dup
System:currentTimeMillis	()J
193: lload_1
194: lsub
195: ldc2_w 125
198: ldiv
String;
String;)V
205: ldc -123
StringBuilder;
String;
String;)V
216: return
		Line number table:
			Java source line #116	-> byte code offset #0
			Java source line #118	-> byte code offset #4
			Java source line #119	-> byte code offset #19
			Java source line #121	-> byte code offset #23
			Java source line #129	-> byte code offset #27
			Java source line #122	-> byte code offset #63
			Java source line #124	-> byte code offset #64
			Java source line #129	-> byte code offset #68
			Java source line #125	-> byte code offset #104
			Java source line #127	-> byte code offset #105
			Java source line #129	-> byte code offset #109
			Java source line #128	-> byte code offset #145
			Java source line #129	-> byte code offset #147
			Java source line #130	-> byte code offset #180
			Java source line #129	-> byte code offset #183
			Java source line #131	-> byte code offset #216
			Local variable table:
				start	length	slot	name	signature
				0	217	0	args	String[]
						3	191	1	start	long
						22	2	3	e	IOException
						63	2	3	e	CommandLineJavaCLientException
						104	2	3	e	CommandLineJavaServerException
						145	36	4	localObject	Object
						Exception table:
							from	to	target	type
							IOException
							CommandLineJavaCLientException
							CommandLineJavaServerException
							4	27	145	finally
							63	68	145	finally
							104	109	145	finally
	}

	private void handleException() throws CommandLineJavaCLientException, IOException, CommandLineJavaServerException {
		if ((this.ioException == null) && (this.cmdClientException == null) && (this.cmdServerException == null)) {

			throw new CommandLineJavaCLientException("Timeout connected while trying to connect command line");
		}
		if (this.ioException != null)
			throw this.ioException;
		if (this.cmdClientException != null)
			throw this.cmdClientException;
		if (this.cmdServerException != null) {
			throw this.cmdServerException;
		}
	}

	public int addCommand(IJavaCommand command)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		return addCommand(command.writeToString());
	}

	private int addCommand(String commandString)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		return sendCommandAndParse(commandString, new ResponseInterpreter()).getAddedCommandId();
	}

	public String getResult(String commandString)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		return sendCommandAndParse(commandString, new ResponseInterpreter()).getResultString();
	}

	private ResponseInterpreter sendCommandAndParse(String commandString, ResponseInterpreter responseInterpreter)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		sendCommand(commandString);

		responseInterpreter.parse(this.inputStream);
		if (responseInterpreter.getException() != null) {
			throw responseInterpreter.getException();
		}

		return responseInterpreter;
	}

	public CommandStatus getStatus(int commandId)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		String command = "getCommandStatus " + commandId;
		return sendCommandAndParse(command, new ResponseInterpreter()).getCommandStatus();
	}

	public CommandStatus addCommandAndWait(IJavaCommand command)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException, InterruptedException {
		int commandId = addCommand(command.writeToString());

		return getCommandStatusAndWait(commandId);
	}

	public CommandStatus getCommandStatusAndWait(int commandId)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException, InterruptedException {
		CommandStatus status = getStatus(commandId);
		long sequence = 0L;
		while ((status.getLevel() != CommandStatus.CommandStatusLevel.COMPLETED)
				&& (status.getLevel() != CommandStatus.CommandStatusLevel.FAILED)) {
			if (sequence == Long.MAX_VALUE) {
				sequence = Long.MAX_VALUE;
			} else {
				sequence += 1L;
			}
			Thread.sleep(calcSleepTime(sequence));
			status = getStatus(commandId);
		}

		return status;
	}

	private long calcSleepTime(long sequence)
	{
		10L;
		long sleepTime = 100L;
		if (times > 0L) {
			sleepTime *= times;
			if (sleepTime < 0L) {
				sleepTime = Long.MAX_VALUE;
			} else if (sleepTime < 100L) {
				sleepTime = 100L;
			}
		}
		return sleepTime;
	}

	public String getServerVersion()
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		String command = "showVersion";
		return sendCommandAndParse(command, new ResponseInterpreter()).getResultString();
	}

	public void getServerFile(String remotePath, String localPath)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		CommandStringBuilder builder = new CommandStringBuilder("getFile");
		builder.addArgument(remotePath);
		builder.addArgument(localPath);

		ResponseInterpreter responseInterpreter = new ResponseInterpreter();
		responseInterpreter.setFilePath(localPath);

		sendCommandAndParse(builder.toString(), responseInterpreter);
	}

	public void putServerFile(String localPath, String remotePath)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		CommandStringBuilder builder = new CommandStringBuilder("putFile");
		builder.addArgument(localPath);
		builder.addArgument(remotePath);

		ResponseInterpreter responseInterpreter = new ResponseInterpreter();
		responseInterpreter.setFilePath(localPath);
		sendCommandAndParse(builder.toString(), responseInterpreter);
		if (!responseInterpreter.getResultString().equals("OK")) {
			throw new CommandLineJavaCLientException("Client Server communication error");
		}

		this.printWriter.println(EProtocolResponse.BEGIN.getName());
		this.printWriter.print(EProtocolResponse.FILE_SIZE.getName() + " ");
		this.printWriter.println(FileTransferHandler.getFileSize(localPath));
		this.printWriter.println(EProtocolResponse.FILE_CONTENT.getName());
		this.printWriter.flush();
		FileTransferHandler.sendFile(localPath, this.outputStream);
		this.printWriter.print(EProtocolResponse.FILE_MD5.getName() + " ");
		this.printWriter.println(FileTransferHandler.getMD5(localPath));

		responseInterpreter = new ResponseInterpreter();
		responseInterpreter.setFilePath(localPath);
		sendCommandAndParse(EProtocolResponse.END.getName(), responseInterpreter);
		if (!responseInterpreter.getResultString().equals("OK")) {
			throw new CommandLineJavaCLientException("Client Server communication error");
		}
	}

	public String listProjects(boolean showBranch)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		ListProjectsCommand command = new ListProjectsCommand(showBranch);
		String resultString = sendCommandAndParse(command.writeToString(), new ResponseInterpreter()).getResultString();
		try {
			return (String) new Base64SerializationHelper().decode(resultString);
		} catch (Exception e) {
			throw new CommandLineJavaCLientException(e);
		}
	}

	public String listItems(ItemFilter itemFilter, boolean withMetadata)
			throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		CommandStringBuilder builder = new CommandStringBuilder("listItem");
		if (itemFilter != null) {
			builder.addOptionWithArgument("if", itemFilter.writeToString());
		}
		if (withMetadata) {
			builder.addOption("m");
		}

		String resultString = sendCommandAndParse(builder.toString(), new ResponseInterpreter()).getResultString();
		try {
			return (String) new Base64SerializationHelper().decode(resultString);
		} catch (Exception e) {
			throw new CommandLineJavaCLientException(e);
		}
	}

	public CommandLineCommandBean[] listCommands(boolean listAllCommand, boolean listQueueCommand,
			boolean listRunningCommand)
					throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		JavaServerCommand listComamnd = new ListCommandCommand(listAllCommand, listQueueCommand, listRunningCommand);
		return sendCommandAndParse(listComamnd.writeToString(), new ResponseInterpreter()).getCommandlineCommandBeans();
	}

	public String listJob() throws IOException, CommandLineJavaCLientException, CommandLineJavaServerException {
		String command = "listJob";
		String resultString = sendCommandAndParse(command, new ResponseInterpreter()).getResultString();
		try {
			return (String) new Base64SerializationHelper().decode(resultString);
		} catch (Exception e) {
			throw new CommandLineJavaCLientException(e);
		}
	}
}

/*
 * Location:
 * C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.
 * 4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\org\
 * talend\commandline\client\CommandLineJavaClient.class Java compiler version:
 * 8 (52.0) JD-Core Version: 0.7.1
 */