/*     */ package org.talend.utils.time;
/*     */ 
/*     */ import java.io.PrintStream;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TimeTracer
/*     */ {
/*     */   private static final int MAX = 20;
/*     */   private static final String EMPTY = "";
/*     */   private static final String SPACE = " ";
/*     */   private Logger log;
/*     */   private String name;
/*  49 */   private int idx = 0;
/*     */   
/*  51 */   private long[] starts = new long[20];
/*     */   
/*  53 */   private static String[] textIndentations = ;
/*     */   
/*     */   private static String[] makeTextIndentations() {
/*  56 */     String[] indentations = new String[20];
/*  57 */     String m = "";
/*  58 */     for (int i = 0; i < 20; i++) {
/*  59 */       indentations[i] = m;
/*  60 */       m = m + "\t";
/*     */     }
/*  62 */     return indentations;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TimeTracer(String aName, Logger aLog)
/*     */   {
/*  72 */     this.log = aLog;
/*  73 */     this.name = (aName != null ? aName : "");
/*     */   }
/*     */   
/*     */   public void reset()
/*     */   {
/*  78 */     this.idx = 0;
/*     */   }
/*     */   
/*     */   public void start()
/*     */   {
/*  83 */     start("");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void start(String text)
/*     */   {
/*  93 */     String msg = textIndentations[this.idx];
/*  94 */     this.starts[this.idx] = System.currentTimeMillis();
/*  95 */     if (this.idx < 19) {
/*  96 */       this.idx += 1;
/*     */     }
/*  98 */     if ((text == null) || (text.length() == 0)) {
/*  99 */       return;
/*     */     }
/*     */     
/* 102 */     msg = msg + "ZZ***B:" + this.name + " " + text;
/* 103 */     show(msg);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public long end(int nbTest)
/*     */   {
/* 111 */     return end("", nbTest);
/*     */   }
/*     */   
/*     */   public long end()
/*     */   {
/* 116 */     return end("", 1);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public long end(String text)
/*     */   {
/* 124 */     return end(text, 1);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long end(String text, int nbTest)
/*     */   {
/* 138 */     if (this.idx > 0) {
/* 139 */       this.idx -= 1;
/*     */     }
/* 141 */     String msg = textIndentations[this.idx];
/* 142 */     long beginTime = this.starts[this.idx];
/* 143 */     long spent = System.currentTimeMillis() - beginTime;
/*     */     
/* 145 */     msg = msg + "ZZ***E:" + this.name + " ";
/* 146 */     if ((text != null) && (text.length() != 0)) {
/* 147 */       msg = msg + text;
/*     */     }
/*     */     
/* 150 */     if (nbTest >= 2) {
/* 151 */       msg = 
/* 152 */         msg + " nbTest=" + nbTest + " spent=" + spent + " ms" + " avg=" + spent / nbTest + " begin was" + beginTime;
/*     */     } else {
/* 154 */       msg = msg + " spent=" + spent + " ms";
/*     */     }
/* 156 */     show(msg);
/* 157 */     return spent;
/*     */   }
/*     */   
/*     */   private void show(String msg)
/*     */   {
/* 162 */     if (this.log != null) {
/* 163 */       this.log.info(msg);
/*     */     } else {
/* 165 */       System.out.println(msg);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\time\TimeTracer.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */