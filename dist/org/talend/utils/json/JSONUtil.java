/*    */ package org.talend.utils.json;
/*    */ 
/*    */ import java.util.Iterator;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class JSONUtil
/*    */ {
/*    */   public static JSONObject merge(JSONObject source, JSONObject target)
/*    */   {
/* 34 */     JSONObject mergedJson = new JSONObject();
/*    */     try {
/* 36 */       if ((source != null) && (target != null)) {
/* 37 */         mergedJson = new JSONObject(target.toString());
/*    */         
/* 39 */         Iterator<String> keys = source.keys();
/* 40 */         while (keys.hasNext()) {
/* 41 */           String key = (String)keys.next();
/* 42 */           Object o = source.get(key);
/* 43 */           if (mergedJson.has(key)) {
/* 44 */             if ((o instanceof JSONObject)) {
/* 45 */               JSONObject objectSource = (JSONObject)o;
/* 46 */               JSONObject objectTarget = mergedJson.getJSONObject(key);
/* 47 */               JSONObject subJson = merge(objectSource, objectTarget);
/* 48 */               mergedJson.put(key, subJson);
/* 49 */             } else if ((o instanceof JSONArray)) {
/* 50 */               JSONArray sourceArray = (JSONArray)o;
/* 51 */               JSONArray targetArray = mergedJson.getJSONArray(key);
/*    */               
/* 53 */               for (int i = 0; i < sourceArray.length(); i++) {
/* 54 */                 targetArray.put(sourceArray.get(i));
/*    */               }
/*    */             }
/*    */             else {
/* 58 */               mergedJson.put(key, o);
/*    */             }
/*    */           } else {
/* 61 */             mergedJson.put(key, o);
/*    */           }
/*    */         }
/*    */       }
/* 65 */       else if ((source == null) && (target != null)) {
/* 66 */         mergedJson = new JSONObject(target.toString());
/*    */       }
/* 68 */       else if ((target == null) && (source != null)) {
/* 69 */         mergedJson = new JSONObject(source.toString());
/*    */       }
/*    */     }
/*    */     catch (JSONException localJSONException) {}
/*    */     
/*    */ 
/* 75 */     return mergedJson;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\json\JSONUtil.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */