/*     */ package org.talend.utils.json;
/*     */ 
/*     */ import java.io.IOException;
/*     */ import java.io.Writer;
/*     */ import java.lang.reflect.Array;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.Iterator;
/*     */ import java.util.Map;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class JSONArray
/*     */ {
/*     */   private ArrayList myArrayList;
/*     */   
/*     */   public JSONArray()
/*     */   {
/*  75 */     this.myArrayList = new ArrayList();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray(JSONTokener x)
/*     */     throws JSONException
/*     */   {
/*  85 */     this();
/*  86 */     char c = x.nextClean();
/*     */     char q;
/*  88 */     if (c == '[') {
/*  89 */       q = ']'; } else { char q;
/*  90 */       if (c == '(') {
/*  91 */         q = ')';
/*     */       } else
/*  93 */         throw x.syntaxError("A JSONArray text must start with '['"); }
/*     */     char q;
/*  95 */     if (x.nextClean() == ']') {
/*  96 */       return;
/*     */     }
/*  98 */     x.back();
/*     */     for (;;) {
/* 100 */       if (x.nextClean() == ',') {
/* 101 */         x.back();
/* 102 */         synchronized (this.myArrayList) {
/* 103 */           this.myArrayList.add(null);
/*     */         }
/*     */       }
/* 106 */       x.back();
/* 107 */       synchronized (this.myArrayList) {
/* 108 */         this.myArrayList.add(x.nextValue());
/*     */       }
/*     */       
/* 111 */       c = x.nextClean();
/* 112 */       switch (c) {
/*     */       case ',': 
/*     */       case ';': 
/* 115 */         if (x.nextClean() == ']') {
/* 116 */           return;
/*     */         }
/* 118 */         x.back();
/*     */       }
/*     */       
/*     */     }
/* 122 */     if (q != c) {
/* 123 */       throw x.syntaxError("Expected a '" + new Character(q) + "'");
/*     */     }
/* 125 */     return;
/*     */     
/* 127 */     throw x.syntaxError("Expected a ',' or ']'");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray(String source)
/*     */     throws JSONException
/*     */   {
/* 140 */     this(new JSONTokener(source));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray(Collection collection)
/*     */   {
/* 149 */     this.myArrayList = (collection == null ? new ArrayList() : new ArrayList(collection));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray(Collection collection, boolean includeSuperClass)
/*     */   {
/* 159 */     this.myArrayList = new ArrayList();
/* 160 */     if (collection != null) {
/* 161 */       synchronized (this.myArrayList) {
/* 162 */         for (Iterator iter = collection.iterator(); iter.hasNext();) {
/* 163 */           this.myArrayList.add(new JSONObject(iter.next(), includeSuperClass));
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray(Object array)
/*     */     throws JSONException
/*     */   {
/* 175 */     this();
/* 176 */     if (array.getClass().isArray()) {
/* 177 */       int length = Array.getLength(array);
/* 178 */       for (int i = 0; i < length; i++) {
/* 179 */         put(Array.get(array, i));
/*     */       }
/*     */     } else {
/* 182 */       throw new JSONException("JSONArray initial value should be a string or collection or array.");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray(Object array, boolean includeSuperClass)
/*     */     throws JSONException
/*     */   {
/* 192 */     this();
/* 193 */     if (array.getClass().isArray()) {
/* 194 */       int length = Array.getLength(array);
/* 195 */       for (int i = 0; i < length; i++) {
/* 196 */         put(new JSONObject(Array.get(array, i), includeSuperClass));
/*     */       }
/*     */     } else {
/* 199 */       throw new JSONException("JSONArray initial value should be a string or collection or array.");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Object get(int index)
/*     */     throws JSONException
/*     */   {
/* 211 */     Object o = opt(index);
/* 212 */     if (o == null) {
/* 213 */       throw new JSONException("JSONArray[" + index + "] not found.");
/*     */     }
/* 215 */     return o;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean getBoolean(int index)
/*     */     throws JSONException
/*     */   {
/* 226 */     Object o = get(index);
/* 227 */     if ((o.equals(Boolean.FALSE)) || (((o instanceof String)) && (((String)o).equalsIgnoreCase("false"))))
/* 228 */       return false;
/* 229 */     if ((o.equals(Boolean.TRUE)) || (((o instanceof String)) && (((String)o).equalsIgnoreCase("true")))) {
/* 230 */       return true;
/*     */     }
/* 232 */     throw new JSONException("JSONArray[" + index + "] is not a Boolean.");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getDouble(int index)
/*     */     throws JSONException
/*     */   {
/* 243 */     Object o = get(index);
/*     */     try {
/* 245 */       return (o instanceof Number) ? ((Number)o).doubleValue() : Double.valueOf((String)o).doubleValue();
/*     */     } catch (Exception localException) {
/* 247 */       throw new JSONException("JSONArray[" + index + "] is not a number.");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getInt(int index)
/*     */     throws JSONException
/*     */   {
/* 260 */     Object o = get(index);
/* 261 */     return (o instanceof Number) ? ((Number)o).intValue() : (int)getDouble(index);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray getJSONArray(int index)
/*     */     throws JSONException
/*     */   {
/* 272 */     Object o = get(index);
/* 273 */     if ((o instanceof JSONArray)) {
/* 274 */       return (JSONArray)o;
/*     */     }
/* 276 */     throw new JSONException("JSONArray[" + index + "] is not a JSONArray.");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONObject getJSONObject(int index)
/*     */     throws JSONException
/*     */   {
/* 287 */     Object o = get(index);
/* 288 */     if ((o instanceof JSONObject)) {
/* 289 */       return (JSONObject)o;
/*     */     }
/* 291 */     throw new JSONException("JSONArray[" + index + "] is not a JSONObject.");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getLong(int index)
/*     */     throws JSONException
/*     */   {
/* 302 */     Object o = get(index);
/* 303 */     return (o instanceof Number) ? ((Number)o).longValue() : getDouble(index);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getString(int index)
/*     */     throws JSONException
/*     */   {
/* 314 */     return get(index).toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isNull(int index)
/*     */   {
/* 324 */     return JSONObject.NULL.equals(opt(index));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String join(String separator)
/*     */     throws JSONException
/*     */   {
/* 336 */     int len = length();
/* 337 */     StringBuffer sb = new StringBuffer();
/*     */     
/* 339 */     for (int i = 0; i < len; i++) {
/* 340 */       if (i > 0) {
/* 341 */         sb.append(separator);
/*     */       }
/* 343 */       sb.append(JSONObject.valueToString(this.myArrayList.get(i)));
/*     */     }
/* 345 */     return sb.toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int length()
/*     */   {
/* 354 */     return this.myArrayList.size();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Object opt(int index)
/*     */   {
/* 364 */     return (index < 0) || (index >= length()) ? null : this.myArrayList.get(index);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean optBoolean(int index)
/*     */   {
/* 375 */     return optBoolean(index, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean optBoolean(int index, boolean defaultValue)
/*     */   {
/*     */     try
/*     */     {
/* 388 */       return getBoolean(index);
/*     */     } catch (Exception localException) {}
/* 390 */     return defaultValue;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public double optDouble(int index)
/*     */   {
/* 402 */     return optDouble(index, NaN.0D);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public double optDouble(int index, double defaultValue)
/*     */   {
/*     */     try
/*     */     {
/* 415 */       return getDouble(index);
/*     */     } catch (Exception localException) {}
/* 417 */     return defaultValue;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int optInt(int index)
/*     */   {
/* 429 */     return optInt(index, 0);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int optInt(int index, int defaultValue)
/*     */   {
/*     */     try
/*     */     {
/* 442 */       return getInt(index);
/*     */     } catch (Exception localException) {}
/* 444 */     return defaultValue;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray optJSONArray(int index)
/*     */   {
/* 455 */     Object o = opt(index);
/* 456 */     return (o instanceof JSONArray) ? (JSONArray)o : null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONObject optJSONObject(int index)
/*     */   {
/* 467 */     Object o = opt(index);
/* 468 */     return (o instanceof JSONObject) ? (JSONObject)o : null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long optLong(int index)
/*     */   {
/* 479 */     return optLong(index, 0L);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long optLong(int index, long defaultValue)
/*     */   {
/*     */     try
/*     */     {
/* 492 */       return getLong(index);
/*     */     } catch (Exception localException) {}
/* 494 */     return defaultValue;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String optString(int index)
/*     */   {
/* 506 */     return optString(index, "");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String optString(int index, String defaultValue)
/*     */   {
/* 517 */     Object o = opt(index);
/* 518 */     return o != null ? o.toString() : defaultValue;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(boolean value)
/*     */   {
/* 528 */     put(value ? Boolean.TRUE : Boolean.FALSE);
/* 529 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(Collection value)
/*     */   {
/* 539 */     put(new JSONArray(value));
/* 540 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(double value)
/*     */     throws JSONException
/*     */   {
/* 551 */     Double d = new Double(value);
/* 552 */     JSONObject.testValidity(d);
/* 553 */     put(d);
/* 554 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(int value)
/*     */   {
/* 564 */     put(new Integer(value));
/* 565 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(long value)
/*     */   {
/* 575 */     put(new Long(value));
/* 576 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(Map value)
/*     */   {
/* 586 */     put(new JSONObject(value));
/* 587 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(Object value)
/*     */   {
/* 598 */     synchronized (this.myArrayList) {
/* 599 */       this.myArrayList.add(value);
/*     */     }
/* 601 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(int index, boolean value)
/*     */     throws JSONException
/*     */   {
/* 614 */     put(index, value ? Boolean.TRUE : Boolean.FALSE);
/* 615 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(int index, Collection value)
/*     */     throws JSONException
/*     */   {
/* 627 */     put(index, new JSONArray(value));
/* 628 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(int index, double value)
/*     */     throws JSONException
/*     */   {
/* 641 */     put(index, new Double(value));
/* 642 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(int index, int value)
/*     */     throws JSONException
/*     */   {
/* 655 */     put(index, new Integer(value));
/* 656 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(int index, long value)
/*     */     throws JSONException
/*     */   {
/* 669 */     put(index, new Long(value));
/* 670 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(int index, Map value)
/*     */     throws JSONException
/*     */   {
/* 682 */     put(index, new JSONObject(value));
/* 683 */     return this;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONArray put(int index, Object value)
/*     */     throws JSONException
/*     */   {
/* 697 */     JSONObject.testValidity(value);
/* 698 */     if (index < 0) {
/* 699 */       throw new JSONException("JSONArray[" + index + "] not found.");
/*     */     }
/* 701 */     if (index < length()) {
/* 702 */       this.myArrayList.set(index, value);
/*     */     } else {
/* 704 */       while (index != length()) {
/* 705 */         put(JSONObject.NULL);
/*     */       }
/* 707 */       put(value);
/*     */     }
/* 709 */     return this;
/*     */   }
/*     */   
/*     */   public void remove(Object value) {
/* 713 */     synchronized (this.myArrayList) {
/* 714 */       this.myArrayList.remove(value);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONObject toJSONObject(JSONArray names)
/*     */     throws JSONException
/*     */   {
/* 726 */     if ((names == null) || (names.length() == 0) || (length() == 0)) {
/* 727 */       return null;
/*     */     }
/* 729 */     JSONObject jo = new JSONObject();
/* 730 */     for (int i = 0; i < names.length(); i++) {
/* 731 */       jo.put(names.getString(i), opt(i));
/*     */     }
/* 733 */     return jo;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*     */     try
/*     */     {
/* 748 */       return '[' + join(",") + ']';
/*     */     } catch (Exception localException) {}
/* 750 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString(int indentFactor)
/*     */     throws JSONException
/*     */   {
/* 764 */     return toString(indentFactor, 0);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   String toString(int indentFactor, int indent)
/*     */     throws JSONException
/*     */   {
/* 777 */     int len = length();
/* 778 */     if (len == 0) {
/* 779 */       return "[]";
/*     */     }
/*     */     
/* 782 */     StringBuffer sb = new StringBuffer("[");
/* 783 */     if (len == 1) {
/* 784 */       sb.append(JSONObject.valueToString(this.myArrayList.get(0), indentFactor, indent));
/*     */     } else {
/* 786 */       int newindent = indent + indentFactor;
/* 787 */       sb.append('\n');
/* 788 */       for (int i = 0; i < len; i++) {
/* 789 */         if (i > 0) {
/* 790 */           sb.append(",\n");
/*     */         }
/* 792 */         for (int j = 0; j < newindent; j++) {
/* 793 */           sb.append(' ');
/*     */         }
/* 795 */         sb.append(JSONObject.valueToString(this.myArrayList.get(i), indentFactor, newindent));
/*     */       }
/* 797 */       sb.append('\n');
/* 798 */       for (i = 0; i < indent; i++) {
/* 799 */         sb.append(' ');
/*     */       }
/*     */     }
/* 802 */     sb.append(']');
/* 803 */     return sb.toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Writer write(Writer writer)
/*     */     throws JSONException
/*     */   {
/*     */     try
/*     */     {
/* 816 */       boolean b = false;
/* 817 */       int len = length();
/*     */       
/* 819 */       writer.write(91);
/*     */       
/* 821 */       for (int i = 0; i < len; i++) {
/* 822 */         if (b) {
/* 823 */           writer.write(44);
/*     */         }
/* 825 */         Object v = this.myArrayList.get(i);
/* 826 */         if ((v instanceof JSONObject)) {
/* 827 */           ((JSONObject)v).write(writer);
/* 828 */         } else if ((v instanceof JSONArray)) {
/* 829 */           ((JSONArray)v).write(writer);
/*     */         } else {
/* 831 */           writer.write(JSONObject.valueToString(v));
/*     */         }
/* 833 */         b = true;
/*     */       }
/* 835 */       writer.write(93);
/* 836 */       return writer;
/*     */     } catch (IOException e) {
/* 838 */       throw new JSONException(e);
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\json\JSONArray.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */