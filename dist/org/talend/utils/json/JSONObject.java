/*      */ package org.talend.utils.json;
/*      */ 
/*      */ import java.io.IOException;
/*      */ import java.io.Writer;
/*      */ import java.lang.reflect.Field;
/*      */ import java.lang.reflect.Method;
/*      */ import java.util.Collection;
/*      */ import java.util.HashMap;
/*      */ import java.util.Iterator;
/*      */ import java.util.Map;
/*      */ import java.util.Map.Entry;
/*      */ import java.util.Set;
/*      */ import java.util.TreeSet;
/*      */ import org.apache.log4j.Logger;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class JSONObject
/*      */ {
/*   77 */   private static Logger log = Logger.getLogger(JSONObject.class);
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */   private Map map;
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   private static final class Null
/*      */   {
/*      */     protected final Object clone()
/*      */     {
/*   92 */       return this;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     public boolean equals(Object object)
/*      */     {
/*  103 */       return (object == null) || (object == this);
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */     public String toString()
/*      */     {
/*  113 */       return "null";
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  127 */   public static final Object NULL = new Null(null);
/*      */   
/*      */ 
/*      */ 
/*      */   public JSONObject()
/*      */   {
/*  133 */     this.map = new HashMap();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject(JSONObject jo, String[] names)
/*      */     throws JSONException
/*      */   {
/*  145 */     this();
/*  146 */     String[] arrayOfString; int j = (arrayOfString = names).length; for (int i = 0; i < j; i++) { String name = arrayOfString[i];
/*  147 */       putOnce(name, jo.opt(name));
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject(JSONTokener x)
/*      */     throws JSONException
/*      */   {
/*  158 */     this();
/*      */     
/*      */ 
/*      */ 
/*  162 */     if (x.nextClean() != '{') {
/*  163 */       throw x.syntaxError("A JSONObject text must begin with '{'");
/*      */     }
/*      */     for (;;) {
/*  166 */       char c = x.nextClean();
/*  167 */       switch (c) {
/*      */       case '\000': 
/*  169 */         throw x.syntaxError("A JSONObject text must end with '}'");
/*      */       case '}': 
/*  171 */         return;
/*      */       }
/*  173 */       x.back();
/*  174 */       String key = x.nextValue().toString();
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  181 */       c = x.nextClean();
/*  182 */       if (c == '=') {
/*  183 */         if (x.next() != '>') {
/*  184 */           x.back();
/*      */         }
/*  186 */       } else if (c != ':') {
/*  187 */         throw x.syntaxError("Expected a ':' after a key");
/*      */       }
/*  189 */       putOnce(key, x.nextValue());
/*      */       
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*  195 */       switch (x.nextClean()) {
/*      */       case ',': 
/*      */       case ';': 
/*  198 */         if (x.nextClean() == '}') {
/*  199 */           return;
/*      */         }
/*  201 */         x.back();
/*      */       }
/*      */     }
/*  204 */     return;
/*      */     
/*  206 */     throw x.syntaxError("Expected a ',' or '}'");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject(Map map)
/*      */   {
/*  217 */     this.map = (map == null ? new HashMap() : map);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject(Map map, boolean includeSuperClass)
/*      */   {
/*  229 */     this.map = new HashMap();
/*  230 */     if (map != null) {
/*  231 */       for (Iterator i = map.entrySet().iterator(); i.hasNext();) {
/*  232 */         Map.Entry e = (Map.Entry)i.next();
/*  233 */         this.map.put(e.getKey(), new JSONObject(e.getValue(), includeSuperClass));
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject(Object bean)
/*      */   {
/*  254 */     this();
/*  255 */     populateInternalMap(bean, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject(Object bean, boolean includeSuperClass)
/*      */   {
/*  268 */     this();
/*  269 */     populateInternalMap(bean, includeSuperClass);
/*      */   }
/*      */   
/*      */   private void populateInternalMap(Object bean, boolean includeSuperClass) {
/*  273 */     Class klass = bean.getClass();
/*      */     
/*      */ 
/*      */ 
/*  277 */     if (klass.getClassLoader() == null) {
/*  278 */       includeSuperClass = false;
/*      */     }
/*      */     
/*  281 */     Method[] methods = includeSuperClass ? klass.getMethods() : klass.getDeclaredMethods();
/*  282 */     Method[] arrayOfMethod1; int j = (arrayOfMethod1 = methods).length; for (int i = 0; i < j; i++) { Method method = arrayOfMethod1[i];
/*      */       try {
/*  284 */         String name = method.getName();
/*  285 */         String key = "";
/*  286 */         if (name.startsWith("get")) {
/*  287 */           key = name.substring(3);
/*  288 */         } else if (name.startsWith("is")) {
/*  289 */           key = name.substring(2);
/*      */         }
/*  291 */         if ((key.length() > 0) && (Character.isUpperCase(key.charAt(0))) && (method.getParameterTypes().length == 0)) {
/*  292 */           if (key.length() == 1) {
/*  293 */             key = key.toLowerCase();
/*  294 */           } else if (!Character.isUpperCase(key.charAt(1))) {
/*  295 */             key = key.substring(0, 1).toLowerCase() + key.substring(1);
/*      */           }
/*      */           
/*  298 */           Object result = method.invoke(bean, null);
/*  299 */           if (result == null) {
/*  300 */             this.map.put(key, NULL);
/*  301 */           } else if (result.getClass().isArray()) {
/*  302 */             this.map.put(key, new JSONArray(result, includeSuperClass));
/*  303 */           } else if ((result instanceof Collection)) {
/*  304 */             this.map.put(key, new JSONArray((Collection)result, includeSuperClass));
/*  305 */           } else if ((result instanceof Map)) {
/*  306 */             this.map.put(key, new JSONObject((Map)result, includeSuperClass));
/*  307 */           } else if (isStandardProperty(result.getClass())) {
/*  308 */             this.map.put(key, result);
/*      */           }
/*  310 */           else if ((result.getClass().getPackage().getName().startsWith("java")) || 
/*  311 */             (result.getClass().getClassLoader() == null)) {
/*  312 */             this.map.put(key, result.toString());
/*      */           } else {
/*  314 */             this.map.put(key, new JSONObject(result, includeSuperClass));
/*      */           }
/*      */         }
/*      */       }
/*      */       catch (Exception e) {
/*  319 */         throw new RuntimeException(e);
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */   private boolean isStandardProperty(Class clazz) {
/*  325 */     return (clazz.isPrimitive()) || (clazz.isAssignableFrom(Byte.class)) || (clazz.isAssignableFrom(Short.class)) || 
/*  326 */       (clazz.isAssignableFrom(Integer.class)) || (clazz.isAssignableFrom(Long.class)) || 
/*  327 */       (clazz.isAssignableFrom(Float.class)) || (clazz.isAssignableFrom(Double.class)) || 
/*  328 */       (clazz.isAssignableFrom(Character.class)) || (clazz.isAssignableFrom(String.class)) || 
/*  329 */       (clazz.isAssignableFrom(Boolean.class));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject(Object object, String[] names)
/*      */   {
/*  341 */     this();
/*  342 */     Class c = object.getClass();
/*  343 */     String[] arrayOfString; int j = (arrayOfString = names).length; for (int i = 0; i < j; i++) { String name = arrayOfString[i];
/*      */       try {
/*  345 */         putOpt(name, c.getField(name).get(object));
/*      */       }
/*      */       catch (Exception localException) {}
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject(String source)
/*      */     throws JSONException
/*      */   {
/*  360 */     this(new JSONTokener(source));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject accumulate(String key, Object value)
/*      */     throws JSONException
/*      */   {
/*  374 */     testValidity(value);
/*  375 */     Object o = opt(key);
/*  376 */     if (o == null) {
/*  377 */       put(key, (value instanceof JSONArray) ? new JSONArray().put(value) : value);
/*  378 */     } else if ((o instanceof JSONArray)) {
/*  379 */       ((JSONArray)o).put(value);
/*      */     } else {
/*  381 */       put(key, new JSONArray().put(o).put(value));
/*      */     }
/*  383 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject append(String key, Object value)
/*      */     throws JSONException
/*      */   {
/*  397 */     testValidity(value);
/*  398 */     Object o = opt(key);
/*  399 */     if (o == null) {
/*  400 */       put(key, new JSONArray().put(value));
/*  401 */     } else if ((o instanceof JSONArray)) {
/*  402 */       put(key, ((JSONArray)o).put(value));
/*      */     } else {
/*  404 */       throw new JSONException("JSONObject[" + key + "] is not a JSONArray.");
/*      */     }
/*  406 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String doubleToString(double d)
/*      */   {
/*  416 */     if ((Double.isInfinite(d)) || (Double.isNaN(d))) {
/*  417 */       return "null";
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*  422 */     String s = Double.toString(d);
/*  423 */     if ((s.indexOf('.') > 0) && (s.indexOf('e') < 0) && (s.indexOf('E') < 0)) {
/*  424 */       while (s.endsWith("0")) {
/*  425 */         s = s.substring(0, s.length() - 1);
/*      */       }
/*  427 */       if (s.endsWith(".")) {
/*  428 */         s = s.substring(0, s.length() - 1);
/*      */       }
/*      */     }
/*  431 */     return s;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Object get(String key)
/*      */     throws JSONException
/*      */   {
/*  442 */     Object o = opt(key);
/*  443 */     if (o == null) {
/*  444 */       throw new JSONException("JSONObject[" + quote(key) + "] not found.");
/*      */     }
/*  446 */     return o;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean getBoolean(String key)
/*      */     throws JSONException
/*      */   {
/*  457 */     Object o = get(key);
/*  458 */     if ((o.equals(Boolean.FALSE)) || (((o instanceof String)) && (((String)o).equalsIgnoreCase("false"))))
/*  459 */       return false;
/*  460 */     if ((o.equals(Boolean.TRUE)) || (((o instanceof String)) && (((String)o).equalsIgnoreCase("true")))) {
/*  461 */       return true;
/*      */     }
/*  463 */     throw new JSONException("JSONObject[" + quote(key) + "] is not a Boolean.");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double getDouble(String key)
/*      */     throws JSONException
/*      */   {
/*  475 */     Object o = get(key);
/*      */     try {
/*  477 */       return (o instanceof Number) ? ((Number)o).doubleValue() : Double.valueOf((String)o).doubleValue();
/*      */     } catch (Exception localException) {
/*  479 */       throw new JSONException("JSONObject[" + quote(key) + "] is not a number.");
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int getInt(String key)
/*      */     throws JSONException
/*      */   {
/*  491 */     Object o = get(key);
/*  492 */     return (o instanceof Number) ? ((Number)o).intValue() : (int)getDouble(key);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONArray getJSONArray(String key)
/*      */     throws JSONException
/*      */   {
/*  503 */     Object o = get(key);
/*  504 */     if ((o instanceof JSONArray)) {
/*  505 */       return (JSONArray)o;
/*      */     }
/*  507 */     throw new JSONException("JSONObject[" + quote(key) + "] is not a JSONArray.");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject getJSONObject(String key)
/*      */     throws JSONException
/*      */   {
/*  518 */     Object o = get(key);
/*  519 */     if ((o instanceof JSONObject)) {
/*  520 */       return (JSONObject)o;
/*      */     }
/*  522 */     throw new JSONException("JSONObject[" + quote(key) + "] is not a JSONObject.");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public long getLong(String key)
/*      */     throws JSONException
/*      */   {
/*  533 */     Object o = get(key);
/*  534 */     return (o instanceof Number) ? ((Number)o).longValue() : getDouble(key);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String[] getNames(JSONObject jo)
/*      */   {
/*  543 */     int length = jo.length();
/*  544 */     if (length == 0) {
/*  545 */       return null;
/*      */     }
/*  547 */     Iterator i = jo.keys();
/*  548 */     String[] names = new String[length];
/*  549 */     int j = 0;
/*  550 */     while (i.hasNext()) {
/*  551 */       names[j] = ((String)i.next());
/*  552 */       j++;
/*      */     }
/*  554 */     return names;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String[] getNames(Object object)
/*      */   {
/*  563 */     if (object == null) {
/*  564 */       return null;
/*      */     }
/*  566 */     Class klass = object.getClass();
/*  567 */     Field[] fields = klass.getFields();
/*  568 */     int length = fields.length;
/*  569 */     if (length == 0) {
/*  570 */       return null;
/*      */     }
/*  572 */     String[] names = new String[length];
/*  573 */     for (int i = 0; i < length; i++) {
/*  574 */       names[i] = fields[i].getName();
/*      */     }
/*  576 */     return names;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String getString(String key)
/*      */     throws JSONException
/*      */   {
/*  587 */     return get(key).toString();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean has(String key)
/*      */   {
/*  597 */     return this.map.containsKey(key);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean isNull(String key)
/*      */   {
/*  607 */     return NULL.equals(opt(key));
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Iterator keys()
/*      */   {
/*  616 */     return this.map.keySet().iterator();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int length()
/*      */   {
/*  625 */     return this.map.size();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONArray names()
/*      */   {
/*  634 */     JSONArray ja = new JSONArray();
/*  635 */     Iterator keys = keys();
/*  636 */     while (keys.hasNext()) {
/*  637 */       ja.put(keys.next());
/*      */     }
/*  639 */     return ja.length() == 0 ? null : ja;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String numberToString(Number n)
/*      */     throws JSONException
/*      */   {
/*  650 */     if (n == null) {
/*  651 */       throw new JSONException("Null pointer");
/*      */     }
/*  653 */     testValidity(n);
/*      */     
/*      */ 
/*      */ 
/*  657 */     String s = n.toString();
/*  658 */     if ((s.indexOf('.') > 0) && (s.indexOf('e') < 0) && (s.indexOf('E') < 0)) {
/*  659 */       while (s.endsWith("0")) {
/*  660 */         s = s.substring(0, s.length() - 1);
/*      */       }
/*  662 */       if (s.endsWith(".")) {
/*  663 */         s = s.substring(0, s.length() - 1);
/*      */       }
/*      */     }
/*  666 */     return s;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Object opt(String key)
/*      */   {
/*  676 */     return key == null ? null : this.map.get(key);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean optBoolean(String key)
/*      */   {
/*  687 */     return optBoolean(key, false);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public boolean optBoolean(String key, boolean defaultValue)
/*      */   {
/*      */     try
/*      */     {
/*  700 */       return getBoolean(key);
/*      */     } catch (Exception localException) {}
/*  702 */     return defaultValue;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject put(String key, Collection value)
/*      */     throws JSONException
/*      */   {
/*  715 */     put(key, new JSONArray(value));
/*  716 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double optDouble(String key)
/*      */   {
/*  727 */     return optDouble(key, NaN.0D);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public double optDouble(String key, double defaultValue)
/*      */   {
/*      */     try
/*      */     {
/*  740 */       Object o = opt(key);
/*  741 */       return (o instanceof Number) ? ((Number)o).doubleValue() : new Double((String)o).doubleValue();
/*      */     } catch (Exception localException) {}
/*  743 */     return defaultValue;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int optInt(String key)
/*      */   {
/*  755 */     return optInt(key, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public int optInt(String key, int defaultValue)
/*      */   {
/*      */     try
/*      */     {
/*  768 */       return getInt(key);
/*      */     } catch (Exception localException) {}
/*  770 */     return defaultValue;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONArray optJSONArray(String key)
/*      */   {
/*  782 */     Object o = opt(key);
/*  783 */     return (o instanceof JSONArray) ? (JSONArray)o : null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject optJSONObject(String key)
/*      */   {
/*  794 */     Object o = opt(key);
/*  795 */     return (o instanceof JSONObject) ? (JSONObject)o : null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public long optLong(String key)
/*      */   {
/*  806 */     return optLong(key, 0L);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public long optLong(String key, long defaultValue)
/*      */   {
/*      */     try
/*      */     {
/*  819 */       return getLong(key);
/*      */     } catch (Exception localException) {}
/*  821 */     return defaultValue;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String optString(String key)
/*      */   {
/*  833 */     return optString(key, "");
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String optString(String key, String defaultValue)
/*      */   {
/*  844 */     Object o = opt(key);
/*  845 */     return o != null ? o.toString() : defaultValue;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject put(String key, boolean value)
/*      */     throws JSONException
/*      */   {
/*  857 */     put(key, value ? Boolean.TRUE : Boolean.FALSE);
/*  858 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject put(String key, double value)
/*      */     throws JSONException
/*      */   {
/*  870 */     put(key, new Double(value));
/*  871 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject put(String key, int value)
/*      */     throws JSONException
/*      */   {
/*  883 */     put(key, new Integer(value));
/*  884 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject put(String key, long value)
/*      */     throws JSONException
/*      */   {
/*  896 */     put(key, new Long(value));
/*  897 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject put(String key, Map value)
/*      */     throws JSONException
/*      */   {
/*  909 */     put(key, new JSONObject(value));
/*  910 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject put(String key, Object value)
/*      */     throws JSONException
/*      */   {
/*  924 */     if (key == null) {
/*  925 */       throw new JSONException("Null key.");
/*      */     }
/*  927 */     if (value != null) {
/*  928 */       testValidity(value);
/*  929 */       this.map.put(key, value);
/*      */     } else {
/*  931 */       remove(key);
/*      */     }
/*  933 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject putOnce(String key, Object value)
/*      */     throws JSONException
/*      */   {
/*  946 */     if ((key != null) && (value != null)) {
/*  947 */       if (opt(key) != null) {
/*  948 */         throw new JSONException("Duplicate key \"" + key + "\"");
/*      */       }
/*  950 */       put(key, value);
/*      */     }
/*  952 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONObject putOpt(String key, Object value)
/*      */     throws JSONException
/*      */   {
/*  965 */     if ((key != null) && (value != null)) {
/*  966 */       put(key, value);
/*      */     }
/*  968 */     return this;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static String quote(String string)
/*      */   {
/*  980 */     if ((string == null) || (string.length() == 0)) {
/*  981 */       return "\"\"";
/*      */     }
/*      */     
/*      */ 
/*  985 */     char c = '\000';
/*      */     
/*  987 */     int len = string.length();
/*  988 */     StringBuffer sb = new StringBuffer(len + 4);
/*      */     
/*      */ 
/*  991 */     sb.append('"');
/*  992 */     for (int i = 0; i < len; i++) {
/*  993 */       char b = c;
/*  994 */       c = string.charAt(i);
/*  995 */       switch (c) {
/*      */       case '"': 
/*      */       case '\\': 
/*  998 */         sb.append('\\');
/*  999 */         sb.append(c);
/* 1000 */         break;
/*      */       case '/': 
/* 1002 */         if (b == '<') {
/* 1003 */           sb.append('\\');
/*      */         }
/* 1005 */         sb.append(c);
/* 1006 */         break;
/*      */       case '\b': 
/* 1008 */         sb.append("\\b");
/* 1009 */         break;
/*      */       case '\t': 
/* 1011 */         sb.append("\\t");
/* 1012 */         break;
/*      */       case '\n': 
/* 1014 */         sb.append("\\n");
/* 1015 */         break;
/*      */       case '\f': 
/* 1017 */         sb.append("\\f");
/* 1018 */         break;
/*      */       case '\r': 
/* 1020 */         sb.append("\\r");
/* 1021 */         break;
/*      */       default: 
/* 1023 */         if ((c < ' ') || ((c >= '') && (c < ' ')) || ((c >= ' ') && (c < '℀'))) {
/* 1024 */           String t = "000" + Integer.toHexString(c);
/* 1025 */           sb.append("\\u" + t.substring(t.length() - 4));
/*      */         } else {
/* 1027 */           sb.append(c);
/*      */         }
/*      */         break; }
/*      */     }
/* 1031 */     sb.append('"');
/* 1032 */     return sb.toString();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Object remove(String key)
/*      */   {
/* 1042 */     return this.map.remove(key);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Iterator sortedKeys()
/*      */   {
/* 1051 */     return new TreeSet(this.map.keySet()).iterator();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public static Object stringToValue(String s)
/*      */   {
/* 1061 */     if (s.equals("")) {
/* 1062 */       return s;
/*      */     }
/* 1064 */     if (s.equalsIgnoreCase("true")) {
/* 1065 */       return Boolean.TRUE;
/*      */     }
/* 1067 */     if (s.equalsIgnoreCase("false")) {
/* 1068 */       return Boolean.FALSE;
/*      */     }
/* 1070 */     if (s.equalsIgnoreCase("null")) {
/* 1071 */       return NULL;
/*      */     }
/*      */     
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/* 1081 */     char b = s.charAt(0);
/* 1082 */     if (((b >= '0') && (b <= '9')) || (b == '.') || (b == '-') || (b == '+')) {
/* 1083 */       if (b == '0') {
/* 1084 */         if ((s.length() > 2) && ((s.charAt(1) == 'x') || (s.charAt(1) == 'X'))) {
/*      */           try {
/* 1086 */             return new Integer(Integer.parseInt(s.substring(2), 16));
/*      */           }
/*      */           catch (Exception localException1) {}
/*      */         } else {
/*      */           try
/*      */           {
/* 1092 */             return new Integer(Integer.parseInt(s, 8));
/*      */           }
/*      */           catch (Exception localException2) {}
/*      */         }
/*      */       }
/*      */       try
/*      */       {
/* 1099 */         return new Integer(s);
/*      */       } catch (Exception localException3) {
/*      */         try {
/* 1102 */           return new Long(s);
/*      */         } catch (Exception localException4) {
/*      */           try {
/* 1105 */             return new Double(s);
/*      */           }
/*      */           catch (Exception localException5) {}
/*      */         }
/*      */       }
/*      */     }
/*      */     
/* 1112 */     return s;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   static void testValidity(Object o)
/*      */     throws JSONException
/*      */   {
/* 1122 */     if (o != null) {
/* 1123 */       if ((o instanceof Double)) {
/* 1124 */         if ((((Double)o).isInfinite()) || (((Double)o).isNaN())) {
/* 1125 */           throw new JSONException("JSON does not allow non-finite numbers.");
/*      */         }
/* 1127 */       } else if (((o instanceof Float)) && (
/* 1128 */         (((Float)o).isInfinite()) || (((Float)o).isNaN()))) {
/* 1129 */         throw new JSONException("JSON does not allow non-finite numbers.");
/*      */       }
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public JSONArray toJSONArray(JSONArray names)
/*      */     throws JSONException
/*      */   {
/* 1144 */     if ((names == null) || (names.length() == 0)) {
/* 1145 */       return null;
/*      */     }
/* 1147 */     JSONArray ja = new JSONArray();
/* 1148 */     for (int i = 0; i < names.length(); i++) {
/* 1149 */       ja.put(opt(names.getString(i)));
/*      */     }
/* 1151 */     return ja;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String toString()
/*      */   {
/*      */     try
/*      */     {
/* 1166 */       Iterator keys = sortedKeys();
/* 1167 */       StringBuffer sb = new StringBuffer("{");
/*      */       
/* 1169 */       while (keys.hasNext()) {
/* 1170 */         if (sb.length() > 1) {
/* 1171 */           sb.append(',');
/*      */         }
/* 1173 */         Object o = keys.next();
/* 1174 */         sb.append(quote(o.toString()));
/* 1175 */         sb.append(':');
/* 1176 */         sb.append(valueToString(this.map.get(o)));
/*      */       }
/* 1178 */       sb.append('}');
/* 1179 */       return sb.toString();
/*      */     } catch (Exception localException) {}
/* 1181 */     return null;
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public String toString(int indentFactor)
/*      */     throws JSONException
/*      */   {
/* 1196 */     return toString(indentFactor, 0);
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   String toString(int indentFactor, int indent)
/*      */     throws JSONException
/*      */   {
/* 1212 */     int n = length();
/* 1213 */     if (n == 0) {
/* 1214 */       return "{}";
/*      */     }
/* 1216 */     Iterator keys = sortedKeys();
/* 1217 */     StringBuffer sb = new StringBuffer("{");
/* 1218 */     int newindent = indent + indentFactor;
/*      */     
/* 1220 */     if (n == 1) {
/* 1221 */       o = keys.next();
/* 1222 */       sb.append(quote(o.toString()));
/* 1223 */       sb.append(": ");
/* 1224 */       sb.append(valueToString(this.map.get(o), indentFactor, indent));
/*      */     } else {
/* 1226 */       while (keys.hasNext()) { Object o;
/* 1227 */         Object o = keys.next();
/* 1228 */         if (sb.length() > 1) {
/* 1229 */           sb.append(",\n");
/*      */         } else {
/* 1231 */           sb.append('\n');
/*      */         }
/* 1233 */         for (int j = 0; j < newindent; j++) {
/* 1234 */           sb.append(' ');
/*      */         }
/* 1236 */         sb.append(quote(o.toString()));
/* 1237 */         sb.append(": ");
/* 1238 */         sb.append(valueToString(this.map.get(o), indentFactor, newindent));
/*      */       }
/* 1240 */       if (sb.length() > 1) {
/* 1241 */         sb.append('\n');
/* 1242 */         for (int j = 0; j < indent; j++) {
/* 1243 */           sb.append(' ');
/*      */         }
/*      */       }
/*      */     }
/* 1247 */     sb.append('}');
/* 1248 */     return sb.toString();
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   static String valueToString(Object value)
/*      */     throws JSONException
/*      */   {
/* 1268 */     if ((value == null) || (value.equals(null))) {
/* 1269 */       return "null";
/*      */     }
/* 1271 */     if ((value instanceof JSONString))
/*      */     {
/*      */       try {
/* 1274 */         o = ((JSONString)value).toJSONString();
/*      */       } catch (Exception e) { Object o;
/* 1276 */         throw new JSONException(e); }
/*      */       Object o;
/* 1278 */       if ((o instanceof String)) {
/* 1279 */         return (String)o;
/*      */       }
/* 1281 */       throw new JSONException("Bad value from toJSONString: " + o);
/*      */     }
/* 1283 */     if ((value instanceof Number)) {
/* 1284 */       return numberToString((Number)value);
/*      */     }
/* 1286 */     if (((value instanceof Boolean)) || ((value instanceof JSONObject)) || ((value instanceof JSONArray))) {
/* 1287 */       return value.toString();
/*      */     }
/* 1289 */     if ((value instanceof Map)) {
/* 1290 */       return new JSONObject((Map)value).toString();
/*      */     }
/* 1292 */     if ((value instanceof Collection)) {
/* 1293 */       return new JSONArray((Collection)value).toString();
/*      */     }
/* 1295 */     if (value.getClass().isArray()) {
/* 1296 */       return new JSONArray(value).toString();
/*      */     }
/* 1298 */     return quote(value.toString());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   static String valueToString(Object value, int indentFactor, int indent)
/*      */     throws JSONException
/*      */   {
/* 1314 */     if ((value == null) || (value.equals(null))) {
/* 1315 */       return "null";
/*      */     }
/*      */     try {
/* 1318 */       if ((value instanceof JSONString)) {
/* 1319 */         Object o = ((JSONString)value).toJSONString();
/* 1320 */         if ((o instanceof String)) {
/* 1321 */           return (String)o;
/*      */         }
/*      */       }
/*      */     }
/*      */     catch (Exception localException)
/*      */     {
/* 1327 */       if ((value instanceof Number)) {
/* 1328 */         return numberToString((Number)value);
/*      */       }
/* 1330 */       if ((value instanceof Boolean)) {
/* 1331 */         return value.toString();
/*      */       }
/* 1333 */       if ((value instanceof JSONObject)) {
/* 1334 */         return ((JSONObject)value).toString(indentFactor, indent);
/*      */       }
/* 1336 */       if ((value instanceof JSONArray)) {
/* 1337 */         return ((JSONArray)value).toString(indentFactor, indent);
/*      */       }
/* 1339 */       if ((value instanceof Map)) {
/* 1340 */         return new JSONObject((Map)value).toString(indentFactor, indent);
/*      */       }
/* 1342 */       if ((value instanceof Collection)) {
/* 1343 */         return new JSONArray((Collection)value).toString(indentFactor, indent);
/*      */       }
/* 1345 */       if (value.getClass().isArray())
/* 1346 */         return new JSONArray(value).toString(indentFactor, indent);
/*      */     }
/* 1348 */     return quote(value.toString());
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */   public Writer write(Writer writer)
/*      */     throws JSONException
/*      */   {
/*      */     try
/*      */     {
/* 1361 */       boolean b = false;
/* 1362 */       Iterator keys = keys();
/* 1363 */       writer.write(123);
/*      */       
/* 1365 */       while (keys.hasNext()) {
/* 1366 */         if (b) {
/* 1367 */           writer.write(44);
/*      */         }
/* 1369 */         Object k = keys.next();
/* 1370 */         writer.write(quote(k.toString()));
/* 1371 */         writer.write(58);
/* 1372 */         Object v = this.map.get(k);
/* 1373 */         if ((v instanceof JSONObject)) {
/* 1374 */           ((JSONObject)v).write(writer);
/* 1375 */         } else if ((v instanceof JSONArray)) {
/* 1376 */           ((JSONArray)v).write(writer);
/*      */         } else {
/* 1378 */           writer.write(valueToString(v));
/*      */         }
/* 1380 */         b = true;
/*      */       }
/* 1382 */       writer.write(125);
/* 1383 */       return writer;
/*      */     } catch (IOException e) {
/* 1385 */       throw new JSONException(e);
/*      */     }
/*      */   }
/*      */   
/*      */ 
/*      */ 
/*      */   public Object fromJsonToObject(JSONObject ob, Class<?> clazz)
/*      */     throws JSONException
/*      */   {
/* 1394 */     Object o = null;
/*      */     try {
/* 1396 */       o = clazz.newInstance();
/* 1397 */       Field[] arrayOfField; int j = (arrayOfField = clazz.getDeclaredFields()).length; for (int i = 0; i < j; i++) { Field f = arrayOfField[i];
/* 1398 */         String name = f.getName();
/* 1399 */         String upperName = name.substring(0, 1).toUpperCase() + name.substring(1);
/*      */         try
/*      */         {
/* 1402 */           Method m = clazz.getDeclaredMethod("set" + upperName, new Class[] { f.getType() });
/* 1403 */           Object value = ob.get(name);
/* 1404 */           if ((f.getType().isAssignableFrom(String.class)) && (NULL == value)) {
/* 1405 */             value = "";
/*      */           }
/* 1407 */           m.invoke(o, new Object[] { value });
/*      */         } catch (NoSuchMethodException nsme) {
/* 1409 */           log.equals(nsme);
/*      */         }
/*      */       }
/*      */     } catch (Exception e) {
/* 1413 */       throw new JSONException(e);
/*      */     }
/* 1415 */     return o;
/*      */   }
/*      */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\json\JSONObject.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */