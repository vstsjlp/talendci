/*    */ package org.talend.utils.json;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class JSONException
/*    */   extends Exception
/*    */ {
/*    */   private Throwable cause;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public JSONException(String message)
/*    */   {
/* 19 */     super(message);
/*    */   }
/*    */   
/*    */   public JSONException(Throwable t) {
/* 23 */     super(t.getMessage());
/* 24 */     this.cause = t;
/*    */   }
/*    */   
/*    */   public Throwable getCause() {
/* 28 */     return this.cause;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\json\JSONException.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */