/*     */ package org.talend.utils.json;
/*     */ 
/*     */ import java.io.BufferedReader;
/*     */ import java.io.IOException;
/*     */ import java.io.Reader;
/*     */ import java.io.StringReader;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class JSONTokener
/*     */ {
/*     */   private int index;
/*     */   private Reader reader;
/*     */   private char lastChar;
/*     */   private boolean useLastChar;
/*     */   
/*     */   public JSONTokener(Reader reader)
/*     */   {
/*  50 */     this.reader = (reader.markSupported() ? reader : new BufferedReader(reader));
/*  51 */     this.useLastChar = false;
/*  52 */     this.index = 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONTokener(String s)
/*     */   {
/*  61 */     this(new StringReader(s));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public void back()
/*     */     throws JSONException
/*     */   {
/*  69 */     if ((this.useLastChar) || (this.index <= 0)) {
/*  70 */       throw new JSONException("Stepping back two steps is not supported");
/*     */     }
/*  72 */     this.index -= 1;
/*  73 */     this.useLastChar = true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int dehexchar(char c)
/*     */   {
/*  83 */     if ((c >= '0') && (c <= '9')) {
/*  84 */       return c - '0';
/*     */     }
/*  86 */     if ((c >= 'A') && (c <= 'F')) {
/*  87 */       return c - '7';
/*     */     }
/*  89 */     if ((c >= 'a') && (c <= 'f')) {
/*  90 */       return c - 'W';
/*     */     }
/*  92 */     return -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean more()
/*     */     throws JSONException
/*     */   {
/* 101 */     char nextChar = next();
/* 102 */     if (nextChar == 0) {
/* 103 */       return false;
/*     */     }
/* 105 */     back();
/* 106 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public char next()
/*     */     throws JSONException
/*     */   {
/* 115 */     if (this.useLastChar) {
/* 116 */       this.useLastChar = false;
/* 117 */       if (this.lastChar != 0) {
/* 118 */         this.index += 1;
/*     */       }
/* 120 */       return this.lastChar;
/*     */     }
/*     */     try
/*     */     {
/* 124 */       c = this.reader.read();
/*     */     } catch (IOException exc) { int c;
/* 126 */       throw new JSONException(exc);
/*     */     }
/*     */     int c;
/* 129 */     if (c <= 0) {
/* 130 */       this.lastChar = '\000';
/* 131 */       return '\000';
/*     */     }
/* 133 */     this.index += 1;
/* 134 */     this.lastChar = ((char)c);
/* 135 */     return this.lastChar;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public char next(char c)
/*     */     throws JSONException
/*     */   {
/* 146 */     char n = next();
/* 147 */     if (n != c) {
/* 148 */       throw syntaxError("Expected '" + c + "' and instead saw '" + n + "'");
/*     */     }
/* 150 */     return n;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String next(int n)
/*     */     throws JSONException
/*     */   {
/* 161 */     if (n == 0) {
/* 162 */       return "";
/*     */     }
/*     */     
/* 165 */     char[] buffer = new char[n];
/* 166 */     int pos = 0;
/*     */     
/* 168 */     if (this.useLastChar) {
/* 169 */       this.useLastChar = false;
/* 170 */       buffer[0] = this.lastChar;
/* 171 */       pos = 1;
/*     */     }
/*     */     try {
/*     */       int len;
/*     */       do {
/*     */         int len;
/* 177 */         pos += len;
/* 176 */         if (pos >= n) break; } while ((len = this.reader.read(buffer, pos, n - pos)) != -1);
/*     */     }
/*     */     catch (IOException exc)
/*     */     {
/* 180 */       throw new JSONException(exc);
/*     */     }
/* 182 */     this.index += pos;
/*     */     
/* 184 */     if (pos < n) {
/* 185 */       throw syntaxError("Substring bounds error");
/*     */     }
/*     */     
/* 188 */     this.lastChar = buffer[(n - 1)];
/* 189 */     return new String(buffer);
/*     */   }
/*     */   
/*     */ 
/*     */   public char nextClean()
/*     */     throws JSONException
/*     */   {
/*     */     char c;
/*     */     
/*     */     do
/*     */     {
/* 200 */       c = next();
/* 201 */     } while ((c != 0) && (c <= ' '));
/* 202 */     return c;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String nextString(char quote)
/*     */     throws JSONException
/*     */   {
/* 218 */     StringBuffer sb = new StringBuffer();
/*     */     for (;;) {
/* 220 */       char c = next();
/* 221 */       switch (c) {
/*     */       case '\000': 
/*     */       case '\n': 
/*     */       case '\r': 
/* 225 */         throw syntaxError("Unterminated string");
/*     */       case '\\': 
/* 227 */         c = next();
/* 228 */         switch (c) {
/*     */         case 'b': 
/* 230 */           sb.append('\b');
/* 231 */           break;
/*     */         case 't': 
/* 233 */           sb.append('\t');
/* 234 */           break;
/*     */         case 'n': 
/* 236 */           sb.append('\n');
/* 237 */           break;
/*     */         case 'f': 
/* 239 */           sb.append('\f');
/* 240 */           break;
/*     */         case 'r': 
/* 242 */           sb.append('\r');
/* 243 */           break;
/*     */         case 'u': 
/* 245 */           sb.append((char)Integer.parseInt(next(4), 16));
/* 246 */           break;
/*     */         case 'x': 
/* 248 */           sb.append((char)Integer.parseInt(next(2), 16));
/* 249 */           break;
/*     */         default: 
/* 251 */           sb.append(c);
/*     */         }
/* 253 */         break;
/*     */       default: 
/* 255 */         if (c == quote) {
/* 256 */           return sb.toString();
/*     */         }
/* 258 */         sb.append(c);
/*     */       }
/*     */       
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String nextTo(char d)
/*     */     throws JSONException
/*     */   {
/* 270 */     StringBuffer sb = new StringBuffer();
/*     */     for (;;) {
/* 272 */       char c = next();
/* 273 */       if ((c == d) || (c == 0) || (c == '\n') || (c == '\r')) {
/* 274 */         if (c != 0) {
/* 275 */           back();
/*     */         }
/* 277 */         return sb.toString().trim();
/*     */       }
/* 279 */       sb.append(c);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String nextTo(String delimiters)
/*     */     throws JSONException
/*     */   {
/* 292 */     StringBuffer sb = new StringBuffer();
/*     */     for (;;) {
/* 294 */       char c = next();
/* 295 */       if ((delimiters.indexOf(c) >= 0) || (c == 0) || (c == '\n') || (c == '\r')) {
/* 296 */         if (c != 0) {
/* 297 */           back();
/*     */         }
/* 299 */         return sb.toString().trim();
/*     */       }
/* 301 */       sb.append(c);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Object nextValue()
/*     */     throws JSONException
/*     */   {
/* 314 */     char c = nextClean();
/*     */     
/*     */ 
/* 317 */     switch (c) {
/*     */     case '"': 
/*     */     case '\'': 
/* 320 */       return nextString(c);
/*     */     case '{': 
/* 322 */       back();
/* 323 */       return new JSONObject(this);
/*     */     case '(': 
/*     */     case '[': 
/* 326 */       back();
/* 327 */       return new JSONArray(this);
/*     */     }
/*     */     
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/* 337 */     StringBuffer sb = new StringBuffer();
/* 338 */     while ((c >= ' ') && (",:]}/\\\"[{;=#".indexOf(c) < 0)) {
/* 339 */       sb.append(c);
/* 340 */       c = next();
/*     */     }
/* 342 */     back();
/*     */     
/* 344 */     String s = sb.toString().trim();
/* 345 */     if (s.equals("")) {
/* 346 */       throw syntaxError("Missing value");
/*     */     }
/* 348 */     return JSONObject.stringToValue(s);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public char skipTo(char to)
/*     */     throws JSONException
/*     */   {
/*     */     try
/*     */     {
/* 361 */       int startIndex = this.index;
/* 362 */       this.reader.mark(Integer.MAX_VALUE);
/*     */       char c;
/* 364 */       do { c = next();
/* 365 */         if (c == 0) {
/* 366 */           this.reader.reset();
/* 367 */           this.index = startIndex;
/* 368 */           return c;
/*     */         }
/* 370 */       } while (c != to);
/*     */     } catch (IOException exc) {
/* 372 */       throw new JSONException(exc);
/*     */     }
/*     */     char c;
/* 375 */     back();
/* 376 */     return c;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public JSONException syntaxError(String message)
/*     */   {
/* 386 */     return new JSONException(message + toString());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 396 */     return " at character " + this.index;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\json\JSONTokener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */