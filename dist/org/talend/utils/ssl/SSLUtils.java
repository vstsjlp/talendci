/*     */ package org.talend.utils.ssl;
/*     */ 
/*     */ import java.io.BufferedReader;
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.InputStreamReader;
/*     */ import java.net.URL;
/*     */ import java.security.KeyStore;
/*     */ import java.security.cert.CertificateException;
/*     */ import java.security.cert.X509Certificate;
/*     */ import javax.net.ssl.HostnameVerifier;
/*     */ import javax.net.ssl.HttpsURLConnection;
/*     */ import javax.net.ssl.KeyManager;
/*     */ import javax.net.ssl.KeyManagerFactory;
/*     */ import javax.net.ssl.SSLContext;
/*     */ import javax.net.ssl.SSLSession;
/*     */ import javax.net.ssl.SSLSocketFactory;
/*     */ import javax.net.ssl.TrustManager;
/*     */ import javax.net.ssl.TrustManagerFactory;
/*     */ import javax.net.ssl.X509TrustManager;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SSLUtils
/*     */ {
/*     */   private static SSLContext sslcontext;
/*     */   private static final String TAC_SSL_KEYSTORE = "clientKeystore.jks";
/*     */   private static final String TAC_SSL_TRUSTSTORE = "clientTruststore.jks";
/*     */   private static final String TAC_SSL_CLIENT_KEY = "tac.net.ssl.ClientKeyStore";
/*     */   private static final String TAC_SSL_CLIENT_TRUST_KEY = "tac.net.ssl.ClientTrustStore";
/*     */   private static final String TAC_SSL_KEYSTORE_PASS = "tac.net.ssl.KeyStorePass";
/*     */   private static final String TAC_SSL_TRUSTSTORE_PASS = "tac.net.ssl.TrustStorePass";
/*     */   
/*     */   public static String getContent(StringBuffer buffer, URL url, String userDir)
/*     */     throws Exception
/*     */   {
/*  64 */     BufferedReader in = null;
/*  65 */     if ("https".equals(url.getProtocol())) {
/*  66 */       SSLSocketFactory socketFactory = getSSLContext(userDir).getSocketFactory();
/*  67 */       HttpsURLConnection httpsCon = (HttpsURLConnection)url.openConnection();
/*  68 */       httpsCon.setSSLSocketFactory(socketFactory);
/*  69 */       httpsCon.setHostnameVerifier(new HostnameVerifier()
/*     */       {
/*     */         public boolean verify(String arg0, SSLSession arg1)
/*     */         {
/*  73 */           return true;
/*     */         }
/*  75 */       });
/*  76 */       httpsCon.connect();
/*  77 */       in = new BufferedReader(new InputStreamReader(httpsCon.getInputStream()));
/*     */     } else {
/*  79 */       in = new BufferedReader(new InputStreamReader(url.openStream()));
/*     */     }
/*     */     String inputLine;
/*  82 */     while ((inputLine = in.readLine()) != null) { String inputLine;
/*  83 */       buffer.append(inputLine);
/*     */     }
/*  85 */     in.close();
/*  86 */     return buffer.toString();
/*     */   }
/*     */   
/*     */   public static SSLContext getSSLContext(String userDir) throws Exception {
/*  90 */     if (sslcontext == null) {
/*  91 */       String keystorePath = System.getProperty("tac.net.ssl.ClientKeyStore");
/*  92 */       String trustStorePath = System.getProperty("tac.net.ssl.ClientTrustStore");
/*  93 */       String keystorePass = System.getProperty("tac.net.ssl.KeyStorePass");
/*  94 */       String truststorePass = System.getProperty("tac.net.ssl.TrustStorePass");
/*  95 */       if (keystorePath == null)
/*     */       {
/*     */ 
/*  98 */         File keystorePathFile = new File(userDir + "clientKeystore.jks");
/*  99 */         if (keystorePathFile.exists()) {
/* 100 */           keystorePath = keystorePathFile.getAbsolutePath();
/*     */         }
/*     */       }
/* 103 */       if (trustStorePath == null) {
/* 104 */         File trustStorePathFile = new File(userDir + "clientTruststore.jks");
/* 105 */         if (trustStorePathFile.exists()) {
/* 106 */           trustStorePath = trustStorePathFile.getAbsolutePath();
/*     */         }
/*     */       }
/* 109 */       if (keystorePass == null)
/*     */       {
/*     */ 
/* 112 */         keystorePass = "";
/*     */       }
/* 114 */       if (truststorePass == null)
/*     */       {
/*     */ 
/* 117 */         truststorePass = "";
/*     */       }
/*     */       
/* 120 */       sslcontext = SSLContext.getInstance("SSL");
/* 121 */       KeyManager[] keystoreManagers = null;
/* 122 */       if (keystorePath != null) {
/* 123 */         KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
/* 124 */         KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
/* 125 */         ks.load(new FileInputStream(keystorePath), keystorePass.toCharArray());
/* 126 */         kmf.init(ks, keystorePass.toCharArray());
/* 127 */         keystoreManagers = kmf.getKeyManagers();
/*     */       }
/*     */       
/* 130 */       TrustManager[] truststoreManagers = null;
/* 131 */       if (trustStorePath != null) {
/* 132 */         TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
/* 133 */         KeyStore tks = KeyStore.getInstance(KeyStore.getDefaultType());
/* 134 */         tks.load(new FileInputStream(trustStorePath), truststorePass.toCharArray());
/* 135 */         tmf.init(tks);
/* 136 */         truststoreManagers = tmf.getTrustManagers();
/*     */       } else {
/* 138 */         truststoreManagers = new TrustManager[] { new TrustAnyTrustManager(null) };
/*     */       }
/* 140 */       sslcontext.init(keystoreManagers, truststoreManagers, null);
/*     */     }
/* 142 */     return sslcontext;
/*     */   }
/*     */   
/*     */   private static class TrustAnyTrustManager
/*     */     implements X509TrustManager
/*     */   {
/*     */     public void checkClientTrusted(X509Certificate[] chain, String authType)
/*     */       throws CertificateException
/*     */     {}
/*     */     
/*     */     public void checkServerTrusted(X509Certificate[] chain, String authType)
/*     */       throws CertificateException
/*     */     {}
/*     */     
/*     */     public X509Certificate[] getAcceptedIssuers()
/*     */     {
/* 158 */       return new X509Certificate[0];
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\ssl\SSLUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */