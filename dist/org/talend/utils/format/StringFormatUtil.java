/*     */ package org.talend.utils.format;
/*     */ 
/*     */ import java.math.BigDecimal;
/*     */ import java.text.DecimalFormat;
/*     */ import java.text.ParseException;
/*     */ import java.util.Locale;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class StringFormatUtil
/*     */ {
/*     */   public static final int PERCENT = 0;
/*     */   public static final int INT_NUMBER = 1;
/*     */   public static final int DOUBLE_NUMBER = 2;
/*     */   public static final int OTHER = 99999;
/*     */   
/*     */   public static String padString(String stringToPad, int size)
/*     */   {
/*  43 */     return String.format("%" + size + "s", new Object[] { stringToPad });
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Object format(Object input, int style)
/*     */   {
/*     */     try
/*     */     {
/*  56 */       if (checkInput(input)) {
/*  57 */         DecimalFormat format = null;
/*     */         
/*     */ 
/*  60 */         BigDecimal zero = new BigDecimal(0);
/*  61 */         BigDecimal temp = new BigDecimal(input.toString());
/*  62 */         BigDecimal min = new BigDecimal(1.0E-4D);
/*  63 */         BigDecimal max = new BigDecimal(0.9999D);
/*  64 */         boolean isUseScientific = false;
/*  65 */         switch (style) {
/*     */         case 0: 
/*  67 */           if ((temp.compareTo(min) == -1) && (temp.compareTo(zero) == 1)) {
/*  68 */             isUseScientific = true;
/*  69 */           } else if ((temp.compareTo(max) == 1) && (temp.compareTo(new BigDecimal(1)) == -1)) {
/*  70 */             input = max.toString();
/*     */           }
/*  72 */           format = (DecimalFormat)DecimalFormat.getPercentInstance(Locale.ENGLISH);
/*  73 */           format.applyPattern("0.00%");
/*  74 */           break;
/*     */         case 1: 
/*  76 */           min = new BigDecimal(0.01D);
/*  77 */           if ((temp.compareTo(min) == -1) && (temp.compareTo(zero) == 1)) {
/*  78 */             isUseScientific = true;
/*     */           }
/*  80 */           format = (DecimalFormat)DecimalFormat.getNumberInstance(Locale.ENGLISH);
/*  81 */           format.applyPattern("0");
/*  82 */           break;
/*     */         case 2: 
/*  84 */           min = new BigDecimal(0.01D);
/*  85 */           if ((temp.compareTo(min) == -1) && (temp.compareTo(zero) == 1)) {
/*  86 */             isUseScientific = true;
/*     */           }
/*  88 */           format = (DecimalFormat)DecimalFormat.getNumberInstance(Locale.ENGLISH);
/*  89 */           format.applyPattern("0.00");
/*  90 */           break;
/*     */         default: 
/*  92 */           format = (DecimalFormat)DecimalFormat.getInstance(Locale.getDefault());
/*  93 */           return format.parse(input.toString());
/*     */         }
/*  95 */         if (isUseScientific) {
/*  96 */           format.applyPattern("0.###E0%");
/*     */         }
/*     */         
/*  99 */         return format.format(new Double(input.toString()));
/*     */       }
/* 101 */       return input;
/*     */     }
/*     */     catch (Exception localException) {}
/* 104 */     return input;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String formatPersent(Object input)
/*     */   {
/* 116 */     if (checkInput(input)) {
/* 117 */       Double db = new Double(input.toString());
/* 118 */       DecimalFormat format = (DecimalFormat)DecimalFormat.getPercentInstance(Locale.ENGLISH);
/* 119 */       format.applyPattern("0.00%");
/* 120 */       return format.format(db);
/*     */     }
/*     */     
/* 123 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Double formatDouble(Object input)
/*     */   {
/* 134 */     if (checkInput(input)) {
/* 135 */       Double db = new Double(input.toString());
/* 136 */       DecimalFormat format = (DecimalFormat)DecimalFormat.getNumberInstance(Locale.ENGLISH);
/* 137 */       format.applyPattern("0.00");
/* 138 */       return Double.valueOf(format.format(db));
/*     */     }
/*     */     
/* 141 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Double formatFourDecimalDouble(Object input)
/*     */   {
/* 152 */     if (checkInput(input)) {
/* 153 */       Double db = new Double(input.toString());
/* 154 */       DecimalFormat format = (DecimalFormat)DecimalFormat.getNumberInstance(Locale.ENGLISH);
/* 155 */       format.applyPattern("0.0000");
/* 156 */       return Double.valueOf(format.format(db));
/*     */     }
/*     */     
/* 159 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Double parseDouble(Object input)
/*     */   {
/* 169 */     if (input != null)
/*     */     {
/* 171 */       DecimalFormat format = (DecimalFormat)DecimalFormat.getInstance(Locale.US);
/*     */       try {
/* 173 */         Number number = format.parse(input.toString());
/* 174 */         return Double.valueOf(number.doubleValue());
/*     */       } catch (ParseException localParseException) {
/* 176 */         return Double.valueOf(NaN.0D);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/* 181 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean checkInput(Object input)
/*     */   {
/* 191 */     if ((input == null) || ("".equals(input))) {
/* 192 */       return false;
/*     */     }
/* 194 */     Double db = new Double(input.toString());
/* 195 */     if (db.equals(Double.valueOf(NaN.0D))) {
/* 196 */       return false;
/*     */     }
/*     */     
/*     */ 
/* 200 */     return true;
/*     */   }
/*     */   
/*     */   public static Double formatPercentDecimalDouble(Object input) {
/* 204 */     if (checkInput(input)) {
/* 205 */       BigDecimal bd1 = new BigDecimal(input.toString());
/* 206 */       BigDecimal bd2 = new BigDecimal("100");
/* 207 */       return Double.valueOf(bd1.multiply(bd2).doubleValue());
/*     */     }
/* 209 */     return null;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\format\StringFormatUtil.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */