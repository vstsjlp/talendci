/*     */ package org.talend.utils.format;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class PresentableBox
/*     */ {
/*     */   private static final String ENCAP = "!!";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static final String CR = "\n";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static final String DASH = "-";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static final String SPACE = " ";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static final int MAX_BOX_SIZE = 256;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  48 */   private static final String MANY_DASHES = makeRepeat("-", 256);
/*     */   
/*     */ 
/*  51 */   private static final String MANY_SPACES = makeRepeat(" ", 256);
/*     */   private final String title;
/*     */   private final String subTitle;
/*     */   private final int boxSize;
/*     */   private final String topLine;
/*     */   
/*     */   private static String makeRepeat(String in, int nbFold)
/*     */   {
/*  59 */     String ret = "";
/*  60 */     for (int i = 0; i < nbFold; i++) {
/*  61 */       ret = ret + in;
/*     */     }
/*  63 */     return ret;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private final String titleLine;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private final String subTitleLine;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private final String bottomLine;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private final String emptyLine;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTitleLine()
/*     */   {
/*  92 */     return this.titleLine;
/*     */   }
/*     */   
/*     */   public String getTopLine()
/*     */   {
/*  97 */     return this.topLine;
/*     */   }
/*     */   
/*     */   public String getBottomLine()
/*     */   {
/* 102 */     return this.bottomLine;
/*     */   }
/*     */   
/*     */   public String getEmptyLine()
/*     */   {
/* 107 */     return this.emptyLine;
/*     */   }
/*     */   
/*     */   public String getSubTitleLine()
/*     */   {
/* 112 */     return this.subTitleLine;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFullBox(String startTokAtEachLine)
/*     */   {
/* 134 */     String tmp = startTokAtEachLine == null ? "" : startTokAtEachLine;
/* 135 */     String tokPar = !tmp.startsWith("\n") ? "\n" + tmp : tmp;
/* 136 */     String ret = "";
/* 137 */     ret = ret + tokPar + getTopLine();
/* 138 */     ret = ret + tokPar + getEmptyLine();
/* 139 */     ret = ret + tokPar + getTitleLine();
/* 140 */     ret = ret + tokPar + getSubTitleLine();
/* 141 */     ret = ret + tokPar + getEmptyLine();
/* 142 */     ret = ret + tokPar + getBottomLine();
/* 143 */     return ret;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getFullBox()
/*     */   {
/* 153 */     return getFullBox("");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public PresentableBox(String aTitle, String aSubTitle, int aBoxSize)
/*     */   {
/* 162 */     this.title = (aTitle == null ? "" : aTitle.trim());
/* 163 */     this.subTitle = (aSubTitle == null ? "" : aSubTitle.trim());
/* 164 */     int maxTextSize = Math.max(this.title.length(), this.subTitle.length());
/*     */     
/*     */ 
/* 167 */     int lgAddedEncap = 2 * "!!".length();
/* 168 */     this.boxSize = Math.max(aBoxSize + aBoxSize % 2, maxTextSize + 2 * lgAddedEncap);
/*     */     
/* 170 */     this.topLine = surroundFill("-", "!!", "--", this.boxSize);
/* 171 */     this.bottomLine = this.topLine;
/* 172 */     this.emptyLine = surroundFill(" ", "!!", "  ", this.boxSize);
/* 173 */     this.titleLine = surroundFill(" ", "!!", this.title, this.boxSize);
/* 174 */     this.subTitleLine = surroundFill(" ", "!!", this.subTitle, this.boxSize);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static String surroundFill(String filler, String startEnd, String center, int totalSize)
/*     */   {
/* 184 */     int totFillers = totalSize - center.length() - startEnd.length();
/* 185 */     String filler1 = getFiller(filler).substring(0, totFillers / 2);
/* 186 */     String filler2 = getFiller(filler).substring(0, totFillers - totFillers / 2);
/* 187 */     return startEnd + filler1 + center + filler2 + startEnd;
/*     */   }
/*     */   
/*     */   private static String getFiller(String filler) {
/* 191 */     if (filler == null) {
/* 192 */       return MANY_SPACES;
/*     */     }
/* 194 */     if (filler.length() == 0) {
/* 195 */       return MANY_SPACES;
/*     */     }
/* 197 */     if (filler.charAt(0) == '-') {
/* 198 */       return MANY_DASHES;
/*     */     }
/* 200 */     return MANY_SPACES;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\format\PresentableBox.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */