/*     */ package org.talend.utils.network;
/*     */ 
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.net.ServerSocket;
/*     */ import java.util.Random;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ServerSocketFreePortOpener
/*     */ {
/*  27 */   private static Logger log = Logger.getLogger(ServerSocketFreePortOpener.class);
/*     */   
/*  29 */   private static Random random = new Random(System.currentTimeMillis());
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ServerSocket openServerSocket(int port)
/*     */     throws IOException
/*     */   {
/*  49 */     ServerSocket serverSocket = new ServerSocket(port);
/*  50 */     return serverSocket;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ServerSocket openServerSocketFromRangePort(int portRangeBound1, int portRangeBound2)
/*     */   {
/*  64 */     return openServerSocketFromRangePort(portRangeBound1, portRangeBound2, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ServerSocket openServerSocketFromRangePort(int portRangeBound1, int portRangeBound2, boolean randomizeIndexStart)
/*     */   {
/*  79 */     int portBoundMin = portRangeBound1 < portRangeBound2 ? portRangeBound1 : portRangeBound2;
/*  80 */     int portBoundMax = portRangeBound1 < portRangeBound2 ? portRangeBound2 : portRangeBound1;
/*  81 */     int increment = 0;
/*  82 */     if (randomizeIndexStart) {
/*  83 */       int maxRandomBound = (int)(portBoundMax - portBoundMin) * 3 / 4;
/*  84 */       if (maxRandomBound >= 0) {
/*  85 */         if (maxRandomBound == 0) {
/*  86 */           increment = 0;
/*     */         } else {
/*  88 */           increment = random.nextInt(maxRandomBound);
/*     */         }
/*  90 */         int portStart = portBoundMin + increment;
/*  91 */         boolean isFirstLoop = true;
/*  92 */         boolean isFirstPass = true;
/*     */         
/*  94 */         for (int port = portStart;; port++) {
/*  95 */           if (port > portBoundMax) {
/*  96 */             port = portBoundMin;
/*     */           }
/*  98 */           if ((!isFirstLoop) && (port == portStart)) {
/*  99 */             if (!isFirstPass) break;
/* 100 */             isFirstPass = false;
/*     */           }
/*     */           
/*     */ 
/*     */ 
/*     */           try
/*     */           {
/* 107 */             return openServerSocket(port);
/*     */ 
/*     */           }
/*     */           catch (IOException localIOException)
/*     */           {
/* 112 */             isFirstLoop = false;
/*     */           }
/*     */         }
/*     */       } }
/* 116 */     return null;
/*     */   }
/*     */   
/*     */   public static void main(String[] args) {
/* 120 */     ServerSocketFreePortOpener serverSocketFreePortOpener = new ServerSocketFreePortOpener();
/* 121 */     System.out.println(serverSocketFreePortOpener.openServerSocketFromRangePort(10, 20));
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\network\ServerSocketFreePortOpener.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */