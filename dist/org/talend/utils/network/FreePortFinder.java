/*     */ package org.talend.utils.network;
/*     */ 
/*     */ import java.io.PrintStream;
/*     */ import java.net.ServerSocket;
/*     */ import java.util.Collections;
/*     */ import java.util.HashSet;
/*     */ import java.util.Random;
/*     */ import java.util.Set;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class FreePortFinder
/*     */ {
/*  30 */   private static Logger log = Logger.getLogger(FreePortFinder.class);
/*     */   
/*  32 */   private static Random random = new Random(System.currentTimeMillis());
/*     */   
/*  34 */   private static Object[] randomLock = new Object[0];
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  45 */   public static Set<String> busyPorts = Collections.synchronizedSet(new HashSet());
/*     */   
/*     */   /* Error */
/*     */   public boolean isPortFree(int port)
/*     */   {
/*     */     // Byte code:
/*     */     //   0: iload_1
/*     */     //   1: invokestatic 61	java/lang/String:valueOf	(I)Ljava/lang/String;
/*     */     //   4: astore_2
/*     */     //   5: iconst_1
/*     */     //   6: istore_3
/*     */     //   7: getstatic 52	org/talend/utils/network/FreePortFinder:busyPorts	Ljava/util/Set;
/*     */     //   10: dup
/*     */     //   11: astore 4
/*     */     //   13: monitorenter
/*     */     //   14: getstatic 52	org/talend/utils/network/FreePortFinder:busyPorts	Ljava/util/Set;
/*     */     //   17: aload_2
/*     */     //   18: invokeinterface 67 2 0
/*     */     //   23: istore_3
/*     */     //   24: aconst_null
/*     */     //   25: astore 5
/*     */     //   27: iload_3
/*     */     //   28: ifne +134 -> 162
/*     */     //   31: new 73	java/net/ServerSocket
/*     */     //   34: dup
/*     */     //   35: iload_1
/*     */     //   36: invokespecial 75	java/net/ServerSocket:<init>	(I)V
/*     */     //   39: astore 5
/*     */     //   41: iconst_0
/*     */     //   42: istore_3
/*     */     //   43: goto +88 -> 131
/*     */     //   46: astore 6
/*     */     //   48: iconst_1
/*     */     //   49: istore_3
/*     */     //   50: getstatic 24	org/talend/utils/network/FreePortFinder:log	Lorg/apache/log4j/Logger;
/*     */     //   53: aload 6
/*     */     //   55: invokevirtual 78	java/lang/Throwable:getMessage	()Ljava/lang/String;
/*     */     //   58: invokevirtual 84	org/apache/log4j/Logger:debug	(Ljava/lang/Object;)V
/*     */     //   61: aload 5
/*     */     //   63: invokevirtual 88	java/net/ServerSocket:close	()V
/*     */     //   66: getstatic 52	org/talend/utils/network/FreePortFinder:busyPorts	Ljava/util/Set;
/*     */     //   69: aload_2
/*     */     //   70: invokeinterface 91 2 0
/*     */     //   75: pop
/*     */     //   76: goto +86 -> 162
/*     */     //   79: astore 8
/*     */     //   81: getstatic 24	org/talend/utils/network/FreePortFinder:log	Lorg/apache/log4j/Logger;
/*     */     //   84: aload 8
/*     */     //   86: invokevirtual 78	java/lang/Throwable:getMessage	()Ljava/lang/String;
/*     */     //   89: invokevirtual 84	org/apache/log4j/Logger:debug	(Ljava/lang/Object;)V
/*     */     //   92: goto +70 -> 162
/*     */     //   95: astore 7
/*     */     //   97: aload 5
/*     */     //   99: invokevirtual 88	java/net/ServerSocket:close	()V
/*     */     //   102: getstatic 52	org/talend/utils/network/FreePortFinder:busyPorts	Ljava/util/Set;
/*     */     //   105: aload_2
/*     */     //   106: invokeinterface 91 2 0
/*     */     //   111: pop
/*     */     //   112: goto +16 -> 128
/*     */     //   115: astore 8
/*     */     //   117: getstatic 24	org/talend/utils/network/FreePortFinder:log	Lorg/apache/log4j/Logger;
/*     */     //   120: aload 8
/*     */     //   122: invokevirtual 78	java/lang/Throwable:getMessage	()Ljava/lang/String;
/*     */     //   125: invokevirtual 84	org/apache/log4j/Logger:debug	(Ljava/lang/Object;)V
/*     */     //   128: aload 7
/*     */     //   130: athrow
/*     */     //   131: aload 5
/*     */     //   133: invokevirtual 88	java/net/ServerSocket:close	()V
/*     */     //   136: getstatic 52	org/talend/utils/network/FreePortFinder:busyPorts	Ljava/util/Set;
/*     */     //   139: aload_2
/*     */     //   140: invokeinterface 91 2 0
/*     */     //   145: pop
/*     */     //   146: goto +16 -> 162
/*     */     //   149: astore 8
/*     */     //   151: getstatic 24	org/talend/utils/network/FreePortFinder:log	Lorg/apache/log4j/Logger;
/*     */     //   154: aload 8
/*     */     //   156: invokevirtual 78	java/lang/Throwable:getMessage	()Ljava/lang/String;
/*     */     //   159: invokevirtual 84	org/apache/log4j/Logger:debug	(Ljava/lang/Object;)V
/*     */     //   162: aload 4
/*     */     //   164: monitorexit
/*     */     //   165: goto +7 -> 172
/*     */     //   168: aload 4
/*     */     //   170: monitorexit
/*     */     //   171: athrow
/*     */     //   172: iload_3
/*     */     //   173: ifeq +7 -> 180
/*     */     //   176: iconst_0
/*     */     //   177: goto +4 -> 181
/*     */     //   180: iconst_1
/*     */     //   181: ireturn
/*     */     // Line number table:
/*     */     //   Java source line #58	-> byte code offset #0
/*     */     //   Java source line #59	-> byte code offset #5
/*     */     //   Java source line #60	-> byte code offset #7
/*     */     //   Java source line #61	-> byte code offset #14
/*     */     //   Java source line #62	-> byte code offset #24
/*     */     //   Java source line #63	-> byte code offset #27
/*     */     //   Java source line #65	-> byte code offset #31
/*     */     //   Java source line #66	-> byte code offset #41
/*     */     //   Java source line #67	-> byte code offset #43
/*     */     //   Java source line #68	-> byte code offset #48
/*     */     //   Java source line #69	-> byte code offset #50
/*     */     //   Java source line #72	-> byte code offset #61
/*     */     //   Java source line #73	-> byte code offset #66
/*     */     //   Java source line #74	-> byte code offset #76
/*     */     //   Java source line #75	-> byte code offset #81
/*     */     //   Java source line #70	-> byte code offset #95
/*     */     //   Java source line #72	-> byte code offset #97
/*     */     //   Java source line #73	-> byte code offset #102
/*     */     //   Java source line #74	-> byte code offset #112
/*     */     //   Java source line #75	-> byte code offset #117
/*     */     //   Java source line #77	-> byte code offset #128
/*     */     //   Java source line #72	-> byte code offset #131
/*     */     //   Java source line #73	-> byte code offset #136
/*     */     //   Java source line #74	-> byte code offset #146
/*     */     //   Java source line #75	-> byte code offset #151
/*     */     //   Java source line #60	-> byte code offset #162
/*     */     //   Java source line #81	-> byte code offset #172
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	182	0	this	FreePortFinder
/*     */     //   0	182	1	port	int
/*     */     //   4	136	2	freePort	String
/*     */     //   6	167	3	isBusyPort	boolean
/*     */     //   11	158	4	Ljava/lang/Object;	Object
/*     */     //   25	107	5	serverSocket	ServerSocket
/*     */     //   46	8	6	e	Throwable
/*     */     //   95	34	7	localObject1	Object
/*     */     //   79	6	8	e	Throwable
/*     */     //   115	6	8	e	Throwable
/*     */     //   149	6	8	e	Throwable
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   31	43	46	java/lang/Throwable
/*     */     //   61	76	79	java/lang/Throwable
/*     */     //   31	61	95	finally
/*     */     //   97	112	115	java/lang/Throwable
/*     */     //   131	146	149	java/lang/Throwable
/*     */     //   14	165	168	finally
/*     */     //   168	171	168	finally
/*     */   }
/*     */   
/*     */   public void removePort(int port)
/*     */   {
/* 107 */     boolean containsPort = false;
/* 108 */     String usingPort = String.valueOf(port);
/* 109 */     synchronized (busyPorts) {
/* 110 */       containsPort = busyPorts.contains(usingPort);
/* 111 */       if (containsPort) {
/* 112 */         busyPorts.remove(usingPort);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   protected ServerSocket openServerSocket(int port)
/*     */   {
/* 125 */     ServerSocket serverSocket = null;
/*     */     try {
/* 127 */       serverSocket = new ServerSocket(port);
/*     */     } catch (Throwable e) {
/* 129 */       log.debug(e.getMessage());
/*     */     }
/* 131 */     return serverSocket;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int searchFreePort(int portRangeBound1, int portRangeBound2)
/*     */   {
/* 145 */     return opensServerSocketFromRangePort(portRangeBound1, portRangeBound2, true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int opensServerSocketFromRangePort(int portRangeBound1, int portRangeBound2, boolean randomizeIndexStart)
/*     */   {
/* 161 */     int portBoundMin = portRangeBound1 < portRangeBound2 ? portRangeBound1 : portRangeBound2;
/* 162 */     int portBoundMax = portRangeBound1 < portRangeBound2 ? portRangeBound2 : portRangeBound1;
/* 163 */     int increment = 0;
/* 164 */     if (randomizeIndexStart) {
/* 165 */       int maxRandomBound = (int)(portBoundMax - portBoundMin) * 3 / 4;
/* 166 */       if (maxRandomBound >= 0) {
/* 167 */         if (maxRandomBound == 0) {
/* 168 */           increment = 0;
/*     */         } else {
/* 170 */           synchronized (randomLock) {
/* 171 */             increment = random.nextInt(maxRandomBound);
/*     */           }
/*     */         }
/*     */         
/* 175 */         int portStart = portBoundMin + increment;
/* 176 */         boolean isFirstLoop = true;
/* 177 */         boolean isFirstPass = true;
/*     */         
/* 179 */         for (int port = portStart;; port++) {
/* 180 */           if (port > portBoundMax) {
/* 181 */             port = portBoundMin;
/*     */           }
/* 183 */           if ((!isFirstLoop) && (port == portStart)) {
/* 184 */             if (!isFirstPass) break;
/* 185 */             isFirstPass = false;
/*     */           }
/*     */           
/*     */ 
/*     */ 
/* 190 */           if (isPortFree(port)) {
/* 191 */             return port;
/*     */           }
/*     */           
/* 194 */           isFirstLoop = false;
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 199 */     return -1;
/*     */   }
/*     */   
/*     */   public static void main(String[] args) {
/* 203 */     FreePortFinder freePortFinder = new FreePortFinder();
/* 204 */     System.out.println(freePortFinder.searchFreePort(10, 20));
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\network\FreePortFinder.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */