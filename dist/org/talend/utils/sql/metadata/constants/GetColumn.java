package org.talend.utils.sql.metadata.constants;

public enum GetColumn
{
  TABLE_CAT,  TABLE_SCHEM,  TABLE_NAME,  COLUMN_NAME,  DATA_TYPE,  TYPE_NAME,  COLUMN_SIZE,  DECIMAL_DIGITS,  NUM_PREC_RADIX,  NULLABLE,  REMARKS,  COLUMN_DEF,  SQL_DATA_TYPE,  SQL_DATETIME_SUB,  CHAR_OCTET_LENGTH,  ORDINAL_POSITION,  IS_NULLABLE,  SCOPE_CATLOG,  SCOPE_SCHEMA,  SCOPE_TABLE,  SOURCE_DATA_TYPE;
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sql\metadata\constants\GetColumn.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */