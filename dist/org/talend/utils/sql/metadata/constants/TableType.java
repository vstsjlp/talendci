/*    */ package org.talend.utils.sql.metadata.constants;
/*    */ 
/*    */ import java.util.ArrayList;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public enum TableType
/*    */ {
/* 25 */   TABLE, 
/* 26 */   VIEW, 
/* 27 */   SYSTEM_TABLE, 
/* 28 */   GLOBAL_TEMPORARY, 
/* 29 */   LOCAL_TEMPORARY, 
/* 30 */   ALIAS, 
/* 31 */   SYNONYM;
/*    */   
/*    */   public String toString()
/*    */   {
/* 35 */     switch (this) {
/*    */     case LOCAL_TEMPORARY: 
/* 37 */       return "SYSTEM TABLE";
/*    */     case SYNONYM: 
/* 39 */       return "GLOBAL TEMPORARY";
/*    */     case SYSTEM_TABLE: 
/* 41 */       return "LOCAL TEMPORARY";
/*    */     }
/* 43 */     return name();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static String[] getTableTypes(TableType... type)
/*    */   {
/* 54 */     if (type == null) {
/* 55 */       return null;
/*    */     }
/* 57 */     List<String> tablesTypes = new ArrayList();
/* 58 */     TableType[] arrayOfTableType = type;int j = type.length; for (int i = 0; i < j; i++) { TableType tableType = arrayOfTableType[i];
/* 59 */       if (tableType == null) {
/* 60 */         tablesTypes.add(null);
/*    */       } else {
/* 62 */         tablesTypes.add(tableType.toString());
/*    */       }
/*    */     }
/* 65 */     return (String[])tablesTypes.toArray(new String[tablesTypes.size()]);
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sql\metadata\constants\TableType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */