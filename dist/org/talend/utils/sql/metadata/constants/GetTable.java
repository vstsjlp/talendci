package org.talend.utils.sql.metadata.constants;

public enum GetTable
{
  TABLE_CAT,  TABLE_SCHEM,  TABLE_SCHEMA,  TABLE_NAME,  TABLE_TYPE,  REMARKS,  TYPE_CAT,  TYPE_SCHEM,  TYPE_NAME,  SELF_REFERENCING_COL_NAME,  REF_GENERATION;
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sql\metadata\constants\GetTable.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */