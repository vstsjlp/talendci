/*     */ package org.talend.utils.sql;
/*     */ 
/*     */ import java.sql.Connection;
/*     */ import java.sql.DatabaseMetaData;
/*     */ import java.sql.Driver;
/*     */ import java.sql.DriverManager;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.sql.Statement;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import java.util.Properties;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.talend.utils.sugars.ReturnCode;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class ConnectionUtils
/*     */ {
/*  35 */   private static Logger log = Logger.getLogger(ConnectionUtils.class);
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String PASSPHRASE = "99ZwBDt1L9yMX2ApJx fnv94o99OeHbCGuIHTy22 V9O6cZ2i374fVjdV76VX9g49DG1r3n90hT5c1";
/*     */   
/*     */ 
/*     */ 
/*     */   private static List<String> sybaseDBProductsNames;
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String IBM_DB2_ZOS_PRODUCT_NAME = "DB2";
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String POSTGRESQL_PRODUCT_NAME = "POSTGRESQL";
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String SYBASE_PRODUCT_NAME = "SYBASE";
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String SYBASE_LANGUAGE = "Adaptive Server Enterprise | Sybase Adaptive Server IQ";
/*     */   
/*     */ 
/*     */ 
/*     */   private static final String ACCESS_DRIVER = "Microsoft Access Driver";
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String SHUTDOWN_PARAM = ";shutdown=true";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @Deprecated
/*     */   public static Connection createConnection(String url, String driverClassName, Properties props)
/*     */     throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
/*     */   {
/*  76 */     Driver driver = (Driver)Class.forName(driverClassName).newInstance();
/*     */     
/*  78 */     return createConnection(url, driver, props);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @Deprecated
/*     */   public static Connection createConnection(String url, Driver driver, Properties props)
/*     */     throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
/*     */   {
/* 100 */     Connection connection = null;
/* 101 */     if (driver != null) {
/*     */       try {
/* 103 */         DriverManager.registerDriver(driver);
/* 104 */         Class.forName(driver.getClass().getName());
/* 105 */         if ((isMsSqlServer(url)) || (isSybase(url)) || (isHiveServer(url))) {
/* 106 */           connection = driver.connect(url, props);
/*     */         } else {
/* 108 */           connection = DriverManager.getConnection(url, props);
/*     */         }
/*     */       } catch (ClassNotFoundException localClassNotFoundException) {
/*     */         try {
/* 112 */           connection = driver.connect(url, props);
/*     */         } catch (Exception exception) {
/* 114 */           log.info(exception);
/*     */         }
/*     */         
/*     */       }
/*     */       
/* 119 */     } else if (isODBCServer(url)) {
/* 120 */       connection = DriverManager.getConnection(url, props);
/*     */     }
/*     */     
/* 123 */     return connection;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static boolean isODBCServer(String url)
/*     */   {
/* 133 */     return url.startsWith("jdbc:odbc:");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isJDBCURL(String url)
/*     */   {
/* 144 */     return (url != null) && (url.startsWith("jdbc:"));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isSybase(String url)
/*     */   {
/* 154 */     return url.indexOf("sybase") > -1;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isMsSqlServer(String url)
/*     */   {
/* 164 */     return url.indexOf("sqlserver") > -1;
/*     */   }
/*     */   
/*     */   public static boolean isHiveServer(String url) {
/* 168 */     return url.indexOf("hive") > -1;
/*     */   }
/*     */   
/*     */   public static boolean isHiveConnection(String url) {
/* 172 */     return (url != null) && (url.startsWith("jdbc:hive"));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isAccess(String url)
/*     */   {
/* 182 */     if ((url != null) && (url.contains("Microsoft Access Driver"))) {
/* 183 */       return true;
/*     */     }
/* 185 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isHsql(String url)
/*     */   {
/* 195 */     return (url != null) && (url.startsWith("jdbc:hsqldb"));
/*     */   }
/*     */   
/*     */   public static boolean isInProcessModeHsql(String url) {
/* 199 */     return (url != null) && (url.startsWith("jdbc:hsqldb:file"));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String addShutDownForHSQLUrl(String url, String AdditionalParams)
/*     */   {
/* 211 */     String dbUrl = url;
/* 212 */     if (AdditionalParams.indexOf(";shutdown=true") == -1) {
/* 213 */       dbUrl = dbUrl + ";shutdown=true";
/*     */     }
/* 215 */     return dbUrl;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void executeShutDownForHSQL(Connection connection)
/*     */     throws SQLException
/*     */   {
/* 225 */     Statement statement = connection.createStatement();
/* 226 */     statement.executeUpdate("SHUTDOWN;");
/* 227 */     statement.close();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isServerModeHsql(String url)
/*     */   {
/* 237 */     return (url != null) && (url.startsWith("jdbc:hsqldb:hsql"));
/*     */   }
/*     */   
/*     */   public static boolean isTeradata(String url) {
/* 241 */     return (url != null) && (url.startsWith("jdbc:teradata"));
/*     */   }
/*     */   
/*     */   public static boolean isVertica(String url) {
/* 245 */     return (url != null) && (url.startsWith("jdbc:vertica"));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ReturnCode isValid(Connection connection)
/*     */   {
/* 255 */     ReturnCode retCode = new ReturnCode();
/* 256 */     if (connection == null) {
/* 257 */       retCode.setReturnCode("Connection is null!", Boolean.valueOf(false));
/* 258 */       return retCode;
/*     */     }
/*     */     
/* 261 */     ResultSet ping = null;
/*     */     try {
/* 263 */       if (connection.isClosed()) {
/* 264 */         retCode.setReturnCode("Connection is closed", Boolean.valueOf(false));
/* 265 */         return retCode;
/*     */       }
/*     */       
/*     */ 
/* 269 */       connection.getAutoCommit();
/*     */       
/* 271 */       return retCode;
/*     */     } catch (SQLException sqle) { ReturnCode localReturnCode1;
/* 273 */       retCode.setReturnCode("SQLException caught:" + sqle.getMessage() + " SQL error code: " + sqle.getErrorCode(), Boolean.valueOf(false));
/* 274 */       return retCode;
/*     */     } finally {
/* 276 */       if (ping != null) {
/*     */         try {
/* 278 */           ping.close();
/*     */         }
/*     */         catch (Exception localException4) {}
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public static ReturnCode closeConnection(Connection connection)
/*     */   {
/*     */     // Byte code:
/*     */     //   0: getstatic 45	org/talend/utils/sql/ConnectionUtils:$assertionsDisabled	Z
/*     */     //   3: ifne +15 -> 18
/*     */     //   6: aload_0
/*     */     //   7: ifnonnull +11 -> 18
/*     */     //   10: new 273	java/lang/AssertionError
/*     */     //   13: dup
/*     */     //   14: invokespecial 275	java/lang/AssertionError:<init>	()V
/*     */     //   17: athrow
/*     */     //   18: new 225	org/talend/utils/sugars/ReturnCode
/*     */     //   21: dup
/*     */     //   22: iconst_1
/*     */     //   23: invokestatic 230	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
/*     */     //   26: invokespecial 276	org/talend/utils/sugars/ReturnCode:<init>	(Ljava/lang/Boolean;)V
/*     */     //   29: astore_1
/*     */     //   30: aload_0
/*     */     //   31: ifnull +150 -> 181
/*     */     //   34: aload_0
/*     */     //   35: invokeinterface 239 1 0
/*     */     //   40: ifne +141 -> 181
/*     */     //   43: aload_0
/*     */     //   44: invokeinterface 279 1 0
/*     */     //   49: ifnull +132 -> 181
/*     */     //   52: aload_0
/*     */     //   53: invokeinterface 279 1 0
/*     */     //   58: invokeinterface 283 1 0
/*     */     //   63: astore_2
/*     */     //   64: aload_2
/*     */     //   65: invokestatic 288	org/talend/utils/sql/ConnectionUtils:isInProcessModeHsql	(Ljava/lang/String;)Z
/*     */     //   68: ifeq +113 -> 181
/*     */     //   71: aload_0
/*     */     //   72: invokestatic 290	org/talend/utils/sql/ConnectionUtils:executeShutDownForHSQL	(Ljava/sql/Connection;)V
/*     */     //   75: goto +106 -> 181
/*     */     //   78: astore_2
/*     */     //   79: aload_1
/*     */     //   80: new 178	java/lang/StringBuilder
/*     */     //   83: dup
/*     */     //   84: ldc_w 292
/*     */     //   87: invokespecial 184	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   90: aload_2
/*     */     //   91: invokevirtual 252	java/sql/SQLException:getMessage	()Ljava/lang/String;
/*     */     //   94: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   97: invokevirtual 191	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   100: iconst_0
/*     */     //   101: invokestatic 230	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
/*     */     //   104: invokevirtual 235	org/talend/utils/sugars/ReturnCode:setReturnCode	(Ljava/lang/String;Ljava/lang/Boolean;)V
/*     */     //   107: aload_0
/*     */     //   108: ifnull +107 -> 215
/*     */     //   111: aload_0
/*     */     //   112: invokeinterface 239 1 0
/*     */     //   117: ifne +98 -> 215
/*     */     //   120: aload_0
/*     */     //   121: invokeinterface 294 1 0
/*     */     //   126: goto +89 -> 215
/*     */     //   129: astore 4
/*     */     //   131: getstatic 53	org/talend/utils/sql/ConnectionUtils:log	Lorg/apache/log4j/Logger;
/*     */     //   134: aload 4
/*     */     //   136: aload 4
/*     */     //   138: invokevirtual 295	org/apache/log4j/Logger:warn	(Ljava/lang/Object;Ljava/lang/Throwable;)V
/*     */     //   141: goto +74 -> 215
/*     */     //   144: astore_3
/*     */     //   145: aload_0
/*     */     //   146: ifnull +33 -> 179
/*     */     //   149: aload_0
/*     */     //   150: invokeinterface 239 1 0
/*     */     //   155: ifne +24 -> 179
/*     */     //   158: aload_0
/*     */     //   159: invokeinterface 294 1 0
/*     */     //   164: goto +15 -> 179
/*     */     //   167: astore 4
/*     */     //   169: getstatic 53	org/talend/utils/sql/ConnectionUtils:log	Lorg/apache/log4j/Logger;
/*     */     //   172: aload 4
/*     */     //   174: aload 4
/*     */     //   176: invokevirtual 295	org/apache/log4j/Logger:warn	(Ljava/lang/Object;Ljava/lang/Throwable;)V
/*     */     //   179: aload_3
/*     */     //   180: athrow
/*     */     //   181: aload_0
/*     */     //   182: ifnull +33 -> 215
/*     */     //   185: aload_0
/*     */     //   186: invokeinterface 239 1 0
/*     */     //   191: ifne +24 -> 215
/*     */     //   194: aload_0
/*     */     //   195: invokeinterface 294 1 0
/*     */     //   200: goto +15 -> 215
/*     */     //   203: astore 4
/*     */     //   205: getstatic 53	org/talend/utils/sql/ConnectionUtils:log	Lorg/apache/log4j/Logger;
/*     */     //   208: aload 4
/*     */     //   210: aload 4
/*     */     //   212: invokevirtual 295	org/apache/log4j/Logger:warn	(Ljava/lang/Object;Ljava/lang/Throwable;)V
/*     */     //   215: aload_1
/*     */     //   216: areturn
/*     */     // Line number table:
/*     */     //   Java source line #295	-> byte code offset #0
/*     */     //   Java source line #296	-> byte code offset #18
/*     */     //   Java source line #298	-> byte code offset #30
/*     */     //   Java source line #299	-> byte code offset #43
/*     */     //   Java source line #300	-> byte code offset #52
/*     */     //   Java source line #303	-> byte code offset #64
/*     */     //   Java source line #304	-> byte code offset #71
/*     */     //   Java source line #308	-> byte code offset #75
/*     */     //   Java source line #309	-> byte code offset #79
/*     */     //   Java source line #312	-> byte code offset #107
/*     */     //   Java source line #313	-> byte code offset #120
/*     */     //   Java source line #315	-> byte code offset #126
/*     */     //   Java source line #316	-> byte code offset #131
/*     */     //   Java source line #310	-> byte code offset #144
/*     */     //   Java source line #312	-> byte code offset #145
/*     */     //   Java source line #313	-> byte code offset #158
/*     */     //   Java source line #315	-> byte code offset #164
/*     */     //   Java source line #316	-> byte code offset #169
/*     */     //   Java source line #318	-> byte code offset #179
/*     */     //   Java source line #312	-> byte code offset #181
/*     */     //   Java source line #313	-> byte code offset #194
/*     */     //   Java source line #315	-> byte code offset #200
/*     */     //   Java source line #316	-> byte code offset #205
/*     */     //   Java source line #319	-> byte code offset #215
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	217	0	connection	Connection
/*     */     //   29	187	1	rc	ReturnCode
/*     */     //   63	2	2	url	String
/*     */     //   78	13	2	e	SQLException
/*     */     //   144	36	3	localObject	Object
/*     */     //   129	8	4	e	SQLException
/*     */     //   167	8	4	e	SQLException
/*     */     //   203	8	4	e	SQLException
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   30	75	78	java/sql/SQLException
/*     */     //   107	126	129	java/sql/SQLException
/*     */     //   30	107	144	finally
/*     */     //   145	164	167	java/sql/SQLException
/*     */     //   181	200	203	java/sql/SQLException
/*     */   }
/*     */   
/*     */   public static boolean isSybase(Connection connection)
/*     */     throws SQLException
/*     */   {
/* 331 */     DatabaseMetaData connectionMetadata = connection.getMetaData();
/* 332 */     if ((connectionMetadata.getDriverName() != null) && (connectionMetadata.getDatabaseProductName() != null)) { String[] arrayOfString;
/* 333 */       int j = (arrayOfString = getSybaseDBProductsName()).length; for (int i = 0; i < j; i++) { String keyString = arrayOfString[i];
/* 334 */         if (keyString.equals(connectionMetadata.getDatabaseProductName().trim())) {
/* 335 */           return true;
/*     */         }
/*     */       }
/*     */     }
/* 339 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @Deprecated
/*     */   public static DatabaseMetaData getConnectionMetadata(Connection conn)
/*     */     throws SQLException
/*     */   {
/* 353 */     DatabaseMetaData dbMetaData = conn.getMetaData();
/*     */     
/* 355 */     if ((dbMetaData != null) && (dbMetaData.getDatabaseProductName() != null) && 
/* 356 */       (dbMetaData.getDatabaseProductName().equals("DB2"))) {
/* 357 */       dbMetaData = conn.getMetaData();
/* 358 */       log.info("IBM DB2 for z/OS");
/*     */     }
/*     */     
/* 361 */     return dbMetaData;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isDB2(DatabaseMetaData metadata)
/*     */     throws SQLException
/*     */   {
/* 372 */     if ((metadata != null) && (metadata.getDatabaseProductName() != null) && 
/* 373 */       (metadata.getDatabaseProductName().indexOf("DB2") > -1)) {
/* 374 */       return true;
/*     */     }
/* 376 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isOracleForSid(DatabaseMetaData metadata, String oracleProduct) throws SQLException {
/* 380 */     if ((metadata != null) && (metadata.getDatabaseProductName() != null) && 
/* 381 */       (metadata.getDatabaseProductName().indexOf(oracleProduct) > -1)) {
/* 382 */       return true;
/*     */     }
/* 384 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isSqlite(DatabaseMetaData metadata, String sqliteProduct) throws SQLException {
/* 388 */     if ((metadata != null) && (metadata.getDatabaseProductName() != null) && 
/* 389 */       (metadata.getDatabaseProductName().toUpperCase().indexOf(sqliteProduct) > -1)) {
/* 390 */       return true;
/*     */     }
/* 392 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isPostgresql(DatabaseMetaData metadata) {
/* 396 */     boolean result = false;
/*     */     try {
/* 398 */       if ((metadata != null) && (metadata.getDatabaseProductName() != null) && 
/* 399 */         (metadata.getDatabaseProductName().toUpperCase().indexOf("POSTGRESQL") > -1)) {
/* 400 */         result = true;
/*     */       }
/*     */     } catch (SQLException localSQLException) {
/* 403 */       result = false;
/*     */     }
/* 405 */     return result;
/*     */   }
/*     */   
/*     */   public static boolean isSybase(DatabaseMetaData metadata) {
/* 409 */     boolean result = false;
/*     */     
/* 411 */     if ((metadata != null) && ("org.talend.commons.utils.database.SybaseDatabaseMetaData".equals(metadata.getClass().getName()))) {
/* 412 */       result = true;
/*     */     }
/* 414 */     return result;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isExasol(DatabaseMetaData metadata)
/*     */     throws SQLException
/*     */   {
/* 426 */     if ((metadata != null) && (metadata.getDriverName() != null) && 
/* 427 */       (metadata.getDriverName().toLowerCase().startsWith("exasol")) && 
/* 428 */       (metadata.getDatabaseProductName() != null) && 
/* 429 */       (metadata.getDatabaseProductName().toLowerCase().startsWith("exasol"))) {
/* 430 */       return true;
/*     */     }
/* 432 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String[] getSybaseDBProductsName()
/*     */   {
/* 444 */     if (sybaseDBProductsNames == null) {
/* 445 */       sybaseDBProductsNames = new ArrayList();
/* 446 */       String[] arrayOfString; int j = (arrayOfString = "Adaptive Server Enterprise | Sybase Adaptive Server IQ".split("\\|")).length; for (int i = 0; i < j; i++) { String name = arrayOfString[i];
/* 447 */         sybaseDBProductsNames.add(name.trim());
/*     */       }
/* 449 */       sybaseDBProductsNames.add("Sybase");
/* 450 */       sybaseDBProductsNames.add("Sybase IQ");
/* 451 */       sybaseDBProductsNames.add("Adaptive Server Enterprise | Sybase Adaptive Server IQ");
/*     */     }
/* 453 */     return (String[])sybaseDBProductsNames.toArray(new String[sybaseDBProductsNames.size()]);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isOdbcTeradata(DatabaseMetaData metadata)
/*     */     throws SQLException
/*     */   {
/* 464 */     if ((metadata.getDriverName() != null) && 
/* 465 */       (metadata.getDriverName().toLowerCase().startsWith("jdbc-odbc bridge (tdata32.dll)")) && 
/* 466 */       (metadata.getDatabaseProductName() != null) && 
/* 467 */       (metadata.getDatabaseProductName().toLowerCase().indexOf("teradata") > -1)) {
/* 468 */       return true;
/*     */     }
/* 470 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isOdbcHyperFileSQL(DatabaseMetaData metadata) {
/*     */     try {
/* 475 */       if ((metadata.getDriverName() != null) && 
/* 476 */         (metadata.getDriverName().toLowerCase().startsWith("jdbc-odbc bridge")) && 
/* 477 */         (metadata.getDatabaseProductName() != null) && 
/* 478 */         (metadata.getDatabaseProductName().toLowerCase().indexOf("hyperfilesql") > -1)) {
/* 479 */         return true;
/*     */       }
/*     */     } catch (SQLException localSQLException) {
/* 482 */       return false;
/*     */     }
/* 484 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isNetezza(DatabaseMetaData databaseMetaData)
/*     */   {
/*     */     try
/*     */     {
/* 497 */       if ((databaseMetaData != null) && 
/* 498 */         (databaseMetaData.getDriverName() != null) && 
/* 499 */         (databaseMetaData.getDriverName().toLowerCase().startsWith("netezza")) && 
/* 500 */         (databaseMetaData.getDatabaseProductName() != null) && 
/* 501 */         (databaseMetaData.getDatabaseProductName().toLowerCase().startsWith("netezza"))) {
/* 502 */         return true;
/*     */       }
/*     */     } catch (SQLException e) {
/* 505 */       log.warn(e);
/*     */     }
/* 507 */     return false;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sql\ConnectionUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */