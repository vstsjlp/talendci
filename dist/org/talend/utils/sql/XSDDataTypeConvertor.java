/*    */ package org.talend.utils.sql;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class XSDDataTypeConvertor
/*    */ {
/*    */   private static Map<String, Integer> map;
/*    */   
/*    */   public static Map<String, Integer> getMap()
/*    */   {
/* 43 */     if (map == null) {
/* 44 */       map = new HashMap();
/* 45 */       map.put("anySimpleType", Integer.valueOf(2009));
/* 46 */       map.put("anyURI", Integer.valueOf(70));
/* 47 */       map.put("duration", Integer.valueOf(12));
/* 48 */       map.put("base64Binary", Integer.valueOf(-2));
/* 49 */       map.put("boolean", Integer.valueOf(16));
/* 50 */       map.put("date", Integer.valueOf(91));
/* 51 */       map.put("dateTime", Integer.valueOf(93));
/* 52 */       map.put("decimal", Integer.valueOf(3));
/* 53 */       map.put("integer", Integer.valueOf(3));
/* 54 */       map.put("nonPositiveInteger", Integer.valueOf(3));
/* 55 */       map.put("long", Integer.valueOf(-5));
/* 56 */       map.put("nonNegativeInteger", Integer.valueOf(3));
/* 57 */       map.put("negativeInteger", Integer.valueOf(3));
/* 58 */       map.put("int", Integer.valueOf(4));
/* 59 */       map.put("unsignedLong", Integer.valueOf(3));
/* 60 */       map.put("positiveInteger", Integer.valueOf(3));
/* 61 */       map.put("short", Integer.valueOf(5));
/* 62 */       map.put("unsignedInt", Integer.valueOf(3));
/* 63 */       map.put("byte", Integer.valueOf(3));
/* 64 */       map.put("unsignedShort", Integer.valueOf(3));
/* 65 */       map.put("unsignedByte", Integer.valueOf(3));
/* 66 */       map.put("double", Integer.valueOf(8));
/* 67 */       map.put("float", Integer.valueOf(6));
/* 68 */       map.put("gMonth", Integer.valueOf(91));
/* 69 */       map.put("gMonthDay", Integer.valueOf(91));
/* 70 */       map.put("gDay", Integer.valueOf(91));
/* 71 */       map.put("gYearMonth", Integer.valueOf(91));
/* 72 */       map.put("gYear", Integer.valueOf(91));
/* 73 */       map.put("NOTATION", Integer.valueOf(2005));
/* 74 */       map.put("hexBinary", Integer.valueOf(-2));
/* 75 */       map.put("QName", Integer.valueOf(2005));
/* 76 */       map.put("time", Integer.valueOf(93));
/* 77 */       map.put("string", Integer.valueOf(2005));
/*    */     }
/* 79 */     return map;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static int convertToJDBCType(String xsdDataType)
/*    */   {
/* 89 */     Integer type = (Integer)getMap().get(xsdDataType);
/* 90 */     return type == null ? 0 : type.intValue();
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sql\XSDDataTypeConvertor.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */