/*     */ package org.talend.utils.sql;
/*     */ 
/*     */ import java.io.BufferedReader;
/*     */ import java.io.IOException;
/*     */ import java.io.PrintStream;
/*     */ import java.io.Reader;
/*     */ import java.sql.Clob;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.ResultSetMetaData;
/*     */ import java.sql.SQLException;
/*     */ import org.talend.utils.format.StringFormatUtil;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class ResultSetUtils
/*     */ {
/*     */   private static final String NULLDATE = "0000-00-00 00:00:00";
/*     */   
/*     */   public static void printResultSet(ResultSet set, int width)
/*     */     throws SQLException
/*     */   {
/*  50 */     ResultSetMetaData metaData = set.getMetaData();
/*  51 */     int columnCount = metaData.getColumnCount();
/*     */     
/*  53 */     int minWidth = width;
/*  54 */     for (int i = 1; i <= columnCount; i++) {
/*  55 */       minWidth = Math.max(minWidth, metaData.getColumnName(i).length());
/*     */     }
/*     */     
/*  58 */     String header = new String();
/*  59 */     for (int i = 1; i <= columnCount; i++) {
/*  60 */       String columnName = StringFormatUtil.padString(metaData.getColumnName(i), minWidth);
/*  61 */       header = header + columnName;
/*     */     }
/*  63 */     System.out.println(header);
/*     */     
/*     */ 
/*  66 */     String types = new String();
/*  67 */     for (int i = 1; i <= columnCount; i++) {
/*  68 */       String columnTypeName = StringFormatUtil.padString(metaData.getColumnTypeName(i), minWidth);
/*  69 */       types = types + columnTypeName;
/*     */     }
/*  71 */     System.out.println(types);
/*     */     
/*     */ 
/*  74 */     while (set.next()) {
/*  75 */       System.out.println(formatRow(set, columnCount, minWidth));
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String formatRow(ResultSet set, int nbColumns, int width)
/*     */     throws SQLException
/*     */   {
/*  89 */     String row = new String();
/*  90 */     for (int i = 1; i <= nbColumns; i++) {
/*  91 */       Object col = set.getObject(i);
/*  92 */       row = row + StringFormatUtil.padString(col != null ? col.toString() : "", width);
/*     */     }
/*  94 */     return row;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Object getBigObject(ResultSet set, int columnIndex)
/*     */     throws SQLException
/*     */   {
/* 108 */     Object object = null;
/*     */     try {
/* 110 */       object = set.getObject(columnIndex);
/* 111 */       if ((object != null) && ((object instanceof Clob))) {
/* 112 */         Reader is = ((Clob)object).getCharacterStream();
/* 113 */         BufferedReader br = new BufferedReader(is);
/* 114 */         String str = br.readLine();
/* 115 */         StringBuffer sb = new StringBuffer();
/* 116 */         while (str != null) {
/* 117 */           sb.append(str);
/* 118 */           str = br.readLine();
/*     */         }
/* 120 */         return sb.toString();
/*     */       }
/*     */     } catch (SQLException e) {
/* 123 */       if ("0000-00-00 00:00:00".equals(set.getString(columnIndex))) {
/* 124 */         object = null;
/*     */       } else {
/* 126 */         throw e;
/*     */       }
/*     */     }
/*     */     catch (IOException localIOException) {
/* 130 */       object = null;
/*     */     }
/* 132 */     return object;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Object getBigObject(ResultSet set, String columnName)
/*     */     throws SQLException
/*     */   {
/* 146 */     Object object = null;
/*     */     try {
/* 148 */       object = set.getObject(columnName);
/* 149 */       if ((object != null) && ((object instanceof Clob))) {
/* 150 */         Reader is = ((Clob)object).getCharacterStream();
/* 151 */         BufferedReader br = new BufferedReader(is);
/* 152 */         String str = br.readLine();
/* 153 */         StringBuffer sb = new StringBuffer();
/* 154 */         while (str != null) {
/* 155 */           sb.append(str);
/* 156 */           str = br.readLine();
/*     */         }
/* 158 */         return sb.toString();
/*     */       }
/*     */     } catch (SQLException e) {
/* 161 */       if ("0000-00-00 00:00:00".equals(set.getString(columnName))) {
/* 162 */         object = null;
/*     */       } else {
/* 164 */         throw e;
/*     */       }
/*     */     }
/*     */     catch (IOException localIOException) {
/* 168 */       object = null;
/*     */     }
/* 170 */     return object;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Object getObject(ResultSet set, int columnIndex)
/*     */     throws SQLException
/*     */   {
/* 183 */     Object object = null;
/*     */     try {
/* 185 */       object = set.getObject(columnIndex);
/*     */     } catch (SQLException e) {
/* 187 */       if ("0000-00-00 00:00:00".equals(set.getString(columnIndex))) {
/* 188 */         object = null;
/*     */       } else {
/* 190 */         throw e;
/*     */       }
/*     */     }
/*     */     
/* 194 */     return object;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Object getObject(ResultSet set, String columnName)
/*     */     throws SQLException
/*     */   {
/* 207 */     Object object = null;
/*     */     try {
/* 209 */       object = set.getObject(columnName);
/*     */     } catch (SQLException e) {
/* 211 */       if ("0000-00-00 00:00:00".equals(set.getString(columnName))) {
/* 212 */         object = null;
/*     */       } else {
/* 214 */         throw e;
/*     */       }
/*     */     }
/*     */     
/* 218 */     return object;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sql\ResultSetUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */