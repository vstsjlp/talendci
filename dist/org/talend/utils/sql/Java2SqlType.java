/*     */ package org.talend.utils.sql;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class Java2SqlType
/*     */ {
/*     */   public static final int NCHAR = -15;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final int NTEXT = -16;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final int NVARCHAR = -9;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final int NVARCHAR2 = 1111;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final int BIT = -7;
/*     */   
/*     */ 
/*     */ 
/*     */   public static final int VARBINARY = 2004;
/*     */   
/*     */ 
/*     */ 
/*     */   public static final int TERADATA_INTERVAL = 1000;
/*     */   
/*     */ 
/*     */ 
/*     */   public static final int TERADATA_INTERVAL_TO = 1001;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isBinaryInSQL(int type)
/*     */   {
/*  48 */     switch (type) {
/*     */     case -7: 
/*     */     case 2004: 
/*  51 */       return true;
/*     */     }
/*  53 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isTextInSQL(int type)
/*     */   {
/*  58 */     switch (type)
/*     */     {
/*     */ 
/*     */     case -16: 
/*     */     case -15: 
/*     */     case -9: 
/*     */     case -1: 
/*     */     case 1: 
/*     */     case 12: 
/*     */     case 1111: 
/*     */     case 2005: 
/*  69 */       return true; }
/*     */     
/*  71 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isNumbericInSQL(int type)
/*     */   {
/*  76 */     switch (type) {
/*     */     case -6: 
/*     */     case -5: 
/*     */     case 2: 
/*     */     case 3: 
/*     */     case 4: 
/*     */     case 5: 
/*     */     case 6: 
/*     */     case 7: 
/*     */     case 8: 
/*  86 */       return true;
/*     */     }
/*  88 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isDateInSQL(int type)
/*     */   {
/*  93 */     switch (type)
/*     */     {
/*     */     case 91: 
/*     */     case 92: 
/*     */     case 93: 
/*  98 */       return true;
/*     */     }
/* 100 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isDateTimeSQL(int type)
/*     */   {
/* 105 */     return type == 93;
/*     */   }
/*     */   
/*     */   public static boolean isTimeSQL(int type)
/*     */   {
/* 110 */     return type == 92;
/*     */   }
/*     */   
/*     */   public static boolean isOtherTypeInSQL(int type) {
/* 114 */     if ((isTextInSQL(type)) || (isNumbericInSQL(type)) || (isDateInSQL(type))) {
/* 115 */       return false;
/*     */     }
/*     */     
/* 118 */     return true;
/*     */   }
/*     */   
/*     */   public static boolean isGenericSameType(int type1, int type2) {
/* 122 */     if ((type1 == type2) || ((isTextInSQL(type1)) && (isTextInSQL(type2))) || ((isNumbericInSQL(type1)) && (isNumbericInSQL(type2))) || (
/* 123 */       (isDateInSQL(type1)) && (isDateInSQL(type2)))) {
/* 124 */       return true;
/*     */     }
/*     */     
/* 127 */     return false;
/*     */   }
/*     */   
/*     */   public static int getJavaTypeBySqlType(String sqlType)
/*     */   {
/* 132 */     if ("DATE".equalsIgnoreCase(sqlType.trim()))
/* 133 */       return 91;
/* 134 */     if (("BIGINT".equalsIgnoreCase(sqlType.trim())) || ("LONG".equalsIgnoreCase(sqlType.trim())))
/* 135 */       return -5;
/* 136 */     if ("INTEGER".equalsIgnoreCase(sqlType.trim()))
/* 137 */       return 4;
/* 138 */     if ("SMALLINT".equalsIgnoreCase(sqlType.trim()))
/* 139 */       return 5;
/* 140 */     if ("FLOAT".equalsIgnoreCase(sqlType.trim()))
/* 141 */       return 6;
/* 142 */     if ("CHAR".equalsIgnoreCase(sqlType.trim()))
/* 143 */       return 1;
/* 144 */     if ("VARCHAR".equalsIgnoreCase(sqlType.trim()))
/* 145 */       return 12;
/* 146 */     if ("DECIMAL".equalsIgnoreCase(sqlType.trim()))
/* 147 */       return 3;
/* 148 */     if ("TIME".equalsIgnoreCase(sqlType.trim()))
/* 149 */       return 92;
/* 150 */     if ("TIMESTMP".equalsIgnoreCase(sqlType.trim()))
/* 151 */       return 93;
/* 152 */     if ("TIMESTAMP".equalsIgnoreCase(sqlType.trim()))
/* 153 */       return 93;
/* 154 */     if ("BLOB".equalsIgnoreCase(sqlType.trim()))
/* 155 */       return 2004;
/* 156 */     if ("CLOB".equalsIgnoreCase(sqlType.trim()))
/* 157 */       return 2005;
/* 158 */     if ("DISTINCT".equalsIgnoreCase(sqlType.trim()))
/* 159 */       return 2001;
/* 160 */     if ("DOUBLE".equalsIgnoreCase(sqlType.trim()))
/* 161 */       return 8;
/* 162 */     if ("LONGVAR".equalsIgnoreCase(sqlType.trim()))
/* 163 */       return -1;
/* 164 */     if ("LONGVARCHAR".equalsIgnoreCase(sqlType.trim()))
/* 165 */       return -1;
/* 166 */     if ("REAL".equalsIgnoreCase(sqlType.trim())) {
/* 167 */       return 7;
/*     */     }
/* 169 */     return 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int getTeradataJavaTypeBySqlTypeAsInt(String sqlType)
/*     */   {
/* 183 */     if (sqlType.trim().equals("DA"))
/* 184 */       return 91;
/* 185 */     if (sqlType.trim().equals("I"))
/* 186 */       return 4;
/* 187 */     if ((sqlType.trim().equals("I2")) || (sqlType.trim().equals("I1")))
/* 188 */       return 5;
/* 189 */     if (sqlType.trim().equals("F"))
/* 190 */       return 6;
/* 191 */     if ((sqlType.trim().equals("CF")) || (sqlType.trim().equals("BF")))
/* 192 */       return 1;
/* 193 */     if ((sqlType.trim().equals("CV")) || (sqlType.trim().equals("BV")))
/* 194 */       return 12;
/* 195 */     if (sqlType.trim().equals("D"))
/* 196 */       return 3;
/* 197 */     if ((sqlType.trim().equals("TS")) || (sqlType.trim().equals("SZ")))
/* 198 */       return 93;
/* 199 */     if (sqlType.trim().equals("BO"))
/* 200 */       return 2004;
/* 201 */     if (sqlType.trim().equals("CO"))
/* 202 */       return 2005;
/* 203 */     if ((sqlType.trim().equals("YR")) || (sqlType.trim().equals("MO")) || (sqlType.trim().equals("DY")) || 
/* 204 */       (sqlType.trim().equals("HR")) || (sqlType.trim().equals("SC")) || (sqlType.trim().equals("MI")))
/*     */     {
/* 206 */       return 7; }
/* 207 */     if ((sqlType.trim().equals("YM")) || (sqlType.trim().equals("DM")) || (sqlType.trim().equals("DH")) || 
/* 208 */       (sqlType.trim().equals("DS")) || (sqlType.trim().equals("HM")) || (sqlType.trim().equals("HS")) || (sqlType.trim().equals("MS")))
/*     */     {
/* 210 */       return -9;
/*     */     }
/* 212 */     return 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String getTeradataJavaTypeBySqlTypeAsString(String sqlType)
/*     */   {
/* 222 */     if (sqlType.trim().equals("DA"))
/* 223 */       return "DATE";
/* 224 */     if (sqlType.trim().equals("I"))
/* 225 */       return "INTEGER";
/* 226 */     if ((sqlType.trim().equals("I2")) || (sqlType.trim().equals("I1")))
/* 227 */       return "SMALLINT";
/* 228 */     if (sqlType.trim().equals("F"))
/* 229 */       return "FLOAT";
/* 230 */     if ((sqlType.trim().equals("CF")) || (sqlType.trim().equals("BF")))
/* 231 */       return "CHAR";
/* 232 */     if ((sqlType.trim().equals("CV")) || (sqlType.trim().equals("BV")))
/* 233 */       return "VARCHAR";
/* 234 */     if (sqlType.trim().equals("D"))
/* 235 */       return "DECIMAL";
/* 236 */     if ((sqlType.trim().equals("TS")) || (sqlType.trim().equals("SZ")))
/* 237 */       return "TIMESTAMP";
/* 238 */     if (sqlType.trim().equals("BO"))
/* 239 */       return "BLOB";
/* 240 */     if (sqlType.trim().equals("CO"))
/* 241 */       return "CLOB";
/* 242 */     if (sqlType.trim().equals("YR"))
/*     */     {
/* 244 */       return "INTERVAL YEAR"; }
/* 245 */     if (sqlType.trim().equals("DH"))
/* 246 */       return "INTERVAL DAY TO HOUR";
/* 247 */     if (sqlType.trim().equals("DM"))
/* 248 */       return "INTERVAL DAY TO MINUTE";
/* 249 */     if (sqlType.trim().equals("DS"))
/* 250 */       return "INTERVAL DAY TO SECOND";
/* 251 */     if (sqlType.trim().equals("DY"))
/* 252 */       return "INTERVAL DAY";
/* 253 */     if (sqlType.trim().equals("HM"))
/* 254 */       return "INTERVAL HOUR TO MINUTE";
/* 255 */     if (sqlType.trim().equals("HR"))
/* 256 */       return "INTERVAL HOUR";
/* 257 */     if (sqlType.trim().equals("HS"))
/* 258 */       return "INTERVAL HOUR TO SECOND";
/* 259 */     if (sqlType.trim().equals("MI"))
/* 260 */       return "INTERVAL MINUTE";
/* 261 */     if (sqlType.trim().equals("MO"))
/* 262 */       return "INTERVAL MONTH";
/* 263 */     if (sqlType.trim().equals("MS"))
/* 264 */       return "INTERVAL MINUTE TO SECOND";
/* 265 */     if (sqlType.trim().equals("SC"))
/* 266 */       return "INTERVAL SECOND";
/* 267 */     if (sqlType.trim().equals("YM")) {
/* 268 */       return "INTERVAL YEAR TO MONTH";
/*     */     }
/* 270 */     return "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int isTeradataIntervalType(String typeName)
/*     */   {
/* 280 */     if ((typeName != null) && (typeName.startsWith("INTERVAL"))) {
/* 281 */       if (typeName.contains("TO")) {
/* 282 */         return 1001;
/*     */       }
/* 284 */       return 1000;
/*     */     }
/* 286 */     return 0;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sql\Java2SqlType.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */