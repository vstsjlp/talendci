/*     */ package org.talend.utils.sql;
/*     */ 
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Date;
/*     */ import java.util.HashMap;
/*     */ import java.util.Locale;
/*     */ import java.util.Map;
/*     */ import org.apache.commons.lang.StringUtils;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class TalendTypeConvert
/*     */ {
/*     */   private static Map<String, Integer> map;
/*  31 */   private static String idStr = "id_";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private static Map<String, Integer> getMap()
/*     */   {
/*  38 */     if (map == null) {
/*  39 */       map = new HashMap();
/*  40 */       map.put(talendTypeName(Boolean.class), Integer.valueOf(16));
/*  41 */       map.put(talendTypeName(Byte.class), Integer.valueOf(3));
/*  42 */       map.put(talendTypeName(Character.class), Integer.valueOf(1));
/*  43 */       map.put(talendTypeName(Date.class), Integer.valueOf(91));
/*  44 */       map.put(talendTypeName(String.class), Integer.valueOf(2005));
/*  45 */       map.put(talendTypeName(Double.class), Integer.valueOf(8));
/*  46 */       map.put(talendTypeName(Float.class), Integer.valueOf(6));
/*  47 */       map.put(talendTypeName(Integer.class), Integer.valueOf(4));
/*  48 */       map.put(talendTypeName(Long.class), Integer.valueOf(4));
/*  49 */       map.put(talendTypeName(Short.class), Integer.valueOf(5));
/*     */     }
/*  51 */     return map;
/*     */   }
/*     */   
/*     */   private static String talendTypeName(Class<?> nullableClass) {
/*  55 */     return idStr + nullableClass.getSimpleName();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int convertToJDBCType(String talendType)
/*     */   {
/*  66 */     Integer type = (Integer)getMap().get(talendType);
/*  67 */     return type == null ? 0 : type.intValue();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Object convertToObject(String talendType, String value, String datePattern)
/*     */   {
/*  80 */     Object object = null;
/*     */     
/*  82 */     value = value.trim();
/*  83 */     value = StringUtils.remove(value, "\r");
/*  84 */     value = StringUtils.remove(value, "\n");
/*     */     try
/*     */     {
/*  87 */       if (talendType.equals(talendTypeName(Boolean.class))) {
/*  88 */         object = Boolean.valueOf(Boolean.valueOf(value).booleanValue());
/*  89 */       } else if (talendType.equals(talendTypeName(Byte.class))) {
/*  90 */         object = Byte.valueOf(Byte.valueOf(value).byteValue());
/*  91 */       } else if (talendType.equals(talendTypeName(Date.class)))
/*     */       {
/*  93 */         if ((datePattern == null) || ("".equals(datePattern.trim()))) {
/*  94 */           datePattern = "yyyy-MM-dd";
/*     */         } else {
/*  96 */           datePattern = StringUtils.replace(datePattern, "\"", "");
/*     */         }
/*     */         
/*     */ 
/* 100 */         SimpleDateFormat sdf = new SimpleDateFormat(datePattern, Locale.US);
/* 101 */         object = sdf.parse(value);
/* 102 */       } else if (talendType.equals(talendTypeName(Double.class))) {
/* 103 */         object = Double.valueOf(Double.parseDouble(value));
/* 104 */       } else if (talendType.equals(talendTypeName(Float.class))) {
/* 105 */         object = Float.valueOf(Float.parseFloat(value));
/* 106 */       } else if (talendType.equals(talendTypeName(Integer.class))) {
/* 107 */         object = Integer.valueOf(Integer.parseInt(value));
/* 108 */       } else if (talendType.equals(talendTypeName(Long.class))) {
/* 109 */         object = Long.valueOf(Long.parseLong(value));
/* 110 */       } else if (talendType.equals(talendTypeName(Short.class))) {
/* 111 */         object = Short.valueOf(Short.parseShort(value));
/* 112 */       } else if ((talendType.equals(talendTypeName(String.class))) || (talendType.equals(talendTypeName(Character.class)))) {
/* 113 */         object = value;
/*     */       }
/*     */     } catch (ClassCastException localClassCastException) {
/* 116 */       return null;
/*     */     } catch (Exception localException) {
/* 118 */       return null;
/*     */     }
/* 120 */     return object;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String convertToJavaType(String talendType)
/*     */   {
/* 131 */     if (talendType == null) {
/* 132 */       return "";
/*     */     }
/* 134 */     talendType = StringUtils.remove(talendType, idStr);
/* 135 */     return talendType;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sql\TalendTypeConvert.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */