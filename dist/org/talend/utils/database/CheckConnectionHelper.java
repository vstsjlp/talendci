/*    */ package org.talend.utils.database;
/*    */ 
/*    */ import java.sql.Connection;
/*    */ import java.sql.DriverManager;
/*    */ import java.sql.SQLException;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CheckConnectionHelper
/*    */ {
/*    */   public static void connect(String login, String passwd, String url)
/*    */     throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
/*    */   {
/* 26 */     Class.forName(Constants.getDriverFromUrl(url)).newInstance();
/* 27 */     Connection connection = DriverManager.getConnection(url, login, passwd);
/* 28 */     connection.close();
/*    */   }
/*    */   
/*    */   public static Connection getConnection(String login, String passwd, String url) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
/*    */   {
/* 33 */     Class.forName(Constants.getDriverFromUrl(url)).newInstance();
/* 34 */     return DriverManager.getConnection(url, login, passwd);
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\database\CheckConnectionHelper.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */