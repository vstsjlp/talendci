/*    */ package org.talend.utils.database;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Constants
/*    */ {
/*    */   public static final String JDBC_MYSQL_URL = "jdbc:mysql://";
/*    */   
/*    */ 
/*    */   public static final String JDBC_MSSQL_URL = "jdbc:jtds:sqlserver://";
/*    */   
/*    */ 
/*    */   public static final String JDBC_ORACLE_URL = "jdbc:oracle:thin:";
/*    */   
/*    */ 
/*    */   public static final String JDBC_H2_URL = "jdbc:h2";
/*    */   
/*    */ 
/*    */   public static final String JDBC_POSTGRESQL_URL = "jdbc:postgresql";
/*    */   
/*    */ 
/*    */   public static final String JDBC_MYSQL_DRIVER = "org.gjt.mm.mysql.Driver";
/*    */   
/*    */ 
/*    */   public static final String JDBC_MSSQL_DRIVER = "net.sourceforge.jtds.jdbc.Driver";
/*    */   
/*    */ 
/*    */   public static final String JDBC_ORACLE_DRIVER = "oracle.jdbc.OracleDriver";
/*    */   
/*    */ 
/*    */   public static final String JDBC_H2_DRIVER = "org.h2.Driver";
/*    */   
/*    */ 
/*    */   public static final String JDBC_POSTGRESQL_DRIVER = "org.postgresql.Driver";
/*    */   
/*    */ 
/*    */   public static final String JDBC_MARIADB_URL = "jdbc:mariadb://";
/*    */   
/*    */ 
/*    */   public static final String JDBC_MARIADB_DRIVER = "org.mariadb.jdbc.Driver";
/*    */   
/*    */ 
/*    */   public static String getDriverFromUrl(String url)
/*    */   {
/* 45 */     if (url.startsWith("jdbc:mysql://")) {
/* 46 */       return "org.gjt.mm.mysql.Driver";
/*    */     }
/* 48 */     if (url.startsWith("jdbc:jtds:sqlserver://")) {
/* 49 */       return "net.sourceforge.jtds.jdbc.Driver";
/*    */     }
/* 51 */     if (url.startsWith("jdbc:oracle:thin:")) {
/* 52 */       return "oracle.jdbc.OracleDriver";
/*    */     }
/* 54 */     if (url.startsWith("jdbc:h2")) {
/* 55 */       return "org.h2.Driver";
/*    */     }
/* 57 */     if (url.startsWith("jdbc:postgresql")) {
/* 58 */       return "org.postgresql.Driver";
/*    */     }
/* 60 */     if (url.startsWith("jdbc:mariadb://")) {
/* 61 */       return "org.mariadb.jdbc.Driver";
/*    */     }
/* 63 */     throw new IllegalStateException("unknown url type");
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\database\Constants.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */