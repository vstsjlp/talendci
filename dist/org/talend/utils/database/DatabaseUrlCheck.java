/*    */ package org.talend.utils.database;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DatabaseUrlCheck
/*    */ {
/*    */   public static boolean isH2DB(String url)
/*    */   {
/* 21 */     if (url.startsWith("jdbc:h2")) {
/* 22 */       return true;
/*    */     }
/* 24 */     return false;
/*    */   }
/*    */   
/*    */   public static boolean isOracleDB(String url) {
/* 28 */     if (url.startsWith("jdbc:oracle:thin:")) {
/* 29 */       return true;
/*    */     }
/* 31 */     return false;
/*    */   }
/*    */   
/*    */   public static boolean isMsDB(String url) {
/* 35 */     if (url.startsWith("jdbc:jtds:sqlserver://")) {
/* 36 */       return true;
/*    */     }
/* 38 */     return false;
/*    */   }
/*    */   
/*    */   public static boolean isMySQLDB(String url) {
/* 42 */     if (url.startsWith("jdbc:mysql://")) {
/* 43 */       return true;
/*    */     }
/* 45 */     return false;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\database\DatabaseUrlCheck.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */