/*     */ package org.talend.utils.string;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import java.util.Random;
/*     */ import java.util.StringTokenizer;
/*     */ import org.talend.utils.sugars.ReturnCode;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class StringUtilities
/*     */ {
/*     */   public static List<String> tokenize(String input, String delimiters)
/*     */   {
/*  39 */     List<String> stringArray = new ArrayList();
/*     */     
/*  41 */     if (input == null) {
/*  42 */       return stringArray;
/*     */     }
/*  44 */     if (delimiters == null) {
/*  45 */       return stringArray;
/*     */     }
/*     */     
/*  48 */     StringTokenizer t = new StringTokenizer(input, delimiters);
/*     */     
/*  50 */     while (t.hasMoreTokens()) {
/*  51 */       stringArray.add(t.nextToken());
/*     */     }
/*  53 */     return stringArray;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ReturnCode checkBalancedParenthesis(String input, char openingBlock, char closingBlock)
/*     */   {
/*  65 */     int level = 0;
/*     */     
/*  67 */     for (int i = 0; i < input.length(); i++) {
/*  68 */       char currentChar = input.charAt(i);
/*  69 */       if (currentChar == openingBlock) {
/*  70 */         level++;
/*  71 */       } else if (currentChar == closingBlock) {
/*  72 */         level--;
/*     */       }
/*  74 */       if (level < 0) {
/*  75 */         return new ReturnCode("too many " + closingBlock + " at position " + i, Boolean.valueOf(false));
/*     */       }
/*     */     }
/*  78 */     if (level < 0) {
/*  79 */       return new ReturnCode("too many " + closingBlock + " at position " + i, Boolean.valueOf(false));
/*     */     }
/*  81 */     if (level > 0) {
/*  82 */       return new ReturnCode("too many " + openingBlock + " at position " + i, Boolean.valueOf(false));
/*     */     }
/*  84 */     return new ReturnCode();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String getRandomString(int length)
/*     */   {
/*  94 */     String str = "abcdefghigklmnopkrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ0123456789";
/*  95 */     Random random = new Random();
/*  96 */     StringBuffer sf = new StringBuffer();
/*  97 */     for (int i = 0; i < length; i++) {
/*  98 */       int number = random.nextInt(62);
/*  99 */       sf.append(str.charAt(number));
/*     */     }
/* 101 */     return sf.toString();
/*     */   }
/*     */   
/*     */   public static String removeEndingString(String fullStr, String endingStr) {
/* 105 */     String newStr = fullStr;
/*     */     
/* 107 */     while (newStr.length() > 0) {
/* 108 */       if (!newStr.endsWith(endingStr)) break;
/* 109 */       newStr = newStr.substring(0, newStr.length() - endingStr.length());
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 115 */     return newStr;
/*     */   }
/*     */   
/*     */   public static String removeStartingString(String fullStr, String startingStr) {
/* 119 */     String newStr = fullStr;
/*     */     
/* 121 */     while (newStr.length() > 0) {
/* 122 */       if (!newStr.startsWith(startingStr)) break;
/* 123 */       newStr = newStr.substring(startingStr.length());
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/* 129 */     return newStr;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\string\StringUtilities.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */