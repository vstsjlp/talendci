/*    */ package org.talend.utils.string;
/*    */ 
/*    */ import java.io.PrintStream;
/*    */ import java.nio.charset.Charset;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class EncodingExplainer
/*    */ {
/* 22 */   private static final String[] TEST_CASES = { "éàùïô", "中国", "ºÜºÃºÜÇ¿´ó" };
/*    */   
/* 24 */   private static final String[] CHARSETS = { "iso8859-1", "gb2312", "utf-8" };
/*    */   
/*    */   private static final String TOP_LEFT_LABEL = "Encode\\Decode";
/*    */   
/*    */   private static final int MAX_LENGTH = 14;
/*    */   
/*    */   private static void printEncodingTableFor(String text)
/*    */   {
/* 32 */     System.out.println("----------Test for <" + text + ">-----------");
/* 33 */     for (int i = 0; i < CHARSETS.length + 1; i++) {
/* 34 */       for (int j = 0; j < CHARSETS.length + 1; j++) {
/* 35 */         if (i == 0) {
/* 36 */           if (j == 0) {
/* 37 */             System.out.print(appendWhitespaces("Encode\\Decode"));
/*    */           } else {
/* 39 */             System.out.print(appendWhitespaces(CHARSETS[(j - 1)]));
/*    */           }
/*    */         }
/* 42 */         else if (j == 0) {
/* 43 */           System.out.print(appendWhitespaces(CHARSETS[(i - 1)]));
/*    */         } else {
/* 45 */           System.out.print(appendWhitespaces(decodeWith(encodeWith(text, CHARSETS[(i - 1)]), CHARSETS[(j - 1)])));
/*    */         }
/*    */         
/* 48 */         if (j == CHARSETS.length) {
/* 49 */           System.out.println();
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 54 */     System.out.println();
/*    */   }
/*    */   
/*    */   private static byte[] encodeWith(String text, String charsetName) {
/* 58 */     return text.getBytes(Charset.forName(charsetName));
/*    */   }
/*    */   
/*    */   private static String decodeWith(byte[] bytes, String charsetName) {
/* 62 */     return new String(bytes, Charset.forName(charsetName));
/*    */   }
/*    */   
/*    */   private static String appendWhitespaces(String text) {
/* 66 */     int num = 14 - text.length();
/* 67 */     for (int i = 0; i < num; i++) {
/* 68 */       text = text + " ";
/*    */     }
/* 70 */     text = text + "\t";
/* 71 */     return text;
/*    */   }
/*    */   
/*    */   public static void main(String[] args) { String[] arrayOfString;
/* 75 */     int j = (arrayOfString = TEST_CASES).length; for (int i = 0; i < j; i++) { String text = arrayOfString[i];
/* 76 */       printEncodingTableFor(text);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\string\EncodingExplainer.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */