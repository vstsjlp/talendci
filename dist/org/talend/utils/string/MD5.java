/*    */ package org.talend.utils.string;
/*    */ 
/*    */ import java.security.MessageDigest;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class MD5
/*    */ {
/*    */   public static String getMD5(byte[] source)
/*    */   {
/* 27 */     String s = null;
/* 28 */     char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
/*    */     try {
/* 30 */       MessageDigest md = MessageDigest.getInstance("MD5");
/* 31 */       md.update(source);
/* 32 */       byte[] tmp = md.digest();
/* 33 */       char[] str = new char[32];
/* 34 */       int k = 0;
/* 35 */       for (int i = 0; i < 16; i++) {
/* 36 */         byte byte0 = tmp[i];
/* 37 */         str[(k++)] = hexDigits[(byte0 >>> 4 & 0xF)];
/* 38 */         str[(k++)] = hexDigits[(byte0 & 0xF)];
/*    */       }
/* 40 */       s = new String(str);
/*    */     }
/*    */     catch (Exception e) {
/* 43 */       e.printStackTrace();
/*    */     }
/* 45 */     return s;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\string\MD5.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */