/*    */ package org.talend.utils.exceptions;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class MissingDriverException
/*    */   extends RuntimeException
/*    */ {
/*    */   private static final long serialVersionUID = 1L;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 28 */   private String errorMessage = "";
/*    */   
/*    */ 
/*    */   public MissingDriverException() {}
/*    */   
/*    */   public MissingDriverException(String message)
/*    */   {
/* 35 */     super(message);
/* 36 */     this.errorMessage = message;
/*    */   }
/*    */   
/*    */   public MissingDriverException(Throwable cause, String message) {
/* 40 */     super(cause);
/* 41 */     this.errorMessage = message;
/*    */   }
/*    */   
/*    */   public MissingDriverException(Throwable cause) {
/* 45 */     super(cause);
/* 46 */     this.errorMessage = cause.getMessage();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getErrorMessage()
/*    */   {
/* 55 */     return this.errorMessage;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\exceptions\MissingDriverException.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */