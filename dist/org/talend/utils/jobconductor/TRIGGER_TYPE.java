/*     */ package org.talend.utils.jobconductor;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public enum TRIGGER_TYPE
/*     */ {
/*  23 */   SIMPLE_TRIGGER("SimpleTrigger", true, getEmptyString()), 
/*  24 */   CRON_TRIGGER("CronTrigger", true, getEmptyString()), 
/*  25 */   CRON_UI_TRIGGER("CronUITrigger", true, getEmptyString()), 
/*  26 */   FILE_TRIGGER("FileTrigger", false, getFileTriggerPrefix());
/*     */   
/*     */ 
/*     */   private static final String EMPTY_STRING = "";
/*     */   
/*     */   private static final String TIME_TRIGGER_PREFIX = "";
/*     */   
/*     */   private static final String FILE_TRIGGER_PREFIX = "FileTrigger";
/*     */   
/*     */   private static final String FILE_TRIGGER_PARENT_PREFIX = "FileTriggerJob";
/*     */   
/*     */   private String triggerType;
/*     */   private boolean timeTrigger;
/*     */   private String prefix;
/*     */   
/*     */   private TRIGGER_TYPE(String mappingForward, boolean timeTrigger)
/*     */   {
/*  43 */     this.triggerType = mappingForward;
/*  44 */     this.timeTrigger = timeTrigger;
/*     */   }
/*     */   
/*     */   private TRIGGER_TYPE(String mappingForward, boolean timeTrigger, String prefix) {
/*  48 */     this(mappingForward, timeTrigger);
/*  49 */     this.prefix = prefix;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getTriggerTypeStr()
/*     */   {
/*  58 */     return this.triggerType;
/*     */   }
/*     */   
/*     */   public static TRIGGER_TYPE get(String triggerTypeStr) {
/*  62 */     TRIGGER_TYPE[] values = values();
/*  63 */     for (int i = 0; i < values.length; i++) {
/*  64 */       TRIGGER_TYPE triggerTypeCurrent = values[i];
/*  65 */       if (triggerTypeCurrent.triggerType.equals(triggerTypeStr)) {
/*  66 */         return triggerTypeCurrent;
/*     */       }
/*     */     }
/*  69 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isTimeTrigger()
/*     */   {
/*  78 */     return this.timeTrigger;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isFileTrigger()
/*     */   {
/*  87 */     return !this.timeTrigger;
/*     */   }
/*     */   
/*     */   public String getPrefix() {
/*  91 */     return this.prefix;
/*     */   }
/*     */   
/*     */   private static String getEmptyString() {
/*  95 */     return "";
/*     */   }
/*     */   
/*     */   public static String getFileTriggerPrefix() {
/*  99 */     return "FileTrigger";
/*     */   }
/*     */   
/*     */   public static String getFileTriggerParentPrefix() {
/* 103 */     return "FileTriggerJob";
/*     */   }
/*     */   
/*     */   public static String getTimeTriggerPrefix() {
/* 107 */     return "";
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\jobconductor\TRIGGER_TYPE.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */