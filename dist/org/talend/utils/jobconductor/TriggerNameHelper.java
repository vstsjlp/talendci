/*     */ package org.talend.utils.jobconductor;
/*     */ 
/*     */ import java.util.Date;
/*     */ import org.talend.utils.IdGenerator;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class TriggerNameHelper
/*     */ {
/*     */   private static final String P_TASK = "_task";
/*     */   public static final String PREFIX_TALEND_RECOVER_TRIGGER = "RecoverTrigger_task";
/*     */   public static final String PREFIX_QUARTZ_RECOVER_TRIGGER = "recover_";
/*     */   public static final String PREFIX_INSTANT_RUN_TRIGGER = "InstantRunTrigger";
/*     */   public static final String PREFIX_FILE_TRIGGER = "FileTrigger";
/*     */   private static final String PREFIX_TIME_TRIGGER = "";
/*     */   private static TriggerNameHelper instance;
/*     */   
/*     */   public static TriggerNameHelper getInstance()
/*     */   {
/*  41 */     if (instance == null) {
/*  42 */       instance = new TriggerNameHelper();
/*     */     }
/*  44 */     return instance;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Integer parseQuartzTriggerId(String triggerName)
/*     */   {
/*  61 */     String[] split = triggerName.split("\\.");
/*  62 */     triggerName = split[(split.length - 1)];
/*     */     
/*  64 */     if (isTalendRecoverTriggerName(triggerName))
/*     */     {
/*     */ 
/*  67 */       String idStr = triggerName.substring("RecoverTrigger_task".length());
/*  68 */       return Integer.valueOf(Integer.parseInt(idStr)); }
/*  69 */     if (isInstantRunTriggerName(triggerName))
/*     */     {
/*     */ 
/*  72 */       return null; }
/*  73 */     if (isFileTriggerName(triggerName)) {
/*  74 */       String idStr = triggerName.substring("FileTrigger".length());
/*  75 */       return Integer.valueOf(Integer.parseInt(idStr));
/*     */     }
/*     */     try {
/*  78 */       return Integer.valueOf(Integer.parseInt(triggerName));
/*     */     } catch (NumberFormatException localNumberFormatException) {
/*  80 */       throw new IllegalArgumentException(triggerName + " is not a valid trigger name");
/*     */     }
/*     */   }
/*     */   
/*     */   public int parseQuartzJobNameId(String quartzJobName)
/*     */   {
/*     */     try {
/*  87 */       String[] split = quartzJobName.split("\\.");
/*  88 */       quartzJobName = split[(split.length - 1)];
/*     */       
/*  90 */       if (quartzJobName.startsWith(TRIGGER_TYPE.getFileTriggerParentPrefix())) {
/*  91 */         String idStr = quartzJobName.substring(TRIGGER_TYPE.getFileTriggerParentPrefix().length());
/*  92 */         return Integer.parseInt(idStr);
/*     */       }
/*  94 */       return Integer.parseInt(quartzJobName);
/*     */     }
/*     */     catch (NumberFormatException localNumberFormatException) {
/*  97 */       throw new IllegalArgumentException(quartzJobName + " is not a valid job name");
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean isInstantRunTriggerName(String triggerName) {
/* 102 */     return triggerName.matches(".*InstantRunTrigger.+");
/*     */   }
/*     */   
/*     */   public String buildRecoverTriggerName(int idTask) {
/* 106 */     return "RecoverTrigger_task" + idTask;
/*     */   }
/*     */   
/*     */   public String buildInstantRunTriggerName(int idTask) {
/* 110 */     return 
/*     */     
/* 112 */       new StringBuilder("InstantRunTrigger").append(idTask).append("_").append(new Date().getTime()).append("_") + IdGenerator.getAsciiRandomString(5);
/*     */   }
/*     */   
/*     */   public String buildQuartzTriggerName(TRIGGER_TYPE triggerType, int talendTriggerId) {
/* 116 */     String quartzTriggerName = null;
/* 117 */     if (triggerType == TRIGGER_TYPE.FILE_TRIGGER) {
/* 118 */       quartzTriggerName = "FileTrigger" + talendTriggerId;
/*     */     } else {
/* 120 */       quartzTriggerName = String.valueOf(talendTriggerId);
/*     */     }
/* 122 */     return quartzTriggerName;
/*     */   }
/*     */   
/*     */   public boolean isTalendRecoverTriggerName(String triggerName) {
/* 126 */     return triggerName.matches(".*RecoverTrigger_task.+");
/*     */   }
/*     */   
/*     */   public boolean isQuartzRecoverTriggerName(String triggerName) {
/* 130 */     return triggerName.matches("recover_.+");
/*     */   }
/*     */   
/*     */   public boolean isRecoverTriggerName(String triggerName) {
/* 134 */     return (isTalendRecoverTriggerName(triggerName)) || (isQuartzRecoverTriggerName(triggerName));
/*     */   }
/*     */   
/*     */   public boolean isFileTriggerName(String triggerName) {
/* 138 */     return triggerName.matches(".*FileTrigger.+");
/*     */   }
/*     */   
/*     */   public boolean isTimeTriggerName(String triggerName) {
/* 142 */     return triggerName.matches("\\d+");
/*     */   }
/*     */   
/*     */   public boolean isTalendTriggerDependent(String quartzTriggerName) {
/* 146 */     return (isTimeTriggerName(quartzTriggerName)) || (isFileTriggerName(quartzTriggerName));
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\jobconductor\TriggerNameHelper.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */