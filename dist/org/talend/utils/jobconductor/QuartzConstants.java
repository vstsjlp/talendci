/*    */ package org.talend.utils.jobconductor;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class QuartzConstants
/*    */ {
/*    */   public static final String GROUP_JOB_CONDUCTOR = "JOB_CONDUCTOR";
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static final String GROUP_SYSTEM = "SYSTEM";
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static final String GROUP_DATABASE_SVN_BACKUP = "DATABASE_SVN_BACKUP";
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static boolean equals(String group1, String group2)
/*    */   {
/* 32 */     return (group1 == group2) || ((group1 != null) && (group1.equals(group2))) || ((group1 == null) && ("DEFAULT".equals(group2))) || (
/* 33 */       ("DEFAULT".equals(group1)) && (group2 == null));
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\jobconductor\QuartzConstants.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */