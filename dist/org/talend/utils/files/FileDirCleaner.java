/*     */ package org.talend.utils.files;
/*     */ 
/*     */ import java.io.File;
/*     */ import java.util.Arrays;
/*     */ import java.util.Comparator;
/*     */ import org.apache.commons.io.FileUtils;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class FileDirCleaner
/*     */ {
/*  29 */   private static Logger log = Logger.getLogger(FileDirCleaner.class);
/*     */   
/*     */   private long currentTime;
/*     */   
/*     */   private CleanResult cleanResult;
/*     */   
/*     */   private String filesRegExpPattern;
/*     */   
/*     */   private String directoriesRegExpPattern;
/*     */   
/*     */   private int maxEntriesByDirectoryAndByType;
/*     */   
/*     */   private long maxDurationBeforeCleaning;
/*     */   
/*     */   private boolean cleanDirectories;
/*     */   
/*     */   private boolean cleanFiles;
/*     */   
/*     */   private boolean recursively;
/*     */   
/*  49 */   private boolean doAction = false;
/*     */   
/*     */   private FileDirCleanerFilter filter;
/*     */   
/*     */ 
/*     */   static class CleanResult
/*     */   {
/*     */     Throwable firstException;
/*     */     
/*     */     int countExceptions;
/*     */     
/*     */     int deletedEntries;
/*     */     
/*     */     boolean alreadyLogged;
/*     */   }
/*     */   
/*     */   public static enum SCAN_STRATEGY
/*     */   {
/*  67 */     FILES(false, true, false), 
/*  68 */     DIRECTORIES(true, false, false), 
/*  69 */     FILES_AND_DIRECTORIES(true, true, false), 
/*  70 */     FILES_RECURSIVELY(false, true, true), 
/*  71 */     DIRECTORIES_RECURSIVELY(true, false, true), 
/*  72 */     FILES_AND_DIRECTORIES_RECURSIVELY(true, true, true);
/*     */     
/*     */ 
/*     */     private boolean cleanDirectories;
/*     */     private boolean cleanFiles;
/*     */     private boolean recursively;
/*     */     
/*     */     private SCAN_STRATEGY(boolean cleanDirectories, boolean cleanFiles, boolean recursively)
/*     */     {
/*  81 */       this.cleanFiles = cleanFiles;
/*  82 */       this.cleanDirectories = cleanDirectories;
/*  83 */       this.recursively = recursively;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public boolean isCleanDirectories()
/*     */     {
/*  92 */       return this.cleanDirectories;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public boolean isCleanFiles()
/*     */     {
/* 101 */       return this.cleanFiles;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public boolean isRecursively()
/*     */     {
/* 110 */       return this.recursively;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public FileDirCleaner(boolean doAction, int maxEntriesByDirectoryAndByType, long maxDurationBeforeCleaning)
/*     */   {
/* 140 */     this(doAction, SCAN_STRATEGY.FILES, maxEntriesByDirectoryAndByType, maxDurationBeforeCleaning);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public FileDirCleaner(boolean doAction, SCAN_STRATEGY strategy, int maxEntriesByDirectory, long cleanAfterThisDuration)
/*     */   {
/* 156 */     this.doAction = doAction;
/* 157 */     this.maxEntriesByDirectoryAndByType = maxEntriesByDirectory;
/* 158 */     this.maxDurationBeforeCleaning = cleanAfterThisDuration;
/* 159 */     this.cleanDirectories = strategy.isCleanDirectories();
/* 160 */     this.cleanFiles = strategy.isCleanFiles();
/* 161 */     this.recursively = strategy.isRecursively();
/*     */   }
/*     */   
/*     */ 
/*     */   public FileDirCleaner(boolean doAction, SCAN_STRATEGY strategy, int maxEntriesByDirectory, long cleanAfterThisDuration, boolean isCleanLibs)
/*     */   {
/* 167 */     this.doAction = doAction;
/* 168 */     this.maxEntriesByDirectoryAndByType = maxEntriesByDirectory;
/* 169 */     this.maxDurationBeforeCleaning = cleanAfterThisDuration;
/* 170 */     this.cleanDirectories = strategy.isCleanDirectories();
/* 171 */     this.cleanFiles = strategy.isCleanFiles();
/* 172 */     this.recursively = strategy.isRecursively();
/*     */   }
/*     */   
/* 175 */   final Comparator<File> datComparatorFiles = new Comparator()
/*     */   {
/*     */ 
/*     */ 
/*     */ 
/*     */     public int compare(File o1, File o2)
/*     */     {
/*     */ 
/*     */ 
/* 184 */       long compareResult = o1.lastModified() - o2.lastModified();
/* 185 */       if (compareResult == 0L) {
/* 186 */         return 0;
/*     */       }
/* 188 */       return (int)(compareResult / Math.abs(compareResult));
/*     */     }
/*     */   };
/*     */   
/*     */ 
/*     */   public int clean(String pathDir)
/*     */   {
/* 195 */     return clean(pathDir, null, null);
/*     */   }
/*     */   
/*     */   public int clean(String pathDir, String filesRegExpPattern) {
/* 199 */     return clean(pathDir, filesRegExpPattern, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int clean(String pathDir, String filesRegExpPattern, String directoriesRegExpPattern)
/*     */   {
/* 212 */     return clean(pathDir, filesRegExpPattern, directoriesRegExpPattern, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int clean(String pathDir, String filesRegExpPattern, String directoriesRegExpPattern, FileDirCleanerFilter filter)
/*     */   {
/* 225 */     if (pathDir == null) {
/* 226 */       throw new IllegalArgumentException("pathFolder can't be null");
/*     */     }
/* 228 */     this.cleanResult = new CleanResult();
/* 229 */     this.directoriesRegExpPattern = directoriesRegExpPattern;
/* 230 */     this.filesRegExpPattern = filesRegExpPattern;
/* 231 */     this.currentTime = System.currentTimeMillis();
/* 232 */     this.filter = filter;
/* 233 */     File dir = new File(pathDir);
/* 234 */     if (dir.isDirectory()) {
/* 235 */       cleanFilesDirRecursively(dir, true);
/*     */     }
/* 237 */     return this.cleanResult.deletedEntries;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void cleanFilesDirRecursively(File dir, boolean isRootDirectory)
/*     */   {
/*     */     try
/*     */     {
/* 249 */       File[] listFilesDirs = dir.listFiles();
/* 250 */       Arrays.sort(listFilesDirs, this.datComparatorFiles);
/* 251 */       int countMatchingDirs = 0;
/* 252 */       int countMatchingFiles = 0;
/* 253 */       int levelDeletedDir = 0;
/* 254 */       int levelDeletedFile = 0;
/* 255 */       File[] arrayOfFile1; int j = (arrayOfFile1 = listFilesDirs).length; boolean isDirectory; for (int i = 0; i < j; i++) { File fileDirJob = arrayOfFile1[i];
/* 256 */         isDirectory = fileDirJob.isDirectory();
/* 257 */         boolean fileMatches = false;
/* 258 */         boolean dirMatches = false;
/* 259 */         String fileDirName = fileDirJob.getName();
/* 260 */         if (isDirectory) {
/* 261 */           dirMatches = (this.directoriesRegExpPattern == null) || (fileDirName.matches(this.directoriesRegExpPattern));
/*     */         } else {
/* 263 */           fileMatches = (this.filesRegExpPattern == null) || (fileDirName.matches(this.filesRegExpPattern));
/*     */         }
/* 265 */         if ((isDirectory) && (dirMatches)) {
/* 266 */           countMatchingDirs++;
/* 267 */         } else if ((!isDirectory) && (fileMatches)) {
/* 268 */           countMatchingFiles++;
/*     */         }
/*     */       }
/*     */       
/*     */ 
/* 273 */       boolean parentDirMatches = (this.directoriesRegExpPattern == null) || (dir.getName().matches(this.directoriesRegExpPattern));
/*     */       
/* 275 */       int k = (isDirectory = listFilesDirs).length; for (j = 0; j < k; j++) { File fileDirJob = isDirectory[j];
/*     */         
/* 277 */         String fileDirName = fileDirJob.getName();
/* 278 */         boolean fileMatches = false;
/* 279 */         boolean dirMatches = false;
/* 280 */         boolean isDirectory = fileDirJob.isDirectory();
/* 281 */         boolean tooManyDirs = ((isRootDirectory) || ((!isRootDirectory) && (this.recursively))) && (isDirectory) && 
/* 282 */           (this.maxEntriesByDirectoryAndByType > 0) && (
/* 283 */           countMatchingDirs - levelDeletedDir > this.maxEntriesByDirectoryAndByType);
/* 284 */         boolean tooManyFiles = (!isDirectory) && (this.maxEntriesByDirectoryAndByType > 0) && (
/* 285 */           countMatchingFiles - levelDeletedFile > this.maxEntriesByDirectoryAndByType);
/* 286 */         boolean timeExceeded = (this.maxDurationBeforeCleaning > 0L) && (
/* 287 */           this.currentTime - fileDirJob.lastModified() > this.maxDurationBeforeCleaning * 1000L);
/*     */         try {
/* 289 */           if ((timeExceeded) || (tooManyDirs) || (tooManyFiles)) {
/* 290 */             if (isDirectory) {
/* 291 */               dirMatches = (this.directoriesRegExpPattern == null) || (fileDirName.matches(this.directoriesRegExpPattern));
/*     */             } else {
/* 293 */               fileMatches = (this.filesRegExpPattern == null) || (fileDirName.matches(this.filesRegExpPattern));
/*     */             }
/* 295 */             if (isDirectory) {
/* 296 */               if ((this.cleanDirectories) && (dirMatches)) {
/* 297 */                 if (checkFilter(fileDirJob)) {
/* 298 */                   if (this.doAction) {
/* 299 */                     FileUtils.deleteDirectory(fileDirJob);
/*     */                   } else {
/* 301 */                     StringBuilder reason = new StringBuilder();
/* 302 */                     String sep = "";
/* 303 */                     if (timeExceeded) {
/* 304 */                       reason.append("timeExceeded");
/* 305 */                       sep = ", ";
/*     */                     }
/* 307 */                     if (tooManyDirs) {
/* 308 */                       reason.append(sep + "tooManyDirs");
/*     */                     }
/* 310 */                     log.debug("'doAction' has to be true to remove recursively the directory (" + 
/* 311 */                       reason.toString() + "): " + fileDirJob);
/*     */                   }
/* 313 */                   this.cleanResult.deletedEntries += 1;
/* 314 */                   levelDeletedDir++;
/*     */                 }
/* 316 */               } else if (this.recursively) {
/* 317 */                 cleanFilesDirRecursively(fileDirJob, false);
/*     */               }
/*     */             }
/* 320 */             else if ((this.cleanFiles) && (fileMatches) && (parentDirMatches) && 
/* 321 */               (checkFilter(fileDirJob))) {
/* 322 */               if (this.doAction) {
/* 323 */                 FileUtils.forceDelete(fileDirJob);
/*     */               } else {
/* 325 */                 StringBuilder reason = new StringBuilder();
/* 326 */                 String sep = "";
/* 327 */                 if (timeExceeded) {
/* 328 */                   reason.append("timeExceeded");
/* 329 */                   sep = ", ";
/*     */                 }
/* 331 */                 if (tooManyFiles) {
/* 332 */                   reason.append(sep + "tooManyFiles");
/*     */                 }
/* 334 */                 log.debug("'doAction' has to be true to remove the file (" + reason.toString() + "): " + 
/* 335 */                   fileDirJob);
/*     */               }
/* 337 */               this.cleanResult.deletedEntries += 1;
/* 338 */               levelDeletedFile++;
/*     */             }
/*     */             
/*     */           }
/* 342 */           else if ((this.recursively) && (isDirectory)) {
/* 343 */             levelDeletedDir++;
/* 344 */             cleanFilesDirRecursively(fileDirJob, false);
/*     */           }
/*     */         } catch (Throwable t) {
/* 347 */           this.cleanResult.countExceptions += 1;
/* 348 */           if (this.cleanResult.firstException == null) {
/* 349 */             this.cleanResult.firstException = t;
/*     */           }
/*     */         }
/*     */       }
/* 353 */       if ((this.cleanResult.firstException != null) && (!this.cleanResult.alreadyLogged)) {
/* 354 */         log.warn("TempDataCleaner: " + this.cleanResult.countExceptions + 
/* 355 */           " error(s) have occured when trying to clean the following file or directory '" + dir.getAbsolutePath() + 
/* 356 */           "', the first error is the following : ", this.cleanResult.firstException);
/* 357 */         this.cleanResult.alreadyLogged = true;
/*     */       }
/*     */     }
/*     */     catch (Throwable e) {
/* 361 */       log.error(e.getMessage(), e);
/*     */     }
/*     */   }
/*     */   
/*     */   private boolean checkFilter(File fileDirJob) {
/* 366 */     fileDirJob.isDirectory();
/* 367 */     if ((this.filter != null) && 
/* 368 */       (!this.filter.acceptClean(fileDirJob)))
/*     */     {
/* 370 */       return false;
/*     */     }
/*     */     
/*     */ 
/* 374 */     return true;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\files\FileDirCleaner.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */