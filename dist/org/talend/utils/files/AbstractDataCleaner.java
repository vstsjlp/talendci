/*     */ package org.talend.utils.files;
/*     */ 
/*     */ import org.apache.log4j.Logger;
/*     */ import org.talend.utils.thread.ThreadUtils;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public abstract class AbstractDataCleaner
/*     */ {
/*  24 */   private static Logger log = Logger.getLogger(AbstractDataCleaner.class);
/*     */   
/*     */   private String cleanerLabel;
/*     */   
/*     */   private Thread threadCleaner;
/*     */   
/*     */   private int frequencyCleaningAction;
/*     */   
/*     */   private boolean stop;
/*     */   
/*     */   public AbstractDataCleaner(String cleanerLabel, int frequencyCleaningAction)
/*     */   {
/*  36 */     this.cleanerLabel = cleanerLabel;
/*  37 */     this.frequencyCleaningAction = frequencyCleaningAction;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean start()
/*     */   {
/*  48 */     if (this.frequencyCleaningAction > 0)
/*     */     {
/*  50 */       this.threadCleaner = new Thread(this.cleanerLabel)
/*     */       {
/*     */ 
/*     */ 
/*     */ 
/*     */         public void run()
/*     */         {
/*     */ 
/*     */ 
/*  59 */           AbstractDataCleaner.this.cleanLoop();
/*     */         }
/*     */         
/*  62 */       };
/*  63 */       this.threadCleaner.start();
/*  64 */       return true;
/*     */     }
/*  66 */     return false;
/*     */   }
/*     */   
/*     */   public void stop()
/*     */   {
/*  71 */     this.stop = true;
/*  72 */     this.threadCleaner.interrupt();
/*     */   }
/*     */   
/*     */   private void cleanLoop() {
/*  76 */     log.info(this.cleanerLabel + " started.");
/*  77 */     while (!this.stop) {
/*     */       try {
/*  79 */         clean();
/*     */       } catch (Throwable e) {
/*  81 */         if (log.isDebugEnabled()) {
/*  82 */           log.debug(e.getMessage(), e);
/*     */         } else {
/*  84 */           log.warn(e.getMessage());
/*     */         }
/*     */       }
/*  87 */       ThreadUtils.waitTimeBool(this.frequencyCleaningAction * 1000);
/*     */     }
/*  89 */     log.info(this.cleanerLabel + " stopped.");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   protected abstract void clean();
/*     */   
/*     */ 
/*     */ 
/*     */   public int getFrequencyCleaningAction()
/*     */   {
/* 100 */     return this.frequencyCleaningAction;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setFrequencyCleaningAction(int frequencyCleaningAction)
/*     */   {
/* 109 */     this.frequencyCleaningAction = frequencyCleaningAction;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\files\AbstractDataCleaner.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */