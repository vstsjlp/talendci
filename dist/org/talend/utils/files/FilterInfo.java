/*    */ package org.talend.utils.files;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FilterInfo
/*    */ {
/*    */   private String mPrefix;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   private String mSuffix;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public FilterInfo(String prefix, String suffix)
/*    */   {
/* 22 */     this.mPrefix = prefix;
/* 23 */     this.mSuffix = suffix;
/*    */   }
/*    */   
/*    */   public String getPrefix() {
/* 27 */     return this.mPrefix;
/*    */   }
/*    */   
/*    */   public String getSuffix() {
/* 31 */     return this.mSuffix;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\files\FilterInfo.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */