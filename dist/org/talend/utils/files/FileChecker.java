/*    */ package org.talend.utils.files;
/*    */ 
/*    */ import java.io.File;
/*    */ import org.talend.utils.sugars.ReturnCode;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FileChecker
/*    */ {
/*    */   public static final String NO_RIGHT_TO_WRITE = "No right to write";
/*    */   public static final String PATH_MUST_BE_A_FILE = "Path must be a file";
/*    */   public static final String PATH_MUST_BE_A_DIRECTORY = "Path must be a directory";
/*    */   public static final String PATH_DOES_NOT_EXIST = "Path does not exist";
/*    */   
/*    */   public static void evaluateFilePath(ReturnCode returnCode, File file, boolean mustBeFile, boolean mustBeFolder)
/*    */   {
/* 33 */     if (!file.exists()) {
/* 34 */       returnCode.setOk(Boolean.valueOf(false));
/* 35 */       returnCode.setMessage("Path does not exist");
/* 36 */     } else if ((!file.isDirectory()) && (mustBeFolder)) {
/* 37 */       returnCode.setOk(Boolean.valueOf(false));
/* 38 */       returnCode.setMessage("Path must be a directory");
/* 39 */     } else if ((!file.isFile()) && (mustBeFile)) {
/* 40 */       returnCode.setOk(Boolean.valueOf(false));
/* 41 */       returnCode.setMessage("Path must be a file");
/* 42 */     } else if (!file.canWrite()) {
/* 43 */       returnCode.setOk(Boolean.valueOf(false));
/* 44 */       returnCode.setMessage("No right to write");
/*    */     } else {
/* 46 */       returnCode.setOk(Boolean.valueOf(true));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\files\FileChecker.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */