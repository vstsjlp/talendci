package org.talend.utils.files;

import java.io.File;

public abstract interface FileDirCleanerFilter
{
  public abstract boolean acceptClean(File paramFile);
}


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\files\FileDirCleanerFilter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */