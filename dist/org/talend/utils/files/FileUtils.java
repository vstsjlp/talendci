/*     */ package org.talend.utils.files;
/*     */ 
/*     */ import java.io.BufferedInputStream;
/*     */ import java.io.BufferedReader;
/*     */ import java.io.BufferedWriter;
/*     */ import java.io.DataInputStream;
/*     */ import java.io.File;
/*     */ import java.io.FileFilter;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.FileReader;
/*     */ import java.io.FilenameFilter;
/*     */ import java.io.IOException;
/*     */ import java.io.OutputStream;
/*     */ import java.io.OutputStreamWriter;
/*     */ import java.net.URISyntaxException;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.List;
/*     */ import java.util.Set;
/*     */ import java.util.function.Function;
/*     */ import org.talend.utils.string.StringUtilities;
/*     */ import org.talend.utils.sugars.ReturnCode;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class FileUtils
/*     */ {
/*     */   public static synchronized void replaceInFile(String path, String oldString, String newString)
/*     */     throws IOException, URISyntaxException
/*     */   {
/*  51 */     File file = new File(path);
/*  52 */     File tmpFile = new File(path + ".tmp");
/*     */     
/*  54 */     FileInputStream fis = null;
/*  55 */     BufferedInputStream bis = null;
/*  56 */     DataInputStream dis = null;
/*     */     
/*  58 */     fis = new FileInputStream(file);
/*  59 */     bis = new BufferedInputStream(fis);
/*  60 */     dis = new DataInputStream(bis);
/*     */     
/*  62 */     OutputStream tempOutputStream = new FileOutputStream(tmpFile);
/*  63 */     BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(tempOutputStream, "UTF8"));
/*     */     
/*     */ 
/*  66 */     int len = 0;
/*     */     
/*  68 */     byte[] buf2 = new byte['Ѐ'];
/*     */     
/*  70 */     while ((len = dis.read(buf2)) != -1) {
/*  71 */       String line = new String(buf2, 0, len);
/*  72 */       String newLine = line.replace(oldString, newString);
/*  73 */       newLine = new String(newLine.getBytes(), "UTF8");
/*  74 */       bufferedWriter.write(newLine);
/*  75 */       bufferedWriter.flush();
/*     */     }
/*     */     
/*  78 */     bufferedWriter.close();
/*  79 */     dis.close();
/*     */     
/*  81 */     file.delete();
/*  82 */     tmpFile.renameTo(file);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static synchronized List<ReturnCode> checkBracketsInFile(String path)
/*     */     throws IOException, URISyntaxException
/*     */   {
/*  94 */     List<ReturnCode> returncodes = new ArrayList();
/*  95 */     File file = new File(path);
/*  96 */     BufferedReader in = new BufferedReader(new FileReader(file));
/*     */     
/*     */ 
/*  99 */     int lineNb = 0;
/*     */     String line;
/* 101 */     while ((line = in.readLine()) != null) { String line;
/* 102 */       ReturnCode checkBlocks = StringUtilities.checkBalancedParenthesis(line, '(', ')');
/* 103 */       lineNb++;
/* 104 */       if (!checkBlocks.isOk().booleanValue()) {
/* 105 */         String errorMsg = "Line " + lineNb + ": " + checkBlocks.getMessage();
/* 106 */         returncodes.add(new ReturnCode(errorMsg, Boolean.valueOf(false)));
/*     */       }
/*     */     }
/*     */     
/* 110 */     in.close();
/* 111 */     return returncodes;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void getAllFilesFromFolder(File aFolder, List<File> fileList, FilenameFilter filenameFilter)
/*     */   {
/* 122 */     if (aFolder != null) {
/* 123 */       File[] folderFiles = aFolder.listFiles(filenameFilter);
/* 124 */       if ((fileList != null) && (folderFiles != null)) {
/* 125 */         Collections.addAll(fileList, folderFiles);
/*     */       }
/* 127 */       File[] allFolders = aFolder.listFiles(new FileFilter()
/*     */       {
/*     */         public boolean accept(File arg0)
/*     */         {
/* 131 */           return arg0.isDirectory();
/*     */         }
/*     */       });
/* 134 */       if (allFolders != null) { File[] arrayOfFile1;
/* 135 */         int j = (arrayOfFile1 = allFolders).length; for (int i = 0; i < j; i++) { File folder = arrayOfFile1[i];
/* 136 */           getAllFilesFromFolder(folder, fileList, filenameFilter);
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<File> getAllFilesFromFolder(File aFolder, FilenameFilter filenameFilter)
/*     */   {
/* 150 */     List<File> files = new ArrayList();
/* 151 */     getAllFilesFromFolder(aFolder, files, filenameFilter);
/* 152 */     return files;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static List<File> getAllFilesFromFolder(File aFolder, Set<FilterInfo> filterInfo)
/*     */   {
/* 164 */     List<File> files = new ArrayList();
/* 165 */     if (filterInfo != null) {
/* 166 */       for (FilterInfo info : filterInfo) {
/* 167 */         FilterInfo thatInfo = info;
/* 168 */         files.addAll(getAllFilesFromFolder(aFolder, new FilenameFilter()
/*     */         {
/*     */           public boolean accept(File dir, String name)
/*     */           {
/* 172 */             if (name == null) {
/* 173 */               return false;
/*     */             }
/* 175 */             return (name.startsWith(FileUtils.this.getPrefix())) && (name.endsWith(FileUtils.this.getSuffix()));
/*     */           }
/*     */         }));
/*     */       }
/*     */     }
/* 180 */     return files;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void deleteFiles(File folder, Function<String, Boolean> func)
/*     */   {
/* 191 */     if ((folder != null) && (func != null) && 
/* 192 */       (folder.exists())) {
/* 193 */       FilenameFilter filter = new FilenameFilter()
/*     */       {
/*     */         public boolean accept(File _dir, String name)
/*     */         {
/* 197 */           return ((Boolean)FileUtils.this.apply(name)).booleanValue();
/*     */         }
/*     */         
/* 200 */       };
/* 201 */       List<File> filesToRemove = getAllFilesFromFolder(folder, filter);
/* 202 */       for (File fileToRemove : filesToRemove) {
/* 203 */         fileToRemove.delete();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static File createTmpFolder(String prefix, String suffix)
/*     */   {
/* 218 */     File tempFolder = null;
/*     */     try {
/* 220 */       tempFolder = File.createTempFile(prefix, suffix);
/* 221 */       tempFolder.delete();
/*     */     } catch (IOException localIOException) {
/* 223 */       String tempFolderName = prefix + System.currentTimeMillis() + suffix;
/* 224 */       tempFolder = createUserTmpFolder(tempFolderName);
/*     */     }
/* 226 */     tempFolder.mkdirs();
/* 227 */     return tempFolder;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static File createUserTmpFolder(String folderName)
/*     */   {
/* 238 */     File tmpFolder = new File(System.getProperty("user.dir"), "temp/" + folderName);
/* 239 */     tmpFolder.mkdirs();
/* 240 */     return tmpFolder;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\files\FileUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */