/*     */ package org.talend.utils.dates;
/*     */ 
/*     */ import java.text.DateFormat;
/*     */ import java.text.ParseException;
/*     */ import java.text.SimpleDateFormat;
/*     */ import java.util.Calendar;
/*     */ import java.util.Date;
/*     */ import java.util.GregorianCalendar;
/*     */ import java.util.TimeZone;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class DateUtils
/*     */ {
/*  31 */   protected static Logger log = Logger.getLogger(DateUtils.class);
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String PATTERN_1 = "MM/dd/yyyy";
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String PATTERN_2 = "yyyy-MM-dd hh:mm:ss";
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String PATTERN_3 = "yyyy-MM-dd";
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String PATTERN_4 = "MM/dd/yyyy HH:mm";
/*     */   
/*     */ 
/*     */   public static final String PATTERN_5 = "yyyy-MM-dd HH:mm:ss";
/*     */   
/*     */ 
/*     */   public static final String PATTERN_6 = "yyyyMMddHHmmss";
/*     */   
/*     */ 
/*     */   public static final String PATTERN_7 = "hh:mm:ss";
/*     */   
/*     */ 
/*     */   public static final String UTC = "UTC";
/*     */   
/*     */ 
/*     */ 
/*     */   public static Date parse(String pattern, String dateText)
/*     */     throws ParseException
/*     */   {
/*  66 */     Date date = null;
/*  67 */     SimpleDateFormat sdf = new SimpleDateFormat(pattern);
/*  68 */     date = sdf.parse(dateText);
/*  69 */     return date;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String getCurrentDate(String pattern)
/*     */   {
/*  79 */     SimpleDateFormat sdf = new SimpleDateFormat(pattern);
/*  80 */     return sdf.format(new Date());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static String formatTimeStamp(String pattern, long date)
/*     */   {
/*  92 */     if ((pattern == null) || (pattern.length() == 0)) {
/*  93 */       pattern = "yyyyMMddHHmmss";
/*     */     }
/*  95 */     Calendar nowDate = new GregorianCalendar();
/*  96 */     nowDate.setTimeInMillis(date);
/*  97 */     DateFormat df = new SimpleDateFormat(pattern);
/*  98 */     return df.format(nowDate.getTime());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Date convert2StandardTime(Date localDate)
/*     */   {
/* 108 */     Date result = localDate;
/* 109 */     SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
/* 110 */     dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
/* 111 */     String dateStr = dateFormat.format(localDate);
/*     */     try {
/* 113 */       dateFormat.setTimeZone(TimeZone.getDefault());
/* 114 */       result = dateFormat.parse(dateStr);
/*     */     } catch (ParseException e) {
/* 116 */       log.error(e);
/*     */     }
/* 118 */     return result;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static Date getDateWithoutTime(Date utcExecutionDate)
/*     */   {
/* 128 */     Date calendarDate = utcExecutionDate;
/*     */     try {
/* 130 */       calendarDate = parse("yyyy-MM-dd", formatTimeStamp("yyyy-MM-dd", calendarDate.getTime()));
/*     */     } catch (ParseException e) {
/* 132 */       log.warn(e);
/*     */     }
/* 134 */     return calendarDate;
/*     */   }
/*     */   
/*     */ 
/*     */   public static DateFormat createDateFormater(int dataType)
/*     */   {
/*     */     SimpleDateFormat ret;
/*     */     
/*     */     SimpleDateFormat ret;
/*     */     
/*     */     SimpleDateFormat ret;
/*     */     
/* 146 */     switch (dataType) {
/*     */     case 92: 
/* 148 */       ret = new SimpleDateFormat("HH:mm:ss");
/* 149 */       break;
/*     */     case 93: 
/* 151 */       ret = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
/* 152 */       break;
/*     */     case 91: 
/*     */     default: 
/* 155 */       ret = new SimpleDateFormat("yyyy-MM-dd");
/*     */     }
/*     */     
/* 158 */     return ret;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\dates\DateUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */