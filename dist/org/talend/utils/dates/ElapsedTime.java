/*     */ package org.talend.utils.dates;
/*     */ 
/*     */ import java.util.Calendar;
/*     */ import java.util.Date;
/*     */ import java.util.GregorianCalendar;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class ElapsedTime
/*     */ {
/*     */   public static final int GREGORIAN_CAL_YEAR = 1582;
/*     */   public static final int GREGORIAN_CAL_MONTH = 10;
/*     */   public static final int GREGORIAN_CAL_DAY = 15;
/*     */   public static final double TROPICAL_YEAR_LENGTH = 365.25D;
/*  46 */   private static final Calendar TMP_GREG_CAL_1 = new GregorianCalendar();
/*     */   
/*     */ 
/*  49 */   private static final Calendar TMP_GREG_CAL_2 = new GregorianCalendar();
/*     */   
/*     */ 
/*     */ 
/*     */   private static final int NB_MILLIS_PER_MINUTE = 60000;
/*     */   
/*     */ 
/*     */ 
/*     */   private static final int NB_MILLIS_PER_SECOND = 1000;
/*     */   
/*     */ 
/*     */ 
/*     */   public static long getNbDays(Date first, Date last)
/*     */   {
/*  63 */     Calendar c1 = TMP_GREG_CAL_1;
/*  64 */     Calendar c2 = TMP_GREG_CAL_2;
/*     */     
/*  66 */     c1.setTime(last);
/*  67 */     c2.setTime(first);
/*     */     
/*  69 */     long j1 = getJulianDays(c1.get(1), c1.get(2) + 1, c1.get(5));
/*  70 */     long j2 = getJulianDays(c2.get(1), c2.get(2) + 1, c2.get(5));
/*     */     
/*  72 */     long daysElapsed = j1 - j2;
/*  73 */     return daysElapsed;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long getNbMinutes(Date first, Date last)
/*     */   {
/*  83 */     long min = (last.getTime() - first.getTime()) / 60000L;
/*  84 */     return min;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long getNbSeconds(Date first, Date last)
/*     */   {
/*  94 */     long min = (last.getTime() - first.getTime()) / 1000L;
/*  95 */     return min;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static long getJulianDays(int year, int month, int day)
/*     */     throws IllegalArgumentException
/*     */   {
/* 110 */     if ((year < 1582) || ((year == 1582) && (month < 10)) || (
/* 111 */       (year == 1582) && (month == 10) && (day < 15))) {
/* 112 */       throw new IllegalArgumentException("Dates before 15 Oct 1582 are not valid Gregorian dates " + year + "/" + month + 
/* 113 */         "/" + day);
/*     */     }
/*     */     
/*     */ 
/* 117 */     int y = year;
/* 118 */     int m = month;
/* 119 */     if ((m == 1) || (m == 2)) {
/* 120 */       y--;
/* 121 */       m += 12;
/*     */     }
/*     */     
/* 124 */     int a = y / 100;
/* 125 */     int b = a / 4;
/* 126 */     int c = 2 - a + b;
/*     */     
/* 128 */     double e = 365.25D * (y + 4716);
/* 129 */     double e1 = Math.floor(e);
/* 130 */     long e2 = Math.round(e1);
/*     */     
/* 132 */     double f = 30.6001D * (m + 1);
/* 133 */     double f1 = Math.floor(f);
/* 134 */     long f2 = Math.round(f1);
/*     */     
/*     */ 
/*     */ 
/* 138 */     long julianDay = c + day + e2 + f2 - 1524L;
/*     */     
/* 140 */     return julianDay;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static int fieldDifference(Calendar start, Date endTime, int field)
/*     */   {
/* 163 */     Date startTime = start.getTime();
/* 164 */     if (!startTime.before(endTime)) {
/* 165 */       return 0;
/*     */     }
/* 167 */     for (int i = 1;; i++) {
/* 168 */       start.add(field, 1);
/* 169 */       if (start.getTime().after(endTime)) {
/* 170 */         start.add(field, -1);
/* 171 */         return i - 1;
/*     */       }
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\dates\ElapsedTime.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */