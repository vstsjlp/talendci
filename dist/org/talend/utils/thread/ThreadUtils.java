/*    */ package org.talend.utils.thread;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ThreadUtils
/*    */ {
/*    */   public static boolean waitTimeBool(Object object, long time)
/*    */   {
/* 30 */     synchronized (object) {
/*    */       try {
/* 32 */         object.wait(time);
/*    */       } catch (InterruptedException localInterruptedException) {
/* 34 */         return true;
/*    */       }
/*    */     }
/* 37 */     return false;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static boolean waitTimeBool(long time)
/*    */   {
/* 48 */     synchronized () {
/*    */       try {
/* 50 */         Thread.currentThread().wait(time);
/*    */       } catch (InterruptedException localInterruptedException) {
/* 52 */         return true;
/*    */       }
/*    */     }
/* 55 */     return false;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void waitTimeExcept(Object object, long time)
/*    */     throws InterruptedException
/*    */   {
/* 67 */     synchronized (object) {
/* 68 */       object.wait(time);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static void waitTimeExcept(long time)
/*    */     throws InterruptedException
/*    */   {
/* 80 */     synchronized () {
/* 81 */       Thread.currentThread().wait(time);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\thread\ThreadUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */