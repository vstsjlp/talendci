/*    */ package org.talend.utils.thread;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class MultiThreadWaiter
/*    */ {
/*    */   private List<? extends Thread> threads;
/*    */   
/*    */   public MultiThreadWaiter(List<? extends Thread> threadsToStart)
/*    */   {
/* 33 */     this.threads = threadsToStart;
/*    */   }
/*    */   
/*    */   public void start() {
/* 37 */     int iterableListSize = this.threads.size();
/* 38 */     for (int i = 0; i < iterableListSize; i++) {
/* 39 */       ((Thread)this.threads.get(i)).start();
/*    */     }
/* 41 */     for (int i = 0; i < iterableListSize; i++) {
/*    */       try {
/* 43 */         ((Thread)this.threads.get(i)).join();
/*    */       }
/*    */       catch (InterruptedException localInterruptedException) {}
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\thread\MultiThreadWaiter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */