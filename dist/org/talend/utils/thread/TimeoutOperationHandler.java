/*     */ package org.talend.utils.thread;
/*     */ 
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public abstract class TimeoutOperationHandler<R>
/*     */ {
/*  25 */   private static Logger log = Logger.getLogger(TimeoutOperationHandler.class);
/*     */   
/*     */   private R result;
/*     */   
/*     */   private boolean timeoutReached;
/*     */   
/*     */   private String labelOperation;
/*     */   
/*     */   private long timeout;
/*     */   
/*     */   private Throwable operationError;
/*     */   
/*     */   public TimeoutOperationHandler(long timeout)
/*     */   {
/*  39 */     this.timeout = timeout;
/*     */   }
/*     */   
/*     */   public TimeoutOperationHandler(long timeout, String labelOperation)
/*     */   {
/*  44 */     this.timeout = timeout;
/*  45 */     this.labelOperation = labelOperation;
/*     */   }
/*     */   
/*     */   public void start()
/*     */   {
/*  50 */     internalStart();
/*     */   }
/*     */   
/*     */   protected void internalStart()
/*     */   {
/*  55 */     Runnable runnable = new Runnable()
/*     */     {
/*     */       /* Error */
/*     */       public void run()
/*     */       {
/*     */         // Byte code:
/*     */         //   0: aload_0
/*     */         //   1: getfield 12	org/talend/utils/thread/TimeoutOperationHandler$1:this$0	Lorg/talend/utils/thread/TimeoutOperationHandler;
/*     */         //   4: aload_0
/*     */         //   5: getfield 12	org/talend/utils/thread/TimeoutOperationHandler$1:this$0	Lorg/talend/utils/thread/TimeoutOperationHandler;
/*     */         //   8: invokevirtual 22	org/talend/utils/thread/TimeoutOperationHandler:internalRun	()Ljava/lang/Object;
/*     */         //   11: invokestatic 28	org/talend/utils/thread/TimeoutOperationHandler:access$0	(Lorg/talend/utils/thread/TimeoutOperationHandler;Ljava/lang/Object;)V
/*     */         //   14: goto +43 -> 57
/*     */         //   17: astore_1
/*     */         //   18: invokestatic 32	org/talend/utils/thread/TimeoutOperationHandler:access$1	()Lorg/apache/log4j/Logger;
/*     */         //   21: aload_1
/*     */         //   22: invokevirtual 36	java/lang/Throwable:getMessage	()Ljava/lang/String;
/*     */         //   25: aload_1
/*     */         //   26: invokevirtual 42	org/apache/log4j/Logger:error	(Ljava/lang/Object;Ljava/lang/Throwable;)V
/*     */         //   29: aload_0
/*     */         //   30: getfield 12	org/talend/utils/thread/TimeoutOperationHandler$1:this$0	Lorg/talend/utils/thread/TimeoutOperationHandler;
/*     */         //   33: aload_1
/*     */         //   34: invokestatic 48	org/talend/utils/thread/TimeoutOperationHandler:access$2	(Lorg/talend/utils/thread/TimeoutOperationHandler;Ljava/lang/Throwable;)V
/*     */         //   37: aload_0
/*     */         //   38: getfield 12	org/talend/utils/thread/TimeoutOperationHandler$1:this$0	Lorg/talend/utils/thread/TimeoutOperationHandler;
/*     */         //   41: invokevirtual 52	org/talend/utils/thread/TimeoutOperationHandler:finalizeOperation	()V
/*     */         //   44: goto +20 -> 64
/*     */         //   47: astore_2
/*     */         //   48: aload_0
/*     */         //   49: getfield 12	org/talend/utils/thread/TimeoutOperationHandler$1:this$0	Lorg/talend/utils/thread/TimeoutOperationHandler;
/*     */         //   52: invokevirtual 52	org/talend/utils/thread/TimeoutOperationHandler:finalizeOperation	()V
/*     */         //   55: aload_2
/*     */         //   56: athrow
/*     */         //   57: aload_0
/*     */         //   58: getfield 12	org/talend/utils/thread/TimeoutOperationHandler$1:this$0	Lorg/talend/utils/thread/TimeoutOperationHandler;
/*     */         //   61: invokevirtual 52	org/talend/utils/thread/TimeoutOperationHandler:finalizeOperation	()V
/*     */         //   64: return
/*     */         // Line number table:
/*     */         //   Java source line #59	-> byte code offset #0
/*     */         //   Java source line #60	-> byte code offset #14
/*     */         //   Java source line #61	-> byte code offset #18
/*     */         //   Java source line #62	-> byte code offset #29
/*     */         //   Java source line #64	-> byte code offset #37
/*     */         //   Java source line #63	-> byte code offset #47
/*     */         //   Java source line #64	-> byte code offset #48
/*     */         //   Java source line #65	-> byte code offset #55
/*     */         //   Java source line #64	-> byte code offset #57
/*     */         //   Java source line #66	-> byte code offset #64
/*     */         // Local variable table:
/*     */         //   start	length	slot	name	signature
/*     */         //   0	65	0	this	1
/*     */         //   17	17	1	t	Throwable
/*     */         //   47	9	2	localObject	Object
/*     */         // Exception table:
/*     */         //   from	to	target	type
/*     */         //   0	14	17	java/lang/Throwable
/*     */         //   0	37	47	finally
/*     */       }
/*  69 */     };
/*  70 */     Thread thread = null;
/*  71 */     if (this.labelOperation != null) {
/*  72 */       thread = new Thread(runnable, this.labelOperation);
/*     */     } else {
/*  74 */       thread = new Thread(runnable);
/*     */     }
/*  76 */     thread.start();
/*     */     
/*  78 */     long timeStart = System.currentTimeMillis();
/*     */     
/*     */     for (;;)
/*     */     {
/*  82 */       if (System.currentTimeMillis() - timeStart > this.timeout) {
/*  83 */         this.timeoutReached = true;
/*     */       }
/*     */       
/*  86 */       if ((this.timeoutReached) || (!thread.isAlive())) {
/*     */         break;
/*     */       }
/*  89 */       ThreadUtils.waitTimeBool(50L);
/*     */     }
/*     */   }
/*     */   
/*     */   protected R internalRun() {
/*  94 */     return (R)run();
/*     */   }
/*     */   
/*     */   public boolean hasValidResultOperation(R result) {
/*  98 */     return result != null;
/*     */   }
/*     */   
/*     */   public abstract R run();
/*     */   
/*     */   public R getResult() {
/* 104 */     return (R)this.result;
/*     */   }
/*     */   
/*     */   public Throwable getOperationError() {
/* 108 */     return this.operationError;
/*     */   }
/*     */   
/*     */ 
/*     */   public void finalizeOperation() {}
/*     */   
/*     */   public void setLabelOperation(String labelOperation)
/*     */   {
/* 116 */     this.labelOperation = labelOperation;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\thread\TimeoutOperationHandler.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */