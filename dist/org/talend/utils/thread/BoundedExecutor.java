/*    */ package org.talend.utils.thread;
/*    */ 
/*    */ import java.util.concurrent.ExecutorService;
/*    */ import java.util.concurrent.Executors;
/*    */ import java.util.concurrent.LinkedBlockingQueue;
/*    */ import java.util.concurrent.RejectedExecutionException;
/*    */ import java.util.concurrent.Semaphore;
/*    */ import java.util.concurrent.ThreadFactory;
/*    */ import java.util.concurrent.ThreadPoolExecutor;
/*    */ import java.util.concurrent.TimeUnit;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class BoundedExecutor
/*    */ {
/*    */   private final ExecutorService exec;
/*    */   private final Semaphore semaphore;
/*    */   
/*    */   public BoundedExecutor(ExecutorService exec, int bound)
/*    */   {
/* 35 */     this.exec = exec;
/* 36 */     this.semaphore = new Semaphore(bound);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public BoundedExecutor(String poolName, int bound)
/*    */   {
/* 47 */     this.exec = intializeBoundedPool(poolName, bound);
/* 48 */     this.semaphore = new Semaphore(bound);
/*    */   }
/*    */   
/*    */   protected ThreadPoolExecutor intializeBoundedPool(final String poolName, int poolSize) {
/* 52 */     LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue();
/* 53 */     ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(poolSize, poolSize, 0L, TimeUnit.SECONDS, workQueue, 
/* 54 */       new ThreadFactory()
/*    */       {
/* 56 */         ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();
/*    */         
/*    */         public Thread newThread(Runnable r) {
/* 59 */           Thread newThread = this.defaultThreadFactory.newThread(r);
/* 60 */           newThread.setName(poolName + "_" + newThread.getName());
/* 61 */           return newThread;
/*    */         }
/*    */         
/* 64 */       });
/* 65 */     return threadPoolExecutor;
/*    */   }
/*    */   
/*    */   public void submitTask(final Runnable command) throws InterruptedException, RejectedExecutionException {
/* 69 */     this.semaphore.acquire();
/*    */     try {
/* 71 */       this.exec.execute(new Runnable()
/*    */       {
/*    */         /* Error */
/*    */         public void run()
/*    */         {
/*    */           // Byte code:
/*    */           //   0: aload_0
/*    */           //   1: getfield 16	org/talend/utils/thread/BoundedExecutor$2:val$command	Ljava/lang/Runnable;
/*    */           //   4: invokeinterface 26 1 0
/*    */           //   9: goto +16 -> 25
/*    */           //   12: astore_1
/*    */           //   13: aload_0
/*    */           //   14: getfield 14	org/talend/utils/thread/BoundedExecutor$2:this$0	Lorg/talend/utils/thread/BoundedExecutor;
/*    */           //   17: invokestatic 28	org/talend/utils/thread/BoundedExecutor:access$0	(Lorg/talend/utils/thread/BoundedExecutor;)Ljava/util/concurrent/Semaphore;
/*    */           //   20: invokevirtual 34	java/util/concurrent/Semaphore:release	()V
/*    */           //   23: aload_1
/*    */           //   24: athrow
/*    */           //   25: aload_0
/*    */           //   26: getfield 14	org/talend/utils/thread/BoundedExecutor$2:this$0	Lorg/talend/utils/thread/BoundedExecutor;
/*    */           //   29: invokestatic 28	org/talend/utils/thread/BoundedExecutor:access$0	(Lorg/talend/utils/thread/BoundedExecutor;)Ljava/util/concurrent/Semaphore;
/*    */           //   32: invokevirtual 34	java/util/concurrent/Semaphore:release	()V
/*    */           //   35: return
/*    */           // Line number table:
/*    */           //   Java source line #75	-> byte code offset #0
/*    */           //   Java source line #76	-> byte code offset #9
/*    */           //   Java source line #77	-> byte code offset #13
/*    */           //   Java source line #78	-> byte code offset #23
/*    */           //   Java source line #77	-> byte code offset #25
/*    */           //   Java source line #79	-> byte code offset #35
/*    */           // Local variable table:
/*    */           //   start	length	slot	name	signature
/*    */           //   0	36	0	this	2
/*    */           //   12	12	1	localObject	Object
/*    */           // Exception table:
/*    */           //   from	to	target	type
/*    */           //   0	12	12	finally
/*    */         }
/*    */       });
/*    */     }
/*    */     catch (RejectedExecutionException e)
/*    */     {
/* 82 */       this.semaphore.release();
/* 83 */       throw e;
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public ExecutorService getExecutorService()
/*    */   {
/* 93 */     return this.exec;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\thread\BoundedExecutor.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */