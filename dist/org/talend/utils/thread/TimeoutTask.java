/*     */ package org.talend.utils.thread;
/*     */ 
/*     */ import java.util.concurrent.Callable;
/*     */ import java.util.concurrent.ExecutionException;
/*     */ import java.util.concurrent.ExecutorService;
/*     */ import java.util.concurrent.Executors;
/*     */ import java.util.concurrent.FutureTask;
/*     */ import java.util.concurrent.ThreadFactory;
/*     */ import java.util.concurrent.TimeUnit;
/*     */ import java.util.concurrent.TimeoutException;
/*     */ import java.util.concurrent.atomic.AtomicInteger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TimeoutTask<R>
/*     */ {
/*  31 */   private AtomicInteger counter = new AtomicInteger();
/*     */   
/*     */   private ExecutorService threadPool;
/*     */   
/*     */   public TimeoutTask()
/*     */   {
/*  37 */     this(null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public TimeoutTask(final String taskName)
/*     */   {
/*  45 */     if (taskName != null) {
/*  46 */       ThreadFactory threadFactory = new ThreadFactory()
/*     */       {
/*  48 */         ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();
/*     */         
/*     */         public Thread newThread(Runnable r) {
/*  51 */           Thread newThread = this.defaultThreadFactory.newThread(r);
/*  52 */           String threadName = TimeoutTask.this.getClass().getSimpleName() + (taskName != null ? "_" + taskName : "") + 
/*  53 */             "_" + newThread.getName();
/*  54 */           newThread.setName(threadName);
/*  55 */           return newThread;
/*     */         }
/*     */         
/*  58 */       };
/*  59 */       this.threadPool = Executors.newCachedThreadPool(threadFactory);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   /**
/*     */    * @deprecated
/*     */    */
/*     */   public FutureTask<R> run(Callable<R> task, long timeoutMs, boolean cancelTaskIfTimeout, boolean interruptIfTimeout)
/*     */     throws TimeoutException, InterruptedException, ExecutionException
/*     */   {
/*  81 */     return run(task, timeoutMs, cancelTaskIfTimeout);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public FutureTask<R> run(Callable<R> task, long timeoutMs, boolean cancelTaskIfTimeout)
/*     */     throws TimeoutException, InterruptedException, ExecutionException
/*     */   {
/*  97 */     FutureTask<R> futureTask = new FutureTask(task);
/*  98 */     run(futureTask, timeoutMs, cancelTaskIfTimeout);
/*  99 */     return futureTask;
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   private R run(FutureTask<R> futureTask, long timeoutMs, boolean cancelAndInterruptTaskIfTimeout)
/*     */     throws InterruptedException, ExecutionException, TimeoutException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: aload_0
/*     */     //   1: getfield 39	org/talend/utils/thread/TimeoutTask:threadPool	Ljava/util/concurrent/ExecutorService;
/*     */     //   4: aload_1
/*     */     //   5: invokeinterface 84 2 0
/*     */     //   10: aload_1
/*     */     //   11: lload_2
/*     */     //   12: getstatic 90	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
/*     */     //   15: invokevirtual 96	java/util/concurrent/FutureTask:get	(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
/*     */     //   18: pop
/*     */     //   19: goto +19 -> 38
/*     */     //   22: astore 5
/*     */     //   24: iload 4
/*     */     //   26: ifeq +9 -> 35
/*     */     //   29: aload_1
/*     */     //   30: iconst_1
/*     */     //   31: invokevirtual 100	java/util/concurrent/FutureTask:cancel	(Z)Z
/*     */     //   34: pop
/*     */     //   35: aload 5
/*     */     //   37: athrow
/*     */     //   38: iload 4
/*     */     //   40: ifeq +9 -> 49
/*     */     //   43: aload_1
/*     */     //   44: iconst_1
/*     */     //   45: invokevirtual 100	java/util/concurrent/FutureTask:cancel	(Z)Z
/*     */     //   48: pop
/*     */     //   49: aload_1
/*     */     //   50: invokevirtual 104	java/util/concurrent/FutureTask:get	()Ljava/lang/Object;
/*     */     //   53: areturn
/*     */     // Line number table:
/*     */     //   Java source line #115	-> byte code offset #0
/*     */     //   Java source line #117	-> byte code offset #10
/*     */     //   Java source line #118	-> byte code offset #19
/*     */     //   Java source line #119	-> byte code offset #24
/*     */     //   Java source line #120	-> byte code offset #29
/*     */     //   Java source line #122	-> byte code offset #35
/*     */     //   Java source line #119	-> byte code offset #38
/*     */     //   Java source line #120	-> byte code offset #43
/*     */     //   Java source line #123	-> byte code offset #49
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	54	0	this	TimeoutTask<R>
/*     */     //   0	54	1	futureTask	FutureTask<R>
/*     */     //   0	54	2	timeoutMs	long
/*     */     //   0	54	4	cancelAndInterruptTaskIfTimeout	boolean
/*     */     //   22	14	5	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   10	22	22	finally
/*     */   }
/*     */   
/*     */   public void release()
/*     */   {
/* 127 */     this.threadPool.shutdown();
/*     */     try
/*     */     {
/* 130 */       if (!this.threadPool.awaitTermination(10L, TimeUnit.SECONDS)) {
/* 131 */         this.threadPool.shutdownNow();
/*     */         
/* 133 */         this.threadPool.awaitTermination(10L, TimeUnit.SECONDS);
/*     */       }
/*     */     }
/*     */     catch (InterruptedException localInterruptedException) {
/* 137 */       this.threadPool.shutdownNow();
/*     */       
/* 139 */       Thread.currentThread().interrupt();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\thread\TimeoutTask.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */