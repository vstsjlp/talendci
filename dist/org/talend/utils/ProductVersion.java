/*     */ package org.talend.utils;
/*     */ 
/*     */ import java.util.regex.Matcher;
/*     */ import java.util.regex.Pattern;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ProductVersion
/*     */   implements Comparable<ProductVersion>
/*     */ {
/*  24 */   private static final Pattern THREE_DIGIT_PATTERN = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+).*");
/*     */   
/*  26 */   private static final Pattern EXTENDED_PATTERN = Pattern.compile("(\\d+)\\.(\\d+)(?:\\.(\\d+))?.*");
/*     */   
/*     */   private int major;
/*     */   
/*     */   private int minor;
/*     */   
/*  32 */   private int micro = 0;
/*     */   
/*  34 */   private boolean setMicro = false;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ProductVersion(int major, int minor, int micro)
/*     */   {
/*  45 */     this.major = major;
/*  46 */     this.minor = minor;
/*  47 */     this.micro = micro;
/*  48 */     this.setMicro = true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ProductVersion(int major, int minor)
/*     */   {
/*  60 */     this.major = major;
/*  61 */     this.minor = minor;
/*  62 */     this.setMicro = false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ProductVersion fromString(String version, boolean extendedVersion)
/*     */   {
/*  73 */     if (!extendedVersion) {
/*  74 */       return fromString(version);
/*     */     }
/*  76 */     Matcher matcher = EXTENDED_PATTERN.matcher(version);
/*  77 */     if (matcher.find()) {
/*  78 */       int major = Integer.parseInt(matcher.group(1));
/*  79 */       int minor = Integer.parseInt(matcher.group(2));
/*  80 */       String microStr = matcher.group(3);
/*  81 */       if (microStr != null) {
/*  82 */         int micro = Integer.parseInt(microStr);
/*  83 */         return new ProductVersion(major, minor, micro);
/*     */       }
/*  85 */       return new ProductVersion(major, minor);
/*     */     }
/*     */     
/*  88 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ProductVersion fromString(String version)
/*     */   {
/*  98 */     Matcher matcher = THREE_DIGIT_PATTERN.matcher(version);
/*  99 */     if (matcher.matches()) {
/* 100 */       int major = Integer.parseInt(matcher.group(1));
/* 101 */       int minor = Integer.parseInt(matcher.group(2));
/* 102 */       String microStr = matcher.group(3);
/* 103 */       int micro = Integer.parseInt(microStr);
/* 104 */       return new ProductVersion(major, minor, micro);
/*     */     }
/* 106 */     return null;
/*     */   }
/*     */   
/*     */   public int compareTo(ProductVersion other) {
/* 110 */     int diff = this.major - other.major;
/* 111 */     if (diff != 0) {
/* 112 */       return diff;
/*     */     }
/* 114 */     diff = this.minor - other.minor;
/* 115 */     if (diff != 0) {
/* 116 */       return diff;
/*     */     }
/* 118 */     if ((this.setMicro) && (other.setMicro)) {
/* 119 */       diff = this.micro - other.micro;
/* 120 */       if (diff != 0) {
/* 121 */         return diff;
/*     */       }
/*     */     }
/* 124 */     return 0;
/*     */   }
/*     */   
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 130 */     int result = 1;
/* 131 */     result = 31 * result + this.major;
/* 132 */     result = 31 * result + this.micro;
/* 133 */     result = 31 * result + this.minor;
/* 134 */     return result;
/*     */   }
/*     */   
/*     */   public boolean equals(Object obj)
/*     */   {
/* 139 */     if (this == obj) {
/* 140 */       return true;
/*     */     }
/* 142 */     if (obj == null) {
/* 143 */       return false;
/*     */     }
/* 145 */     if (getClass() != obj.getClass()) {
/* 146 */       return false;
/*     */     }
/* 148 */     ProductVersion other = (ProductVersion)obj;
/* 149 */     if (this.major != other.major) {
/* 150 */       return false;
/*     */     }
/* 152 */     if (this.minor != other.minor) {
/* 153 */       return false;
/*     */     }
/* 155 */     if (this.micro != other.micro) {
/* 156 */       return false;
/*     */     }
/*     */     
/* 159 */     return true;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 164 */     StringBuilder stringBuilder = new StringBuilder();
/* 165 */     stringBuilder.append(this.major);
/* 166 */     stringBuilder.append(".");
/* 167 */     stringBuilder.append(this.minor);
/* 168 */     if (this.setMicro) {
/* 169 */       stringBuilder.append(".");
/* 170 */       stringBuilder.append(this.micro);
/*     */     }
/* 172 */     return stringBuilder.toString();
/*     */   }
/*     */   
/*     */   public int getMajor() {
/* 176 */     return this.major;
/*     */   }
/*     */   
/*     */   public int getMicro() {
/* 180 */     return this.micro;
/*     */   }
/*     */   
/*     */   public int getMinor() {
/* 184 */     return this.minor;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\ProductVersion.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */