/*     */ package org.talend.utils.properties;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.Enumeration;
/*     */ import java.util.HashSet;
/*     */ import java.util.List;
/*     */ import java.util.Properties;
/*     */ import java.util.Set;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.talend.utils.string.StringUtilities;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TypedProperties
/*     */   extends Properties
/*     */ {
/*  32 */   private static Logger log = Logger.getLogger(TypedProperties.class);
/*     */   
/*     */ 
/*     */ 
/*     */   private static final long serialVersionUID = 2581080352306726049L;
/*     */   
/*     */ 
/*     */ 
/*     */   public static final String DEFAULT_DELIMITERS = ",";
/*     */   
/*     */ 
/*     */ 
/*  44 */   Set<String> anomaliesAlreadyLogged = new HashSet();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TypedProperties() {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TypedProperties(Properties defaults)
/*     */   {
/*  59 */     super(defaults);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> boolean getBooleanValue(Class<T> clazz, String shortKey, boolean defaultValue)
/*     */   {
/*  72 */     return getBooleanValue(buildKey(clazz, shortKey), defaultValue);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean getBooleanValue(String key, boolean defaultValue)
/*     */   {
/*  83 */     String value = getProperty(key);
/*  84 */     if (value == null) {
/*  85 */       logWarningPropertyNotFound(key, Boolean.valueOf(defaultValue));
/*  86 */       return defaultValue;
/*     */     }
/*  88 */     return Boolean.parseBoolean(value.trim());
/*     */   }
/*     */   
/*     */   private void logWarningPropertyNotFound(String notFoundKey, Object defaultValue) {
/*  92 */     if (!this.anomaliesAlreadyLogged.contains(notFoundKey)) {
/*  93 */       this.anomaliesAlreadyLogged.add(notFoundKey);
/*  94 */       log.warn("!!! PROPERTY NOT FOUND !!!: the key '" + notFoundKey + 
/*  95 */         "' can't be found in the JobServer properties file, the default value '" + defaultValue + 
/*  96 */         "' will be used, please be sure this is not an anomaly.");
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean getBooleanValue(String key)
/*     */   {
/* 107 */     String value = getProperty(key);
/* 108 */     if (value == null) {
/* 109 */       return null;
/*     */     }
/* 111 */     return Boolean.valueOf(Boolean.parseBoolean(value.trim()));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> long getLongValue(Class<T> clazz, String shortKey, long defaultValue)
/*     */   {
/* 126 */     return getLongValue(buildKey(clazz, shortKey), defaultValue);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> double getDoubleValue(Class<T> clazz, String shortKey, double defaultValue)
/*     */   {
/* 139 */     return getDoubleValue(buildKey(clazz, shortKey), defaultValue);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public double getDoubleValue(String key, double defaultValue)
/*     */   {
/* 150 */     String value = getProperty(key);
/* 151 */     if (value == null) {
/* 152 */       logWarningPropertyNotFound(key, Double.valueOf(defaultValue));
/* 153 */       return defaultValue;
/*     */     }
/* 155 */     return Double.parseDouble(value.trim());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> int getIntValue(Class<T> clazz, String shortKey, int defaultValue)
/*     */   {
/* 170 */     return getIntValue(buildKey(clazz, shortKey), defaultValue);
/*     */   }
/*     */   
/*     */   public <T> String getStringValue(Class<T> clazz, String shortKey, String defaultValue) {
/* 174 */     return getStringValue(buildKey(clazz, shortKey), defaultValue);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String getStringValue(String key, String defaultValue)
/*     */   {
/* 185 */     String value = getProperty(key);
/* 186 */     if (value == null) {
/* 187 */       logWarningPropertyNotFound(key, defaultValue);
/* 188 */       return defaultValue;
/*     */     }
/* 190 */     return value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public long getLongValue(String key, long defaultValue)
/*     */   {
/* 202 */     String value = getProperty(key);
/* 203 */     if (value == null) {
/* 204 */       logWarningPropertyNotFound(key, Long.valueOf(defaultValue));
/* 205 */       return defaultValue;
/*     */     }
/* 207 */     return Long.parseLong(value.trim());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int getIntValue(String key, int defaultValue)
/*     */   {
/* 219 */     String value = getProperty(key);
/* 220 */     if (value == null) {
/* 221 */       logWarningPropertyNotFound(key, Integer.valueOf(defaultValue));
/* 222 */       return defaultValue;
/*     */     }
/* 224 */     return Integer.parseInt(value.trim());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getValues(String key, List<String> defaultValues, String delimiters)
/*     */   {
/* 236 */     String value = getProperty(key);
/* 237 */     if (value == null) {
/* 238 */       logWarningPropertyNotFound(key, String.valueOf(defaultValues));
/* 239 */       return defaultValues;
/*     */     }
/* 241 */     return StringUtilities.tokenize(value, delimiters);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> List<String> getValuesWithoutWarning(Class<T> clazz, String shortKey, List<String> defaultValues)
/*     */   {
/* 253 */     String key = buildKey(clazz, shortKey);
/* 254 */     String value = getProperty(key);
/* 255 */     if (value == null) {
/* 256 */       return defaultValues;
/*     */     }
/* 258 */     return StringUtilities.tokenize(value, ",");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<String> getValues(String key, List<String> defaultValues)
/*     */   {
/* 269 */     return getValues(key, defaultValues, ",");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> List<String> getValues(Class<T> clazz, String shortKey, List<String> defaultValues)
/*     */   {
/* 282 */     return getValues(buildKey(clazz, shortKey), defaultValues, ",");
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public <T> List<String> getValues(Class<T> clazz, String shortKey, List<String> defaultValues, String delimiters)
/*     */   {
/* 296 */     return getValues(buildKey(clazz, shortKey), defaultValues, delimiters);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private <T> String buildKey(Class<T> clazz, String shortKey)
/*     */   {
/* 308 */     if (clazz == null) {
/* 309 */       return shortKey;
/*     */     }
/* 311 */     return clazz.getName() + "." + shortKey;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String logProperties(boolean all, boolean main)
/*     */   {
/* 326 */     StringBuilder builder = new StringBuilder();
/* 327 */     if (all) {
/* 328 */       builder.append("PROPERTIES: List of input properties:\n");
/* 329 */       Enumeration<?> names = propertyNames();
/*     */       
/* 331 */       ArrayList<String> namesList = new ArrayList();
/*     */       
/* 333 */       while (names.hasMoreElements()) {
/* 334 */         String key = (String)names.nextElement();
/* 335 */         namesList.add(key);
/*     */       }
/* 337 */       Collections.sort(namesList);
/*     */       
/* 339 */       int namesListListSize = namesList.size();
/* 340 */       for (int i = 0; i < namesListListSize; i++) {
/* 341 */         String key = (String)namesList.get(i);
/* 342 */         String property = getProperty(key);
/* 343 */         builder.append(key + "=" + property + "\n");
/*     */       }
/* 345 */       builder.append("PROPERTIES: End of list.\n");
/*     */     }
/* 347 */     if ((!all) && (main)) {
/* 348 */       builder.append("TYPED PROPERTIES=" + toString());
/*     */     }
/* 350 */     return builder.toString();
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\properties\TypedProperties.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */