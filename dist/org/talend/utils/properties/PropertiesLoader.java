/*     */ package org.talend.utils.properties;
/*     */ 
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.net.URISyntaxException;
/*     */ import java.util.Dictionary;
/*     */ import java.util.Enumeration;
/*     */ import java.util.Properties;
/*     */ import org.apache.log4j.Logger;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class PropertiesLoader
/*     */ {
/*  33 */   private static Logger log = Logger.getLogger(PropertiesLoader.class);
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static final String MY_PROP_KEY = "talend_props";
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  44 */   private static final String PROPERTIES_FILENAME = System.getProperty("talend_props");
/*     */   
/*     */   private static final String USAGE = "Try with -Dtalend_props=file.properties with file.properties a relative or absolute file path.";
/*     */   
/*     */   private static final String QUOTE = "'";
/*     */   
/*     */   private static TypedProperties curProp;
/*     */   
/*     */   private static Properties convertToProperties(Dictionary config)
/*     */   {
/*  54 */     Enumeration keys = config.keys();
/*  55 */     Properties props = new Properties();
/*  56 */     while (keys.hasMoreElements()) {
/*  57 */       String key = (String)keys.nextElement();
/*  58 */       props.setProperty(key, (String)config.get(key));
/*     */     }
/*  60 */     return props;
/*     */   }
/*     */   
/*     */   public static void setConfig(Dictionary config) {
/*  64 */     Properties prop = convertToProperties(config);
/*  65 */     curProp = new TypedProperties(prop);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static String quotedAbsolutePath(File in)
/*     */   {
/*  75 */     return "'" + in.getAbsolutePath() + "'";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static synchronized TypedProperties getProperties()
/*     */   {
/*  89 */     if (curProp == null) {
/*  90 */       curProp = initialize();
/*     */     }
/*  92 */     return curProp;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static synchronized TypedProperties getProperties(Class<?> clazz, String propertiesFilename)
/*     */   {
/* 104 */     TypedProperties prop = new TypedProperties();
/*     */     
/* 106 */     InputStream inStream = clazz.getClassLoader().getResourceAsStream(propertiesFilename);
/* 107 */     if (inStream == null)
/*     */     {
/* 109 */       inStream = clazz.getResourceAsStream(propertiesFilename);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 114 */     if (inStream == null) {
/* 115 */       log.error("Properties file not found: " + propertiesFilename);
/*     */     } else {
/*     */       try {
/* 118 */         prop.load(inStream);
/*     */       } catch (IOException e) {
/* 120 */         log.error("Properties file " + propertiesFilename + " not found: " + e.getMessage());
/*     */       }
/*     */     }
/* 123 */     return prop;
/*     */   }
/*     */   
/*     */   public static synchronized void setProperties(Class<?> clazz, String propertiesFilename, String key, String oldValue, String newValue)
/*     */   {
/* 128 */     if (oldValue.equals(newValue)) {
/* 129 */       return;
/*     */     }
/*     */     try {
/* 132 */       PropertiesReloader.setProperties(clazz, propertiesFilename, key, oldValue, newValue);
/*     */     }
/*     */     catch (IOException e) {
/* 135 */       e.printStackTrace();
/*     */     }
/*     */     catch (URISyntaxException e) {
/* 138 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static synchronized TypedProperties initialize()
/*     */   {
/* 148 */     Properties sysProp = System.getProperties();
/* 149 */     TypedProperties prop = new TypedProperties(sysProp);
/* 150 */     if ((PROPERTIES_FILENAME == null) || (PROPERTIES_FILENAME.length() == 0)) {
/* 151 */       log.warn("Warning: no properties file name given in JVM arguments.Try with -Dtalend_props=file.properties with file.properties a relative or absolute file path.");
/*     */     } else {
/* 153 */       initialize(prop, new File(PROPERTIES_FILENAME));
/*     */     }
/* 155 */     return prop;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static synchronized boolean initialize(TypedProperties prop, File in)
/*     */   {
/* 166 */     boolean ok = true;
/*     */     try {
/* 168 */       if (in.exists()) {
/* 169 */         log.info("Loading Properties from file: " + quotedAbsolutePath(in));
/*     */       } else {
/* 171 */         ok = false;
/* 172 */         log.info("Given file for properties does not exist: " + quotedAbsolutePath(in));
/*     */       }
/* 174 */       String filename = in.getAbsolutePath();
/* 175 */       ok = (ok) && (loadPropertiesLow(filename, prop));
/* 176 */       if (!ok) {
/* 177 */         log.warn("Warning: Problem when loading properties from file " + filename);
/*     */       }
/*     */     } catch (Exception e) {
/* 180 */       ok = false;
/* 181 */       log.error("Try with -Dtalend_props=file.properties with file.properties a relative or absolute file path.");
/* 182 */       log.error("ERROR: could not load properties file=" + quoted(in.toString()), e);
/*     */     }
/*     */     
/* 185 */     return ok;
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   private static boolean loadPropertiesLow(String filename, TypedProperties prop)
/*     */     throws Exception
/*     */   {
/*     */     // Byte code:
/*     */     //   0: iconst_1
/*     */     //   1: istore_2
/*     */     //   2: aconst_null
/*     */     //   3: astore_3
/*     */     //   4: new 239	java/io/FileInputStream
/*     */     //   7: dup
/*     */     //   8: aload_0
/*     */     //   9: invokespecial 241	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
/*     */     //   12: astore_3
/*     */     //   13: aload_3
/*     */     //   14: ifnonnull +30 -> 44
/*     */     //   17: new 232	java/lang/Exception
/*     */     //   20: dup
/*     */     //   21: new 96	java/lang/StringBuilder
/*     */     //   24: dup
/*     */     //   25: ldc -14
/*     */     //   27: invokespecial 98	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
/*     */     //   30: aload_0
/*     */     //   31: invokestatic 226	org/talend/utils/properties/PropertiesLoader:quoted	(Ljava/lang/String;)Ljava/lang/String;
/*     */     //   34: invokevirtual 107	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
/*     */     //   37: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*     */     //   40: invokespecial 244	java/lang/Exception:<init>	(Ljava/lang/String;)V
/*     */     //   43: athrow
/*     */     //   44: aload_1
/*     */     //   45: aload_3
/*     */     //   46: invokevirtual 144	org/talend/utils/properties/TypedProperties:load	(Ljava/io/InputStream;)V
/*     */     //   49: aload_3
/*     */     //   50: invokevirtual 245	java/io/FileInputStream:close	()V
/*     */     //   53: aconst_null
/*     */     //   54: astore_3
/*     */     //   55: goto +35 -> 90
/*     */     //   58: astore 4
/*     */     //   60: iconst_0
/*     */     //   61: istore_2
/*     */     //   62: aload_3
/*     */     //   63: ifnull +11 -> 74
/*     */     //   66: aload_3
/*     */     //   67: invokevirtual 245	java/io/FileInputStream:close	()V
/*     */     //   70: goto +4 -> 74
/*     */     //   73: pop
/*     */     //   74: aload 4
/*     */     //   76: athrow
/*     */     //   77: astore 5
/*     */     //   79: aload_3
/*     */     //   80: ifnull +7 -> 87
/*     */     //   83: aload_3
/*     */     //   84: invokevirtual 245	java/io/FileInputStream:close	()V
/*     */     //   87: aload 5
/*     */     //   89: athrow
/*     */     //   90: aload_3
/*     */     //   91: ifnull +7 -> 98
/*     */     //   94: aload_3
/*     */     //   95: invokevirtual 245	java/io/FileInputStream:close	()V
/*     */     //   98: iload_2
/*     */     //   99: ireturn
/*     */     // Line number table:
/*     */     //   Java source line #195	-> byte code offset #0
/*     */     //   Java source line #196	-> byte code offset #2
/*     */     //   Java source line #198	-> byte code offset #4
/*     */     //   Java source line #200	-> byte code offset #13
/*     */     //   Java source line #201	-> byte code offset #17
/*     */     //   Java source line #203	-> byte code offset #44
/*     */     //   Java source line #204	-> byte code offset #49
/*     */     //   Java source line #205	-> byte code offset #53
/*     */     //   Java source line #206	-> byte code offset #55
/*     */     //   Java source line #207	-> byte code offset #60
/*     */     //   Java source line #208	-> byte code offset #62
/*     */     //   Java source line #210	-> byte code offset #66
/*     */     //   Java source line #211	-> byte code offset #70
/*     */     //   Java source line #215	-> byte code offset #74
/*     */     //   Java source line #216	-> byte code offset #77
/*     */     //   Java source line #217	-> byte code offset #79
/*     */     //   Java source line #218	-> byte code offset #83
/*     */     //   Java source line #220	-> byte code offset #87
/*     */     //   Java source line #217	-> byte code offset #90
/*     */     //   Java source line #218	-> byte code offset #94
/*     */     //   Java source line #221	-> byte code offset #98
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	100	0	filename	String
/*     */     //   0	100	1	prop	TypedProperties
/*     */     //   1	98	2	ok	boolean
/*     */     //   3	92	3	is	java.io.FileInputStream
/*     */     //   58	17	4	e	Exception
/*     */     //   77	11	5	localObject	Object
/*     */     //   73	1	6	localException1	Exception
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   4	55	58	java/lang/Exception
/*     */     //   66	70	73	java/lang/Exception
/*     */     //   4	77	77	finally
/*     */   }
/*     */   
/*     */   private static String quoted(String in)
/*     */   {
/* 225 */     return "'" + in + "'";
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\properties\PropertiesLoader.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */