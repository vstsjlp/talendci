/*    */ package org.talend.utils.properties;
/*    */ 
/*    */ import java.io.IOException;
/*    */ import java.net.URI;
/*    */ import java.net.URISyntaxException;
/*    */ import java.net.URL;
/*    */ import org.talend.utils.files.FileUtils;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PropertiesReloader
/*    */ {
/*    */   public static synchronized void changeProperties(String fileName, String key, String oldValue, String newValue)
/*    */     throws IOException, URISyntaxException
/*    */   {
/* 28 */     FileUtils.replaceInFile(fileName, key + "=" + oldValue, key + "=" + newValue);
/*    */   }
/*    */   
/*    */   public static synchronized void setProperties(Class<?> clazz, String propertiesFilename, String key, String oldValue, String newValue) throws IOException, URISyntaxException
/*    */   {
/* 33 */     URL resource = clazz.getClassLoader().getResource(propertiesFilename);
/* 34 */     changeProperties(resource.toURI().getPath(), key, oldValue, newValue);
/*    */   }
/*    */   
/*    */   public static void main(String[] args) {
/* 38 */     String key = "database.driver";
/* 39 */     String newValue = "tagada";
/* 40 */     String oldValue = "org.gjt.mm.mysql.Driver";
/*    */     try
/*    */     {
/* 43 */       new PropertiesReloader();
/* 44 */       changeProperties(
/* 45 */         "C:/Program Files/tomcat/apache-tomcat-5.5.26/apache-tomcat-5.5.26/webapps/org.talend.administrator/WEB-INF/classes/database.properties", 
/* 46 */         key, oldValue, newValue);
/*    */     }
/*    */     catch (IOException e) {
/* 49 */       e.printStackTrace();
/*    */     }
/*    */     catch (URISyntaxException e) {
/* 52 */       e.printStackTrace();
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\properties\PropertiesReloader.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */