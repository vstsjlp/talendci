/*    */ package org.talend.utils;
/*    */ 
/*    */ import java.util.Random;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class IdGenerator
/*    */ {
/*    */   public static String getAsciiRandomString(int length)
/*    */   {
/* 33 */     Random random = new Random();
/* 34 */     int cnt = 0;
/* 35 */     StringBuffer buffer = new StringBuffer();
/*    */     
/* 37 */     int end = 123;
/* 38 */     int start = 32;
/* 39 */     while (cnt < length) {
/* 40 */       char ch = (char)(random.nextInt(end - start) + start);
/* 41 */       if (Character.isLetterOrDigit(ch)) {
/* 42 */         buffer.append(ch);
/* 43 */         cnt++;
/*    */       }
/*    */     }
/* 46 */     return buffer.toString();
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\IdGenerator.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */