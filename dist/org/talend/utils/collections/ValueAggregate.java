/*    */ package org.talend.utils.collections;
/*    */ 
/*    */ import java.util.HashMap;
/*    */ import java.util.HashSet;
/*    */ import java.util.Map;
/*    */ import java.util.Set;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public abstract class ValueAggregate<T, N>
/*    */ {
/* 32 */   protected Map<T, N[]> keyToVal = new HashMap();
/*    */   
/*    */ 
/*    */ 
/*    */ 
/* 37 */   protected Set<T> nullResults = new HashSet();
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public abstract void addValue(T paramT, N[] paramArrayOfN);
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String toString()
/*    */   {
/* 55 */     StringBuilder builder = new StringBuilder();
/* 56 */     for (T key : this.keyToVal.keySet()) {
/* 57 */       builder.append(key.toString()).append(": ");
/* 58 */       Object[] doubles = (Object[])this.keyToVal.get(key);
/* 59 */       Object[] arrayOfObject1; int j = (arrayOfObject1 = doubles).length; for (int i = 0; i < j; i++) { N d = arrayOfObject1[i];
/* 60 */         builder.append(d).append(" ");
/*    */       }
/* 62 */       builder.append('\n');
/*    */     }
/* 64 */     return builder.toString();
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\collections\ValueAggregate.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */