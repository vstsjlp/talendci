/*    */ package org.talend.utils.collections;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Tuple
/*    */ {
/*    */   private Object[] tuple;
/* 26 */   private int hashCode = 1;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public Tuple(Object[] objects)
/*    */   {
/* 35 */     assert (objects != null);
/* 36 */     this.tuple = objects;
/*    */     
/* 38 */     this.hashCode = computeHashCode();
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public Object[] getTuple()
/*    */   {
/* 47 */     return this.tuple;
/*    */   }
/*    */   
/*    */   private int computeHashCode() {
/* 51 */     return Arrays.hashCode(this.tuple);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public int hashCode()
/*    */   {
/* 61 */     return this.hashCode;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public boolean equals(Object obj)
/*    */   {
/* 71 */     if ((obj != null) && ((obj instanceof Tuple))) {
/* 72 */       Object[] otherTuple = ((Tuple)obj).tuple;
/* 73 */       return Arrays.equals(this.tuple, otherTuple);
/*    */     }
/* 75 */     return false;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\collections\Tuple.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */