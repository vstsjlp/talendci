/*     */ package org.talend.utils.collections;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class MultipleKey
/*     */   implements Comparable<MultipleKey>
/*     */ {
/*     */   private Object[] internalKey;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public MultipleKey(Object[] key, int nbElements)
/*     */   {
/*  26 */     this.internalKey = new Object[nbElements];
/*  27 */     for (int i = 0; i < nbElements; i++) {
/*  28 */       this.internalKey[i] = key[i];
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean equals(Object obj)
/*     */   {
/*  39 */     if (obj == null) {
/*  40 */       return false;
/*     */     }
/*  42 */     if (!(obj instanceof MultipleKey)) {
/*  43 */       return false;
/*     */     }
/*  45 */     MultipleKey other = (MultipleKey)obj;
/*  46 */     return compareTo(other) == 0;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/*  56 */     int hash = 0;
/*  57 */     Object[] arrayOfObject; int j = (arrayOfObject = this.internalKey).length; for (int i = 0; i < j; i++) { Object obj = arrayOfObject[i];
/*  58 */       if (obj == null) {
/*  59 */         obj = "null";
/*     */       }
/*  61 */       hash += 13 * obj.hashCode();
/*     */     }
/*  63 */     return hash;
/*     */   }
/*     */   
/*     */   public int compareTo(MultipleKey o) {
/*  67 */     if (o == null) {
/*  68 */       return -1;
/*     */     }
/*  70 */     int diff = this.internalKey.length - o.internalKey.length;
/*  71 */     if (diff != 0) {
/*  72 */       return diff;
/*     */     }
/*  74 */     for (int i = 0; i < this.internalKey.length; i++) {
/*  75 */       String internalObj = String.valueOf(this.internalKey[i]);
/*  76 */       String otherObj = String.valueOf(o.internalKey[i]);
/*  77 */       diff = internalObj.compareTo(otherObj);
/*  78 */       if (diff != 0) {
/*  79 */         return diff;
/*     */       }
/*     */     }
/*     */     
/*  83 */     return diff;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/*  93 */     StringBuilder builder = new StringBuilder();
/*  94 */     for (int i = 0; i < this.internalKey.length; i++) {
/*  95 */       Object obj = this.internalKey[i];
/*  96 */       builder.append(obj);
/*  97 */       if (i < this.internalKey.length - 1) {
/*  98 */         builder.append(" | ");
/*     */       }
/*     */     }
/* 101 */     return builder.toString();
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\collections\MultipleKey.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */