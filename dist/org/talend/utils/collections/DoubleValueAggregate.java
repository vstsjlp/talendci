/*    */ package org.talend.utils.collections;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ import java.util.Map;
/*    */ import java.util.Set;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class DoubleValueAggregate<T>
/*    */   extends ValueAggregate<T, Double>
/*    */ {
/*    */   public void addValue(T key, Double[] values)
/*    */   {
/* 31 */     Double[] doubles = (Double[])this.keyToVal.get(key);
/* 32 */     if (doubles == null) {
/* 33 */       doubles = new Double[values.length];
/* 34 */       Arrays.fill(doubles, Double.valueOf(0.0D));
/*    */     }
/*    */     
/* 37 */     for (int i = 0; i < values.length; i++) {
/* 38 */       Double d = values[i];
/* 39 */       if (d == null) {
/* 40 */         this.nullResults.add(key); return;
/*    */       }
/*    */       
/*    */ 
/* 44 */       int tmp64_62 = i; Double[] tmp64_61 = doubles;tmp64_61[tmp64_62] = Double.valueOf(tmp64_61[tmp64_62].doubleValue() + d.doubleValue());
/*    */     }
/* 46 */     this.keyToVal.put(key, doubles);
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\collections\DoubleValueAggregate.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */