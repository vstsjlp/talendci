/*     */ package org.talend.utils.collections;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collections;
/*     */ import java.util.Comparator;
/*     */ import java.util.Iterator;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class MapValueSorter
/*     */ {
/*     */   private static class AscByValueComparator
/*     */     implements Comparator<Object>
/*     */   {
/*     */     private Map<Object, Long> baseMap;
/*     */     
/*     */     public AscByValueComparator(Map<Object, Long> map)
/*     */     {
/*  42 */       this.baseMap = map;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public int compare(Object arg0, Object arg1)
/*     */     {
/*  51 */       Long value0 = (Long)this.baseMap.get(arg0);
/*  52 */       if (value0 == null) {
/*  53 */         return -1;
/*     */       }
/*     */       
/*  56 */       Long value1 = (Long)this.baseMap.get(arg1);
/*  57 */       return value0.compareTo(value1);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private static class DescByValueComparator
/*     */     implements Comparator<Object>
/*     */   {
/*     */     private Map<Object, Long> baseMap;
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */     public DescByValueComparator(Map<Object, Long> map)
/*     */     {
/*  74 */       this.baseMap = map;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public int compare(Object arg0, Object arg1)
/*     */     {
/*  83 */       Long value1 = (Long)this.baseMap.get(arg1);
/*  84 */       if (value1 == null) {
/*  85 */         return -1;
/*     */       }
/*  87 */       Long value0 = (Long)this.baseMap.get(arg0);
/*     */       
/*  89 */       return value1.compareTo(value0);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Object> getMostFrequent(Map<Object, Long> map, int topN)
/*     */   {
/* 102 */     List<Object> sortedMap = sortMap(map, false);
/* 103 */     return getFirstElements(sortedMap, topN);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Object> getLessFrequent(Map<Object, Long> map, int bottomN)
/*     */   {
/* 114 */     List<Object> sortedMap = sortMap(map, true);
/* 115 */     return getFirstElements(sortedMap, bottomN);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private List<Object> getFirstElements(List<Object> sortedMap, int bottomN)
/*     */   {
/* 125 */     List<Object> mostFrequent = new ArrayList();
/* 126 */     int i = 0;
/* 127 */     for (Iterator<Object> iterator = sortedMap.iterator(); iterator.hasNext();) {
/* 128 */       Object object = iterator.next();
/* 129 */       mostFrequent.add(object);
/* 130 */       i++;
/* 131 */       if (i == bottomN) {
/*     */         break;
/*     */       }
/*     */     }
/* 135 */     return mostFrequent;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public List<Object> sortMap(Map<Object, Long> map, boolean ascendingOrder)
/*     */   {
/* 146 */     Comparator<Object> cmp = ascendingOrder ? new AscByValueComparator(map) : new DescByValueComparator(map);
/* 147 */     List<Object> keySet = new ArrayList(map.keySet());
/* 148 */     Collections.sort(keySet, cmp);
/* 149 */     return keySet;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\collections\MapValueSorter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */