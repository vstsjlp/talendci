/*     */ package org.talend.utils.collections;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.HashSet;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Set;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class MultiMapHelper
/*     */ {
/*     */   public static <KeyT, ValT> boolean addUniqueObjectToListMap(KeyT key, ValT value, Map<KeyT, List<ValT>> map)
/*     */   {
/*  41 */     List<ValT> obj = (List)map.get(key);
/*  42 */     if (obj == null) {
/*  43 */       List<ValT> list = new ArrayList();
/*  44 */       list.add(value);
/*  45 */       return map.put(key, list) == null;
/*     */     }
/*     */     
/*     */ 
/*  49 */     List<ValT> coll = obj;
/*     */     
/*  51 */     if (coll.contains(value)) {
/*  52 */       return true;
/*     */     }
/*     */     
/*     */ 
/*  56 */     coll.add(value);
/*  57 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean addUniqueComparableToListMap(Object key, Comparable<Object> value, Map<Object, List<Comparable<Object>>> map)
/*     */   {
/*  73 */     List<Comparable<Object>> obj = (List)map.get(key);
/*  74 */     if (obj == null) {
/*  75 */       List<Comparable<Object>> list = new ArrayList();
/*  76 */       list.add(value);
/*  77 */       return map.put(key, list) == null;
/*     */     }
/*     */     
/*     */ 
/*  81 */     List<Comparable<Object>> coll = obj;
/*     */     
/*  83 */     int idx = 0;
/*  84 */     for (Comparable<Object> element : coll) {
/*  85 */       if ((value == element) || (value.compareTo(element) == 0)) {
/*  86 */         return true;
/*     */       }
/*  88 */       if (value.compareTo(element) <= 0) {
/*     */         break;
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*  95 */     coll.add(idx, value);
/*  96 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean addUniqueObjectToCollectionMap(Object key, Object value, Map<Object, Set<Object>> map)
/*     */   {
/* 109 */     Set<Object> obj = (Set)map.get(key);
/* 110 */     if (obj == null) {
/* 111 */       Set<Object> list = new HashSet();
/* 112 */       list.add(value);
/* 113 */       return map.put(key, list) == null;
/*     */     }
/*     */     
/*     */ 
/* 117 */     Set<Object> coll = obj;
/* 118 */     coll.add(value);
/* 119 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static <ValT> boolean isContainedInListMap(ValT object, Map<Object, Collection<ValT>> map)
/*     */   {
/* 128 */     Collection<Collection<ValT>> values = map.values();
/*     */     
/* 130 */     for (Collection<ValT> list : values) {
/* 131 */       if (list != null)
/*     */       {
/*     */ 
/* 134 */         if (list.contains(object)) {
/* 135 */           return true;
/*     */         }
/*     */       }
/*     */     }
/* 139 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static <KeyT, ValT> boolean removeObjectFromCollectionMap(KeyT key, ValT toBeRemoved, Map<KeyT, Collection<ValT>> map)
/*     */   {
/* 153 */     Collection<ValT> obj = (Collection)map.get(key);
/* 154 */     if (obj == null) {
/* 155 */       return false;
/*     */     }
/*     */     
/*     */ 
/* 159 */     Collection<ValT> coll = obj;
/* 160 */     return coll.remove(toBeRemoved);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static <KeyT, ValT> boolean removeAndCleanFromCollectionMap(KeyT key, ValT toBeRemoved, Map<KeyT, Collection<ValT>> map)
/*     */   {
/* 174 */     Collection<ValT> obj = (Collection)map.get(key);
/* 175 */     if (obj == null) {
/* 176 */       return false;
/*     */     }
/*     */     
/* 179 */     Collection<ValT> coll = obj;
/* 180 */     boolean ret = coll.remove(toBeRemoved);
/* 181 */     if (coll.isEmpty()) {
/* 182 */       map.remove(key);
/*     */     }
/* 184 */     return ret;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\collections\MultiMapHelper.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */