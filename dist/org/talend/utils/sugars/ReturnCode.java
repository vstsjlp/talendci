/*     */ package org.talend.utils.sugars;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ReturnCode
/*     */ {
/*     */   private Boolean ok;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private String message;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ReturnCode(String mess, Boolean retCode)
/*     */   {
/*  33 */     this.message = mess;
/*  34 */     this.ok = retCode;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public ReturnCode()
/*     */   {
/*  41 */     this.message = null;
/*  42 */     this.ok = Boolean.valueOf(true);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public ReturnCode(Boolean isOk)
/*     */   {
/*  51 */     this.message = null;
/*  52 */     this.ok = isOk;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setReturnCode(String mess, Boolean retCode)
/*     */   {
/*  62 */     setMessage(mess);
/*  63 */     setOk(retCode);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public Boolean isOk()
/*     */   {
/*  72 */     return this.ok;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setOk(Boolean ok)
/*     */   {
/*  81 */     this.ok = ok;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getMessage()
/*     */   {
/*  90 */     return this.message;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setMessage(String message)
/*     */   {
/*  99 */     this.message = message;
/*     */   }
/*     */   
/*     */   public String toString()
/*     */   {
/* 104 */     return "KO: " + this.message;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sugars\ReturnCode.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */