/*    */ package org.talend.utils.sugars;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TypedReturnCode<T>
/*    */   extends ReturnCode
/*    */ {
/*    */   private T object;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public TypedReturnCode(String mess, boolean retCode)
/*    */   {
/* 32 */     super(mess, Boolean.valueOf(retCode));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public TypedReturnCode() {}
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public TypedReturnCode(boolean isOk)
/*    */   {
/* 48 */     super(Boolean.valueOf(isOk));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public T getObject()
/*    */   {
/* 57 */     return (T)this.object;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setObject(T obj)
/*    */   {
/* 66 */     this.object = obj;
/*    */   }
/*    */   
/*    */   public void setReturnCode(String mess, boolean retCode, T obj) {
/* 70 */     super.setReturnCode(mess, Boolean.valueOf(retCode));
/* 71 */     setObject(obj);
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\sugars\TypedReturnCode.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */