/*     */ package org.talend.utils;
/*     */ 
/*     */ import java.io.Serializable;
/*     */ import java.util.Comparator;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LabelValueBean
/*     */   implements Comparable, Serializable
/*     */ {
/*  48 */   public static final Comparator CASE_INSENSITIVE_ORDER = new Comparator()
/*     */   {
/*     */     public int compare(Object o1, Object o2) {
/*  51 */       String label1 = ((LabelValueBean)o1).getLabel();
/*  52 */       String label2 = ((LabelValueBean)o2).getLabel();
/*     */       
/*  54 */       return label1.compareToIgnoreCase(label2);
/*     */     }
/*     */   };
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  63 */   private String label = null;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  68 */   private String value = null;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public LabelValueBean() {}
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public LabelValueBean(String label, String value)
/*     */   {
/*  86 */     this.label = label;
/*  87 */     this.value = value;
/*     */   }
/*     */   
/*     */   public String getLabel() {
/*  91 */     return this.label;
/*     */   }
/*     */   
/*     */   public void setLabel(String label) {
/*  95 */     this.label = label;
/*     */   }
/*     */   
/*     */   public String getValue() {
/*  99 */     return this.value;
/*     */   }
/*     */   
/*     */   public void setValue(String value) {
/* 103 */     this.value = value;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int compareTo(Object o)
/*     */   {
/* 116 */     String otherLabel = ((LabelValueBean)o).getLabel();
/*     */     
/* 118 */     return getLabel().compareTo(otherLabel);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public String toString()
/*     */   {
/* 125 */     StringBuffer sb = new StringBuffer("LabelValueBean[");
/*     */     
/* 127 */     sb.append(this.label);
/* 128 */     sb.append(", ");
/* 129 */     sb.append(this.value);
/* 130 */     sb.append("]");
/*     */     
/* 132 */     return sb.toString();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean equals(Object obj)
/*     */   {
/* 141 */     if (obj == this) {
/* 142 */       return true;
/*     */     }
/*     */     
/* 145 */     if (!(obj instanceof LabelValueBean)) {
/* 146 */       return false;
/*     */     }
/*     */     
/* 149 */     LabelValueBean bean = (LabelValueBean)obj;
/* 150 */     int nil = getValue() == null ? 1 : 0;
/*     */     
/* 152 */     nil += (bean.getValue() == null ? 1 : 0);
/*     */     
/* 154 */     if (nil == 2)
/* 155 */       return true;
/* 156 */     if (nil == 1) {
/* 157 */       return false;
/*     */     }
/* 159 */     return getValue().equals(bean.getValue());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public int hashCode()
/*     */   {
/* 169 */     return getValue() == null ? 17 : getValue().hashCode();
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\LabelValueBean.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */