/*     */ package org.talend.utils.cache;
/*     */ 
/*     */ import java.util.HashMap;
/*     */ import java.util.Iterator;
/*     */ import java.util.Map;
/*     */ import java.util.Set;
/*     */ import java.util.TreeSet;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class SimpleCache<K, V>
/*     */ {
/*     */   class HashKeyValue<K, V>
/*     */     implements Comparable<SimpleCache<K, V>.HashKeyValue<K, V>>
/*     */   {
/*     */     private K key;
/*     */     private V value;
/*     */     private long addTime;
/*     */     
/*     */     public HashKeyValue()
/*     */     {
/*  44 */       this.key = key;
/*  45 */       this.addTime = System.currentTimeMillis();
/*     */     }
/*     */     
/*     */     public HashKeyValue(V key) {
/*  49 */       this(key);
/*  50 */       this.value = value;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public int hashCode()
/*     */     {
/*  61 */       int result = 1;
/*  62 */       result = 31 * result + (this.key == null ? 0 : this.key.hashCode());
/*  63 */       return result;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public boolean equals(Object obj)
/*     */     {
/*  73 */       if (this == obj) {
/*  74 */         return true;
/*     */       }
/*  76 */       if (obj == null) {
/*  77 */         return false;
/*     */       }
/*  79 */       if (getClass() != obj.getClass()) {
/*  80 */         return false;
/*     */       }
/*  82 */       HashKeyValue other = (HashKeyValue)obj;
/*  83 */       if (this.key == null) {
/*  84 */         if (other.key != null) {
/*  85 */           return false;
/*     */         }
/*  87 */       } else if (!this.key.equals(other.key)) {
/*  88 */         return false;
/*     */       }
/*  90 */       return true;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public int compareTo(SimpleCache<K, V>.HashKeyValue<K, V> o)
/*     */     {
/*  99 */       if (equals(o)) {
/* 100 */         return 0;
/*     */       }
/* 102 */       return getAddTime() < o.getAddTime() ? -1 : 1;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public V getValue()
/*     */     {
/* 111 */       return (V)this.value;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public K getKey()
/*     */     {
/* 120 */       return (K)this.key;
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */     public long getAddTime()
/*     */     {
/* 129 */       return this.addTime;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/* 134 */   private Set<SimpleCache<K, V>.HashKeyValue<K, V>> keysOrderedByPutTime = new TreeSet();
/*     */   
/* 136 */   private Map<SimpleCache<K, V>.HashKeyValue<K, V>, SimpleCache<K, V>.HashKeyValue<K, V>> cache = new HashMap();
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Integer maxItems;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private Long maxTime;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public SimpleCache(long maxTime, int maxItems)
/*     */   {
/* 154 */     this.maxTime = Long.valueOf(maxTime);
/* 155 */     this.maxItems = Integer.valueOf(maxItems);
/* 156 */     if (((this.maxTime == null) || (this.maxTime.longValue() < 0L) || (this.maxTime.longValue() == Long.MAX_VALUE) || (this.maxTime.longValue() == 2147483647L)) && (
/* 157 */       (this.maxItems == null) || (this.maxItems.intValue() < 0) || (this.maxItems.intValue() == Integer.MAX_VALUE))) {
/* 158 */       throw new IllegalArgumentException(
/* 159 */         "At least one of maxTime or maxItems must be a value greater or equals 0, excepted the MAX_VALUE");
/*     */     }
/*     */   }
/*     */   
/*     */   public synchronized V get(K key) {
/* 164 */     SimpleCache<K, V>.HashKeyValue<K, V> internalKey = new HashKeyValue(key);
/* 165 */     SimpleCache<K, V>.HashKeyValue<K, V> keyValue = (HashKeyValue)this.cache.get(internalKey);
/* 166 */     if (keyValue != null) {
/* 167 */       return (V)keyValue.getValue();
/*     */     }
/* 169 */     return null;
/*     */   }
/*     */   
/*     */   public synchronized Long getAddTime(K key)
/*     */   {
/* 174 */     SimpleCache<K, V>.HashKeyValue<K, V> internalKey = new HashKeyValue(key);
/* 175 */     SimpleCache<K, V>.HashKeyValue<K, V> keyValue = (HashKeyValue)this.cache.get(internalKey);
/* 176 */     if (keyValue != null) {
/* 177 */       return Long.valueOf(keyValue.getAddTime());
/*     */     }
/* 179 */     return null;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public synchronized V put(K key, V value)
/*     */   {
/* 194 */     SimpleCache<K, V>.HashKeyValue<K, V> internalKeyValue = new HashKeyValue(key, value);
/* 195 */     this.keysOrderedByPutTime.add(internalKeyValue);
/* 196 */     SimpleCache<K, V>.HashKeyValue<K, V> previousKeyValue = (HashKeyValue)this.cache.put(internalKeyValue, internalKeyValue);
/* 197 */     int sizeItems = this.keysOrderedByPutTime.size();
/* 198 */     if ((this.maxTime != null) && (this.maxTime.longValue() >= 0L) && (this.maxTime.longValue() != Long.MAX_VALUE) && (sizeItems > 0)) {
/* 199 */       Iterator<SimpleCache<K, V>.HashKeyValue<K, V>> keysOrderedByPutTimeIterator = this.keysOrderedByPutTime.iterator();
/* 200 */       while (keysOrderedByPutTimeIterator.hasNext()) {
/* 201 */         long currentTimeMillis = System.currentTimeMillis();
/* 202 */         SimpleCache<K, V>.HashKeyValue<K, V> hashKey = (HashKeyValue)keysOrderedByPutTimeIterator.next();
/*     */         
/*     */ 
/* 205 */         if (currentTimeMillis - hashKey.addTime < this.maxTime.longValue()) break;
/* 206 */         keysOrderedByPutTimeIterator.remove();
/* 207 */         this.cache.remove(hashKey);
/*     */       }
/*     */     }
/*     */     
/*     */ 
/*     */ 
/* 213 */     sizeItems = this.keysOrderedByPutTime.size();
/* 214 */     if ((this.maxItems != null) && (this.maxItems.intValue() >= 0) && (this.maxItems.intValue() != Integer.MAX_VALUE) && (sizeItems > this.maxItems.intValue()) && (sizeItems > 0)) {
/* 215 */       Iterator<SimpleCache<K, V>.HashKeyValue<K, V>> setIterator = this.keysOrderedByPutTime.iterator();
/* 216 */       if (setIterator.hasNext()) {
/* 217 */         SimpleCache<K, V>.HashKeyValue<K, V> toRemoveFromMap = (HashKeyValue)setIterator.next();
/* 218 */         setIterator.remove();
/* 219 */         this.cache.remove(toRemoveFromMap);
/*     */       }
/*     */     }
/* 222 */     if (previousKeyValue != null) {
/* 223 */       return (V)previousKeyValue.getValue();
/*     */     }
/* 225 */     return null;
/*     */   }
/*     */   
/*     */   public synchronized int size()
/*     */   {
/* 230 */     return this.cache.size();
/*     */   }
/*     */   
/*     */   protected synchronized int internalTimeListSize() {
/* 234 */     return this.keysOrderedByPutTime.size();
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\cache\SimpleCache.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */