/*     */ package org.talend.utils.wsdl;
/*     */ 
/*     */ import java.io.ByteArrayInputStream;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.io.File;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.lang.reflect.InvocationTargetException;
/*     */ import java.net.MalformedURLException;
/*     */ import java.net.URI;
/*     */ import java.net.URL;
/*     */ import java.util.Collection;
/*     */ import java.util.HashMap;
/*     */ import java.util.HashSet;
/*     */ import java.util.Map;
/*     */ import javax.xml.parsers.DocumentBuilder;
/*     */ import javax.xml.parsers.DocumentBuilderFactory;
/*     */ import javax.xml.parsers.ParserConfigurationException;
/*     */ import javax.xml.transform.Result;
/*     */ import javax.xml.transform.Source;
/*     */ import javax.xml.transform.Transformer;
/*     */ import javax.xml.transform.TransformerFactory;
/*     */ import javax.xml.transform.dom.DOMSource;
/*     */ import javax.xml.transform.stream.StreamResult;
/*     */ import org.w3c.dom.Attr;
/*     */ import org.w3c.dom.Document;
/*     */ import org.w3c.dom.Element;
/*     */ import org.w3c.dom.NamedNodeMap;
/*     */ import org.w3c.dom.Node;
/*     */ import org.w3c.dom.NodeList;
/*     */ import org.xml.sax.SAXException;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class WSDLLoader
/*     */ {
/*     */   public static final String DEFAULT_FILENAME = "";
/*     */   private static final String XSD_NS = "http://www.w3.org/2001/XMLSchema";
/*     */   private static final String WSDL_NS = "http://schemas.xmlsoap.org/wsdl/";
/*     */   private static final String XMLNS_NS = "http://www.w3.org/2000/xmlns/";
/*     */   private static final String NAME_ELEMENT_SCHEMA = "schema";
/*     */   private static final String NAME_ATTRIBUTE_TARGET_NAMESPACE = "targetNamespace";
/*     */   private static final String NAME_ATTRIBUTE_SCHEMA_LOCATION = "schemaLocation";
/*  62 */   private static DocumentBuilder documentBuilder = null;
/*     */   
/*     */   private static int filenameIndex;
/*  65 */   private final Map<String, Collection<URL>> importedSchemas = new HashMap();
/*     */   
/*     */   public Map<String, InputStream> load(String wsdlLocation, String filenameTemplate) throws InvocationTargetException {
/*  68 */     filenameIndex = 0;
/*  69 */     return load(null, wsdlLocation, filenameTemplate);
/*     */   }
/*     */   
/*     */   private Map<String, InputStream> load(URL baseURL, String wsdlLocation, String filenameTemplate) throws InvocationTargetException {
/*  73 */     Map<String, InputStream> wsdls = new HashMap();
/*     */     try {
/*  75 */       URL wsdlURL = getURL(baseURL, wsdlLocation);
/*  76 */       Document wsdlDocument = getDocumentBuilder().parse(wsdlURL.toExternalForm());
/*     */       
/*  78 */       NodeList schemas = wsdlDocument.getElementsByTagNameNS(
/*  79 */         "http://www.w3.org/2001/XMLSchema", "schema");
/*     */       
/*  81 */       Element[] schemaElements = new Element[schemas.getLength()];
/*  82 */       for (int index = 0; index < schemas.getLength(); index++)
/*  83 */         schemaElements[index] = ((Element)schemas.item(index));
/*     */       Element[] arrayOfElement1;
/*  85 */       int j = (arrayOfElement1 = schemaElements).length; for (int i = 0; i < j; i++) { Element schema = arrayOfElement1[i];
/*  86 */         this.importedSchemas.put(schema.getAttribute("targetNamespace"), new HashSet());
/*  87 */         loadSchemas(schema, schema, wsdlURL);
/*     */       }
/*     */       
/*     */ 
/*  91 */       NodeList imports = wsdlDocument.getElementsByTagNameNS(
/*  92 */         "http://schemas.xmlsoap.org/wsdl/", "import");
/*  93 */       for (int index = 0; index < imports.getLength(); index++) {
/*  94 */         Element wsdlImport = (Element)imports.item(index);
/*  95 */         String filename = String.format(filenameTemplate, new Object[] { Integer.valueOf(filenameIndex++) });
/*  96 */         Map<String, InputStream> importedWsdls = new WSDLLoader().load(wsdlURL, wsdlImport.getAttribute("location"), filenameTemplate);
/*  97 */         wsdlImport.setAttribute("location", filename);
/*  98 */         wsdls.put(filename, (InputStream)importedWsdls.remove(""));
/*  99 */         wsdls.putAll(importedWsdls);
/*     */       }
/*     */       
/* 102 */       ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
/* 103 */       Object xmlSource = new DOMSource(wsdlDocument);
/* 104 */       Object outputTarget = new StreamResult(outputStream);
/* 105 */       TransformerFactory.newInstance().newTransformer().transform((Source)xmlSource, (Result)outputTarget);
/* 106 */       wsdls.put("", new ByteArrayInputStream(outputStream.toByteArray()));
/*     */       
/* 108 */       return wsdls;
/*     */     } catch (InvocationTargetException e) {
/* 110 */       throw e;
/*     */     } catch (Exception e) {
/* 112 */       throw new InvocationTargetException(e, "Error occured while processing schemas");
/*     */     } finally {
/* 114 */       this.importedSchemas.clear();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   private static URL getURL(URL contextURL, String spec)
/*     */     throws MalformedURLException
/*     */   {
/*     */     try
/*     */     {
/* 124 */       return new URL(contextURL, spec);
/*     */     } catch (MalformedURLException e) {
/* 126 */       File tempFile = new File(spec);
/* 127 */       if ((contextURL == null) || (
/* 128 */         (contextURL != null) && (tempFile.isAbsolute()))) {
/* 129 */         return tempFile.toURI().toURL();
/*     */       }
/*     */       
/*     */ 
/*     */ 
/* 134 */       throw e;
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void loadSchemas(Element ownerSchemaNode, Element schemaNode, URL ownerFile)
/*     */     throws InvocationTargetException, IOException
/*     */   {
/* 145 */     Map<String, String> prefixMapping = new HashMap();
/*     */     
/* 147 */     Node childNode = schemaNode.getFirstChild();
/* 148 */     while (childNode != null) {
/* 149 */       Node nextNode = childNode.getNextSibling();
/* 150 */       if ((childNode.getNodeType() == 1) && 
/* 151 */         ("http://www.w3.org/2001/XMLSchema".equals(childNode.getNamespaceURI()))) {
/* 152 */         if ("import".equals(childNode.getLocalName())) {
/* 153 */           Element importElement = (Element)childNode;
/*     */           
/* 155 */           String schemaLocation = importElement.getAttribute("schemaLocation");
/* 156 */           String schemaNS = importElement.getAttribute("namespace");
/*     */           
/* 158 */           if ((schemaLocation != null) && (schemaLocation.length() != 0) && 
/* 159 */             (schemaNS != null) && (schemaNS.length() != 0)) {
/*     */             try
/*     */             {
/* 162 */               URL schemaURL = getURL(ownerFile, schemaLocation);
/* 163 */               if (!this.importedSchemas.containsKey(schemaNS)) {
/* 164 */                 Element schemaImported = 
/* 165 */                   (Element)ownerSchemaNode.getOwnerDocument().importNode(
/* 166 */                   loadSchema(schemaURL, false), true);
/* 167 */                 ownerSchemaNode.getParentNode().insertBefore(schemaImported, ownerSchemaNode);
/*     */                 
/*     */ 
/* 170 */                 Collection<URL> urls = new HashSet();
/* 171 */                 urls.add(schemaURL);
/* 172 */                 this.importedSchemas.put(schemaNS, urls);
/*     */                 
/* 174 */                 loadSchemas(schemaImported, schemaImported, schemaURL);
/*     */ 
/*     */               }
/* 177 */               else if (((Collection)this.importedSchemas.get(schemaNS)).add(schemaURL)) {
/* 178 */                 NodeList nl = ((Element)ownerSchemaNode.getParentNode()).getElementsByTagNameNS("http://www.w3.org/2001/XMLSchema", "schema");
/* 179 */                 for (int i = 0; i < nl.getLength(); i++) {
/* 180 */                   Element schema = (Element)nl.item(i);
/* 181 */                   if (schemaNS.equals(schema.getAttribute("targetNamespace"))) {
/* 182 */                     Element schemaElement = loadSchema(schemaURL, true);
/*     */                     
/* 184 */                     loadSchemas(schema, schemaElement, schemaURL);
/*     */                     
/* 186 */                     Node refChild = getInsertLocation(schema.getLastChild());
/* 187 */                     Node child = schemaElement.getFirstChild();
/* 188 */                     while (child != null) {
/* 189 */                       Node next = child.getNextSibling();
/* 190 */                       child = schema.getOwnerDocument().importNode(child, true);
/*     */                       
/*     */ 
/*     */ 
/*     */ 
/* 195 */                       schema.insertBefore(child, refChild);
/* 196 */                       child = next;
/*     */                     }
/* 198 */                     break;
/*     */                   }
/*     */                 }
/*     */               }
/*     */             }
/*     */             catch (InvocationTargetException e) {
/* 204 */               throw e;
/*     */             } catch (IOException e) {
/* 206 */               throw e;
/*     */             } catch (Exception e) {
/* 208 */               throw new InvocationTargetException(e, 
/* 209 */                 "Unexpected error while loading external schema file: " + e.getMessage());
/*     */             }
/*     */           }
/*     */           
/*     */ 
/*     */ 
/* 215 */           if (importElement != null)
/*     */           {
/* 217 */             importElement.removeAttribute("schemaLocation");
/*     */             
/* 219 */             importElement.getParentNode().insertBefore(importElement, getInsertLocation(importElement));
/*     */           }
/* 221 */         } else if ("include".equals(childNode.getLocalName())) {
/* 222 */           Element includeElement = (Element)childNode;
/*     */           
/* 224 */           String schemaLocation = includeElement.getAttribute("schemaLocation");
/* 225 */           if ((schemaLocation == null) || (schemaLocation.length() == 0)) {
/* 226 */             String errMsg = "The schema include is incorrect: schemaLocation = [" + schemaLocation + "]";
/* 227 */             throw new InvocationTargetException(new Exception(errMsg));
/*     */           }
/*     */           try {
/* 230 */             URL schemaURL = getURL(ownerFile, schemaLocation);
/* 231 */             String schemaNamespace = ownerSchemaNode.getAttribute("targetNamespace");
/* 232 */             if (((Collection)this.importedSchemas.get(schemaNamespace)).add(schemaURL)) {
/* 233 */               Element schemaIncluded = loadSchema(schemaURL, true);
/* 234 */               String includeNamespace = schemaIncluded.getAttribute("targetNamespace");
/* 235 */               if ((includeNamespace != null) && (includeNamespace.length() != 0) && 
/* 236 */                 (!schemaNamespace.equals(includeNamespace))) {
/* 237 */                 String errMsg = "The schema include is incorrect: namespaces are not equals";
/* 238 */                 throw new InvocationTargetException(new Exception(errMsg));
/*     */               }
/* 240 */               loadSchemas(ownerSchemaNode, schemaIncluded, schemaURL);
/*     */               
/*     */ 
/* 243 */               NamedNodeMap nnm = schemaIncluded.getAttributes();
/* 244 */               for (int i = 0; i < nnm.getLength(); i++) {
/* 245 */                 Node attr = nnm.item(i);
/* 246 */                 String attrNamespace = attr.getNamespaceURI();
/* 247 */                 String attrLocalName = attr.getLocalName();
/* 248 */                 String attrValueNew = attr.getNodeValue();
/* 249 */                 String attrValueOld = getAttributeValue(schemaNode, attrNamespace, attrLocalName);
/* 250 */                 if (attrValueOld != null) {
/* 251 */                   if (("http://www.w3.org/2000/xmlns/".equals(attrNamespace)) && (!attrValueNew.equals(attrValueOld)))
/*     */                   {
/* 253 */                     String prefixNew = getPrefix(schemaNode, attrValueNew);
/* 254 */                     if (prefixNew == null) {
/* 255 */                       prefixNew = generatePrefix(schemaIncluded, attrLocalName);
/* 256 */                       schemaNode.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:" + prefixNew, attrValueNew);
/*     */                     }
/* 258 */                     prefixMapping.put(attrLocalName, prefixNew);
/*     */                   }
/*     */                 } else {
/* 261 */                   String attrName = attr.getNodeName();
/*     */                   
/* 263 */                   if ("http://www.w3.org/2000/xmlns/".equals(attrNamespace))
/*     */                   {
/* 265 */                     String prefixNew = getPrefix(schemaNode, attrValueNew);
/* 266 */                     if (prefixNew != null) {
/* 267 */                       prefixMapping.put(attrLocalName, prefixNew);
/*     */                     } else {
/* 269 */                       schemaNode.setAttributeNS("http://www.w3.org/2000/xmlns/", attrName, attrValueNew);
/*     */                     }
/*     */                   } else {
/* 272 */                     schemaNode.setAttributeNS(attrNamespace, attrName, attrValueNew);
/*     */                   }
/*     */                 }
/*     */               }
/*     */               
/*     */ 
/* 278 */               Node firstIncludedNode = null;
/* 279 */               Node child = schemaIncluded.getFirstChild();
/* 280 */               while (child != null) {
/* 281 */                 Node next = child.getNextSibling();
/* 282 */                 child = schemaNode.getOwnerDocument().importNode(child, true);
/* 283 */                 if (child.getNodeType() == 1) {
/* 284 */                   fixPrefixes((Element)child, prefixMapping);
/*     */                 }
/* 286 */                 child = schemaNode.insertBefore(child, includeElement);
/* 287 */                 if (firstIncludedNode == null) {
/* 288 */                   firstIncludedNode = child;
/*     */                 }
/* 290 */                 child = next;
/*     */               }
/* 292 */               if (firstIncludedNode != null) {
/* 293 */                 nextNode = firstIncludedNode;
/*     */               }
/*     */             }
/*     */           } catch (InvocationTargetException e) {
/* 297 */             throw e;
/*     */           } catch (IOException e) {
/* 299 */             throw e;
/*     */           } catch (Exception e) {
/* 301 */             throw new InvocationTargetException(e, 
/* 302 */               "Unexpected error while loading external schema file: " + e.getMessage());
/*     */           }
/*     */           
/* 305 */           includeElement.getParentNode().removeChild(includeElement);
/*     */         }
/*     */       }
/* 308 */       childNode = nextNode;
/*     */     }
/*     */   }
/*     */   
/*     */   private static final String generatePrefix(Element element, String initialPrefix) {
/* 313 */     String prefix = initialPrefix;
/*     */     
/* 315 */     for (int index = 0; 
/* 316 */         getAttributeValue(element, "http://www.w3.org/2000/xmlns/", prefix) != null; 
/* 317 */         index++) {
/* 318 */       prefix = initialPrefix + index;
/*     */     }
/* 320 */     return prefix;
/*     */   }
/*     */   
/*     */   private static final String getPrefix(Element element, String namespace) {
/* 324 */     NamedNodeMap nnm = element.getAttributes();
/* 325 */     for (int i = 0; i < nnm.getLength(); i++) {
/* 326 */       Node attr = nnm.item(i);
/* 327 */       if (("http://www.w3.org/2000/xmlns/".equals(attr.getNamespaceURI())) && (namespace.equals(attr.getNodeValue()))) {
/* 328 */         return attr.getLocalName();
/*     */       }
/*     */     }
/* 331 */     return null;
/*     */   }
/*     */   
/*     */   private static final String getAttributeValue(Element element, String namespace, String localName) {
/* 335 */     Attr attr = element.getAttributeNodeNS(namespace, localName);
/* 336 */     return attr != null ? attr.getNodeValue() : null;
/*     */   }
/*     */   
/*     */   private static final void fixPrefixes(Element element, Map<String, String> prefixMapping) {
/* 340 */     NamedNodeMap nnm = element.getAttributes();
/*     */     
/* 342 */     String prefix = element.getPrefix();
/* 343 */     String prefixNew = (String)prefixMapping.get(prefix);
/* 344 */     if (prefixNew != null) {
/* 345 */       if ("xmlns".equals(prefixNew)) {
/* 346 */         prefixNew = null;
/*     */       }
/* 348 */       element.setPrefix(prefixNew);
/*     */     }
/*     */     
/* 351 */     for (int i = 0; i < nnm.getLength(); i++) {
/* 352 */       Node attr = nnm.item(i);
/* 353 */       String value = attr.getNodeValue();
/* 354 */       if (value != null) {
/* 355 */         int index = value.indexOf(':');
/* 356 */         if (index != -1) {
/* 357 */           String prefixOld = value.substring(0, index);
/* 358 */           prefixNew = (String)prefixMapping.get(prefixOld);
/* 359 */           if (prefixNew != null) {
/* 360 */             if ("xmlns".equals(prefixNew)) {
/* 361 */               attr.setNodeValue(value.substring(index + 1));
/*     */             } else {
/* 363 */               attr.setNodeValue(prefixNew + ':' + value.substring(index + 1));
/*     */             }
/*     */           }
/*     */         } else {
/* 367 */           prefixNew = (String)prefixMapping.get("xmlns");
/* 368 */           if (prefixNew != null) {
/* 369 */             String name = attr.getLocalName();
/* 370 */             if (("type".equals(name)) || 
/* 371 */               ("base".equals(name)) || 
/* 372 */               ("ref".equals(name))) {
/* 373 */               attr.setNodeValue(prefixNew + ':' + value);
/*     */             }
/*     */           }
/*     */         }
/*     */       }
/*     */     }
/* 379 */     for (Node child = element.getFirstChild(); child != null; child = child.getNextSibling()) {
/* 380 */       if (child.getNodeType() == 1) {
/* 381 */         fixPrefixes((Element)child, prefixMapping);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private static final Element loadSchema(URL schemaFile, boolean cleanup) throws IOException, SAXException, ParserConfigurationException {
/* 387 */     InputStream is = null;
/*     */     try {
/* 389 */       is = schemaFile.openStream();
/* 390 */       Element schemaElement = getDocumentBuilder().parse(is).getDocumentElement();
/* 391 */       if (cleanup) {
/* 392 */         cleanupSchemaElement(schemaElement);
/*     */       }
/* 394 */       return schemaElement;
/*     */     } finally {
/* 396 */       if (is != null) {
/* 397 */         is.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private static final void cleanupSchemaElement(Element element) {
/* 403 */     Node node = element.getFirstChild();
/* 404 */     while (node != null) {
/* 405 */       Node next = node.getNextSibling();
/* 406 */       if (8 == node.getNodeType()) {
/* 407 */         element.removeChild(node);
/* 408 */       } else if (1 == node.getNodeType()) {
/* 409 */         Element child = (Element)node;
/* 410 */         if (("http://www.w3.org/2001/XMLSchema".equals(child.getNamespaceURI())) && 
/* 411 */           ("annotation".equals(child.getLocalName()))) {
/* 412 */           element.removeChild(child);
/*     */         } else {
/* 414 */           cleanupSchemaElement(child);
/*     */         }
/*     */       }
/* 417 */       node = next;
/*     */     }
/*     */   }
/*     */   
/*     */   private static Node getInsertLocation(Node currentNode)
/*     */   {
/* 423 */     for (Node refChild = currentNode.getPreviousSibling(); refChild != null; refChild = refChild.getPreviousSibling()) {
/* 424 */       if ((refChild.getNodeType() == 1) && 
/* 425 */         ("http://www.w3.org/2001/XMLSchema".equals(refChild.getNamespaceURI())) && 
/* 426 */         ("import".equals(refChild.getLocalName()))) {
/* 427 */         refChild = refChild.getNextSibling();
/* 428 */         break;
/*     */       }
/*     */     }
/* 431 */     if (refChild == null) {
/* 432 */       refChild = currentNode.getParentNode().getFirstChild();
/* 433 */       while (refChild.getNodeType() != 1) { refChild = refChild.getNextSibling();
/*     */       }
/*     */     }
/* 436 */     return refChild;
/*     */   }
/*     */   
/*     */   private static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
/* 440 */     if (documentBuilder == null) {
/* 441 */       DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
/* 442 */       factory.setValidating(false);
/* 443 */       factory.setNamespaceAware(true);
/* 444 */       documentBuilder = factory.newDocumentBuilder();
/*     */     }
/* 446 */     return documentBuilder;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\wsdl\WSDLLoader.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */