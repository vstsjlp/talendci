/*    */ package org.talend.utils.color;
/*    */ 
/*    */ import java.awt.Color;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class AWTColorUtils
/*    */ {
/* 24 */   public static final String[] COLOR_STRS = { "#236192", "#C4D600", "#DB662A", "#F7A800", "#787121", "#00A9CE", "#ECAB7C", 
/* 25 */     "#B8B370", "#D4D3D3", "#83D3E6", "#FFD38B" };
/*    */   
/* 27 */   private static final Color[] COLORS = initializeColors();
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static Color getColor(int idx)
/*    */   {
/* 36 */     assert (idx >= 0);
/* 37 */     idx %= COLORS.length;
/* 38 */     return COLORS[idx];
/*    */   }
/*    */   
/*    */   private static Color[] initializeColors() {
/* 42 */     Color[] colors = new Color[COLOR_STRS.length];
/* 43 */     int i = 0;
/* 44 */     String[] arrayOfString; int j = (arrayOfString = COLOR_STRS).length; for (int i = 0; i < j; i++) { String str = arrayOfString[i];
/* 45 */       colors[i] = Color.decode(str);
/* 46 */       i++;
/*    */     }
/* 48 */     return colors;
/*    */   }
/*    */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\color\AWTColorUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */