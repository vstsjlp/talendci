/*     */ package org.talend.utils.io;
/*     */ 
/*     */ import java.io.BufferedInputStream;
/*     */ import java.io.ByteArrayInputStream;
/*     */ import java.io.ByteArrayOutputStream;
/*     */ import java.io.File;
/*     */ import java.io.FileFilter;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileNotFoundException;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.PrintStream;
/*     */ import java.util.jar.Manifest;
/*     */ import java.util.zip.Adler32;
/*     */ import java.util.zip.CheckedInputStream;
/*     */ import java.util.zip.Checksum;
/*     */ import org.talend.utils.sugars.ReturnCode;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class FilesUtils
/*     */ {
/*     */   private static final int BUFFER_SIZE = 65536;
/*     */   public static final String GITKEEP = ".gitkeep";
/*  57 */   public static final String[] SVN_FOLDER_NAMES = { ".svn", "_svn" };
/*     */   
/*     */   public static boolean isSVNFolder(String name) {
/*  60 */     if (name != null) {
/*  61 */       String checkedName = name.toLowerCase();
/*  62 */       String[] arrayOfString; int j = (arrayOfString = SVN_FOLDER_NAMES).length; for (int i = 0; i < j; i++) { String element = arrayOfString[i];
/*  63 */         if (element.equals(checkedName)) {
/*  64 */           return true;
/*     */         }
/*     */       }
/*     */     }
/*  68 */     return false;
/*     */   }
/*     */   
/*     */   public static boolean isSVNFolder(File file) {
/*  72 */     if (file != null) {
/*  73 */       return isSVNFolder(file.getName());
/*     */     }
/*  75 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean isEmptyFolder(String path)
/*     */   {
/*  83 */     File file = new File(path);
/*  84 */     if (file.isDirectory()) {
/*  85 */       File[] files = file.listFiles();
/*  86 */       if (files.length <= 0) {
/*  87 */         return true;
/*     */       }
/*     */     }
/*  90 */     return false;
/*     */   }
/*     */   
/*     */   public static void copyFolder(File source, File target, boolean emptyTargetBeforeCopy, FileFilter sourceFolderFilter, FileFilter sourceFileFilter, boolean copyFolder) throws IOException
/*     */   {
/*  95 */     if (!target.exists()) {
/*  96 */       target.mkdirs();
/*     */     }
/*     */     
/*  99 */     if (emptyTargetBeforeCopy) {
/* 100 */       emptyFolder(target);
/*     */     }
/*     */     
/* 103 */     FileFilter folderFilter = new FileFilter()
/*     */     {
/*     */       public boolean accept(File pathname)
/*     */       {
/* 107 */         return (pathname.isDirectory()) && ((FilesUtils.this == null) || (FilesUtils.this.accept(pathname)));
/*     */       }
/*     */       
/* 110 */     };
/* 111 */     FileFilter fileFilter = new FileFilter()
/*     */     {
/*     */       public boolean accept(File pathname)
/*     */       {
/* 115 */         return (!pathname.isDirectory()) && ((FilesUtils.this == null) || (FilesUtils.this.accept(pathname)));
/*     */       }
/*     */       
/* 118 */     };
/* 119 */     File[] folders = source.listFiles(folderFilter);
/* 120 */     int j; File newFolder; if (folders != null) { File[] arrayOfFile1;
/* 121 */       j = (arrayOfFile1 = folders).length; for (int i = 0; i < j; i++) { File current = arrayOfFile1[i];
/* 122 */         if (copyFolder) {
/* 123 */           newFolder = new File(target, current.getName());
/* 124 */           newFolder.mkdir();
/* 125 */           copyFolder(current, newFolder, emptyTargetBeforeCopy, sourceFolderFilter, sourceFileFilter, copyFolder);
/*     */         } else {
/* 127 */           copyFolder(current, target, emptyTargetBeforeCopy, sourceFolderFilter, sourceFileFilter, copyFolder);
/*     */         }
/*     */       }
/*     */     }
/*     */     
/* 132 */     File[] files = source.listFiles(fileFilter);
/* 133 */     if (files != null) {
/* 134 */       int k = (newFolder = files).length; for (j = 0; j < k; j++) { File current = newFolder[j];
/* 135 */         File out = new File(target, current.getName());
/* 136 */         copyFile(current, out);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   private static void emptyFolder(File toEmpty) { File[] arrayOfFile;
/* 142 */     int j = (arrayOfFile = toEmpty.listFiles()).length; for (int i = 0; i < j; i++) { File current = arrayOfFile[i];
/* 143 */       if (current.isDirectory()) {
/* 144 */         emptyFolder(current);
/*     */       }
/* 146 */       current.delete();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void copyFile(File source, File target)
/*     */     throws IOException
/*     */   {
/* 155 */     if ((!target.exists()) || (source.lastModified() > target.lastModified())) {
/* 156 */       InputStream in = new FileInputStream(source);
/* 157 */       copyFile(in, target);
/*     */     }
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public static void copyFile(InputStream source, File target)
/*     */     throws IOException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: aload_1
/*     */     //   1: invokevirtual 144	java/io/File:getParentFile	()Ljava/io/File;
/*     */     //   4: astore_2
/*     */     //   5: aload_2
/*     */     //   6: ifnull +15 -> 21
/*     */     //   9: aload_2
/*     */     //   10: invokevirtual 78	java/io/File:exists	()Z
/*     */     //   13: ifne +8 -> 21
/*     */     //   16: aload_2
/*     */     //   17: invokevirtual 81	java/io/File:mkdirs	()Z
/*     */     //   20: pop
/*     */     //   21: new 148	java/io/FileOutputStream
/*     */     //   24: dup
/*     */     //   25: aload_1
/*     */     //   26: invokespecial 150	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
/*     */     //   29: astore_3
/*     */     //   30: aload_0
/*     */     //   31: aload_3
/*     */     //   32: invokestatic 151	org/apache/commons/io/IOUtils:copy	(Ljava/io/InputStream;Ljava/io/OutputStream;)I
/*     */     //   35: pop
/*     */     //   36: goto +28 -> 64
/*     */     //   39: astore 4
/*     */     //   41: aload_0
/*     */     //   42: invokevirtual 157	java/io/InputStream:close	()V
/*     */     //   45: goto +4 -> 49
/*     */     //   48: pop
/*     */     //   49: aload_3
/*     */     //   50: ifnull +11 -> 61
/*     */     //   53: aload_3
/*     */     //   54: invokevirtual 162	java/io/OutputStream:close	()V
/*     */     //   57: goto +4 -> 61
/*     */     //   60: pop
/*     */     //   61: aload 4
/*     */     //   63: athrow
/*     */     //   64: aload_0
/*     */     //   65: invokevirtual 157	java/io/InputStream:close	()V
/*     */     //   68: goto +4 -> 72
/*     */     //   71: pop
/*     */     //   72: aload_3
/*     */     //   73: ifnull +11 -> 84
/*     */     //   76: aload_3
/*     */     //   77: invokevirtual 162	java/io/OutputStream:close	()V
/*     */     //   80: goto +4 -> 84
/*     */     //   83: pop
/*     */     //   84: return
/*     */     // Line number table:
/*     */     //   Java source line #162	-> byte code offset #0
/*     */     //   Java source line #163	-> byte code offset #5
/*     */     //   Java source line #164	-> byte code offset #16
/*     */     //   Java source line #166	-> byte code offset #21
/*     */     //   Java source line #168	-> byte code offset #30
/*     */     //   Java source line #169	-> byte code offset #36
/*     */     //   Java source line #171	-> byte code offset #41
/*     */     //   Java source line #172	-> byte code offset #45
/*     */     //   Java source line #176	-> byte code offset #49
/*     */     //   Java source line #177	-> byte code offset #53
/*     */     //   Java source line #179	-> byte code offset #57
/*     */     //   Java source line #182	-> byte code offset #61
/*     */     //   Java source line #171	-> byte code offset #64
/*     */     //   Java source line #172	-> byte code offset #68
/*     */     //   Java source line #176	-> byte code offset #72
/*     */     //   Java source line #177	-> byte code offset #76
/*     */     //   Java source line #179	-> byte code offset #80
/*     */     //   Java source line #184	-> byte code offset #84
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	85	0	source	InputStream
/*     */     //   0	85	1	target	File
/*     */     //   4	13	2	parentFile	File
/*     */     //   29	48	3	out	java.io.OutputStream
/*     */     //   39	23	4	localObject	Object
/*     */     //   48	1	5	localException1	Exception
/*     */     //   60	1	6	localException2	Exception
/*     */     //   71	1	7	localException3	Exception
/*     */     //   83	1	8	localException4	Exception
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   30	39	39	finally
/*     */     //   41	45	48	java/lang/Exception
/*     */     //   49	57	60	java/lang/Exception
/*     */     //   64	68	71	java/lang/Exception
/*     */     //   72	80	83	java/lang/Exception
/*     */   }
/*     */   
/*     */   public static void copyDirectory(File source, File target)
/*     */   {
/* 187 */     File tarpath = new File(target, source.getName());
/* 188 */     if (source.isDirectory()) {
/* 189 */       tarpath.mkdir();
/* 190 */       File[] dir = source.listFiles();
/* 191 */       File[] arrayOfFile1; int j = (arrayOfFile1 = dir).length; for (int i = 0; i < j; i++) { File element = arrayOfFile1[i];
/* 192 */         copyDirectory(element, tarpath);
/*     */       }
/*     */     } else {
/*     */       try {
/* 196 */         InputStream in = new FileInputStream(source);
/* 197 */         copyFile(in, tarpath);
/*     */       } catch (FileNotFoundException e) {
/* 199 */         e.printStackTrace();
/*     */       } catch (IOException e) {
/* 201 */         e.printStackTrace();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public static void replaceInFile(String regex, String fileName, String replacement)
/*     */     throws IOException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: new 135	java/io/FileInputStream
/*     */     //   3: dup
/*     */     //   4: aload_1
/*     */     //   5: invokespecial 187	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
/*     */     //   8: astore_3
/*     */     //   9: new 188	java/lang/StringBuffer
/*     */     //   12: dup
/*     */     //   13: invokespecial 190	java/lang/StringBuffer:<init>	()V
/*     */     //   16: astore 4
/*     */     //   18: new 191	java/io/InputStreamReader
/*     */     //   21: dup
/*     */     //   22: aload_3
/*     */     //   23: invokespecial 193	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
/*     */     //   26: astore 5
/*     */     //   28: new 196	java/io/BufferedReader
/*     */     //   31: dup
/*     */     //   32: aload 5
/*     */     //   34: invokespecial 198	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
/*     */     //   37: astore 6
/*     */     //   39: goto +21 -> 60
/*     */     //   42: aload 4
/*     */     //   44: aload 7
/*     */     //   46: aload_0
/*     */     //   47: aload_2
/*     */     //   48: invokevirtual 201	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/*     */     //   51: invokevirtual 205	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   54: ldc -47
/*     */     //   56: invokevirtual 205	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
/*     */     //   59: pop
/*     */     //   60: aload 6
/*     */     //   62: invokevirtual 211	java/io/BufferedReader:readLine	()Ljava/lang/String;
/*     */     //   65: dup
/*     */     //   66: astore 7
/*     */     //   68: ifnonnull -26 -> 42
/*     */     //   71: goto +12 -> 83
/*     */     //   74: astore 8
/*     */     //   76: aload_3
/*     */     //   77: invokevirtual 157	java/io/InputStream:close	()V
/*     */     //   80: aload 8
/*     */     //   82: athrow
/*     */     //   83: aload_3
/*     */     //   84: invokevirtual 157	java/io/InputStream:close	()V
/*     */     //   87: new 148	java/io/FileOutputStream
/*     */     //   90: dup
/*     */     //   91: aload_1
/*     */     //   92: invokespecial 214	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
/*     */     //   95: astore 5
/*     */     //   97: aload 5
/*     */     //   99: aload 4
/*     */     //   101: invokevirtual 215	java/lang/StringBuffer:toString	()Ljava/lang/String;
/*     */     //   104: invokevirtual 218	java/lang/String:getBytes	()[B
/*     */     //   107: invokevirtual 222	java/io/OutputStream:write	([B)V
/*     */     //   110: aload 5
/*     */     //   112: invokevirtual 162	java/io/OutputStream:close	()V
/*     */     //   115: return
/*     */     // Line number table:
/*     */     //   Java source line #209	-> byte code offset #0
/*     */     //   Java source line #210	-> byte code offset #9
/*     */     //   Java source line #212	-> byte code offset #18
/*     */     //   Java source line #213	-> byte code offset #28
/*     */     //   Java source line #215	-> byte code offset #39
/*     */     //   Java source line #216	-> byte code offset #42
/*     */     //   Java source line #215	-> byte code offset #60
/*     */     //   Java source line #218	-> byte code offset #71
/*     */     //   Java source line #219	-> byte code offset #76
/*     */     //   Java source line #220	-> byte code offset #80
/*     */     //   Java source line #219	-> byte code offset #83
/*     */     //   Java source line #222	-> byte code offset #87
/*     */     //   Java source line #223	-> byte code offset #97
/*     */     //   Java source line #224	-> byte code offset #110
/*     */     //   Java source line #225	-> byte code offset #115
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	116	0	regex	String
/*     */     //   0	116	1	fileName	String
/*     */     //   0	116	2	replacement	String
/*     */     //   8	76	3	in	InputStream
/*     */     //   16	84	4	buffer	StringBuffer
/*     */     //   26	7	5	inR	java.io.InputStreamReader
/*     */     //   95	16	5	os	java.io.OutputStream
/*     */     //   37	24	6	buf	java.io.BufferedReader
/*     */     //   42	3	7	line	String
/*     */     //   66	3	7	line	String
/*     */     //   74	7	8	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   18	74	74	finally
/*     */   }
/*     */   
/*     */   public static FileFilter getExcludeSystemFilesFilter()
/*     */   {
/* 228 */     FileFilter filter = new FileFilter()
/*     */     {
/*     */       public boolean accept(File pathname)
/*     */       {
/* 232 */         return !FilesUtils.isSVNFolder(pathname);
/*     */       }
/*     */       
/* 235 */     };
/* 236 */     return filter;
/*     */   }
/*     */   
/*     */   public static FileFilter getAcceptJARFilesFilter() {
/* 240 */     FileFilter filter = new FileFilter()
/*     */     {
/*     */       public boolean accept(File pathname)
/*     */       {
/* 244 */         return pathname.toString().endsWith(".jar");
/*     */       }
/*     */       
/* 247 */     };
/* 248 */     return filter;
/*     */   }
/*     */   
/*     */   public static FileFilter getAcceptPMFilesFilter() {
/* 252 */     FileFilter filter = new FileFilter()
/*     */     {
/*     */       public boolean accept(File pathname)
/*     */       {
/* 256 */         return pathname.toString().endsWith(".pm");
/*     */       }
/*     */       
/* 259 */     };
/* 260 */     return filter;
/*     */   }
/*     */   
/*     */   public static boolean createFolder(String path) {
/* 264 */     File folderPath = new File(path);
/* 265 */     return createFolder(folderPath);
/*     */   }
/*     */   
/*     */   public static boolean createFolder(File path) {
/* 269 */     if (!path.exists()) {
/* 270 */       return path.mkdir();
/*     */     }
/* 272 */     if (!path.isDirectory()) {
/* 273 */       return path.mkdir();
/*     */     }
/* 275 */     return true;
/*     */   }
/*     */   
/*     */   public static int getLastSeparatorLocation(String pathName) {
/* 279 */     int lastseparator = -1;
/* 280 */     if (pathName.contains("\\")) {
/* 281 */       lastseparator = pathName.lastIndexOf("\\");
/*     */     }
/* 283 */     if (pathName.contains("/")) {
/* 284 */       int temp = pathName.lastIndexOf("/");
/* 285 */       if (temp > lastseparator) {
/* 286 */         lastseparator = temp;
/*     */       }
/*     */     }
/* 289 */     return lastseparator;
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public static java.util.List<String> getContentLines(String filePath)
/*     */     throws IOException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: new 196	java/io/BufferedReader
/*     */     //   3: dup
/*     */     //   4: new 191	java/io/InputStreamReader
/*     */     //   7: dup
/*     */     //   8: new 135	java/io/FileInputStream
/*     */     //   11: dup
/*     */     //   12: aload_0
/*     */     //   13: invokespecial 187	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
/*     */     //   16: invokespecial 193	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
/*     */     //   19: invokespecial 198	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
/*     */     //   22: astore_1
/*     */     //   23: new 275	java/util/ArrayList
/*     */     //   26: dup
/*     */     //   27: invokespecial 277	java/util/ArrayList:<init>	()V
/*     */     //   30: astore_2
/*     */     //   31: goto +11 -> 42
/*     */     //   34: aload_2
/*     */     //   35: aload_3
/*     */     //   36: invokeinterface 278 2 0
/*     */     //   41: pop
/*     */     //   42: aload_1
/*     */     //   43: invokevirtual 211	java/io/BufferedReader:readLine	()Ljava/lang/String;
/*     */     //   46: dup
/*     */     //   47: astore_3
/*     */     //   48: ifnonnull -14 -> 34
/*     */     //   51: goto +12 -> 63
/*     */     //   54: astore 4
/*     */     //   56: aload_1
/*     */     //   57: invokevirtual 283	java/io/BufferedReader:close	()V
/*     */     //   60: aload 4
/*     */     //   62: athrow
/*     */     //   63: aload_1
/*     */     //   64: invokevirtual 283	java/io/BufferedReader:close	()V
/*     */     //   67: aload_2
/*     */     //   68: areturn
/*     */     // Line number table:
/*     */     //   Java source line #298	-> byte code offset #0
/*     */     //   Java source line #299	-> byte code offset #23
/*     */     //   Java source line #302	-> byte code offset #31
/*     */     //   Java source line #303	-> byte code offset #34
/*     */     //   Java source line #302	-> byte code offset #42
/*     */     //   Java source line #305	-> byte code offset #51
/*     */     //   Java source line #306	-> byte code offset #56
/*     */     //   Java source line #307	-> byte code offset #60
/*     */     //   Java source line #306	-> byte code offset #63
/*     */     //   Java source line #308	-> byte code offset #67
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	69	0	filePath	String
/*     */     //   22	42	1	in	java.io.BufferedReader
/*     */     //   30	38	2	lines	java.util.List<String>
/*     */     //   34	2	3	line	String
/*     */     //   47	2	3	line	String
/*     */     //   54	7	4	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   31	54	54	finally
/*     */   }
/*     */   
/*     */   public static ReturnCode createFoldersIfNotExists(String path)
/*     */   {
/* 319 */     return createFoldersIfNotExists(path, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static ReturnCode createFoldersIfNotExists(String path, boolean pathIsFilePath)
/*     */   {
/* 331 */     ReturnCode rc = new ReturnCode();
/* 332 */     File filePath = new File(path);
/* 333 */     File fileFolder = filePath;
/* 334 */     if (pathIsFilePath) {
/* 335 */       fileFolder = filePath.getParentFile();
/*     */     }
/*     */     
/* 338 */     if ((!fileFolder.exists()) && 
/* 339 */       (!fileFolder.mkdirs())) {
/* 340 */       rc.setOk(Boolean.valueOf(false));
/* 341 */       rc.setMessage("Failed to create the directory: " + fileFolder.getAbsolutePath());
/*     */     }
/*     */     
/* 344 */     return rc;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public static void zipDirWithApache(String zipFileName, String inputFile)
/*     */     throws Exception
/*     */   {
/* 352 */     org.apache.tools.zip.ZipOutputStream out = new org.apache.tools.zip.ZipOutputStream(new FileOutputStream(zipFileName));
/* 353 */     zipDirWithApache(out, new File(inputFile), "");
/* 354 */     out.close();
/*     */   }
/*     */   
/*     */   private static void zipDirWithApache(org.apache.tools.zip.ZipOutputStream out, File f, String base) throws Exception {
/* 358 */     String baseValue = base;
/* 359 */     if (f.isDirectory()) {
/* 360 */       File[] fl = f.listFiles();
/* 361 */       out.putNextEntry(new org.apache.tools.zip.ZipEntry(baseValue + "/"));
/* 362 */       baseValue = baseValue + "/";
/* 363 */       File[] arrayOfFile1; int j = (arrayOfFile1 = fl).length; for (int i = 0; i < j; i++) { File element = arrayOfFile1[i];
/* 364 */         zipDirWithApache(out, element, baseValue + element.getName());
/*     */       }
/*     */     } else {
/* 367 */       out.putNextEntry(new org.apache.tools.zip.ZipEntry(baseValue));
/* 368 */       FileInputStream in = new FileInputStream(f);
/*     */       int b;
/* 370 */       while ((b = in.read()) != -1) { int b;
/* 371 */         out.write(b);
/*     */       }
/* 373 */       in.close();
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean removeFolder(String pathFolder, boolean recursiveRemove)
/*     */   {
/* 383 */     File folder = new File(pathFolder);
/* 384 */     if (folder.isDirectory()) {
/* 385 */       return removeFolder(folder, recursiveRemove);
/*     */     }
/* 387 */     return false;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean removeFolder(File folder, boolean removeRecursivly)
/*     */   {
/* 397 */     if (removeRecursivly) { File[] arrayOfFile;
/* 398 */       int j = (arrayOfFile = folder.listFiles()).length; for (int i = 0; i < j; i++) { File current = arrayOfFile[i];
/* 399 */         if (current.isDirectory()) {
/* 400 */           removeFolder(current, true);
/*     */         } else {
/* 402 */           current.delete();
/*     */         }
/*     */       }
/*     */     }
/* 406 */     return folder.delete();
/*     */   }
/*     */   
/*     */   public static String extractPathFolderFromFilePath(String filePath) {
/* 410 */     File completePath = new File(filePath);
/* 411 */     return completePath.getParent();
/*     */   }
/*     */   
/*     */   public static long getChecksumAlder32(File file) throws IOException {
/* 415 */     BufferedInputStream bufferedInputStream = null;
/* 416 */     CheckedInputStream cis = null;
/*     */     try {
/* 418 */       bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
/*     */       
/* 420 */       cis = new CheckedInputStream(bufferedInputStream, new Adler32());
/* 421 */       byte[] tempBuf = new byte[''];
/* 422 */       while (cis.read(tempBuf) >= 0) {}
/*     */       
/*     */ 
/* 425 */       return cis.getChecksum().getValue();
/*     */     } catch (IOException e) {
/* 427 */       throw e;
/*     */     } finally {
/* 429 */       if (bufferedInputStream != null) {
/* 430 */         bufferedInputStream.close();
/*     */       }
/* 432 */       if (cis != null) {
/* 433 */         cis.close();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static byte[] getBytes(File archiveFile)
/*     */     throws IOException
/*     */   {
/* 445 */     long length = archiveFile.length();
/*     */     
/* 447 */     int lengthFinalSize = 0;
/*     */     
/* 449 */     if (length > 2147483647L) {
/* 450 */       throw new IllegalStateException("Capacity is over !");
/*     */     }
/* 452 */     lengthFinalSize = (int)length;
/*     */     
/*     */ 
/* 455 */     FileInputStream fis = new FileInputStream(archiveFile);
/* 456 */     int bufferSize = 1024;
/* 457 */     byte[] buf = new byte[bufferSize];
/* 458 */     int readBytes = 0;
/*     */     
/* 460 */     ByteArrayOutputStream bos = new ByteArrayOutputStream(lengthFinalSize);
/*     */     
/* 462 */     while ((readBytes = fis.read(buf)) != -1) {
/* 463 */       bos.write(buf, 0, readBytes);
/*     */     }
/*     */     
/* 466 */     return bos.toByteArray();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static File getFile(String filePath, byte[] jobScriptArchive)
/*     */     throws IOException
/*     */   {
/* 476 */     File file = new File(filePath);
/*     */     
/* 478 */     ByteArrayInputStream bis = new ByteArrayInputStream(jobScriptArchive, 0, jobScriptArchive.length);
/*     */     
/* 480 */     FileOutputStream fos = new FileOutputStream(file);
/*     */     
/* 482 */     int bufferSize = 1024;
/* 483 */     byte[] buf = new byte[bufferSize];
/*     */     
/* 485 */     int readBytes = 0;
/*     */     
/* 487 */     while ((readBytes = bis.read(buf)) != -1) {
/* 488 */       fos.write(buf, 0, readBytes);
/*     */     }
/*     */     
/* 491 */     bis.close();
/* 492 */     fos.close();
/*     */     
/* 494 */     return file;
/*     */   }
/*     */   
/*     */   private static void printFailure(ReturnCode rc) {
/* 498 */     if (!rc.isOk().booleanValue()) {
/* 499 */       System.err.println("Failure: " + rc.getMessage());
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void zip(String sourceFileName, String zippedFileName)
/*     */     throws IOException
/*     */   {
/* 511 */     zip(new File(sourceFileName), zippedFileName, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void zip(String sourceFileName, String zippedFileName, FileFilter fileFilter)
/*     */     throws IOException
/*     */   {
/* 523 */     zip(new File(sourceFileName), zippedFileName, fileFilter);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void zip(File sourceFile, String zippedFileName)
/*     */     throws IOException
/*     */   {
/* 534 */     zip(sourceFile, zippedFileName, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void zip(File sourceFile, String zippedFileName, FileFilter fileFilter)
/*     */     throws IOException
/*     */   {
/* 546 */     if (sourceFile.isDirectory()) {
/* 547 */       zips(sourceFile.listFiles(fileFilter), zippedFileName, fileFilter);
/*     */     } else {
/* 549 */       zips(new File[] { sourceFile }, zippedFileName, fileFilter);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void jar(Manifest manifest, File sourceDir, File zip)
/*     */     throws IOException
/*     */   {
/* 563 */     jar(manifest, sourceDir, zip, null);
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public static void jar(Manifest manifest, File sourceDir, File zip, FileFilter fileFilter)
/*     */     throws IOException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: new 507	java/util/jar/JarOutputStream
/*     */     //   3: dup
/*     */     //   4: new 148	java/io/FileOutputStream
/*     */     //   7: dup
/*     */     //   8: aload_2
/*     */     //   9: invokespecial 150	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
/*     */     //   12: aload_0
/*     */     //   13: invokespecial 509	java/util/jar/JarOutputStream:<init>	(Ljava/io/OutputStream;Ljava/util/jar/Manifest;)V
/*     */     //   16: astore 4
/*     */     //   18: aload_1
/*     */     //   19: aload_3
/*     */     //   20: invokevirtual 96	java/io/File:listFiles	(Ljava/io/FileFilter;)[Ljava/io/File;
/*     */     //   23: aload 4
/*     */     //   25: aload_3
/*     */     //   26: invokestatic 512	org/talend/utils/io/FilesUtils:zips	([Ljava/io/File;Ljava/util/zip/ZipOutputStream;Ljava/io/FileFilter;)V
/*     */     //   29: goto +13 -> 42
/*     */     //   32: astore 5
/*     */     //   34: aload 4
/*     */     //   36: invokevirtual 515	java/util/zip/ZipOutputStream:close	()V
/*     */     //   39: aload 5
/*     */     //   41: athrow
/*     */     //   42: aload 4
/*     */     //   44: invokevirtual 515	java/util/zip/ZipOutputStream:close	()V
/*     */     //   47: return
/*     */     // Line number table:
/*     */     //   Java source line #577	-> byte code offset #0
/*     */     //   Java source line #579	-> byte code offset #18
/*     */     //   Java source line #580	-> byte code offset #29
/*     */     //   Java source line #581	-> byte code offset #34
/*     */     //   Java source line #582	-> byte code offset #39
/*     */     //   Java source line #581	-> byte code offset #42
/*     */     //   Java source line #583	-> byte code offset #47
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	48	0	manifest	Manifest
/*     */     //   0	48	1	sourceDir	File
/*     */     //   0	48	2	zip	File
/*     */     //   0	48	3	fileFilter	FileFilter
/*     */     //   16	27	4	out	java.util.zip.ZipOutputStream
/*     */     //   32	8	5	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   18	32	32	finally
/*     */   }
/*     */   
/*     */   public static void zips(File[] sourceFile, String zippedFileName)
/*     */     throws IOException
/*     */   {
/* 595 */     zips(sourceFile, zippedFileName, null);
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public static void zips(File[] sourceFile, String zippedFileName, FileFilter fileFilter)
/*     */     throws IOException
/*     */   {
/*     */     // Byte code:
/*     */     //   0: new 148	java/io/FileOutputStream
/*     */     //   3: dup
/*     */     //   4: aload_1
/*     */     //   5: invokespecial 214	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
/*     */     //   8: astore_3
/*     */     //   9: new 516	java/util/zip/ZipOutputStream
/*     */     //   12: dup
/*     */     //   13: aload_3
/*     */     //   14: invokespecial 522	java/util/zip/ZipOutputStream:<init>	(Ljava/io/OutputStream;)V
/*     */     //   17: astore 4
/*     */     //   19: aload_0
/*     */     //   20: aload 4
/*     */     //   22: aload_2
/*     */     //   23: invokestatic 512	org/talend/utils/io/FilesUtils:zips	([Ljava/io/File;Ljava/util/zip/ZipOutputStream;Ljava/io/FileFilter;)V
/*     */     //   26: goto +25 -> 51
/*     */     //   29: astore 5
/*     */     //   31: aload_0
/*     */     //   32: arraylength
/*     */     //   33: ifle +11 -> 44
/*     */     //   36: aload 4
/*     */     //   38: invokevirtual 515	java/util/zip/ZipOutputStream:close	()V
/*     */     //   41: goto +7 -> 48
/*     */     //   44: aload_3
/*     */     //   45: invokevirtual 162	java/io/OutputStream:close	()V
/*     */     //   48: aload 5
/*     */     //   50: athrow
/*     */     //   51: aload_0
/*     */     //   52: arraylength
/*     */     //   53: ifle +11 -> 64
/*     */     //   56: aload 4
/*     */     //   58: invokevirtual 515	java/util/zip/ZipOutputStream:close	()V
/*     */     //   61: goto +7 -> 68
/*     */     //   64: aload_3
/*     */     //   65: invokevirtual 162	java/io/OutputStream:close	()V
/*     */     //   68: return
/*     */     // Line number table:
/*     */     //   Java source line #608	-> byte code offset #0
/*     */     //   Java source line #609	-> byte code offset #9
/*     */     //   Java source line #611	-> byte code offset #19
/*     */     //   Java source line #612	-> byte code offset #26
/*     */     //   Java source line #614	-> byte code offset #31
/*     */     //   Java source line #615	-> byte code offset #36
/*     */     //   Java source line #616	-> byte code offset #41
/*     */     //   Java source line #617	-> byte code offset #44
/*     */     //   Java source line #619	-> byte code offset #48
/*     */     //   Java source line #614	-> byte code offset #51
/*     */     //   Java source line #615	-> byte code offset #56
/*     */     //   Java source line #616	-> byte code offset #61
/*     */     //   Java source line #617	-> byte code offset #64
/*     */     //   Java source line #620	-> byte code offset #68
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	69	0	sourceFile	File[]
/*     */     //   0	69	1	zippedFileName	String
/*     */     //   0	69	2	fileFilter	FileFilter
/*     */     //   8	57	3	fos	java.io.OutputStream
/*     */     //   17	40	4	out	java.util.zip.ZipOutputStream
/*     */     //   29	20	5	localObject	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   19	29	29	finally
/*     */   }
/*     */   
/*     */   private static void zips(File[] sourceFile, java.util.zip.ZipOutputStream out, FileFilter fileFilter)
/*     */     throws IOException
/*     */   {
/* 623 */     File[] arrayOfFile = sourceFile;int j = sourceFile.length; for (int i = 0; i < j; i++) { File theFile = arrayOfFile[i];
/* 624 */       zips(out, theFile, "", fileFilter);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private static void zips(java.util.zip.ZipOutputStream out, File f, String base, FileFilter fileFilter)
/*     */     throws IOException
/*     */   {
/* 639 */     if (f.isDirectory()) {
/* 640 */       base = base + f.getName() + '/';
/* 641 */       out.putNextEntry(new java.util.zip.ZipEntry(base));
/* 642 */       File[] arrayOfFile; int j = (arrayOfFile = f.listFiles(fileFilter)).length; for (int i = 0; i < j; i++) { File element = arrayOfFile[i];
/* 643 */         zips(out, element, base, fileFilter);
/*     */       }
/*     */     } else {
/* 646 */       out.putNextEntry(new java.util.zip.ZipEntry(base + f.getName()));
/* 647 */       InputStream in = new FileInputStream(f);
/*     */       
/* 649 */       byte[] b = new byte[65536];
/* 650 */       int readBytes = 0;
/* 651 */       while ((readBytes = in.read(b, 0, 65536)) != -1) {
/* 652 */         out.write(b, 0, readBytes);
/*     */       }
/* 654 */       in.close();
/* 655 */       out.flush();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void zipFiles(String source, String target) throws Exception {
/* 660 */     zip(source, target);
/*     */   }
/*     */   
/*     */   public static void zipFolderRecursion(String sourceFileName, String zippedFileName) throws IOException {
/* 664 */     zip(sourceFileName, zippedFileName, null);
/*     */   }
/*     */   
/*     */   public static void zipFolderRecursion(String sourceFileName, String zippedFileName, FileFilter fileFilter) throws Exception {
/* 668 */     zip(sourceFileName, zippedFileName, fileFilter);
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public static void unzip(String zipFile, String targetFolder, String... fileSuffixes)
/*     */     throws Exception
/*     */   {
/*     */     // Byte code:
/*     */     //   0: aconst_null
/*     */     //   1: astore_3
/*     */     //   2: new 551	java/util/zip/ZipFile
/*     */     //   5: dup
/*     */     //   6: aload_0
/*     */     //   7: invokespecial 553	java/util/zip/ZipFile:<init>	(Ljava/lang/String;)V
/*     */     //   10: astore 4
/*     */     //   12: sipush 8192
/*     */     //   15: newarray <illegal type>
/*     */     //   17: astore 5
/*     */     //   19: aload 4
/*     */     //   21: invokevirtual 554	java/util/zip/ZipFile:entries	()Ljava/util/Enumeration;
/*     */     //   24: astore 6
/*     */     //   26: goto +235 -> 261
/*     */     //   29: aload 6
/*     */     //   31: invokeinterface 558 1 0
/*     */     //   36: checkcast 530	java/util/zip/ZipEntry
/*     */     //   39: astore 7
/*     */     //   41: new 45	java/io/File
/*     */     //   44: dup
/*     */     //   45: aload_1
/*     */     //   46: aload 7
/*     */     //   48: invokevirtual 564	java/util/zip/ZipEntry:getName	()Ljava/lang/String;
/*     */     //   51: invokespecial 565	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
/*     */     //   54: astore 8
/*     */     //   56: aload 7
/*     */     //   58: invokevirtual 567	java/util/zip/ZipEntry:isDirectory	()Z
/*     */     //   61: ifeq +20 -> 81
/*     */     //   64: aload 8
/*     */     //   66: invokevirtual 78	java/io/File:exists	()Z
/*     */     //   69: ifne +192 -> 261
/*     */     //   72: aload 8
/*     */     //   74: invokevirtual 102	java/io/File:mkdir	()Z
/*     */     //   77: pop
/*     */     //   78: goto +183 -> 261
/*     */     //   81: aload_2
/*     */     //   82: arraylength
/*     */     //   83: ifle +18 -> 101
/*     */     //   86: aload 8
/*     */     //   88: invokevirtual 44	java/io/File:getName	()Ljava/lang/String;
/*     */     //   91: aload_2
/*     */     //   92: invokestatic 568	org/talend/utils/io/FilesUtils:isReservedFile	(Ljava/lang/String;[Ljava/lang/String;)Z
/*     */     //   95: ifne +6 -> 101
/*     */     //   98: goto +163 -> 261
/*     */     //   101: aload 8
/*     */     //   103: invokevirtual 144	java/io/File:getParentFile	()Ljava/io/File;
/*     */     //   106: invokevirtual 78	java/io/File:exists	()Z
/*     */     //   109: ifne +12 -> 121
/*     */     //   112: aload 8
/*     */     //   114: invokevirtual 144	java/io/File:getParentFile	()Ljava/io/File;
/*     */     //   117: invokevirtual 81	java/io/File:mkdirs	()Z
/*     */     //   120: pop
/*     */     //   121: aload 4
/*     */     //   123: aload 7
/*     */     //   125: invokevirtual 572	java/util/zip/ZipFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
/*     */     //   128: astore 9
/*     */     //   130: new 148	java/io/FileOutputStream
/*     */     //   133: dup
/*     */     //   134: aload 8
/*     */     //   136: invokespecial 150	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
/*     */     //   139: astore 10
/*     */     //   141: aload 8
/*     */     //   143: invokevirtual 144	java/io/File:getParentFile	()Ljava/io/File;
/*     */     //   146: astore 11
/*     */     //   148: aload 11
/*     */     //   150: invokevirtual 62	java/io/File:isDirectory	()Z
/*     */     //   153: ifeq +17 -> 170
/*     */     //   156: aload 11
/*     */     //   158: invokevirtual 78	java/io/File:exists	()Z
/*     */     //   161: ifne +9 -> 170
/*     */     //   164: aload 11
/*     */     //   166: invokevirtual 81	java/io/File:mkdirs	()Z
/*     */     //   169: pop
/*     */     //   170: aload 9
/*     */     //   172: aload 5
/*     */     //   174: invokevirtual 576	java/io/InputStream:read	([B)I
/*     */     //   177: istore 12
/*     */     //   179: iload 12
/*     */     //   181: iconst_m1
/*     */     //   182: if_icmpne +6 -> 188
/*     */     //   185: goto +16 -> 201
/*     */     //   188: aload 10
/*     */     //   190: aload 5
/*     */     //   192: iconst_0
/*     */     //   193: iload 12
/*     */     //   195: invokevirtual 577	java/io/OutputStream:write	([BII)V
/*     */     //   198: goto -28 -> 170
/*     */     //   201: aload 10
/*     */     //   203: invokevirtual 578	java/io/OutputStream:flush	()V
/*     */     //   206: goto +45 -> 251
/*     */     //   209: astore 12
/*     */     //   211: aload 12
/*     */     //   213: astore_3
/*     */     //   214: aload 9
/*     */     //   216: invokevirtual 157	java/io/InputStream:close	()V
/*     */     //   219: aload 10
/*     */     //   221: invokevirtual 162	java/io/OutputStream:close	()V
/*     */     //   224: aload 4
/*     */     //   226: invokevirtual 579	java/util/zip/ZipFile:close	()V
/*     */     //   229: aload_3
/*     */     //   230: ifnull +5 -> 235
/*     */     //   233: aload_3
/*     */     //   234: athrow
/*     */     //   235: return
/*     */     //   236: astore 13
/*     */     //   238: aload 9
/*     */     //   240: invokevirtual 157	java/io/InputStream:close	()V
/*     */     //   243: aload 10
/*     */     //   245: invokevirtual 162	java/io/OutputStream:close	()V
/*     */     //   248: aload 13
/*     */     //   250: athrow
/*     */     //   251: aload 9
/*     */     //   253: invokevirtual 157	java/io/InputStream:close	()V
/*     */     //   256: aload 10
/*     */     //   258: invokevirtual 162	java/io/OutputStream:close	()V
/*     */     //   261: aload 6
/*     */     //   263: invokeinterface 580 1 0
/*     */     //   268: ifne -239 -> 29
/*     */     //   271: goto +35 -> 306
/*     */     //   274: astore 6
/*     */     //   276: aload 6
/*     */     //   278: astore_3
/*     */     //   279: aload 4
/*     */     //   281: invokevirtual 579	java/util/zip/ZipFile:close	()V
/*     */     //   284: aload_3
/*     */     //   285: ifnull +32 -> 317
/*     */     //   288: aload_3
/*     */     //   289: athrow
/*     */     //   290: astore 14
/*     */     //   292: aload 4
/*     */     //   294: invokevirtual 579	java/util/zip/ZipFile:close	()V
/*     */     //   297: aload_3
/*     */     //   298: ifnull +5 -> 303
/*     */     //   301: aload_3
/*     */     //   302: athrow
/*     */     //   303: aload 14
/*     */     //   305: athrow
/*     */     //   306: aload 4
/*     */     //   308: invokevirtual 579	java/util/zip/ZipFile:close	()V
/*     */     //   311: aload_3
/*     */     //   312: ifnull +5 -> 317
/*     */     //   315: aload_3
/*     */     //   316: athrow
/*     */     //   317: return
/*     */     // Line number table:
/*     */     //   Java source line #683	-> byte code offset #0
/*     */     //   Java source line #684	-> byte code offset #2
/*     */     //   Java source line #685	-> byte code offset #12
/*     */     //   Java source line #688	-> byte code offset #19
/*     */     //   Java source line #689	-> byte code offset #26
/*     */     //   Java source line #690	-> byte code offset #29
/*     */     //   Java source line #692	-> byte code offset #41
/*     */     //   Java source line #694	-> byte code offset #56
/*     */     //   Java source line #695	-> byte code offset #64
/*     */     //   Java source line #696	-> byte code offset #72
/*     */     //   Java source line #698	-> byte code offset #78
/*     */     //   Java source line #700	-> byte code offset #81
/*     */     //   Java source line #701	-> byte code offset #98
/*     */     //   Java source line #703	-> byte code offset #101
/*     */     //   Java source line #704	-> byte code offset #112
/*     */     //   Java source line #707	-> byte code offset #121
/*     */     //   Java source line #708	-> byte code offset #130
/*     */     //   Java source line #710	-> byte code offset #141
/*     */     //   Java source line #711	-> byte code offset #148
/*     */     //   Java source line #712	-> byte code offset #164
/*     */     //   Java source line #717	-> byte code offset #170
/*     */     //   Java source line #718	-> byte code offset #179
/*     */     //   Java source line #719	-> byte code offset #185
/*     */     //   Java source line #721	-> byte code offset #188
/*     */     //   Java source line #716	-> byte code offset #198
/*     */     //   Java source line #724	-> byte code offset #201
/*     */     //   Java source line #725	-> byte code offset #206
/*     */     //   Java source line #726	-> byte code offset #211
/*     */     //   Java source line #730	-> byte code offset #214
/*     */     //   Java source line #731	-> byte code offset #219
/*     */     //   Java source line #738	-> byte code offset #224
/*     */     //   Java source line #740	-> byte code offset #229
/*     */     //   Java source line #742	-> byte code offset #233
/*     */     //   Java source line #728	-> byte code offset #235
/*     */     //   Java source line #729	-> byte code offset #236
/*     */     //   Java source line #730	-> byte code offset #238
/*     */     //   Java source line #731	-> byte code offset #243
/*     */     //   Java source line #732	-> byte code offset #248
/*     */     //   Java source line #730	-> byte code offset #251
/*     */     //   Java source line #731	-> byte code offset #256
/*     */     //   Java source line #689	-> byte code offset #261
/*     */     //   Java source line #735	-> byte code offset #271
/*     */     //   Java source line #736	-> byte code offset #276
/*     */     //   Java source line #738	-> byte code offset #279
/*     */     //   Java source line #740	-> byte code offset #284
/*     */     //   Java source line #742	-> byte code offset #288
/*     */     //   Java source line #737	-> byte code offset #290
/*     */     //   Java source line #738	-> byte code offset #292
/*     */     //   Java source line #740	-> byte code offset #297
/*     */     //   Java source line #742	-> byte code offset #301
/*     */     //   Java source line #744	-> byte code offset #303
/*     */     //   Java source line #738	-> byte code offset #306
/*     */     //   Java source line #740	-> byte code offset #311
/*     */     //   Java source line #742	-> byte code offset #315
/*     */     //   Java source line #745	-> byte code offset #317
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	318	0	zipFile	String
/*     */     //   0	318	1	targetFolder	String
/*     */     //   0	318	2	fileSuffixes	String[]
/*     */     //   1	315	3	exception	Exception
/*     */     //   10	297	4	zip	java.util.zip.ZipFile
/*     */     //   17	174	5	buf	byte[]
/*     */     //   24	238	6	enumeration	java.util.Enumeration<java.util.zip.ZipEntry>
/*     */     //   274	3	6	e	Exception
/*     */     //   39	85	7	entry	java.util.zip.ZipEntry
/*     */     //   54	88	8	file	File
/*     */     //   128	124	9	zin	InputStream
/*     */     //   139	118	10	fout	java.io.OutputStream
/*     */     //   146	19	11	dir	File
/*     */     //   177	17	12	bytesRead	int
/*     */     //   209	3	12	e	Exception
/*     */     //   236	13	13	localObject1	Object
/*     */     //   290	14	14	localObject2	Object
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   170	206	209	java/lang/Exception
/*     */     //   170	214	236	finally
/*     */     //   19	224	274	java/lang/Exception
/*     */     //   236	271	274	java/lang/Exception
/*     */     //   19	224	290	finally
/*     */     //   236	279	290	finally
/*     */   }
/*     */   
/*     */   private static boolean isReservedFile(String name, String[] fileSuffixes)
/*     */   {
/* 748 */     if (name != null) {
/* 749 */       String checkedName = name.toLowerCase();
/* 750 */       String[] arrayOfString; int j = (arrayOfString = fileSuffixes).length; for (int i = 0; i < j; i++) { String element = arrayOfString[i];
/* 751 */         if ((element.equals(checkedName)) || (checkedName.endsWith(element))) {
/* 752 */           return true;
/*     */         }
/*     */       }
/*     */     }
/* 756 */     return false;
/*     */   }
/*     */   
/*     */   /* Error */
/*     */   public static void downloadFileFromWeb(java.net.URL resourceURL, String fullPath)
/*     */   {
/*     */     // Byte code:
/*     */     //   0: aconst_null
/*     */     //   1: astore_2
/*     */     //   2: aconst_null
/*     */     //   3: astore_3
/*     */     //   4: aconst_null
/*     */     //   5: astore 4
/*     */     //   7: ldc 8
/*     */     //   9: newarray <illegal type>
/*     */     //   11: astore 5
/*     */     //   13: iconst_0
/*     */     //   14: istore 6
/*     */     //   16: aconst_null
/*     */     //   17: astore 7
/*     */     //   19: aload_0
/*     */     //   20: invokevirtual 602	java/net/URL:openConnection	()Ljava/net/URLConnection;
/*     */     //   23: checkcast 608	java/net/HttpURLConnection
/*     */     //   26: astore 4
/*     */     //   28: aload 4
/*     */     //   30: invokevirtual 610	java/net/HttpURLConnection:connect	()V
/*     */     //   33: aload 4
/*     */     //   35: invokevirtual 613	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
/*     */     //   38: astore 8
/*     */     //   40: new 386	java/io/BufferedInputStream
/*     */     //   43: dup
/*     */     //   44: aload 8
/*     */     //   46: invokespecial 388	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
/*     */     //   49: astore_3
/*     */     //   50: new 45	java/io/File
/*     */     //   53: dup
/*     */     //   54: aload_1
/*     */     //   55: invokespecial 59	java/io/File:<init>	(Ljava/lang/String;)V
/*     */     //   58: astore 7
/*     */     //   60: aload 7
/*     */     //   62: invokevirtual 78	java/io/File:exists	()Z
/*     */     //   65: ifne +12 -> 77
/*     */     //   68: aload 7
/*     */     //   70: invokevirtual 616	java/io/File:createNewFile	()Z
/*     */     //   73: pop
/*     */     //   74: goto +15 -> 89
/*     */     //   77: aload 7
/*     */     //   79: invokevirtual 127	java/io/File:delete	()Z
/*     */     //   82: pop
/*     */     //   83: aload 7
/*     */     //   85: invokevirtual 616	java/io/File:createNewFile	()Z
/*     */     //   88: pop
/*     */     //   89: new 148	java/io/FileOutputStream
/*     */     //   92: dup
/*     */     //   93: aload_1
/*     */     //   94: invokespecial 214	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
/*     */     //   97: astore_2
/*     */     //   98: goto +12 -> 110
/*     */     //   101: aload_2
/*     */     //   102: aload 5
/*     */     //   104: iconst_0
/*     */     //   105: iload 6
/*     */     //   107: invokevirtual 453	java/io/FileOutputStream:write	([BII)V
/*     */     //   110: aload_3
/*     */     //   111: aload 5
/*     */     //   113: invokevirtual 619	java/io/BufferedInputStream:read	([B)I
/*     */     //   116: dup
/*     */     //   117: istore 6
/*     */     //   119: iconst_m1
/*     */     //   120: if_icmpne -19 -> 101
/*     */     //   123: goto +142 -> 265
/*     */     //   126: astore 8
/*     */     //   128: aload 8
/*     */     //   130: invokevirtual 620	java/net/MalformedURLException:printStackTrace	()V
/*     */     //   133: aconst_null
/*     */     //   134: astore 5
/*     */     //   136: aload_3
/*     */     //   137: invokevirtual 409	java/io/BufferedInputStream:close	()V
/*     */     //   140: aload_2
/*     */     //   141: invokevirtual 456	java/io/FileOutputStream:close	()V
/*     */     //   144: goto +10 -> 154
/*     */     //   147: astore 10
/*     */     //   149: aload 10
/*     */     //   151: invokevirtual 179	java/io/IOException:printStackTrace	()V
/*     */     //   154: aload 4
/*     */     //   156: invokevirtual 623	java/net/HttpURLConnection:disconnect	()V
/*     */     //   159: goto +132 -> 291
/*     */     //   162: astore 8
/*     */     //   164: aload 8
/*     */     //   166: invokevirtual 174	java/io/FileNotFoundException:printStackTrace	()V
/*     */     //   169: aconst_null
/*     */     //   170: astore 5
/*     */     //   172: aload_3
/*     */     //   173: invokevirtual 409	java/io/BufferedInputStream:close	()V
/*     */     //   176: aload_2
/*     */     //   177: invokevirtual 456	java/io/FileOutputStream:close	()V
/*     */     //   180: goto +10 -> 190
/*     */     //   183: astore 10
/*     */     //   185: aload 10
/*     */     //   187: invokevirtual 179	java/io/IOException:printStackTrace	()V
/*     */     //   190: aload 4
/*     */     //   192: invokevirtual 623	java/net/HttpURLConnection:disconnect	()V
/*     */     //   195: goto +96 -> 291
/*     */     //   198: astore 8
/*     */     //   200: aload 8
/*     */     //   202: invokevirtual 179	java/io/IOException:printStackTrace	()V
/*     */     //   205: aconst_null
/*     */     //   206: astore 5
/*     */     //   208: aload_3
/*     */     //   209: invokevirtual 409	java/io/BufferedInputStream:close	()V
/*     */     //   212: aload_2
/*     */     //   213: invokevirtual 456	java/io/FileOutputStream:close	()V
/*     */     //   216: goto +10 -> 226
/*     */     //   219: astore 10
/*     */     //   221: aload 10
/*     */     //   223: invokevirtual 179	java/io/IOException:printStackTrace	()V
/*     */     //   226: aload 4
/*     */     //   228: invokevirtual 623	java/net/HttpURLConnection:disconnect	()V
/*     */     //   231: goto +60 -> 291
/*     */     //   234: astore 9
/*     */     //   236: aconst_null
/*     */     //   237: astore 5
/*     */     //   239: aload_3
/*     */     //   240: invokevirtual 409	java/io/BufferedInputStream:close	()V
/*     */     //   243: aload_2
/*     */     //   244: invokevirtual 456	java/io/FileOutputStream:close	()V
/*     */     //   247: goto +10 -> 257
/*     */     //   250: astore 10
/*     */     //   252: aload 10
/*     */     //   254: invokevirtual 179	java/io/IOException:printStackTrace	()V
/*     */     //   257: aload 4
/*     */     //   259: invokevirtual 623	java/net/HttpURLConnection:disconnect	()V
/*     */     //   262: aload 9
/*     */     //   264: athrow
/*     */     //   265: aconst_null
/*     */     //   266: astore 5
/*     */     //   268: aload_3
/*     */     //   269: invokevirtual 409	java/io/BufferedInputStream:close	()V
/*     */     //   272: aload_2
/*     */     //   273: invokevirtual 456	java/io/FileOutputStream:close	()V
/*     */     //   276: goto +10 -> 286
/*     */     //   279: astore 10
/*     */     //   281: aload 10
/*     */     //   283: invokevirtual 179	java/io/IOException:printStackTrace	()V
/*     */     //   286: aload 4
/*     */     //   288: invokevirtual 623	java/net/HttpURLConnection:disconnect	()V
/*     */     //   291: return
/*     */     // Line number table:
/*     */     //   Java source line #761	-> byte code offset #0
/*     */     //   Java source line #762	-> byte code offset #2
/*     */     //   Java source line #763	-> byte code offset #4
/*     */     //   Java source line #764	-> byte code offset #7
/*     */     //   Java source line #765	-> byte code offset #13
/*     */     //   Java source line #766	-> byte code offset #16
/*     */     //   Java source line #768	-> byte code offset #19
/*     */     //   Java source line #769	-> byte code offset #28
/*     */     //   Java source line #770	-> byte code offset #33
/*     */     //   Java source line #771	-> byte code offset #40
/*     */     //   Java source line #772	-> byte code offset #50
/*     */     //   Java source line #774	-> byte code offset #60
/*     */     //   Java source line #775	-> byte code offset #68
/*     */     //   Java source line #776	-> byte code offset #74
/*     */     //   Java source line #777	-> byte code offset #77
/*     */     //   Java source line #778	-> byte code offset #83
/*     */     //   Java source line #780	-> byte code offset #89
/*     */     //   Java source line #781	-> byte code offset #98
/*     */     //   Java source line #782	-> byte code offset #101
/*     */     //   Java source line #781	-> byte code offset #110
/*     */     //   Java source line #784	-> byte code offset #123
/*     */     //   Java source line #785	-> byte code offset #128
/*     */     //   Java source line #792	-> byte code offset #133
/*     */     //   Java source line #793	-> byte code offset #136
/*     */     //   Java source line #794	-> byte code offset #140
/*     */     //   Java source line #795	-> byte code offset #144
/*     */     //   Java source line #796	-> byte code offset #149
/*     */     //   Java source line #798	-> byte code offset #154
/*     */     //   Java source line #786	-> byte code offset #162
/*     */     //   Java source line #787	-> byte code offset #164
/*     */     //   Java source line #792	-> byte code offset #169
/*     */     //   Java source line #793	-> byte code offset #172
/*     */     //   Java source line #794	-> byte code offset #176
/*     */     //   Java source line #795	-> byte code offset #180
/*     */     //   Java source line #796	-> byte code offset #185
/*     */     //   Java source line #798	-> byte code offset #190
/*     */     //   Java source line #788	-> byte code offset #198
/*     */     //   Java source line #789	-> byte code offset #200
/*     */     //   Java source line #792	-> byte code offset #205
/*     */     //   Java source line #793	-> byte code offset #208
/*     */     //   Java source line #794	-> byte code offset #212
/*     */     //   Java source line #795	-> byte code offset #216
/*     */     //   Java source line #796	-> byte code offset #221
/*     */     //   Java source line #798	-> byte code offset #226
/*     */     //   Java source line #790	-> byte code offset #234
/*     */     //   Java source line #792	-> byte code offset #236
/*     */     //   Java source line #793	-> byte code offset #239
/*     */     //   Java source line #794	-> byte code offset #243
/*     */     //   Java source line #795	-> byte code offset #247
/*     */     //   Java source line #796	-> byte code offset #252
/*     */     //   Java source line #798	-> byte code offset #257
/*     */     //   Java source line #799	-> byte code offset #262
/*     */     //   Java source line #792	-> byte code offset #265
/*     */     //   Java source line #793	-> byte code offset #268
/*     */     //   Java source line #794	-> byte code offset #272
/*     */     //   Java source line #795	-> byte code offset #276
/*     */     //   Java source line #796	-> byte code offset #281
/*     */     //   Java source line #798	-> byte code offset #286
/*     */     //   Java source line #800	-> byte code offset #291
/*     */     // Local variable table:
/*     */     //   start	length	slot	name	signature
/*     */     //   0	292	0	resourceURL	java.net.URL
/*     */     //   0	292	1	fullPath	String
/*     */     //   1	272	2	fos	FileOutputStream
/*     */     //   3	266	3	bis	BufferedInputStream
/*     */     //   5	282	4	conn	java.net.HttpURLConnection
/*     */     //   11	256	5	buf	byte[]
/*     */     //   14	104	6	size	int
/*     */     //   17	67	7	fileTostore	File
/*     */     //   38	7	8	inputStream	InputStream
/*     */     //   126	3	8	e	java.net.MalformedURLException
/*     */     //   162	3	8	e	FileNotFoundException
/*     */     //   198	3	8	e	IOException
/*     */     //   234	29	9	localObject	Object
/*     */     //   147	3	10	e	IOException
/*     */     //   183	3	10	e	IOException
/*     */     //   219	3	10	e	IOException
/*     */     //   250	3	10	e	IOException
/*     */     //   279	3	10	e	IOException
/*     */     // Exception table:
/*     */     //   from	to	target	type
/*     */     //   19	123	126	java/net/MalformedURLException
/*     */     //   133	144	147	java/io/IOException
/*     */     //   19	123	162	java/io/FileNotFoundException
/*     */     //   169	180	183	java/io/IOException
/*     */     //   19	123	198	java/io/IOException
/*     */     //   205	216	219	java/io/IOException
/*     */     //   19	133	234	finally
/*     */     //   162	169	234	finally
/*     */     //   198	205	234	finally
/*     */     //   236	247	250	java/io/IOException
/*     */     //   265	276	279	java/io/IOException
/*     */   }
/*     */   
/*     */   public static void deleteFile(File file, boolean delete)
/*     */   {
/* 803 */     if (file.exists()) {
/* 804 */       if ((file.isFile()) && (delete)) {
/* 805 */         file.delete();
/* 806 */       } else if (file.isDirectory()) {
/* 807 */         File[] files = file.listFiles();
/* 808 */         File[] arrayOfFile1; int j = (arrayOfFile1 = files).length; for (int i = 0; i < j; i++) { File file2 = arrayOfFile1[i];
/* 809 */           deleteFile(file2, true);
/*     */         }
/*     */       }
/* 812 */       if (delete) {
/* 813 */         file.delete();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static void deleteFolder(File file, boolean withCurrentFolder)
/*     */   {
/* 823 */     if ((file.exists()) && (file.isDirectory())) {
/* 824 */       File[] files = file.listFiles();
/* 825 */       File[] arrayOfFile1; int j = (arrayOfFile1 = files).length; for (int i = 0; i < j; i++) { File file2 = arrayOfFile1[i];
/* 826 */         deleteFile(file2, true);
/*     */       }
/* 828 */       if (withCurrentFolder) {
/* 829 */         file.delete();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static boolean allInSameFolder(File baseFile, String... fileNames)
/*     */   {
/* 842 */     if ((baseFile != null) && (baseFile.exists())) {
/* 843 */       if ((fileNames == null) || (fileNames.length == 0)) {
/* 844 */         return true;
/*     */       }
/* 846 */       File baseFoler = baseFile;
/* 847 */       if (baseFile.isFile()) {
/* 848 */         baseFoler = baseFile.getParentFile();
/*     */       }
/*     */       String[] arrayOfString;
/* 851 */       int j = (arrayOfString = fileNames).length; for (int i = 0; i < j; i++) { String fileName = arrayOfString[i];
/* 852 */         if (fileName != null) {
/* 853 */           File subFile = new File(baseFoler, fileName);
/* 854 */           if (!subFile.exists()) {
/* 855 */             return false;
/*     */           }
/*     */         }
/*     */       }
/* 859 */       return true;
/*     */     }
/* 861 */     return false;
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\io\FilesUtils.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */