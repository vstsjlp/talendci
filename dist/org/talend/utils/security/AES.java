/*     */ package org.talend.utils.security;
/*     */ 
/*     */ import java.io.PrintStream;
/*     */ import java.io.UnsupportedEncodingException;
/*     */ import java.security.Key;
/*     */ import java.security.Provider;
/*     */ import java.security.SecureRandom;
/*     */ import java.security.Security;
/*     */ import javax.crypto.BadPaddingException;
/*     */ import javax.crypto.Cipher;
/*     */ import javax.crypto.IllegalBlockSizeException;
/*     */ import javax.crypto.KeyGenerator;
/*     */ import org.apache.commons.codec.binary.Base64;
/*     */ import org.apache.log4j.Logger;
/*     */ import org.bouncycastle.jce.provider.BouncyCastleProvider;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class AES
/*     */ {
/*     */   static
/*     */   {
/*  36 */     if (Security.getProvider("BC") == null) {
/*  37 */       Security.addProvider(new BouncyCastleProvider());
/*     */     }
/*     */   }
/*     */   
/*  41 */   private static Logger log = Logger.getLogger(AES.class);
/*     */   
/*     */ 
/*     */   private static final String RANDOM_SHA1PRNG = "SHA1PRNG";
/*     */   
/*     */   private static final String ENCRYPTION_ALGORITHM = "AES";
/*     */   
/*     */   private static final String EMPTY_STRING = "";
/*     */   
/*     */   private static final String UTF8 = "UTF8";
/*     */   
/*  52 */   private static final byte[] KeyValues = { -87, -101, -56, 50, 86, 53, 
/*  53 */     -29, 3 };
/*     */   
/*     */   private Cipher ecipher;
/*     */   private Cipher dcipher;
/*     */   
/*     */   public static AES getInstance()
/*     */   {
/*  60 */     return new AES();
/*     */   }
/*     */   
/*     */ 
/*     */   public AES()
/*     */   {
/*     */     try
/*     */     {
/*  68 */       Provider p = Security.getProvider("BC");
/*  69 */       KeyGenerator keyGen = KeyGenerator.getInstance("AES", p);
/*     */       
/*  71 */       SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
/*  72 */       random.setSeed(KeyValues);
/*  73 */       keyGen.init(128, random);
/*     */       
/*  75 */       Key key = keyGen.generateKey();
/*     */       
/*  77 */       this.ecipher = Cipher.getInstance("AES", p);
/*  78 */       this.dcipher = Cipher.getInstance("AES", p);
/*     */       
/*  80 */       this.ecipher.init(1, key);
/*  81 */       this.dcipher.init(2, key);
/*     */     }
/*     */     catch (Exception e) {
/*  84 */       log.error(e.getMessage(), e);
/*     */     }
/*     */   }
/*     */   
/*     */   public String encrypt(String data) throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
/*  89 */     if ("".equals(data)) {
/*  90 */       return "";
/*     */     }
/*  92 */     byte[] enc = this.ecipher.doFinal(data.getBytes("UTF8"));
/*  93 */     String encryptedData = new String(Base64.encodeBase64(enc), "UTF8");
/*  94 */     return encryptedData;
/*     */   }
/*     */   
/*     */   public String decrypt(String encryptedData) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException
/*     */   {
/*  99 */     if ("".equals(encryptedData)) {
/* 100 */       return "";
/*     */     }
/* 102 */     byte[] dec = Base64.decodeBase64(encryptedData.getBytes("UTF8"));
/* 103 */     dec = this.dcipher.doFinal(dec);
/* 104 */     String decryptedData = new String(dec, "UTF8");
/* 105 */     return decryptedData;
/*     */   }
/*     */   
/*     */   public static void main(String[] args) {
/* 109 */     AES aes = new AES();
/* 110 */     String[] arr = { "bt4AUzTV14kK8FwkcK/BNg==", "3IqdoqEElsy8Dzz9iP3HVQ==", "w4AXOA1a34afqqnlmVLB4A==", 
/* 111 */       "m9Ut0k3oP5pLE2BH1r9xQA==", "zPfoS7aDB2mNUrpRfbfwcOza/VXudqA9QYULYn4xTb8=", 
/* 112 */       "3mTjF2v1D4ZYqnJleFKl/wFybG4/24iyhCFKyEuveDY=" };
/*     */     try { String[] arrayOfString1;
/* 114 */       int j = (arrayOfString1 = arr).length; for (int i = 0; i < j; i++) { String t = arrayOfString1[i];
/* 115 */         System.out.println(aes.decrypt(t));
/*     */       }
/*     */     } catch (Exception e) {
/* 118 */       e.printStackTrace();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\vstsjlp\AppData\Local\Temp\Temp1_Talend-CI-Builder-20170623_1246-V6.4.1.zip\Talend-CI-Builder-20170623_1246-V6.4.1\ci.builder-6.4.1.jar!\dist\org.talend.utils.jar!\org\talend\utils\security\AES.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */